import 'dart:ui';
import 'package:biznes/model/roleModel.dart';
import 'package:biznes/register/registerNext.dart';
import 'package:biznes/repeatedWigets/checkBox.dart';
import 'package:biznes/repeatedWigets/filledButton.dart';
import 'package:biznes/services/register_logic.dart';
import 'package:biznes/model/regiterModel.dart';
import 'package:biznes/products_Detail/extra.dart';
import 'package:biznes/size_config.dart';
import 'package:biznes/res/size.dart';
import 'package:biznes/res/style.dart';
import 'package:country_code_picker/country_code_picker.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:toast/toast.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';

List<RoleModel> rolesModelList = [];

class EnterYourRole extends StatefulWidget {
  @override
  _EnterYourRoleState createState() => _EnterYourRoleState();
}

class _EnterYourRoleState extends State<EnterYourRole> {
  var formKey = GlobalKey<FormState>();
  String roleValue;
  int responseLength;
  var response;
  bool loading = true;
  RegisterModel registerforrole;

  @override
  void initState() {
    registerforrole = RegisterModel(
      role: [],
    );
    getRoles(registerpage: "Register").then((rolesList) {
      setState(() {
        rolesModelList = rolesList;
        loading = false;
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
//    final double width=MediaQuery.of(context).size.width;
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).requestFocus(new FocusNode());
      },
      child: Scaffold(
        body: loading
            ? Center(
                child: Container(
                    alignment: Alignment.center,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Image.asset(
                          "assets/images/logo2.gif",
                          height: 200,
                          width: 100,
                        ),
                      ],
                    )),
              )
            : _registerBody(),
      ),
    );
  }

  _registerBody() {
    return Container(
      child: SingleChildScrollView(
        child: Column(
          children: [
            SizedBox(height: size.convert(context, 40)),
            Container(
              margin:
                  EdgeInsets.symmetric(horizontal: size.convert(context, 16)),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  InkWell(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Icon(
                      Icons.arrow_back_sharp,
                      color: Colors.black,
                    ),
                  ),
                  Container(),
                ],
              ),
            ),
//            Container(
//              child: Image.asset(
//                "assets/images/logo.png",
//                width: size.convertWidth(context, 200),
//                height: size.convert(context, 90),
//              ),
//            ),
            Container(
              margin: EdgeInsets.symmetric(
                  horizontal: size.convertWidth(context, 21)),
              padding:
                  EdgeInsets.symmetric(vertical: size.convert(context, 15)),
              decoration: BoxDecoration(
                  boxShadow: [
                    BoxShadow(
                      color: Color(0xffd4d4d4),
                      blurRadius: 6,
                      offset: Offset(0, 3), // changes position of shadow
                    ),
                  ],
                  color: Colors.white,
                  borderRadius:
                      BorderRadius.circular(size.convert(context, 30))),
              child: Column(
                children: [
                  SvgPicture.asset(
                    "assets/icons/SelectRole.svg",
                    width: size.convertWidth(context, 260),
                    height: size.convert(context, 150),
                  ),
                  SizedBox(
                    height: size.convert(context, 5),
                  ),
                  Text(
                    "Select Your Role",
                    style: style.QuicksandBold(
                        fontSize: size.convert(context, 25),
                        color: Color(0xff394c81)),
                  ),
                  Text(
                    "You can select one or more roles",
                    style: style.MontserratRegular(
                        fontSize: size.convert(context, 15)),
                  ),
                  SizedBox(
                    height: size.convert(context, 10),
                  ),
                  Container(
                    margin: EdgeInsets.symmetric(
                        horizontal: size.convertWidth(context, 20)),
                    color: Colors.white,
                    child: StaggeredGridView.countBuilder(
                        physics: NeverScrollableScrollPhysics(),
                        padding: EdgeInsets.all(0),
                        mainAxisSpacing: 10,
                        crossAxisSpacing: 10,
                        shrinkWrap: true,
                        crossAxisCount: 2,
                        staggeredTileBuilder: (int index) =>
                            StaggeredTile.fit(1),
                        itemCount: 4,
                        itemBuilder: (context, index) {
                          print("Role Title " + rolesModelList[index].title);
                          String IconUrl = "";
                          if (index == 0)
                            IconUrl = "assets/icons/Manufacturer.svg";
                          if (index == 1) IconUrl = "assets/icons/Retailer.svg";
                          if (index == 2) IconUrl = "assets/icons/Importer.svg";
                          if (index == 3) IconUrl = "assets/icons/Exporter.svg";
                          return rolesModelList[index].title.toLowerCase() ==
                                  "broker"
                              ? Container()
                              : Column(
                                  children: [
                                    Container(
                                      padding: EdgeInsets.all(
                                          size.convert(context, 10)),
//                                    width: size.convert(context, 122),
//                                    height: size.convert(context, 84),
                                      decoration: BoxDecoration(
                                          color: Colors.white,
                                          borderRadius: BorderRadius.circular(
                                              size.convert(context, 15)),
                                          boxShadow: [
                                            BoxShadow(
                                                color: Color(0xff29000000),
                                                blurRadius: 6,
                                                offset: Offset(0, 3))
                                          ]),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Container(),
                                          Column(
                                            children: [
                                              Container(
                                                child: SvgPicture.asset(
                                                  IconUrl,
                                                  width: size.convertWidth(
                                                      context, 60),
                                                  height:
                                                      size.convert(context, 75),
                                                ),
                                              ),
                                              SizedBox(
                                                height:
                                                    size.convert(context, 5),
                                              ),
                                              Text(
                                                rolesModelList[index].title,
                                                style: style.MontserratRegular(
                                                    fontSize: size.convert(
                                                        context, 12)),
                                              ),
                                            ],
                                          ),
                                          Container(
                                            child: checkBox(
                                              val: rolesModelList[index]
                                                  .selected,
                                              ontap: () {
                                                print("printptsdfaldfs");
                                                setState(() {
                                                  rolesModelList[index]
                                                          .selected =
                                                      !rolesModelList[index]
                                                          .selected;
                                                });
                                                if (rolesModelList[index]
                                                        .selected ==
                                                    true) {
                                                  this.setState(() {
                                                    registerforrole.role.add(
                                                        rolesModelList[index]
                                                            .id);
                                                    print(registerforrole.role);
                                                  });
                                                } else {
                                                  this.setState(() {
                                                    registerforrole.role.remove(
                                                        rolesModelList[index]
                                                            .id);
                                                    print(registerforrole.role);
                                                  });
                                                }
                                              },
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                );
                        }),
                  ),
                  SizedBox(
                    height: size.convert(context, 10),
                  ),
                  filledButton(
                    w: size.convertWidth(context, 300),
                    endColor: Color(0xffffcd02),
                    startColor: Color(0xffffcd02),
                    shadowColor: Colors.white,
                    txt: "Next",
                    onTap: () {

                      if (registerforrole.role.isEmpty) {
                        Toast.show("please select one or more", context,
                            gravity: Toast.TOP);
                      } else {
                        Navigator.push(
                            context,
                            PageTransition(
                                type: PageTransitionType.bottomToTop,
                                duration: Duration(milliseconds: 500),
                                child: Register(
                                  role: rolesModelList[0],
                                  regModel: registerforrole,
                                )));
                      }
                    },
                  ),
                  SizedBox(
                    height: size.convert(context, 12),
                  ),
                  Container(
                    margin: EdgeInsets.symmetric(
                        horizontal: size.convertWidth(context, 20)),
                    child: Row(
                      children: [
                        Text(
                          "Register As a Broker",
                          style: style.QuicksandBold(
                              fontSize: size.convert(context, 15),
                              color: Color(0xff394c81)),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: size.convert(context, 10),
                  ),
                  filledButton(
                    w: size.convertWidth(context, 300),
                    shadowColor: Colors.white,
                    txt: "Broker",
                    onTap: () {
                      if (registerforrole.role.contains(rolesModelList[0].id) ||
                          registerforrole.role.contains(rolesModelList[1].id) ||
                          registerforrole.role.contains(rolesModelList[2].id) ||
                          registerforrole.role.contains(rolesModelList[3].id)) {
                        Toast.show(
                            "You have already selected other roles", context,
                            gravity: Toast.TOP);
                      } else {
                        setState(() {
                          registerforrole.roleidforbroker =
                              rolesModelList[4].id;
                        });
                        Navigator.push(
                            context,
                            PageTransition(
                                type: PageTransitionType.bottomToTop,
                                duration: Duration(milliseconds: 500),
                                child: Register(
                                  role: rolesModelList[4],
                                  regModel: registerforrole,
                                )));
                      }
                    },
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class Register extends StatefulWidget {
  final RoleModel role;
  final RegisterModel regModel;

  Register({this.role, this.regModel});

  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register>
    with SingleTickerProviderStateMixin {
  final _focusNode = FocusNode();
  final _focusNode2 = FocusNode();
  TextEditingController _textEditingcontrollerforusername =
      TextEditingController();
  TextEditingController _textEditingcontrollerforemail =
      TextEditingController();
  var formKey = GlobalKey<FormState>();
  var countryNumber = 92;
  var daa = getCountries();
  bool validator = false;

  @override
  void initState() {
    super.initState();
    _focusNode.addListener(() {
      _focusNode.hasFocus
          ? print("hello")
          :_textEditingcontrollerforusername.text.length==0?null: checkingusername(_textEditingcontrollerforusername.text)
              .then((value) {
              if (value["Success"] == false) {
                Toast.show(value["message"], context, gravity: Toast.TOP);
              }
            });
    });
    _focusNode2.addListener(() {
      _focusNode2.hasFocus
          ? print("hello")
          : checkingemail(_textEditingcontrollerforemail.text).then((value) {
              if (value["Success"] == false) {
                Toast.show(value["message"], context, gravity: Toast.TOP);
              }
            });
    });
  }

  //form made here
  Widget buildForm(orientation) {
    String validateEmail(String value) {
      Pattern pattern =
          r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
      RegExp regex = new RegExp(pattern);

      if (value.isEmpty) {
        return 'Please enter email address';
      } else {
        if (!regex.hasMatch(value))
          return 'Enter Valid Email';
        else
          return null;
      }
    }

    return Padding(
      padding: EdgeInsets.symmetric(
        horizontal: orientation == Orientation.portrait ? 12.2 : 50,
      ),
      child: Form(
          key: formKey,
          autovalidateMode: AutovalidateMode.onUserInteraction,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 1.0, vertical: 1),
                child: Text(
                  "Full Name",
                  style: style.MontserratMedium(
                    fontSize: size.convert(context, 14),
                  ),
                ),
              ),
              SizedBox(
                height: orientation == Orientation.portrait
                    ? (4 / 8.148314082864863) * SizeConfig.heightMultiplier
                    : (2 / 8.148314082864863) * SizeConfig.heightMultiplier,
              ),
              Padding(
                padding: const EdgeInsets.all(2.0),
                child: Stack(
                  children: [
                    material(),
                    TextFormField(
                      keyboardType: TextInputType.text,
                      decoration: InputDecoration(
                        focusedErrorBorder: outlineInputBorder(),
                        errorBorder: outlineInputBorder(),
                        errorStyle:style.MontserratRegular(
                            color: Colors.red, fontSize: size.convert(context, 12)),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(25),
                          borderSide: BorderSide(color: Color(0xff69BEF7)),
                        ),
                        contentPadding:
                            EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(25.0),
                        ),
                        hintStyle: style.MontserratRegular(
                            fontSize: size.convert(context, 12)),
                        hintText: 'Enter your full name',
                        prefixIcon: Icon(
                          Icons.person_outline,
                          size: 18,
                        ),
                      ),
                      onSaved: (value) {
                        setState(() {
                          widget.regModel.fullName = value;
                        });
                      },
                      validator: (value) {
                        validator = true;
                        if (value.isEmpty && value.length < 6) {
                          return 'Enter full name';
                        }
                        return null;
                      },
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: orientation == Orientation.portrait
                    ? (4 / 8.148314082864863) * SizeConfig.heightMultiplier
                    : (2 / 8.148314082864863) * SizeConfig.heightMultiplier,
              ),
              Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 2.0, vertical: 1.0),
                child: Text(
                  "First Name",
                  style: style.MontserratMedium(
                    fontSize: size.convert(context, 14),
                  ),
                ),
              ),
              SizedBox(
                height: orientation == Orientation.portrait
                    ? (4 / 8.148314082864863) * SizeConfig.heightMultiplier
                    : (2 / 8.148314082864863) * SizeConfig.heightMultiplier,
              ),
              Padding(
                padding: const EdgeInsets.all(2.0),
                child: Stack(
                  children: [
                    material(),
                    TextFormField(
                      controller: _textEditingcontrollerforusername,
                      focusNode: _focusNode,
                      keyboardType: TextInputType.text,
                      textInputAction: TextInputAction.done,
                      decoration: InputDecoration(
                        focusedErrorBorder: outlineInputBorder(),
                        errorBorder: outlineInputBorder(),
                        errorStyle:style.MontserratRegular(
                            color: Colors.red, fontSize: size.convert(context, 12)),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(25),
                          borderSide: BorderSide(color: Color(0xff69BEF7)),
                        ),
                        contentPadding:
                            EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(25.0),
                        ),
                        hintText: 'Enter your first name',
                        hintStyle: style.MontserratRegular(
                            fontSize: size.convert(context, 12)),
                        prefixIcon: Icon(
                          Icons.person,
                          size: 18,
                        ),
                      ),
                      onSaved: (value) {
                        setState(() {
                          widget.regModel.username = value;
                        });
                      },
                      validator: (value) {
                        if (value.isEmpty && value.length < 6) {
                          return 'Enter First Name';
                        }else if (value.length < 3) {
                          return 'Name must be greater than 3 characters';
                        }else if(value.length>12){
                          return 'Name must be 12 characters';
                        }
                        return null;
                      },
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: orientation == Orientation.portrait
                    ? (4 / 8.148314082864863) * SizeConfig.heightMultiplier
                    : (2 / 8.148314082864863) * SizeConfig.heightMultiplier,
              ),
              Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 1.0, vertical: 1.0),
                child: Text(
                  "Email",
                  style: style.MontserratMedium(
                    fontSize: size.convert(context, 14),
                  ),
                ),
              ),
              SizedBox(
                height: orientation == Orientation.portrait
                    ? (4 / 8.148314082864863) * SizeConfig.heightMultiplier
                    : (2 / 8.148314082864863) * SizeConfig.heightMultiplier,
              ),
              Padding(
                padding: const EdgeInsets.all(2.0),
                child: Stack(
                  children: [
                    material(),
                    TextFormField(
                      focusNode: _focusNode2,
                      // textInputAction: TextInputAction.next,
                      controller: _textEditingcontrollerforemail,
                      keyboardType: TextInputType.emailAddress,
                      autovalidate: validator,
                      decoration: InputDecoration(
                        focusedErrorBorder: outlineInputBorder(),
                        errorBorder: outlineInputBorder(),
                        errorStyle:style.MontserratRegular(
                            color: Colors.red, fontSize: size.convert(context, 12)),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(25),
                          borderSide: BorderSide(color: Color(0xff69BEF7)),
                        ),
                        contentPadding: EdgeInsets.symmetric(
                            vertical: 10.0, horizontal: 10.0),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(25.0),
                        ),
                        hintText: 'Email Address',
                        hintStyle: style.MontserratRegular(
                            fontSize: size.convert(context, 12)),
                        prefixIcon: Icon(
                          Icons.email,
                          size: 18,
                        ),
                      ),
                      onSaved: (value) {
                        setState(() {
                          widget.regModel.email = value;
                        });
                      },
                      validator: (value) {
                        return validateEmail(value);
                      },
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: orientation == Orientation.portrait
                    ? (4 / 8.148314082864863) * SizeConfig.heightMultiplier
                    : (2 / 8.148314082864863) * SizeConfig.heightMultiplier,
              ),
              Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 1.0, vertical: 1.0),
                child: Text(
                  "Contact No",
                  style: style.MontserratMedium(
                    fontSize: size.convert(context, 14),
                  ),
                ),
              ),
              SizedBox(
                height: orientation == Orientation.portrait
                    ? (4 / 8.148314082864863) * SizeConfig.heightMultiplier
                    : (2 / 8.148314082864863) * SizeConfig.heightMultiplier,
              ),
              Padding(
                padding: const EdgeInsets.all(1.0),
                child: Stack(
                  children: [
                    material(),
                    TextFormField(
                      keyboardType: TextInputType.phone,
                      decoration: InputDecoration(
                        focusedErrorBorder: outlineInputBorder(),
                        errorBorder: outlineInputBorder(),
                        errorStyle:style.MontserratRegular(
                            color: Colors.red, fontSize: size.convert(context, 12)),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(25),
                          borderSide: BorderSide(color: Color(0xff69BEF7)),
                        ),
                        contentPadding: EdgeInsets.symmetric(
                            vertical: 10.0, horizontal: 10.0),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(25.0),
                        ),
                        hintText: 'Contact Number',
                        hintStyle: style.MontserratRegular(
                            fontSize: size.convert(context, 12)),
                        prefixIcon: CountryCodePicker(
                          onChanged: _onCountryChange,
                          initialSelection: 'PK',
                          favorite: ['+92', 'PK'],
                          showFlag: true,
                          flagWidth: 20,
                          showOnlyCountryWhenClosed: false,
                          alignLeft: false,
                          padding: EdgeInsets.all(10),
                        ),
                      ),
                      onSaved: (value) {
                        setState(() {
                          widget.regModel.contactNumber = int.parse(value);
                        });
                      },
                      // maxLength: 12,
                      validator: (value) {
                        if (value.isEmpty && value.length < 6) {
                          return 'please enter your phone number';
                        }
                        return null;
                      },
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: orientation == Orientation.portrait
                    ? (10 / 8.148314082864863) * SizeConfig.heightMultiplier
                    : (2 / 8.148314082864863) * SizeConfig.heightMultiplier,
              ),
            ],
          )),
    );
  }

  bool isvisible = false;

  void _onCountryChange(CountryCode countryCode) {
    setState(() {
      countryNumber = int.parse(countryCode.toString());
      widget.regModel.countryCode = countryCode.toString();
      print(widget.regModel.countryCode);
    });
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusScope.of(context).requestFocus(new FocusNode()),
      child: Stack(
        children: [
          Scaffold(
            body: OrientationBuilder(builder: (context, orientation) {
              return Container(
                decoration: BoxDecoration(),
                child: SingleChildScrollView(
                  child: Column(
                    children: <Widget>[
                      Container(
                        //color: Colors.red,
                        margin: EdgeInsets.symmetric(
                            horizontal: size.convert(context, 16)),
                        child: Column(
                          children: [
                            SizedBox(height: size.convert(context, 40)),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                InkWell(
                                  onTap: () {
                                    Navigator.pop(context);
                                  },
                                  child: Icon(
                                    Icons.arrow_back_sharp,
                                    color: Colors.black,
                                  ),
                                ),
                                Container(),
                              ],
                            ),
                            // SizedBox(height: size.convert(context, 20),),
                            Container(
                              child: Image.asset(
                                "assets/images/logo.png",
                                width: size.convertWidth(context, 200),
                                height: size.convert(context, 90),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.symmetric(vertical: 20),
                        margin: EdgeInsets.symmetric(
                            horizontal: size.convertWidth(context, 21)),
                        // alignment: Alignment.bottomLeft,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          boxShadow: [
                            BoxShadow(
                              color: Color(0xffd4d4d4),
//                              spreadRadius: 6,
                              blurRadius: 6,
                              offset: Offset(0, 3),
                              // changes position of shadow
                            ),
                          ],
                          borderRadius:
                              BorderRadius.circular(size.convert(context, 20)),
//                          borderRadius: BorderRadius.only(
//                            topLeft: Radius.circular(44.5),
//                            topRight: Radius.circular(44.5),
//                          )
                        ),
                        child: Column(
                          children: [
                            Container(
                              child: SvgPicture.asset(
                                "assets/icons/loginIcon.svg",
                                width: size.convertWidth(context, 250),
                                height: size.convert(context, 150),
                              ),
                            ),

                            Center(
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text(
                                  "Register",
                                  style: style.QuicksandBold(
                                      fontSize: size.convert(context, 30),
                                      color: Color(0xff394c81)),
                                ),
                              ),
                            ),
                            buildForm(orientation),
                            SizedBox(
                              height: size.convert(context, 10),
                            ),
                            filledButton(
                              w: size.convert(context, 240),
                              onTap: () {
                                validateAndSubmit(context);
                              },
                              txt: "Next",
                            ),
//                            Center(
//                              child: MaterialButton(
//                                onPressed: () {
//                                  validateAndSubmit(context);
//                                },
//                                child: Container(
//                                  width: (370 / 4.853932272197492) *
//                                      SizeConfig.widthMultiplier,
//                                  height: 50,
//                                  alignment: Alignment.center,
//                                  decoration: BoxDecoration(
//                                    borderRadius: BorderRadius.circular(25.0),
//                                    gradient: LinearGradient(
//                                      begin: Alignment.topCenter,
//                                      end: Alignment.bottomCenter,
//                                      colors: [
//                                        Color(0xFF69bef7),
//                                        Color(0xFF2a65ff),
//                                      ],
//                                    ),
//                                    boxShadow: [
//                                      BoxShadow(
//                                        color: Color(0xFF69bef7),
//                                        spreadRadius: 1,
//                                        blurRadius: 12,
//                                        offset: Offset(
//                                            0, 2), // changes position of shadow
//                                      ),
//                                    ],
//                                  ),
//                                  child: Text(
//                                    "NEXT",
//                                    style: TextStyle(
//                                        fontSize: (20 / 8.148314082864863) *
//                                            SizeConfig.heightMultiplier,
//                                        color: Colors.white),
//                                  ),
//                                ),
//                                shape: StadiumBorder(),
//                              ),
//                            ),
                            SizedBox(
                              height: 4,
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              );
            }),
          ),
          isvisible
              ? Stack(
                  children: <Widget>[
                    Container(
                      color: Color(0xffffffff).withOpacity(0.6),
                      height: double.infinity,
                      width: double.infinity,
                      child: Opacity(
                        opacity: 1.0,
                        child: Center(
                            child: Image.asset(
                          "assets/images/logo2.gif",
                          height: 200,
                          width: 100,
                        )),
                      ),
                    ),
                  ],
                )
              : Container(),
        ],
      ),
    );
  }


  bool validateAndSave() {
    final form = formKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    } else {
      return false;
    }
  }

  validateAndSubmit(context) {
    print(countryNumber);
    if (validateAndSave()) {
      setState(() {
        isvisible = true;
      });
      widget.regModel.roleName = widget.role.title;
      checkingusername(_textEditingcontrollerforusername.text).then((value) {
        if (value["Success"] == false) {
          Toast.show(value["message"], context, gravity: Toast.TOP);
          setState(() {
            isvisible = false;
          });
        } else {
          checkingemail(_textEditingcontrollerforemail.text).then((value) {
            if (value["Success"] == false) {
              Toast.show(value["message"], context, gravity: Toast.TOP);
              setState(() {
                isvisible = false;
              });
            } else {
              checkingphonenumber(countryNumber.toString() +
                      widget.regModel.contactNumber.toString())
                  .then((value) {
                if (value["Success"] == false) {
                  Toast.show(value["message"], context, gravity: Toast.TOP);
                  setState(() {
                    isvisible = false;
                  });
                } else {
                  setState(() {
                    isvisible = false;
                  });
                  Navigator.push(
                      context,
                      PageTransition(
                          type: PageTransitionType.bottomToTop,
                          duration: Duration(milliseconds: 500),
                          child: RegisterNext(
                            regModel: widget.regModel,
                            roleModel: widget.role,
                            countryCode: countryNumber,
                          )));
                }
              });
            }
          });
        }
      });
    }
  }
}
