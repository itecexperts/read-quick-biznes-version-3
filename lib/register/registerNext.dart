import 'dart:io';
import 'dart:ui';
import '../login_UI/pincode.dart';
import 'package:biznes/services/register_logic.dart';
import 'package:biznes/products_Detail/extra.dart';
import 'package:biznes/size_config.dart';
import 'package:biznes/model/citiesModel.dart';
import 'package:biznes/product_UI/bottomBar_UI/DropDownWidget.dart';
import 'package:biznes/model/countriesModel.dart';
import 'package:biznes/model/regiterModel.dart';
import 'package:biznes/model/roleModel.dart';
import 'package:biznes/res/color.dart' as fontandcolor;
import 'package:biznes/res/size.dart';
import 'package:biznes/res/style.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:biznes/repeatedWigets/filledButton.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:page_transition/page_transition.dart';

class RegisterNext extends StatefulWidget {
  final int countryCode;
  final RegisterModel regModel;
  final RoleModel roleModel;

  RegisterNext({this.regModel, this.roleModel, this.countryCode});

  @override
  _RegisterNextState createState() => _RegisterNextState();
}

class _RegisterNextState extends State<RegisterNext> {
  var formKey = GlobalKey<FormState>();
  String length="0";
  bool isdescription = false;
  bool isdescriptionlength=false;
  String selectedUnit;
  TextEditingController _description = TextEditingController();
  RoleModel roleModel;
  List<CountriesModel> countries = [];
  List<CitiesModel> cities = [];
  bool loading = true;
  String selectCountry;
  String selectCity;
  File nicUpload, billUpload;
  bool isPasswordShow = false;

  Widget countryTile(CountriesModel country) {
    return Text(country.title);
  }

  Widget buildForm() {
    return Padding(
      padding: EdgeInsets.all(16.2),
      child: Form(
          key: formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 1.0, vertical: 1.0),
                child: Text(
                  "Organization",
                  style: style.MontserratMedium(
                    fontSize: size.convert(context, 14),
                  ),
                ),
              ),
              SizedBox(
                height: size.convert(context, 4),
              ),
              Padding(
                padding: const EdgeInsets.all(2.0),
                child: Stack(
                  children: [
                    material(),
                    TextFormField(
                      onFieldSubmitted: (v) {},
                      textInputAction: TextInputAction.next,
                      decoration: InputDecoration(
                        focusedErrorBorder: outlineInputBorder(),
                        errorBorder: outlineInputBorder(),
                        errorStyle:style.MontserratRegular(
                            color: Colors.red, fontSize: size.convert(context, 12)),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(25),
                          borderSide: BorderSide(color: Color(0xff69BEF7)),
                        ),
                        contentPadding: EdgeInsets.symmetric(
                            vertical: 10.0, horizontal: 10.0),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(25.0),
                        ),
                        hintText: 'Organization',
                        hintStyle: style.MontserratRegular(
                            fontSize: size.convert(context, 12)),
                        prefixIcon: Icon(
                          Icons.account_balance,
                          size: 18,
                        ),
                      ),
                      onSaved: (value) {
                        setState(() {
                          widget.regModel.organizationName = value;
                        });
                      },
                      validator: (value) {
                        if (value.isEmpty && value.length < 6) {
                          return 'Enter your company/Organization name';
                        }
                        return null;
                      },
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: size.convert(context, 4),
              ),
              Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 1.0, vertical: 1.0),
                child: Text(
                  "Description",
                  style: style.MontserratMedium(
                    fontSize: size.convert(context, 14),
                  ),
                ),
              ),
              SizedBox(
                height: size.convert(context, 4),
              ),
              Material(
                shadowColor: Color(0xffd4d4d4),
                elevation: 5,
                borderRadius: BorderRadius.circular(30),
                child: TextFormField(
                  controller: _description,
                  onChanged: (value){
                    setState(() {
                      length=value.length.toString();
                    });
                  },
                  decoration: InputDecoration(
                    errorStyle: TextStyle(color: Colors.red),
                    errorBorder: outlineInputBorder(),
                    focusedErrorBorder: outlineInputBorder(),
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(30),
                      borderSide: BorderSide(
                          color: isdescription
                              ? Colors.red
                              : Color(0xff69BEF7)),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(30.0),
                      borderSide: BorderSide(
                          color: isdescription ? Colors.red : Colors.grey,
                          width: 1.0),
                    ),
                    contentPadding: EdgeInsets.symmetric(
                        vertical: 10.0, horizontal: 10.0),
                    hintStyle: style.MontserratRegular(
                        fontSize: size.convert(context, 12)),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(30.0),
                    ),
                    hintText: 'Enter your company Description',
                  ),
                  maxLines: 5,
                  onSaved: (value) {
                    setState(() {
                      widget.regModel.description = value;
                    });
                  },
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  isdescription
                      ? Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      "Enter your company description",
                      style:style.MontserratRegular(
                          color: Colors.red, fontSize: size.convert(context, 12)),
                    ),
                  ):isdescriptionlength? Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      "Description length must be 250 words",
                      style:style.MontserratRegular(
                          color: Colors.red, fontSize: size.convert(context, 10)),
                    ),
                  ): Container(),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text("$length/250",style: TextStyle(
                      color: Colors.grey,
                      fontSize:size.convert(context, 12),
                    ),),
                  ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 5,),
                child: Text(
                  "Country",
                  style: style.MontserratMedium(
                    fontSize: size.convert(context, 14),
                  ),
                ),
              ),
              SizedBox(
                height: (5 / 8.148314082864863) * SizeConfig.heightMultiplier,
              ),

              DropDownWidget(
                  context: context,
                  hintext: "Country",
                  value: selectCountry,
                  selectedCategory: selectCountry,
                  categories: countries,
                  image: selectCountry != null
                      ? 'assets/images/flag2.png'
                      : 'assets/images/countryicon.svg',
                  onChanged: (value) {
                    setState(() {
                      if (selectCity != null) {
                        selectCity = null;
                      }
                      cities = [];
                    });
                    countries.forEach((f) async {
                      if (f.title == value) {
                        getCities(f.sId).then((list) {
                          // regModel;
                          setState(() {
                            cities = list;
                            selectCountry = value;
                            widget.regModel.countryId = f.sId;
                          });
                        });
                      }
                    });
                  }),
              SizedBox(
                height: (5 / 8.148314082864863) * SizeConfig.heightMultiplier,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 5, vertical: 5),
                child: Text(
                  "City",
                  style: style.MontserratMedium(
                    fontSize: size.convert(context, 14),
                  ),
                ),
              ),
              SizedBox(
                height: (5 / 8.148314082864863) * SizeConfig.heightMultiplier,
              ),

              DropDownWidget(
                context: context,
                image: 'assets/images/city.svg',
                selectedCategory: selectCity,
                categories: cities,
                hintext: "City",
                onChanged: (value) {
                  cities.forEach((f) {
                    if (f.title == value) {
                      widget.regModel.cityId = f.sId;
                      // widget.regModel.
                    }
                  });
                  setState(() {
                    selectCity = value;
                  });
                },
                value: selectCity,
              ),

              SizedBox(
                height: (8 / 8.148314082864863) * SizeConfig.heightMultiplier,
              ),
              Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 5, vertical: 1),
                child: Text(
                  "Password",
                  style: style.MontserratMedium(
                    fontSize: size.convert(context, 14),
                  ),
                ),
              ),
              SizedBox(
                  height:
                      (1 / 8.148314082864863) * SizeConfig.heightMultiplier),
              Padding(
                padding: const EdgeInsets.all(6.0),
                child: Stack(
                  children: [
                    material(),
                    TextFormField(
                      decoration: InputDecoration(

                        focusedErrorBorder: outlineInputBorder(),
                        errorBorder: outlineInputBorder(),
                        hintStyle: style.MontserratRegular(
                            fontSize: size.convert(context, 12)),

                        errorStyle:style.MontserratRegular(
                            color: Colors.red, fontSize: size.convert(context, 12)),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(25),
                          borderSide: BorderSide(color: Color(0xff69BEF7)),
                        ),
                        contentPadding: EdgeInsets.symmetric(
                            vertical: 10.0, horizontal: 10.0),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(29.0),
                        ),
                        hintText: 'Password',
                        prefixIcon: Icon(Icons.lock,size: 18,),
                        suffixIcon: InkWell(
                            onTap: () {
                              setState(() {
                                isPasswordShow = !isPasswordShow;
                              });
                            },
                            child: isPasswordShow
                                ? Icon(
                                    Icons.visibility_off,
                                    size: 15,
                                  )
                                : Icon(
                                    Icons.visibility,
                                    size: 15,
                                  )),
                      ),
                      obscureText: !isPasswordShow,
                      onSaved: (value) {
                        widget.regModel.password = value;
                      },
                      validator: (value) {
                        if (value.isEmpty && value.length < 6) {
                          return 'Choose your password';
                        }
                        return null;
                      },
                    ),
                  ],
                ),
              ),
            ],
          )),
    );
  }

  Widget buildFormBroker(orientation) {
    List<Widget> children = [];
    if (!loading) {
      countries.forEach((f) {
        children.add(countryTile(f));
      });
    }
    return Padding(
      padding: EdgeInsets.all(
          (16.2 / 8.148314082864863) * SizeConfig.heightMultiplier),
      child: Form(
          key: formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              widget.roleModel.title == "Broker"?Container(): Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 1.0, vertical: 1.0),
                child: Text(
                  "Organization",
                  style: style.MontserratMedium(
                    fontSize: size.convert(context, 14),
                  ),
                ),
              ),
              SizedBox(
                height: size.convert(context, 4),
              ),
              widget.roleModel.title == "Broker"?Container():
              Padding(
                padding: const EdgeInsets.all(2.0),
                child: Stack(
                  children: [
                    material(),
                    TextFormField(
                      onFieldSubmitted: (v) {},
                      textInputAction: TextInputAction.next,
                      decoration: InputDecoration(
                        focusedErrorBorder: outlineInputBorder(),
                        errorBorder: outlineInputBorder(),
                        hintStyle: style.MontserratRegular(
                            fontSize: size.convert(context, 12)),
                        errorStyle:style.MontserratRegular(
                            color: Colors.red, fontSize: size.convert(context, 12)),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(25),
                          borderSide: BorderSide(color: Color(0xff69BEF7)),
                        ),
                        contentPadding: EdgeInsets.symmetric(
                            vertical: 10.0, horizontal: 10.0),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(25.0),
                        ),
                        hintText: 'Organization',
                        prefixIcon: Icon(
                          Icons.account_balance,
                          size: 18,
                        ),
                      ),
                      onSaved: (value) {
                        setState(() {
                          widget.regModel.organizationName = value;
                        });
                      },
                      validator: (value) {
                        if (value.isEmpty && value.length < 6) {
                          return 'Enter your company/Organization name';
                        }
                        return null;
                      },
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: size.convert(context, 4),
              ),
              Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 5.0, vertical: 1.0),
                child: Text(
                  "About me",
                  style: style.MontserratMedium(
                    fontSize: size.convert(context, 14),
                  ),
                ),
              ),
              SizedBox(
                height: size.convert(context, 4),
              ),
              Material(
                shadowColor: Color(0xffd4d4d4),
                elevation: 5,
                borderRadius: BorderRadius.circular(30),
                child: TextFormField(
                  onChanged: (value){
                    setState(() {
                      length=value.length.toString();
                    });
                  },
                  controller: _description,
                  decoration: InputDecoration(
                    errorStyle: TextStyle(color: Colors.red),
                    errorBorder: outlineInputBorder(),
                    focusedErrorBorder: outlineInputBorder(),
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(30),
                      borderSide: BorderSide(
                          color: isdescription
                              ? Colors.red
                              : Color(0xff69BEF7)),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(30.0),
                      borderSide: BorderSide(
                          color: isdescription ? Colors.red : Colors.grey,
                          width: 1.0),
                    ),
                    contentPadding: EdgeInsets.symmetric(
                        vertical: 10.0, horizontal: 10.0),
                    hintStyle: style.MontserratRegular(
                        fontSize: size.convert(context, 12)),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(30.0),
                    ),
                    hintText: 'Tell us about yourself',
                  ),
                  maxLines: 5,
                  onSaved: (value) {
                    setState(() {
                      widget.regModel.description = value;
                    });
                  },
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  isdescription
                      ? Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      "Enter your company description",
                      style:style.MontserratRegular(
                          color: Colors.red, fontSize: size.convert(context, 12)),
                    ),
                  ):isdescriptionlength? Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      "Description length must be 250 words",
                      style:style.MontserratRegular(
                          color: Colors.red, fontSize: size.convert(context, 12)),
                    ),
                  ): Container(),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text("$length/250",style: TextStyle(
                      color: Colors.grey,
                      fontSize:size.convert(context, 12),
                    ),),
                  ),
                ],
              ),
              Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 1.0,),
                child: Text(
                  "Select Country",
                  style: style.MontserratMedium(
                    fontSize: size.convert(context, 14),
                  ),
                ),
              ),
              DropDownWidget(
                  context: context,
                  hintext: "Country",
                  value: selectCountry,
                  selectedCategory: selectCountry,
                  categories: countries,
                  image: selectCountry != null
                      ? 'assets/images/flag2.png'
                      : 'assets/images/countryicon.svg',
                  onChanged: (value) {
                    setState(() {
                      if (selectCity != null) {
                        selectCity = null;
                      }
                      cities = [];
                    });
                    countries.forEach((f) async {
                      if (f.title == value) {
                        getCities(f.sId).then((list) {
                          // regModel;
                          setState(() {
                            cities = list;
                            selectCountry = value;
                            widget.regModel.countryId = f.sId;
                          });
                        });
                      }
                    });
                  }),
              Padding(
                padding:
                    const EdgeInsets.symmetric(vertical: 5.0, horizontal: 1.0),
                child: Text(
                  "Select City",
                  style: style.MontserratMedium(
                    fontSize: size.convert(context, 14),
                  ),
                ),
              ),
              DropDownWidget(
                context: context,
                image: 'assets/images/city.svg',
                selectedCategory: selectCity,
                categories: cities,
                hintext: "City",
                onChanged: (value) {
                  cities.forEach((f) {
                    if (f.title == value) {
                      widget.regModel.cityId = f.sId;
                    }
                  });
                  setState(() {
                    selectCity = value;
                  });
                },
                value: selectCity,
              ),
              SizedBox(
                height: (20 / 8.148314082864863) * SizeConfig.heightMultiplier,
              ),
              Stack(
                children: [
                  material(),
                  TextFormField(
                    onFieldSubmitted: (v) {},
                    textInputAction: TextInputAction.next,
                    decoration: InputDecoration(

                      focusedErrorBorder: outlineInputBorder(),
                      errorBorder: outlineInputBorder(),
                      hintStyle: style.MontserratRegular(
                          fontSize: size.convert(context, 12)),

                      errorStyle:style.MontserratRegular(
                          color: Colors.red, fontSize: size.convert(context, 12)),
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(25),
                        borderSide: BorderSide(color: Color(0xff69BEF7)),
                      ),
                      contentPadding: EdgeInsets.symmetric(
                          vertical: 10.0, horizontal: 10.0),
                      border: OutlineInputBorder(
                        borderSide: BorderSide(),
                        borderRadius: BorderRadius.circular(25.0),
                      ),
                      hintText: 'Password',
                      prefixIcon: Icon(Icons.lock,size: 18,),
                      suffixIcon: InkWell(
                          onTap: () {
                            setState(() {
                              isPasswordShow = !isPasswordShow;
                            });
                          },
                          child: isPasswordShow
                              ? Icon(
                                  Icons.visibility_off,
                                  size: 14,
                                )
                              : Icon(
                                  Icons.visibility,
                                  size: 14,
                                )),
                    ),
                    onSaved: (value) {
                      widget.regModel.password = value;
                    },
                    validator: (value) {
                      if (value.isEmpty && value.length < 6) {
                        return 'Choose your password';
                      }
                      return null;
                    },
                    obscureText: !isPasswordShow,
                  ),
                ],
              ),
              Container(
                width: orientation == Orientation.portrait
                    ? (485.3932272197492 / 4.853932272197492) *
                        SizeConfig.widthMultiplier
                    : (814.8314082864863 / 8.148314082864863) *
                        SizeConfig.heightMultiplier,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    Column(
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.all(2.0),
                          child: FlatButton(
                            onPressed: () {
                              imageNic();
                            },
                            child: Row(
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                Text(
                                  "Upload CNIC",
                                  style: TextStyle(
                                    fontFamily: fontandcolor.fontfaimlyregular,
                                    fontSize: 1.5 * SizeConfig.textMultiplier,
                                  ),
                                ),
                                Icon(
                                  Icons.add,
                                  size: 2.5 * SizeConfig.heightMultiplier,
                                  color: Color(0xff5e5c5c),
                                ),
                              ],
                            ),
                          ),
                        ),
                        Container(
                          width: orientation == Orientation.portrait
                              ? ((385.3932272197492 / 2.4) /
                                      4.853932272197492) *
                                  SizeConfig.widthMultiplier
                              : ((385.3932272197492 / 2.8) /
                                      4.853932272197492) *
                                  SizeConfig.widthMultiplier,
                          height: orientation == Orientation.portrait
                              ? ((385.3932272197492 / 2) / 4.853932272197492) *
                                  SizeConfig.widthMultiplier
                              : ((385.3932272197492 / 2.6) /
                                      4.853932272197492) *
                                  SizeConfig.widthMultiplier,
                          decoration: BoxDecoration(
                              border: Border.all(color: Colors.black54),
                              borderRadius: BorderRadius.circular(15)),
                          child: nicUpload != null &&
                                  widget.regModel.cnicCopy != null
                              ? Image.file(
                                  nicUpload,
                                  fit: BoxFit.contain,
                                )
                              : Center(
                                  child: Icon(
                                    Icons.image,
                                    color: Color(0xff394c81),
                                    size: (80 / 8.148314082864863) *
                                        SizeConfig.heightMultiplier,
                                  ),
                                ),
                        )
                      ],
                    ),
                    Column(
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.all(2.0),
                          child: FlatButton(
                            onPressed: () {
                              imageBill();
                            },
                            child: Row(
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                Text(
                                  "Upload Home Bill",
                                  style: TextStyle(
                                    fontFamily: fontandcolor.fontfaimlyregular,
                                    fontSize: 1.5 * SizeConfig.textMultiplier,
                                  ),
                                ),
                                Icon(
                                  Icons.add,
                                  size: 2.5 * SizeConfig.heightMultiplier,
                                  color: Color(0xff5e5c5c),
                                ),
                              ],
                            ),
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.zero,
                          width: orientation == Orientation.portrait
                              ? ((385.3932272197492 / 2.4) /
                                      4.853932272197492) *
                                  SizeConfig.widthMultiplier
                              : ((385.3932272197492 / 2.8) /
                                      4.853932272197492) *
                                  SizeConfig.widthMultiplier,
                          height: orientation == Orientation.portrait
                              ? ((385.3932272197492 / 2) / 4.853932272197492) *
                                  SizeConfig.widthMultiplier
                              : ((385.3932272197492 / 2.6) /
                                      4.853932272197492) *
                                  SizeConfig.widthMultiplier,
                          decoration: BoxDecoration(
                              border: Border.all(color: Colors.black54),
                              borderRadius: BorderRadius.circular(15)),
                          child: billUpload != null &&
                                  widget.regModel.homeBill != null
                              ? Image.file(billUpload)
                              : Center(
                                  child: Icon(
                                    Icons.image,
                                    size: (80 / 8.148314082864863) *
                                        SizeConfig.heightMultiplier,
                                    color: Color(0xff394c81),
                                  ),
                                ),
                        )
                      ],
                    ),
                  ],
                ),
              ),
            ],
          )),
    );
  }

  void pickCameraNic() async {
    nicUpload = await ImagePicker.pickImage(source: ImageSource.camera);
    setState(() {
      widget.regModel.cnicCopy = nicUpload.path;
    });
  }

  void pickCameraBill() async {
    billUpload = await ImagePicker.pickImage(source: ImageSource.camera);
    setState(() {
      widget.regModel.homeBill = billUpload.path;
    });
  }

  void pickGalleryBill() async {
    billUpload = await ImagePicker.pickImage(source: ImageSource.camera);
    setState(() {
      widget.regModel.homeBill = billUpload.path;
    });
  }

  void pickGalleryNic() async {
    nicUpload = await ImagePicker.pickImage(source: ImageSource.camera);
    setState(() {
      widget.regModel.cnicCopy = nicUpload.path;
    });
  }

  nullimagedialoug(context) async {
    showDialog(
        context: context,
        builder: (_) {
          return AlertDialog(
            contentPadding: EdgeInsets.all(20.0),
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
            title: Text("Select Images"),
            actions: <Widget>[
              FlatButton(
                child: Text("ok"),
                onPressed: () => Navigator.of(context).pop(),
              )
            ],
          );
        });
  }

  imageNic() async {
    showDialog(
        context: context,
        builder: (_) {
          return AlertDialog(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
            title: Text(
              "Choose your file",
              style: TextStyle(
                fontFamily: fontandcolor.fontfaimly,
              ),
            ),
            content: Container(
              alignment: Alignment.center,
              height: (100 / 8.148314082864863) * SizeConfig.heightMultiplier,
              child: Column(
                children: <Widget>[
                  SizedBox(
                    height:
                        (40 / 8.148314082864863) * SizeConfig.heightMultiplier,
                  ),
                  InkWell(
                      onTap: () {
                        pickCameraNic();

                        Navigator.pop(context);
                      },
                      child: Text(
                        "Use Camera",
                        style: TextStyle(
                          fontFamily: fontandcolor.fontfaimlyregular,
                        ),
                      )),
                ],
              ),
            ),
            actions: <Widget>[
              FlatButton(
                child: Text(
                  "cancel",
                  style: TextStyle(
                    fontFamily: fontandcolor.fontfaimlyregular,
                  ),
                ),
                onPressed: () => Navigator.of(context).pop(),
              )
            ],
          );
        });
  }

  imageBill() async {
    showDialog(
        context: context,
        builder: (_) {
          return AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0)),
            title: Text(
              "Choose your file",
              style: TextStyle(
                fontFamily: fontandcolor.fontfaimly,
              ),
            ),
            content: Container(
              alignment: Alignment.center,
              height: (100 / 8.148314082864863) * SizeConfig.heightMultiplier,
              child: Column(
                children: <Widget>[
                  SizedBox(
                    height:
                        (40 / 8.148314082864863) * SizeConfig.heightMultiplier,
                  ),
                  InkWell(
                      onTap: () {
                        pickGalleryBill();
                        Navigator.pop(context);
                      },
                      child: Text(
                        "Use Camera",
                        style: TextStyle(
                          fontFamily: fontandcolor.fontfaimlyregular,
                        ),
                      )),
                ],
              ),
            ),
            actions: <Widget>[
              FlatButton(
                child: Text(
                  "cancel",
                  style: TextStyle(
                    fontFamily: fontandcolor.fontfaimlyregular,
                  ),
                ),
                onPressed: () => Navigator.of(context).pop(),
              )
            ],
          );
        });
  }

  @override
  void initState() {
    getCountries().then((list) {
      setState(() {
        countries = list;
        loading = false;
      });
    });
    super.initState();
  }

  @override
  void dispose() {
    billUpload?.delete();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    print('title: ${widget.roleModel.title}');
    print("country code ${widget.countryCode}");
    return GestureDetector(
      onTap: () => FocusScope.of(context).requestFocus(new FocusNode()),
      child: Scaffold(body: OrientationBuilder(
        builder: (context, orientation) {
          return SingleChildScrollView(
            child: Column(
              children: <Widget>[
                Container(
                  //color: Colors.red,
                  margin: EdgeInsets.symmetric(
                      horizontal: size.convert(context, 16)),
                  child: Column(
                    children: [
                      SizedBox(height: size.convert(context, 40)),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          InkWell(
                            onTap: () {
                              Navigator.pop(context);
                            },
                            child: Icon(
                              Icons.arrow_back_sharp,
                              color: Colors.black,
                            ),
                          ),
                          Container(),
                        ],
                      ),
                      // SizedBox(height: size.convert(context, 20),),
//                      Container(
//                        child: Image.asset(
//                          "assets/images/logo.png",
//                          width: size.convertWidth(context, 200),
//                          height: size.convert(context, 90),
//                        ),
//                      ),
                    ],
                  ),
                ),
                Container(
                  padding: EdgeInsets.symmetric(vertical: 20),
                  margin: EdgeInsets.symmetric(
                      horizontal: size.convertWidth(context, 21)),
                  // alignment: Alignment.bottomLeft,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    boxShadow: [
                      BoxShadow(
                        color: Color(0xffd4d4d4),
//                              spreadRadius: 6,
                        blurRadius: 6,
                        offset: Offset(0, 3),
                        // changes position of shadow
                      ),
                    ],
                    borderRadius:
                        BorderRadius.circular(size.convert(context, 20)),
//                          borderRadius: BorderRadius.only(
//                            topLeft: Radius.circular(44.5),
//                            topRight: Radius.circular(44.5),
//                          )
                  ),
                  child: Column(
                    children: [
                      Container(
                        child: SvgPicture.asset(
                          "assets/icons/loginIcon.svg",
                          width: size.convertWidth(context, 200),
                          height: size.convert(context, 110),
                        ),
                      ),
                      Center(
                        child: Text(
                          "Register",
                          style: style.QuicksandBold(
                              fontSize: size.convert(context, 30),
                              color: Color(0xff394c81)),
                        ),
                      ),
                      SizedBox(
                        height: orientation == Orientation.portrait
                            ? (5 / 8.148314082864863) *
                                SizeConfig.heightMultiplier
                            : (5 / 8.148314082864863) *
                                SizeConfig.heightMultiplier,
                      ),
                      widget.roleModel.title == "Broker"
                          ? buildFormBroker(orientation)
                          : buildForm(),
                      filledButton(
                        w: size.convertWidth(context, 260),
                        txt: "Sign Up",
                        onTap: () async {
                          if(widget.roleModel.title == "Broker"){
                            if(validateAndSave()){
                              if(_description.text.isEmpty){
                                setState(() {
                                  isdescription=true;
                                });
                              }else if(_description.text.length>250){
                                setState(() {
                                  isdescription=false;
                                  isdescriptionlength=true;
                                });
                              }else if(billUpload == null || nicUpload == null) {
                                return nullimagedialoug(context);
                              }else{
                                Navigator.push(
                                    context,
                                    PageTransition(
                                        type: PageTransitionType.bottomToTop,
                                        duration: Duration(milliseconds: 500),
                                        child: PinCode(
                                          regModel: widget.regModel,
                                          countryCode: widget.countryCode,
                                          phone: widget.regModel.contactNumber
                                              .toString(),
                                        )));
                              }
                            }
                          }else{
                            if(validateAndSave()){
                              if(_description.text.isEmpty){
                                setState(() {
                                  isdescription=true;
                                });
                              }else if(_description.text.length>250){
                                setState(() {
                                  isdescription=false;
                                  isdescriptionlength=true;
                                });
                              }else{
                                setState(() {
                                  isdescription=false;
                                  isdescriptionlength=false;
                                });
                                Navigator.push(
                                    context,
                                    PageTransition(
                                        type: PageTransitionType.bottomToTop,
                                        duration: Duration(milliseconds: 500),
                                        child: PinCode(
                                          regModel: widget.regModel,
                                          countryCode: widget.countryCode,
                                          phone: widget.regModel.contactNumber
                                              .toString(),
                                        )));
                              }
                            }
                          }
                          // if (validateAndSave()) {
                          //   if (widget.roleModel.title == "Broker") {
                          //     if (billUpload == null || nicUpload == null) {
                          //       return nullimagedialoug(context);
                          //     } else {
                          //       Navigator.push(
                          //           context,
                          //           PageTransition(
                          //               type: PageTransitionType.bottomToTop,
                          //               duration: Duration(milliseconds: 500),
                          //               child: PinCode(
                          //                 regModel: widget.regModel,
                          //                 countryCode: widget.countryCode,
                          //                 phone: widget.regModel.contactNumber
                          //                     .toString(),
                          //               )));
                          //     }
                          //   } else {
                          //     Navigator.push(
                          //         context,
                          //         PageTransition(
                          //             type: PageTransitionType.bottomToTop,
                          //             duration: Duration(milliseconds: 500),
                          //             child: PinCode(
                          //               regModel: widget.regModel,
                          //               countryCode: widget.countryCode,
                          //               phone: widget.regModel.contactNumber
                          //                   .toString(),
                          //             )));
                          //   }
                          // }
                        },
                      ),
                      SizedBox(
                        height: size.convert(context, 10),
                      ),
                    ],
                  ),
                ),

//                Center(
//                  child: MaterialButton(
//                    onPressed: () async {
//                      print(billUpload);
//                      print(nicUpload);
//                      if (validateAndSave()) {
//                        if (widget.roleModel.title == "Broker") {
//                          if (billUpload == null || nicUpload == null) {
//                            return nullimagedialoug(context);
//                          } else {
//                            Navigator.push(
//                                context,
//                                PageTransition(
//                                    type: PageTransitionType.downToUp,
//                                    duration: Duration(milliseconds: 500),
//                                    child: PinCode(
//                                      regModel: widget.regModel,
//                                      countryCode: widget.countryCode,
//                                      phone: widget.regModel.contactNumber
//                                          .toString(),
//                                    )));
//                          }
//                        } else {
//                          Navigator.push(
//                              context,
//                              PageTransition(
//                                  type: PageTransitionType.downToUp,
//                                  duration: Duration(milliseconds: 500),
//                                  child: PinCode(
//                                    regModel: widget.regModel,
//                                    countryCode: widget.countryCode,
//                                    phone:
//                                        widget.regModel.contactNumber.toString(),
//                                  )));
//                        }
//                      }
//                    },
//                    child: Container(
//                      width:
//                          (370 / 4.853932272197492) * SizeConfig.widthMultiplier,
//                      height:
//                          (68 / 8.148314082864863) * SizeConfig.heightMultiplier,
//                      alignment: Alignment.center,
//                      decoration: BoxDecoration(
//                        borderRadius: BorderRadius.circular(25.0),
//                        gradient: LinearGradient(
//                          begin: Alignment.topCenter,
//                          end: Alignment.bottomCenter,
//                          colors: [
//                            Color(0xFF69bef7),
//                            Color(0xFF2a65ff),
//                          ],
//                        ),
//                        boxShadow: [
//                          BoxShadow(
//                            color: Color(0xFF69bef7),
//                            spreadRadius: 1,
//                            blurRadius: 12,
//                            offset: Offset(0, 2), // changes position of shadow
//                          ),
//                        ],
//                      ),
//                      child: Text(
//                        "NEXT",
//                        style: TextStyle(
//                            fontSize: (22 / 8.148314082864863) *
//                                SizeConfig.heightMultiplier,
//                            color: Colors.white),
//                      ),
//                    ),
//                    shape: StadiumBorder(),
//                  ),
//                ),
              ],
            ),
          );
        },
      )),
    );
  }

  bool validateAndSave() {
    final form = formKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    } else if(_description.text.isEmpty){
      setState(() {
        isdescription=true;
      });
      return false;
    }else if(_description.text.length>250) {
      setState(() {
        isdescriptionlength=true;
      });
      return false;
    }else{
      return false;
    }
  }
}
