import 'package:flutter/material.dart';
import 'package:biznes/size_config.dart';
import 'package:biznes/res/size.dart';
import 'package:biznes/res/style.dart';
import 'package:biznes/repeatedWigets/filledButton.dart';
import '../login_UI/home.dart';
import 'package:flutter_svg/flutter_svg.dart';

class Dilouge extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        alignment: Alignment.center,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            SvgPicture.asset("assets/icons/AsGuest.svg"),
            SizedBox(
              height: size.convert(context, 3),
            ),
            Text(
              "Get Registered",
              style: style.QuicksandBold(
                  fontSize:
                      (24 / 8.148314082864863) * SizeConfig.heightMultiplier,
                  color: Color(0xff394c81)),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 10, left: 15, right: 15),
              child: Text(
                "Quick Biznes is an online trading messenger. A platform that will help you to buy and sell your business stock / products instantly. Register yourself to gain and chat with new customers and expand your business portfolio.",
                textAlign: TextAlign.center,
                style: style.MontserratRegular(
                  fontSize: size.convert(context, 14),
                  color: Colors.blueGrey,
                ),
              ),
            ),
            SizedBox(
              height: size.convert(context, 15),
            ),
            filledButton(
              txt: "Sign up / Sign in",
              onTap: () {
                Navigator.of(context, rootNavigator: true).pushAndRemoveUntil(
                    MaterialPageRoute(builder: (context) => LoginScreen()),
                    (route) => false);
                // Navigator.of(context,rootNavigator: true).pushAndRemoveUntil(
                //     MaterialPageRoute(builder: (context) => LoginScreen(),),);
              },
            ),
            SizedBox(
              height: (20 / 8.148314082864863) * SizeConfig.heightMultiplier,
            ),
          ],
        ),
      ),
    );
  }
}
