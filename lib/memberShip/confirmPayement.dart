import 'package:biznes/product_UI/bottomBar_UI/write_message.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';

class ConfirmPayment extends StatefulWidget {
  @override
  _ConfirmPaymentState createState() => _ConfirmPaymentState();
}

class _ConfirmPaymentState extends State<ConfirmPayment> {
  var formKey = GlobalKey<FormState>();
  bool _saveCardInfo = false;

  Widget buildForm() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Container(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Image.asset(
                "assets/images/p.png",
                width: 100.0,
                height: 100.0,
              ),
              Image.asset(
                "assets/images/visa.png",
                width: 100.0,
                height: 100.0,
              )
            ],
          ),
        ),
        Padding(
          padding: EdgeInsets.all(16.2),
          child: Form(
              key: formKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(
                        top: 10.0, left: 10.0, right: 10.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          "Card Holder Email / Phone",
                          style: TextStyle(fontSize: 14, color: Colors.black),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 10.0),
                          child: Center(
                            child: TextFormField(
                              decoration: InputDecoration(
                                contentPadding: const EdgeInsets.only(
                                    top: 0.0,
                                    bottom: 0.0,
                                    left: 10.0,
                                    right: 5.0),
                                hintText: "Enter your email or phone",
                                hintStyle: TextStyle(fontSize: 12.0),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.all(
                                        Radius.circular(30.0))),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                        top: 10.0, left: 10.0, right: 10.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          "Card Number",
                          style: TextStyle(fontSize: 14, color: Colors.black),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 10.0),
                          child: Center(
                            child: TextFormField(
                              decoration: InputDecoration(
                                contentPadding: const EdgeInsets.only(
                                    top: 0.0,
                                    bottom: 0.0,
                                    left: 10.0,
                                    right: 5.0),
                                hintText: "Enter your card no",
                                hintStyle: TextStyle(fontSize: 12.0),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.all(
                                        Radius.circular(30.0))),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                        top: 10.0, left: 10.0, right: 10.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          "Card Details",
                          style: TextStyle(fontSize: 14, color: Colors.black),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 10.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Expanded(
                                flex: 2,
                                child: TextFormField(
                                  textAlign: TextAlign.center,
                                  keyboardType: TextInputType.datetime,
                                  decoration: InputDecoration(
                                    contentPadding: const EdgeInsets.only(
                                        top: 0.0,
                                        bottom: 0.0,
                                        left: 10.0,
                                        right: 5.0),
                                    hintText: "MM",
                                    hintStyle: TextStyle(fontSize: 12.0),
                                    border: OutlineInputBorder(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(30.0))),
                                  ),
                                ),
                              ),
                              SizedBox(
                                width: 5.0,
                              ),
                              Expanded(
                                flex: 2,
                                child: TextFormField(
                                  textAlign: TextAlign.center,
                                  keyboardType: TextInputType.datetime,
                                  decoration: InputDecoration(
                                    contentPadding: const EdgeInsets.only(
                                        top: 0.0,
                                        bottom: 0.0,
                                        left: 10.0,
                                        right: 5.0),
                                    hintText: "YYYY",
                                    hintStyle: TextStyle(fontSize: 12.0),
                                    border: OutlineInputBorder(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(30.0))),
                                  ),
                                ),
                              ),
                              SizedBox(
                                width: 5.0,
                              ),
                              Expanded(
                                flex: 2,
                                child: TextFormField(
                                  textAlign: TextAlign.center,
                                  keyboardType: TextInputType.number,
                                  decoration: InputDecoration(
                                    contentPadding: const EdgeInsets.only(
                                        top: 0.0,
                                        bottom: 0.0,
                                        left: 10.0,
                                        right: 5.0),
                                    hintText: "CVV",
                                    hintStyle: TextStyle(fontSize: 12.0),
                                    border: OutlineInputBorder(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(30.0))),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 8.0),
                    child: Row(
                      children: <Widget>[
                        Checkbox(
                            materialTapTargetSize: MaterialTapTargetSize.padded,
                            value: _saveCardInfo,
                            onChanged: (val) {
                              setState(() {
                                _saveCardInfo = val;
                              });
                            }),
                        Text("Save credit card information"),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 20.0),
                    child: Center(
                      child: RaisedButton(
                          padding: const EdgeInsets.only(
                              left: 70.0, right: 70.0, top: 20.0, bottom: 20.0),
                          child: Text(
                            "GET MEMBERSHIP",
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.w400),
                          ),
                          color: Colors.green,
                          shape: RoundedRectangleBorder(
                              borderRadius:
                              BorderRadius.all(Radius.circular(30.0))),
                          onPressed: () {
                            Navigator.of(context).push(PageTransition(
                                child: WriteMessage(),
                                type: PageTransitionType.bottomToTop,
                                duration: Duration(seconds: 0)));
                          }),
                    ),
                  )
                ],
              )),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: ListView(
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                color: Colors.white,
                // boxShadow: <BoxShadow>[
                //   BoxShadow(
                //       color: Colors.grey,
                //       blurRadius: 12.3,
                //       offset: Offset(1.0, 6.0),
                //       spreadRadius: 2)
                // ],
                // borderRadius: BorderRadius.only(
                //   bottomLeft: Radius.circular(35.5),
                //   bottomRight: Radius.circular(35.5),
                // )
              ),
              width: MediaQuery.of(context).size.width,
              // height: MediaQuery.of(context).size.height,
              child: Column(
                children: <Widget>[
                  Padding(
                    padding:
                    const EdgeInsets.only(left: 9.0, top: 33.0, right: 9.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        IconButton(
                            icon: Icon(Icons.arrow_back),
                            onPressed: () {
                              // Navigator.of(context).push(MaterialPageRoute(builder: (_)=>Profile()));
                            }),
                        // SizedBox(
                        //   width: 150.4,
                        // ),
                        Text(
                          "Confirm Payment",
                          style: TextStyle(
                              fontSize: 20.2, fontWeight: FontWeight.w500),
                        ),
                        Stack(
                          children: <Widget>[
                            CircleAvatar(
                              backgroundColor: Colors.green,
                              radius: 9.0,
                              child: Text(
                                "6",
                                style: TextStyle(
                                    color: Colors.white, fontSize: 11),
                              ),
                            ),
                            IconButton(
                                icon: Icon(
                                  Icons.notifications_active,
                                  size: 20,
                                ),
                                onPressed: () {
                                  // Navigator.of(context).push(MaterialPageRoute(builder: (_)=>Profile()));
                                }),
                          ],
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 50.0),
              child: buildForm(),
            )
          ],
        ),
      ),
    );
  }
}
