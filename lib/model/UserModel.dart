class User {
  String description;
  List<RoleId> roleId;
  int userPinCode;
  int oTPCOde;
  bool status;
  bool onlineStatus;
  String profilePic;
  String sId;
  String fullName;
  String username;
  String organizationName;
  String email;
  int contactNumber;
  String countryId;
  String mobileToken;
  String cityId;
  String roleName;
  String lastLogin;
  String password;
  String createdAt;
  String updatedAt;
  int iV;

  User(
      {this.description,
      this.roleId,
      this.userPinCode,
      this.oTPCOde,
      this.status,
      this.onlineStatus,
      this.profilePic,
      this.sId,
      this.fullName,
      this.username,
      this.organizationName,
      this.email,
      this.contactNumber,
      this.countryId,
      this.mobileToken,
      this.cityId,
      this.roleName,
      this.lastLogin,
      this.password,
      this.createdAt,
      this.updatedAt,
      this.iV});

  User.fromJson(Map<String, dynamic> json) {
    description = json['description'];
    if (json['roleId'] != null) {
      roleId = new List<RoleId>();
      json['roleId'].forEach((v) {
        roleId.add(new RoleId.fromJson(v));
      });
    }
    userPinCode = json['userPinCode'];
    oTPCOde = json['OTPCOde'];
    status = json['status'];
    onlineStatus = json['onlineStatus'];
    profilePic = json['profilePic'];
    sId = json['_id'];
    fullName = json['fullName'];
    username = json['username'];
    organizationName = json['organizationName'];
    email = json['email'];
    contactNumber = json['contactNumber'];
    countryId = json['countryId'];
    mobileToken = json['mobileToken'];
    cityId = json['cityId'];
    roleName = json['roleName'];
    lastLogin = json['lastLogin'];
    password = json['password'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    iV = json['__v'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['description'] = this.description;
    if (this.roleId != null) {
      data['roleId'] = this.roleId.map((v) => v.toJson()).toList();
    }
    data['userPinCode'] = this.userPinCode;
    data['OTPCOde'] = this.oTPCOde;
    data['status'] = this.status;
    data['onlineStatus'] = this.onlineStatus;
    data['profilePic'] = this.profilePic;
    data['_id'] = this.sId;
    data['fullName'] = this.fullName;
    data['username'] = this.username;
    data['organizationName'] = this.organizationName;
    data['email'] = this.email;
    data['contactNumber'] = this.contactNumber;
    data['countryId'] = this.countryId;
    data['mobileToken'] = this.mobileToken;
    data['cityId'] = this.cityId;
    data['roleName'] = this.roleName;
    data['lastLogin'] = this.lastLogin;
    data['password'] = this.password;
    data['createdAt'] = this.createdAt;
    data['updatedAt'] = this.updatedAt;
    data['__v'] = this.iV;
    return data;
  }
}

class RoleId {
  String sId;
  String title;
  String createdAt;
  String updatedAt;
  int iV;

  RoleId({this.sId, this.title, this.createdAt, this.updatedAt, this.iV});

  RoleId.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    title = json['title'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    iV = json['__v'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.sId;
    data['title'] = this.title;
    data['createdAt'] = this.createdAt;
    data['updatedAt'] = this.updatedAt;
    data['__v'] = this.iV;
    return data;
  }
}