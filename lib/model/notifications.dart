class NewNotification {
  bool success;
  List<Notifications> notifications;

  NewNotification({this.success, this.notifications});

  NewNotification.fromJson(Map<String, dynamic> json) {
    success = json['Success'];
    if (json['notifications'] != null) {
      notifications = new List<Notifications>();
      json['notifications'].forEach((v) {
        notifications.add(new Notifications.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Success'] = this.success;
    if (this.notifications != null) {
      data['notifications'] =
          this.notifications.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Notifications {
  String createdBy;
  bool isRead;
  String type;
  String info;
  String conversation;
  String notifyId;
  String sId;
  String createdFor;
  String message;
  String createdAt;
  String updatedAt;
  int iV;

  Notifications(
      {this.createdBy,
      this.isRead,
      this.type,
      this.info,
      this.conversation,
      this.notifyId,
      this.sId,
      this.createdFor,
      this.message,
      this.createdAt,
      this.updatedAt,
      this.iV});

  Notifications.fromJson(Map<String, dynamic> json) {
    createdBy = json['createdBy'];
    isRead = json['isRead'];
    type = json['type'];
    info = json['info'];
    conversation = json['conversation'];
    notifyId = json['notifyId'];
    sId = json['_id'];
    createdFor = json['createdFor'];
    message = json['message'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    iV = json['__v'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['createdBy'] = this.createdBy;
    data['isRead'] = this.isRead;
    data['type'] = this.type;
    data['info'] = this.info;
    data['conversation'] = this.conversation;
    data['notifyId'] = this.notifyId;
    data['_id'] = this.sId;
    data['createdFor'] = this.createdFor;
    data['message'] = this.message;
    data['createdAt'] = this.createdAt;
    data['updatedAt'] = this.updatedAt;
    data['__v'] = this.iV;
    return data;
  }
}
