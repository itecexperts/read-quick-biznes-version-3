class TypingStatus {
  String userName;
  String conversationId;

  TypingStatus({this.userName, this.conversationId});

  TypingStatus.fromJson(Map<String, dynamic> json) {
    userName = json['userName'];
    conversationId = json['conversationId'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['userName'] = this.userName;
    data['conversationId'] = this.conversationId;
    return data;
  }
}