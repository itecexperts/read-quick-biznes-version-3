class CitiesModel {
  String sId;
  String title;
  String countryId;
  String createdAt;
  String updatedAt;
  int iV;

  CitiesModel(
      {this.sId,
      this.title,
      this.countryId,
      this.createdAt,
      this.updatedAt,
      this.iV});

  CitiesModel.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    title = json['title'];
    countryId = json['countryId'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    iV = json['__v'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.sId;
    data['title'] = this.title;
    data['countryId'] = this.countryId;
    data['createdAt'] = this.createdAt;
    data['updatedAt'] = this.updatedAt;
    data['__v'] = this.iV;
    return data;
  }
}