class SendMessageToAllMessageModel {
  String userId;
  String subCategoryId;
  String categoryId;
  String image;
  String message;
  String countryId;
  String cityId;
  String userRole;
  SendMessageToAllMessageModel(
      {this.userId,
      this.subCategoryId,
      this.image,
      categoryId,
      this.message,
      this.countryId,
      this.userRole,
      this.cityId});
}
