class PlanCategoriesModel{
  String id;
  String title;

  PlanCategoriesModel({this.id, this.title});

  factory PlanCategoriesModel.fromJson(Map<String,dynamic> json){
    return PlanCategoriesModel(
      id: json['id'],
      title: json['title'],
    );
  }
}