// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'conversationmodel.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class conversationsGeneration extends TypeAdapter<NewMessages> {
  @override
  final int typeId = 1;

  @override
  NewMessages read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return NewMessages(
      status: fields[0] as String,
      attachment: fields[2] as String,
      sId: fields[3] as String,
      message: fields[4] as String,
      createdFor: fields[5] as CreatedBy,
      createdBy: fields[6] as CreatedBy,
      categoryId: fields[7] as CategoryId,
      subCategoryId: fields[8] as CategoryId,
      conversation: fields[9] as Conversation,
      createdAt: fields[10] as String,
      updatedAt: fields[11] as String,
      offer: fields[12] as String,
    )..isselectedchat = fields[1] as bool;
  }

  @override
  void write(BinaryWriter writer, NewMessages obj) {
    writer
      ..writeByte(13)
      ..writeByte(0)
      ..write(obj.status)
      ..writeByte(1)
      ..write(obj.isselectedchat)
      ..writeByte(2)
      ..write(obj.attachment)
      ..writeByte(3)
      ..write(obj.sId)
      ..writeByte(4)
      ..write(obj.message)
      ..writeByte(5)
      ..write(obj.createdFor)
      ..writeByte(6)
      ..write(obj.createdBy)
      ..writeByte(7)
      ..write(obj.categoryId)
      ..writeByte(8)
      ..write(obj.subCategoryId)
      ..writeByte(9)
      ..write(obj.conversation)
      ..writeByte(10)
      ..write(obj.createdAt)
      ..writeByte(11)
      ..write(obj.updatedAt)
      ..writeByte(12)
      ..write(obj.offer);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is conversationsGeneration &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

class conversationsGeneration2 extends TypeAdapter<CreatedBy> {
  @override
  final int typeId = 2;

  @override
  CreatedBy read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return CreatedBy(
      onlineStatus: fields[0] as bool,
      profilePic: fields[1] as String,
      sId: fields[2] as String,
      fullName: fields[3] as String,
      username: fields[4] as String,
      email: fields[5] as String,
    );
  }

  @override
  void write(BinaryWriter writer, CreatedBy obj) {
    writer
      ..writeByte(6)
      ..writeByte(0)
      ..write(obj.onlineStatus)
      ..writeByte(1)
      ..write(obj.profilePic)
      ..writeByte(2)
      ..write(obj.sId)
      ..writeByte(3)
      ..write(obj.fullName)
      ..writeByte(4)
      ..write(obj.username)
      ..writeByte(5)
      ..write(obj.email);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is conversationsGeneration2 &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

class conversationsGeneration3 extends TypeAdapter<CategoryId> {
  @override
  final int typeId = 3;

  @override
  CategoryId read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return CategoryId(
      sId: fields[0] as String,
      title: fields[1] as String,
    );
  }

  @override
  void write(BinaryWriter writer, CategoryId obj) {
    writer
      ..writeByte(2)
      ..writeByte(0)
      ..write(obj.sId)
      ..writeByte(1)
      ..write(obj.title);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is conversationsGeneration3 &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

class conversationsGeneration4 extends TypeAdapter<RoleId> {
  @override
  final int typeId = 4;

  @override
  RoleId read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return RoleId(
      sId: fields[0] as String,
      title: fields[1] as String,
    );
  }

  @override
  void write(BinaryWriter writer, RoleId obj) {
    writer
      ..writeByte(2)
      ..writeByte(0)
      ..write(obj.sId)
      ..writeByte(1)
      ..write(obj.title);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is conversationsGeneration4 &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

class conversationsGeneration5 extends TypeAdapter<Conversation> {
  @override
  final int typeId = 5;

  @override
  Conversation read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return Conversation(
      conBlock: fields[0] as bool,
      blockedBy: fields[1] as String,
      blockedTo: fields[2] as String,
      sId: fields[3] as String,
    );
  }

  @override
  void write(BinaryWriter writer, Conversation obj) {
    writer
      ..writeByte(4)
      ..writeByte(0)
      ..write(obj.conBlock)
      ..writeByte(1)
      ..write(obj.blockedBy)
      ..writeByte(2)
      ..write(obj.blockedTo)
      ..writeByte(3)
      ..write(obj.sId);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is conversationsGeneration5 &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
