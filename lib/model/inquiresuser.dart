import 'dart:convert';

InquiriesUsers inquiriesUsersFromJson(String str) => InquiriesUsers.fromJson(json.decode(str));

String inquiriesUsersToJson(InquiriesUsers data) => json.encode(data.toJson());

class InquiriesUsers {
    InquiriesUsers({
        this.success,
        this.inquiries,
    });

    bool success;
    List<Inquiry> inquiries;

    factory InquiriesUsers.fromJson(Map<String, dynamic> json) => InquiriesUsers(
        success: json["Success"],
        inquiries: List<Inquiry>.from(json["Inquiries"].map((x) => Inquiry.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "Success": success,
        "Inquiries": List<dynamic>.from(inquiries.map((x) => x.toJson())),
    };
}

class Inquiry {
    Inquiry({
        this.participants,
        this.status,
        this.isRead,
        this.id,
        this.categoryId,
        this.createdAt,
        this.updatedAt,
        this.v,
    });

    List<dynamic> participants;
    String status;
    int isRead;
    String id;
    CategoryId categoryId;
    DateTime createdAt;
    DateTime updatedAt;
    int v;

    factory Inquiry.fromJson(Map<String, dynamic> json) => Inquiry(
        participants: List<dynamic>.from(json["participants"].map((x) => x)),
        status: json["status"],
        isRead: json["isRead"],
        id: json["_id"],
        categoryId: CategoryId.fromJson(json["categoryId"]),
        createdAt: DateTime.parse(json["createdAt"]),
        updatedAt: DateTime.parse(json["updatedAt"]),
        v: json["__v"],
    );

    Map<String, dynamic> toJson() => {
        "participants": List<dynamic>.from(participants.map((x) => x)),
        "status": status,
        "isRead": isRead,
        "_id": id,
        "categoryId": categoryId.toJson(),
        "createdAt": createdAt.toIso8601String(),
        "updatedAt": updatedAt.toIso8601String(),
        "__v": v,
    };
}

class CategoryId {
    CategoryId({
        this.id,
        this.title,
        this.createdAt,
        this.updatedAt,
        this.v,
    });

    String id;
    String title;
    DateTime createdAt;
    DateTime updatedAt;
    int v;

    factory CategoryId.fromJson(Map<String, dynamic> json) => CategoryId(
        id: json["_id"],
        title: json["title"],
        createdAt: DateTime.parse(json["createdAt"]),
        updatedAt: DateTime.parse(json["updatedAt"]),
        v: json["__v"],
    );

    Map<String, dynamic> toJson() => {
        "_id": id,
        "title": title,
        "createdAt": createdAt.toIso8601String(),
        "updatedAt": updatedAt.toIso8601String(),
        "__v": v,
    };
}

class ParticipantClass {
    ParticipantClass({
        this.roleId,
        this.profilePic,
        this.id,
        this.username,
    });

    List<RoleId> roleId;
    String profilePic;
    String id;
    String username;

    factory ParticipantClass.fromJson(Map<String, dynamic> json) => ParticipantClass(
        roleId: List<RoleId>.from(json["roleId"].map((x) => RoleId.fromJson(x))),
        profilePic: json["profilePic"],
        id: json["_id"],
        username: json["username"],
    );

    Map<String, dynamic> toJson() => {
        "roleId": List<dynamic>.from(roleId.map((x) => x.toJson())),
        "profilePic": profilePic,
        "_id": id,
        "username": username,
    };
}

class RoleId {
    RoleId({
        this.id,
        this.title,
    });

    String id;
    String title;

    factory RoleId.fromJson(Map<String, dynamic> json) => RoleId(
        id: json["_id"],
        title: json["title"],
    );

    Map<String, dynamic> toJson() => {
        "_id": id,
        "title": title,
    };
}