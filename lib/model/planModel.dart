class PlanModel{

  String description;
  String id;
  String title;
  int duration;
  int price;
  bool planSelected;
  int totalParent;
  int totalSubcategory;
  String createdAt;
  String updatedAt;
  int v;
  PlanModel({
    this.description, this.id, this.title, this.duration, this.price,
    this.totalParent, this.totalSubcategory, this.createdAt, this.updatedAt,
    this.v,
    this.planSelected=false
});
 factory PlanModel.fromJson(Map<String ,dynamic>json){
   return PlanModel(
     description: json['description'],
     id: json['_id'],
     title: json['title'],
     duration: json['duration'],
     price: json['price'],
     totalParent: json['totalParent'],
     totalSubcategory: json['totalSubcategory'],
     createdAt: json['createdAt'],
     updatedAt: json['updatedAt'],
     v: json['__v'],
   );
}
}