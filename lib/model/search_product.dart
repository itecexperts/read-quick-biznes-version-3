class SearchProductModel {
  String category;
  String subcategory;
  String country;
  String city;
  String userRole;
  String userId;
  SearchProductModel({this.category, this.subcategory, this.country, this.city,this.userRole});

  SearchProductModel.fromJson(Map<String, dynamic> json) {
    category = json['category'];
    subcategory = json['subcategory'];
    country = json['country'];
    city = json['city'];
    userId=json['userId'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['category'] = this.category;
    data['subcategory'] = this.subcategory;
    data['country'] = this.country;
    data['city'] = this.city;
    data['userId']=this.userId;
    return data;
  }
}