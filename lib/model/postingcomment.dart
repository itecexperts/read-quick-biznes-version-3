class PostingCommentModel{
  String comments;
  double ratings;
  String productId;
  PostingCommentModel({
    this.comments,
    this.ratings,
    this.productId,
  });
}