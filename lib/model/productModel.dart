class ProductModel {
  int quantity;
  String quantityUnit;
  int moq;
  List<String> images;
  String sId;
  String title;
  String description;
  String organizationName;
  String price;
  String category;
  String subCategory;
  String country;
  String city;
  String createdBy;
  String createdAt;
  String updatedAt;
  String rating;
  bool isProductSelected=false;
  int iV;

  ProductModel(
      {
        this.quantity,
        this.quantityUnit,
        this.moq,
        this.images,
        this.sId,
        this.title,
        this.description,
        this.organizationName,
        this.price,
        this.category,
        this.subCategory,
        this.country,
        this.city,
        this.createdBy,
        this.createdAt,
        this.updatedAt,
        this.rating,
        this.isProductSelected,
        this.iV
      });

  ProductModel.fromJson(Map<String, dynamic> json) {
    quantity = json['quantity'];
    moq = json['moq'];
    images = json['images'].cast<String>();
    sId = json['_id'];
    title = json['title'];
    description = json['description'];
    organizationName = json['organizationName'];
    price = json['price'];
    category = json['category'];
    subCategory = json['subCategory'];
    country = json['country'];
    city = json['city'];
    createdBy = json['createdBy'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    rating=json['ratings'].toString();
    iV = json['__v'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['quantity'] = this.quantity;
    data['moq'] = this.moq;
    data['images'] = this.images;
    data['_id'] = this.sId;
    data['title'] = this.title;
    data['description'] = this.description;
    data['organizationName'] = this.organizationName;
    data['price'] = this.price;
    data['category'] = this.category;
    data['subCategory'] = this.subCategory;
    data['country'] = this.country;
    data['city'] = this.city;
    data['createdBy'] = this.createdBy;
    data['createdAt'] = this.createdAt;
    data['updatedAt'] = this.updatedAt;
    data['ratings']=this.rating;
    data['__v'] = this.iV;
    return data;
  }
}