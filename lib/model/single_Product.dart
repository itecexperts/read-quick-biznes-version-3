class SingleProductdetails {
  bool success;
  List<SingleProduct> singleProduct;
  List<Ratings> ratings;

  SingleProductdetails({this.success, this.singleProduct, this.ratings});

  SingleProductdetails.fromJson(Map<String, dynamic> json) {
    success = json['Success'];
    if (json['singleProduct'] != null) {
      singleProduct = new List<SingleProduct>();
      json['singleProduct'].forEach((v) {
        singleProduct.add(new SingleProduct.fromJson(v));
      });
    }
    if (json['ratings'] != null) {
      ratings = new List<Ratings>();
      json['ratings'].forEach((v) {
        ratings.add(new Ratings.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Success'] = this.success;
    if (this.singleProduct != null) {
      data['singleProduct'] =
          this.singleProduct.map((v) => v.toJson()).toList();
    }
    if (this.ratings != null) {
      data['ratings'] = this.ratings.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class SingleProduct {
  String sId;
  String title;
  String quantityUnit;
  String description;
  int moq;
  List<String> images;
  int quantity;
  String organizationName;
  String price;
  Category category;
  SubCategory subCategory;
  String country;
  String city;
  int overAllRatings;
  CreatedBy createdBy;
  String createdAt;
  bool subscriptionStatus;
  bool canReview;

  SingleProduct(
      {this.sId,
      this.title,
        this.quantityUnit,
      this.description,
      this.moq,
      this.images,
      this.quantity,
      this.organizationName,
      this.price,
      this.category,
      this.subCategory,
      this.country,
      this.city,
      this.overAllRatings,
      this.createdBy,
      this.createdAt,
      this.subscriptionStatus,
      this.canReview});

  SingleProduct.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    title = json['title'];
    quantityUnit = json['quantityUnit'];
    description = json['description'];
    moq = json['moq'];
    images = json['images'].cast<String>();
    quantity = json['quantity'];
    organizationName = json['organizationName'];
    price = json['price'];
    category = json['category'] != null
        ? new Category.fromJson(json['category'])
        : null;
    subCategory = json['subCategory'] != null
        ? new SubCategory.fromJson(json['subCategory'])
        : null;
    country = json['country'];
    city = json['city'];
    overAllRatings = json['overAllRatings'];
    createdBy = json['createdBy'] != null
        ? new CreatedBy.fromJson(json['createdBy'])
        : null;
    createdAt = json['createdAt'];
    subscriptionStatus = json['subscriptionStatus'];
    canReview = json['canReview'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.sId;
    data['title'] = this.title;
    data['quantityUnit'] = this.quantityUnit;
    data['description'] = this.description;
    data['moq'] = this.moq;
    data['images'] = this.images;
    data['quantity'] = this.quantity;
    data['organizationName'] = this.organizationName;
    data['price'] = this.price;
    if (this.category != null) {
      data['category'] = this.category.toJson();
    }
    if (this.subCategory != null) {
      data['subCategory'] = this.subCategory.toJson();
    }
    data['country'] = this.country;
    data['city'] = this.city;
    data['overAllRatings'] = this.overAllRatings;
    if (this.createdBy != null) {
      data['createdBy'] = this.createdBy.toJson();
    }
    data['createdAt'] = this.createdAt;
    data['subscriptionStatus'] = this.subscriptionStatus;
    data['canReview'] = this.canReview;
    return data;
  }
}

class Category {
  String sId;
  String title;
  String createdAt;
  String updatedAt;
  int iV;

  Category({this.sId, this.title, this.createdAt, this.updatedAt, this.iV});

  Category.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    title = json['title'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    iV = json['__v'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.sId;
    data['title'] = this.title;
    data['createdAt'] = this.createdAt;
    data['updatedAt'] = this.updatedAt;
    data['__v'] = this.iV;
    return data;
  }
}

class SubCategory {
  String sId;
  String title;
  String category;
  String createdAt;
  String updatedAt;
  int iV;

  SubCategory(
      {this.sId,
      this.title,
      this.category,
      this.createdAt,
      this.updatedAt,
      this.iV});

  SubCategory.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    title = json['title'];
    category = json['category'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    iV = json['__v'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.sId;
    data['title'] = this.title;
    data['category'] = this.category;
    data['createdAt'] = this.createdAt;
    data['updatedAt'] = this.updatedAt;
    data['__v'] = this.iV;
    return data;
  }
}

class CreatedBy {
  String description;
  List<String> roleId;
  int userPinCode;
  int oTPCOde;
  bool status;
  bool onlineStatus;
  String profilePic;
  String sId;
  String fullName;
  String username;
  String organizationName;
  String email;
  int contactNumber;
  Category countryId;
  String mobileToken;
  CityId cityId;
  String roleName;
  String password;
  String createdAt;
  String updatedAt;
  int iV;
  String lastLogin;

  CreatedBy(
      {this.description,
      this.roleId,
      this.userPinCode,
      this.oTPCOde,
      this.status,
      this.onlineStatus,
      this.profilePic,
      this.sId,
      this.fullName,
      this.username,
      this.organizationName,
      this.email,
      this.contactNumber,
      this.countryId,
      this.mobileToken,
      this.cityId,
      this.roleName,
      this.password,
      this.createdAt,
      this.updatedAt,
      this.iV,
      this.lastLogin});

  CreatedBy.fromJson(Map<String, dynamic> json) {
    description = json['description'];
    roleId = json['roleId'].cast<String>();
    userPinCode = json['userPinCode'];
    oTPCOde = json['OTPCOde'];
    status = json['status'];
    onlineStatus = json['onlineStatus'];
    profilePic = json['profilePic'];
    sId = json['_id'];
    fullName = json['fullName'];
    username = json['username'];
    organizationName = json['organizationName'];
    email = json['email'];
    contactNumber = json['contactNumber'];
    countryId = json['countryId'] != null
        ? new Category.fromJson(json['countryId'])
        : null;
    mobileToken = json['mobileToken'];
    cityId =
        json['cityId'] != null ? new CityId.fromJson(json['cityId']) : null;
    roleName = json['roleName'];
    password = json['password'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    iV = json['__v'];
    lastLogin = json['lastLogin'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['description'] = this.description;
    data['roleId'] = this.roleId;
    data['userPinCode'] = this.userPinCode;
    data['OTPCOde'] = this.oTPCOde;
    data['status'] = this.status;
    data['onlineStatus'] = this.onlineStatus;
    data['profilePic'] = this.profilePic;
    data['_id'] = this.sId;
    data['fullName'] = this.fullName;
    data['username'] = this.username;
    data['organizationName'] = this.organizationName;
    data['email'] = this.email;
    data['contactNumber'] = this.contactNumber;
    if (this.countryId != null) {
      data['countryId'] = this.countryId.toJson();
    }
    data['mobileToken'] = this.mobileToken;
    if (this.cityId != null) {
      data['cityId'] = this.cityId.toJson();
    }
    data['roleName'] = this.roleName;
    data['password'] = this.password;
    data['createdAt'] = this.createdAt;
    data['updatedAt'] = this.updatedAt;
    data['__v'] = this.iV;
    data['lastLogin'] = this.lastLogin;
    return data;
  }
}

class CityId {
  String sId;
  String title;
  String countryId;
  String createdAt;
  String updatedAt;
  int iV;

  CityId(
      {this.sId,
      this.title,
      this.countryId,
      this.createdAt,
      this.updatedAt,
      this.iV});

  CityId.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    title = json['title'];
    countryId = json['countryId'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    iV = json['__v'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.sId;
    data['title'] = this.title;
    data['countryId'] = this.countryId;
    data['createdAt'] = this.createdAt;
    data['updatedAt'] = this.updatedAt;
    data['__v'] = this.iV;
    return data;
  }
}

class Ratings {
  String sId;
  CommentedBy commentedBy;
  String comments;
  String ratings;
  String productId;
  String createdAt;
  String updatedAt;
  int iV;

  Ratings(
      {this.sId,
      this.commentedBy,
      this.comments,
      this.ratings,
      this.productId,
      this.createdAt,
      this.updatedAt,
      this.iV});

  Ratings.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    commentedBy = json['commentedBy'] != null
        ? new CommentedBy.fromJson(json['commentedBy'])
        : null;
    comments = json['comments'];
    ratings = json['ratings'];
    productId = json['productId'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    iV = json['__v'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.sId;
    if (this.commentedBy != null) {
      data['commentedBy'] = this.commentedBy.toJson();
    }
    data['comments'] = this.comments;
    data['ratings'] = this.ratings;
    data['productId'] = this.productId;
    data['createdAt'] = this.createdAt;
    data['updatedAt'] = this.updatedAt;
    data['__v'] = this.iV;
    return data;
  }
}

class CommentedBy {
  String description;
  List<String> roleId;
  int userPinCode;
  int oTPCOde;
  bool status;
  bool onlineStatus;
  String profilePic;
  String sId;
  String fullName;
  String username;
  String organizationName;
  String email;
  int contactNumber;
  String countryId;
  String mobileToken;
  String cityId;
  String roleName;
  String lastLogin;
  String password;
  String createdAt;
  String updatedAt;
  int iV;

  CommentedBy(
      {this.description,
      this.roleId,
      this.userPinCode,
      this.oTPCOde,
      this.status,
      this.onlineStatus,
      this.profilePic,
      this.sId,
      this.fullName,
      this.username,
      this.organizationName,
      this.email,
      this.contactNumber,
      this.countryId,
      this.mobileToken,
      this.cityId,
      this.roleName,
      this.lastLogin,
      this.password,
      this.createdAt,
      this.updatedAt,
      this.iV});

  CommentedBy.fromJson(Map<String, dynamic> json) {
    description = json['description'];
    roleId = json['roleId'].cast<String>();
    userPinCode = json['userPinCode'];
    oTPCOde = json['OTPCOde'];
    status = json['status'];
    onlineStatus = json['onlineStatus'];
    profilePic = json['profilePic'];
    sId = json['_id'];
    fullName = json['fullName'];
    username = json['username'];
    organizationName = json['organizationName'];
    email = json['email'];
    contactNumber = json['contactNumber'];
    countryId = json['countryId'];
    mobileToken = json['mobileToken'];
    cityId = json['cityId'];
    roleName = json['roleName'];
    lastLogin = json['lastLogin'];
    password = json['password'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    iV = json['__v'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['description'] = this.description;
    data['roleId'] = this.roleId;
    data['userPinCode'] = this.userPinCode;
    data['OTPCOde'] = this.oTPCOde;
    data['status'] = this.status;
    data['onlineStatus'] = this.onlineStatus;
    data['profilePic'] = this.profilePic;
    data['_id'] = this.sId;
    data['fullName'] = this.fullName;
    data['username'] = this.username;
    data['organizationName'] = this.organizationName;
    data['email'] = this.email;
    data['contactNumber'] = this.contactNumber;
    data['countryId'] = this.countryId;
    data['mobileToken'] = this.mobileToken;
    data['cityId'] = this.cityId;
    data['roleName'] = this.roleName;
    data['lastLogin'] = this.lastLogin;
    data['password'] = this.password;
    data['createdAt'] = this.createdAt;
    data['updatedAt'] = this.updatedAt;
    data['__v'] = this.iV;
    return data;
  }
}