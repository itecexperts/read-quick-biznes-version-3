import 'dart:convert';
MessagesModel chatMessageModelFromJson(String str) =>
    MessagesModel.fromJson(json.decode(str));

String chatMessageModelToJson(MessagesModel data) =>
    json.encode(data.tojson());
class MessagesModel{
  String username;
  String conversationId;
  MessagesModel({this.username,this.conversationId});
  factory MessagesModel.fromJson(Map<String,dynamic> json)=>
  MessagesModel(
    username:json['userName'],
    conversationId:json['conversationId'],
  );
  
  Map<String,dynamic> tojson()=>{
  'userName':this.username,
  'conversationId':this.conversationId,
};
}