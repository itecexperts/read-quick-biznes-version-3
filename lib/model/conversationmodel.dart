import 'package:hive/hive.dart';
part 'conversationmodel.g.dart';

@HiveType(typeId: 1, adapterName: 'conversationsGeneration')
class NewMessages {
  @HiveField(0)
  String status;
  @HiveField(1)
  bool isselectedchat = false;
  @HiveField(2)
  String attachment;
  @HiveField(3)
  String sId;
  @HiveField(4)
  String message;
  @HiveField(5)
  CreatedBy createdFor;
  @HiveField(6)
  CreatedBy createdBy;
  @HiveField(7)
  CategoryId categoryId;
  @HiveField(8)
  CategoryId subCategoryId;
  @HiveField(9)
  Conversation conversation;
  @HiveField(10)
  String createdAt;
  @HiveField(11)
  String updatedAt;
  @HiveField(12)
  String offer;

  NewMessages(
      {this.status,
      this.attachment,
      this.sId,
      this.message,
      this.createdFor,
      this.createdBy,
      this.categoryId,
      this.subCategoryId,
      this.conversation,
      this.createdAt,
      this.updatedAt,
      this.offer});

  NewMessages.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    attachment = json['attachment'];
    sId = json['_id'];
    message = json['message'];
    createdFor = json['createdFor'] != null
        ? new CreatedBy.fromJson(json['createdFor'])
        : null;
    createdBy = json['createdBy'] != null
        ? new CreatedBy.fromJson(json['createdBy'])
        : null;
    categoryId = json['categoryId'] != null
        ? new CategoryId.fromJson(json['categoryId'])
        : null;
    subCategoryId = json['subCategoryId'] != null
        ? new CategoryId.fromJson(json['subCategoryId'])
        : null;
    conversation = json['conversation'] != null
        ? new Conversation.fromJson(json['conversation'])
        : null;
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    offer = json['offer'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['attachment'] = this.attachment;
    data['_id'] = this.sId;
    data['message'] = this.message;
    if (this.createdFor != null) {
      data['createdFor'] = this.createdFor.toJson();
    }
    if (this.createdBy != null) {
      data['createdBy'] = this.createdBy.toJson();
    }
    if (this.categoryId != null) {
      data['categoryId'] = this.categoryId.toJson();
    }
    if (this.subCategoryId != null) {
      data['subCategoryId'] = this.subCategoryId.toJson();
    }
    if (this.conversation != null) {
      data['conversation'] = this.conversation.toJson();
    }
    data['createdAt'] = this.createdAt;
    data['updatedAt'] = this.updatedAt;
    data['offer'] = this.offer;
    return data;
  }
}

@HiveType(typeId: 2, adapterName: 'conversationsGeneration2')
class CreatedBy {
  @HiveField(0)
  bool onlineStatus;
  @HiveField(1)
  String profilePic;
  @HiveField(2)
  String sId;
  @HiveField(3)
  String fullName;
  @HiveField(4)
  String username;
  @HiveField(5)
  String email;

  CreatedBy(
      {this.onlineStatus,
      this.profilePic,
      this.sId,
      this.fullName,
      this.username,
      this.email});

  CreatedBy.fromJson(Map<String, dynamic> json) {
    onlineStatus = json['onlineStatus'];
    profilePic = json['profilePic'];
    sId = json['_id'];
    fullName = json['fullName'];
    username = json['username'];
    email = json['email'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['onlineStatus'] = this.onlineStatus;
    data['profilePic'] = this.profilePic;
    data['_id'] = this.sId;
    data['fullName'] = this.fullName;
    data['username'] = this.username;
    data['email'] = this.email;
    return data;
  }
}

@HiveType(typeId: 3, adapterName: 'conversationsGeneration3')
class CategoryId {
  @HiveField(0)
  String sId;
  @HiveField(1)
  String title;

  CategoryId({this.sId, this.title});

  CategoryId.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    title = json['title'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.sId;
    data['title'] = this.title;
    return data;
  }
}

@HiveType(typeId: 4, adapterName: 'conversationsGeneration4')
class RoleId {
  @HiveField(0)
  String sId;
  @HiveField(1)
  String title;

  RoleId({this.sId, this.title});

  RoleId.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    title = json['title'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.sId;
    data['title'] = this.title;
    return data;
  }
}

@HiveType(typeId: 5, adapterName: 'conversationsGeneration5')
class Conversation {
  @HiveField(0)
  bool conBlock;
  @HiveField(1)
  String blockedBy;
  @HiveField(2)
  String blockedTo;
  @HiveField(3)
  String sId;

  Conversation({this.conBlock, this.blockedBy, this.blockedTo, this.sId});

  Conversation.fromJson(Map<String, dynamic> json) {
    conBlock = json['conBlock'];
    blockedBy = json['blockedBy'];
    blockedTo = json['blockedTo'];
    sId = json['_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['conBlock'] = this.conBlock;
    data['blockedBy'] = this.blockedBy;
    data['blockedTo'] = this.blockedTo;
    data['_id'] = this.sId;
    return data;
  }
}
