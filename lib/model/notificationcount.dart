class NotificationCount {
  int notifications;

  NotificationCount({this.notifications});

  NotificationCount.fromJson(Map<String, dynamic> json) {
    notifications = json['notifications'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['notifications'] = this.notifications;
    return data;
  }
}