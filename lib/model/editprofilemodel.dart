class EditProfileModel{
  String organizationName;
  String userId;
  String countryId;
  String city;
  String profilePic;
  String description;
  String email;
  String username;
  List<String> addingrole;
  EditProfileModel({this.organizationName,this.userId,this.countryId,this.city,this.profilePic,this.description,
  this.email,this.username,this.addingrole,
  });
  EditProfileModel.fromJson(Map<String,dynamic> json){
  organizationName=json['organizationName'];
  userId=json['userId'];
  countryId=json['countryId'];
  city=json['cityId'];
  email=json['email'];
  username=json['username'];
  addingrole=json['role'].cast<String>();
  }
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
  data['organizationName']=this.organizationName;
  data['userId']=this.userId;
  data['countryId']=this.countryId;
  data['cityId']=this.city;
  data['email']=this.email;
  data['username']=this.username;
  data['role']=this.addingrole;
  return data;
}
}