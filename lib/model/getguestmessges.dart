class GuestMessages {
  String status;
  String sId;
  String brokerId;
  CategoryId categoryId;
  SubCategoryId subCategoryId;
  CategoryId countryId;
  CityId cityId;
  String message;
  String createdAt;
  String updatedAt;
  int iV;

  GuestMessages(
      {this.status,
      this.sId,
      this.brokerId,
      this.categoryId,
      this.subCategoryId,
      this.countryId,
      this.cityId,
      this.message,
      this.createdAt,
      this.updatedAt,
      this.iV});

  GuestMessages.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    sId = json['_id'];
    brokerId = json['brokerId'];
    categoryId = json['categoryId'] != null
        ? new CategoryId.fromJson(json['categoryId'])
        : null;
    subCategoryId = json['subCategoryId'] != null
        ? new SubCategoryId.fromJson(json['subCategoryId'])
        : null;
    countryId = json['countryId'] != null
        ? new CategoryId.fromJson(json['countryId'])
        : null;
    cityId =
        json['cityId'] != null ? new CityId.fromJson(json['cityId']) : null;
    message = json['message'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    iV = json['__v'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['_id'] = this.sId;
    data['brokerId'] = this.brokerId;
    if (this.categoryId != null) {
      data['categoryId'] = this.categoryId.toJson();
    }
    if (this.subCategoryId != null) {
      data['subCategoryId'] = this.subCategoryId.toJson();
    }
    if (this.countryId != null) {
      data['countryId'] = this.countryId.toJson();
    }
    if (this.cityId != null) {
      data['cityId'] = this.cityId.toJson();
    }
    data['message'] = this.message;
    data['createdAt'] = this.createdAt;
    data['updatedAt'] = this.updatedAt;
    data['__v'] = this.iV;
    return data;
  }
}

class CategoryId {
  String sId;
  String title;
  String createdAt;
  String updatedAt;
  int iV;

  CategoryId({this.sId, this.title, this.createdAt, this.updatedAt, this.iV});

  CategoryId.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    title = json['title'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    iV = json['__v'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.sId;
    data['title'] = this.title;
    data['createdAt'] = this.createdAt;
    data['updatedAt'] = this.updatedAt;
    data['__v'] = this.iV;
    return data;
  }
}

class SubCategoryId {
  String sId;
  String title;
  String category;
  String createdAt;
  String updatedAt;
  int iV;

  SubCategoryId(
      {this.sId,
      this.title,
      this.category,
      this.createdAt,
      this.updatedAt,
      this.iV});

  SubCategoryId.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    title = json['title'];
    category = json['category'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    iV = json['__v'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.sId;
    data['title'] = this.title;
    data['category'] = this.category;
    data['createdAt'] = this.createdAt;
    data['updatedAt'] = this.updatedAt;
    data['__v'] = this.iV;
    return data;
  }
}

class CityId {
  String sId;
  String title;
  String countryId;
  String createdAt;
  String updatedAt;
  int iV;

  CityId(
      {this.sId,
      this.title,
      this.countryId,
      this.createdAt,
      this.updatedAt,
      this.iV});

  CityId.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    title = json['title'];
    countryId = json['countryId'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    iV = json['__v'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.sId;
    data['title'] = this.title;
    data['countryId'] = this.countryId;
    data['createdAt'] = this.createdAt;
    data['updatedAt'] = this.updatedAt;
    data['__v'] = this.iV;
    return data;
  }
}
class GuestMessageModel {
  bool success;
  List<GuestMessages> guestMessages;

  GuestMessageModel({this.success, this.guestMessages});

  GuestMessageModel.fromJson(Map<String, dynamic> json) {
    success = json['Success'];
    if (json['guestMessages'] != null) {
      guestMessages = new List<GuestMessages>();
      json['guestMessages'].forEach((v) {
        guestMessages.add(new GuestMessages.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Success'] = this.success;
    if (this.guestMessages != null) {
      data['guestMessages'] =
          this.guestMessages.map((v) => v.toJson()).toList();
    }
    return data;
  }
}