class CategoryInbox {
  bool success;
  List<AllData> allMessages;
  List<SellerArray> buyerArray;

  CategoryInbox({this.success, this.allMessages, this.buyerArray});

  CategoryInbox.fromJson(Map<String, dynamic> json) {
    success = json['success'];
    if (json['allMessages'] != null) {
      allMessages = new List<AllData>();
      json['allMessages'].forEach((v) {
        allMessages.add(new AllData.fromJson(v));
      });
    }
    if (json['buyerArray'] != null) {
      buyerArray = new List<SellerArray>();
      json['buyerArray'].forEach((v) {
        buyerArray.add(new SellerArray.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this.success;
    if (this.allMessages != null) {
      data['allMessages'] = this.allMessages.map((v) => v.toJson()).toList();
    }
    if (this.buyerArray != null) {
      data['buyerArray'] = this.buyerArray.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class AllData {
  String id;
  String title;
  String conversationId;
  String createdBy;
  String userType;
  int totalCount;

  AllData(
      {this.id,
      this.title,
      this.conversationId,
      this.createdBy,
      this.userType,
      this.totalCount});

  AllData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    conversationId = json['conversationId'];
    createdBy = json['createdBy'];
    userType = json['userType'];
    totalCount = json['totalCount'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['title'] = this.title;
    data['conversationId'] = this.conversationId;
    data['createdBy'] = this.createdBy;
    data['userType'] = this.userType;
    data['totalCount'] = this.totalCount;
    return data;
  }
}

class SellerArray {
  String id;
  String title;
  String conversationId;
  String createdBy;
  String userType;
  int totalCount;

  SellerArray(
      {this.id,
      this.title,
      this.conversationId,
      this.createdBy,
      this.userType,
      this.totalCount});

  SellerArray.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    conversationId = json['conversationId'];
    createdBy = json['createdBy'];
    userType = json['userType'];
    totalCount = json['totalCount'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['title'] = this.title;
    data['conversationId'] = this.conversationId;
    data['createdBy'] = this.createdBy;
    data['userType'] = this.userType;
    data['totalCount'] = this.totalCount;
    return data;
  }
}
