class CompanyProducts {
  int quantity;
  int moq;
  int ratings;
  List<String> images;
  String sId;
  String title;
  String quantityUnit;
  String description;
  String organizationName;
  String price;
  Category category;
  SubCategory subCategory;
  Category country;
  City city;
  String createdBy;
  String createdAt;
  String updatedAt;
  int iV;

  CompanyProducts(
      {this.quantity,
      this.moq,
      this.ratings,
      this.images,
      this.sId,
      this.title,
      this.quantityUnit,
      this.description,
      this.organizationName,
      this.price,
      this.category,
      this.subCategory,
      this.country,
      this.city,
      this.createdBy,
      this.createdAt,
      this.updatedAt,
      this.iV});

  CompanyProducts.fromJson(Map<String, dynamic> json) {
    quantity = json['quantity'];
    moq = json['moq'];
    ratings = json['ratings'];
    images = json['images'].cast<String>();
    sId = json['_id'];
    title = json['title'];
    quantityUnit = json['quantityUnit'];
    description = json['description'];
    organizationName = json['organizationName'];
    price = json['price'];
    category = json['category'] != null
        ? new Category.fromJson(json['category'])
        : null;
    subCategory = json['subCategory'] != null
        ? new SubCategory.fromJson(json['subCategory'])
        : null;
    country =
        json['country'] != null ? new Category.fromJson(json['country']) : null;
    city = json['city'] != null ? new City.fromJson(json['city']) : null;
    createdBy = json['createdBy'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    iV = json['__v'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['quantity'] = this.quantity;
    data['moq'] = this.moq;
    data['ratings'] = this.ratings;
    data['images'] = this.images;
    data['_id'] = this.sId;
    data['title'] = this.title;
    data['description'] = this.description;
    data['quantityUnit'] = this.quantityUnit;
    data['organizationName'] = this.organizationName;
    data['price'] = this.price;
    if (this.category != null) {
      data['category'] = this.category.toJson();
    }
    if (this.subCategory != null) {
      data['subCategory'] = this.subCategory.toJson();
    }
    if (this.country != null) {
      data['country'] = this.country.toJson();
    }
    if (this.city != null) {
      data['city'] = this.city.toJson();
    }
    data['createdBy'] = this.createdBy;
    data['createdAt'] = this.createdAt;
    data['updatedAt'] = this.updatedAt;
    data['__v'] = this.iV;
    return data;
  }
}

class Category {
  String sId;
  String title;
  String createdAt;
  String updatedAt;
  int iV;

  Category({this.sId, this.title, this.createdAt, this.updatedAt, this.iV});

  Category.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    title = json['title'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    iV = json['__v'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.sId;
    data['title'] = this.title;
    data['createdAt'] = this.createdAt;
    data['updatedAt'] = this.updatedAt;
    data['__v'] = this.iV;
    return data;
  }
}

class SubCategory {
  String sId;
  String title;
  String category;
  String createdAt;
  String updatedAt;
  int iV;

  SubCategory(
      {this.sId,
      this.title,
      this.category,
      this.createdAt,
      this.updatedAt,
      this.iV});

  SubCategory.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    title = json['title'];
    category = json['category'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    iV = json['__v'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.sId;
    data['title'] = this.title;
    data['category'] = this.category;
    data['createdAt'] = this.createdAt;
    data['updatedAt'] = this.updatedAt;
    data['__v'] = this.iV;
    return data;
  }
}

class City {
  String sId;
  String title;
  String countryId;
  String createdAt;
  String updatedAt;
  int iV;

  City(
      {this.sId,
      this.title,
      this.countryId,
      this.createdAt,
      this.updatedAt,
      this.iV});

  City.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    title = json['title'];
    countryId = json['countryId'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    iV = json['__v'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.sId;
    data['title'] = this.title;
    data['countryId'] = this.countryId;
    data['createdAt'] = this.createdAt;
    data['updatedAt'] = this.updatedAt;
    data['__v'] = this.iV;
    return data;
  }
}