import 'package:flutter/material.dart';
import 'package:biznes/res/size.dart';
import 'package:biznes/res/color.dart' as fontandcolor;
TermsAndConditionText({context,String heading,String subheading,TextAlign textAlign,double left,double right}){
  return Column(
    mainAxisAlignment: MainAxisAlignment.start,
    children: [
      Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(
          children: [
            Expanded(
              child: Text(
                "$heading",
                textAlign: TextAlign.left,
                maxLines: 2,
                style: TextStyle(

                  fontSize:size.convert(context,17),
                  fontFamily: fontandcolor.fontfaimly,
                ),
              ),
            ),
          ],
        ),
      ),
      Padding(
        padding:  EdgeInsets.only(left:left??4.0,right: right??4.0),
        child: Text(
          "$subheading",
          textAlign: textAlign,
          style: TextStyle(
            fontSize:size.convert(context,14),
            fontFamily: fontandcolor.fontfaimlyregular,
          ),
        ),
      ),
    ],
  );
}