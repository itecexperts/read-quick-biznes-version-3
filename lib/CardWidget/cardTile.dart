import 'package:biznes/res/color.dart' as font;
import 'package:biznes/res/size.dart';
import 'package:biznes/services/server.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';
import '../size_config.dart';

Card CardTile(
    {Function onTap,
    String image,
    String title,
    String ratingtext,
    String ratings,
    String price,
    String quantity,
    String moq,
    String country,
    List userRole,
    Widget checkbox,
    String quantityUnit,
    context}) {
  return Card(
    child: Container(
      decoration: BoxDecoration(
        color: Color(0xffffffff),
      ),
      width: double.infinity,
      // height: (125 / 8.148314082864863) * SizeConfig.heightMultiplier,
      margin: EdgeInsets.symmetric(vertical: 2, horizontal: 8),
      child: InkWell(
        onTap: onTap,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              width: (100 / 4.853932272197492) * SizeConfig.widthMultiplier,
              height: (100 / 8.148314082864863) * SizeConfig.heightMultiplier,
              margin: EdgeInsets.only(
                  right: 13, top: 1.0 * SizeConfig.heightMultiplier),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Hero(
                  tag: 'SearchImage',
                  child: CachedNetworkImage(
                    fit: BoxFit.cover,
                    imageUrl: '$base_url/$image' ?? "image",
                    placeholder: (context, url) =>
                        Center(child: CircularProgressIndicator()),
                    errorWidget: (context, url, error) => Icon(Icons.error),
                  ),
                ),
              ),
            ),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    '$title',
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                      fontWeight: FontWeight.w400,
                      color: Color(0xff394C81),
                      fontFamily: font.fontfaimlyquicksandbold,
                      fontSize: (15 / 8.148314082864863) *
                          SizeConfig.heightMultiplier,
                    ),
                  ),
                  SizedBox(
                    height:
                        (2 / 8.148314082864863) * SizeConfig.heightMultiplier,
                  ),
                  SizedBox(
                    height:
                        (5 / 8.148314082864863) * SizeConfig.heightMultiplier,
                  ),
                  Text("Price $price",
                      style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                          fontFamily: font.fontfaimlyregular,
                          fontSize: 1.4 * SizeConfig.textMultiplier,
                          letterSpacing: .3)),
                  Text(
                      "Quantity : $quantity (${quantityUnit ?? "not mention"})",
                      style: TextStyle(
                          color: Colors.black,
                          fontFamily: font.fontfaimlyregular,
                          fontSize: 1.4 * SizeConfig.textMultiplier,
                          letterSpacing: .3)),
                  Text("M.O.Q : $moq (${quantityUnit ?? "not mention"})",
                      style: TextStyle(
                          fontFamily: font.fontfaimlyregular,
                          color: Colors.black,
                          fontSize: 1.4 * SizeConfig.textMultiplier,
                          letterSpacing: .3)),
                  Row(
                    children: <Widget>[
                      Text('$country',
                          style: TextStyle(
                              fontFamily: font.fontfaimly,
                              color: Colors.green,
                              fontSize: 1.2 * SizeConfig.textMultiplier,
                              letterSpacing: .3)),
                      SizedBox(
                        width: (5 / 4.853932272197492) *
                            SizeConfig.widthMultiplier,
                      ),
                      Image.asset(
                        'assets/images/flag2.png',
                        height: 12,
                        width: 15,
                      ),
                    ],
                  ),
                  Container(
                    alignment: Alignment.bottomLeft,
                    width: size.convertWidth(context, 230),
                    child: Row(
                      children: [
                        Text("Role: ",
                            overflow: TextOverflow.clip,
                            textAlign: TextAlign.left,
                            style: TextStyle(
                              fontWeight: FontWeight.w400,
                              color: Color(0xff394C81),
                              fontFamily: font.fontfaimly,
                              fontSize: size.convert(context, 7),
                            )),
                        Wrap(
                          children: userRole
                              .map((role) => Text("${role.title.toString()} ",
                                  textAlign: TextAlign.left,
                                  overflow: TextOverflow.clip,
                                  style: TextStyle(
                                    letterSpacing: .3,
                                    fontWeight: FontWeight.w400,
                                    fontFamily: font.fontfaimly,
                                    color: Color(0xff394C81),
                                    fontSize: size.convert(context, 7),
                                  )))
                              .toList(),
                        ),
                      ],
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        "${ratingtext.toString()}.0",
                        style: TextStyle(
                          color: Color(0xff909090),
                          fontSize: 1.2 * SizeConfig.textMultiplier,
                          fontFamily: font.fontfaimly,
                        ),
                      ),
                      Center(
                          child: SmoothStarRating(
                        isReadOnly: true,
                        borderColor: Color(0xffffb900),
                        rating: ratings == null
                            ? 0.0
                            : double.parse(ratings.toString()),
                        size: (20 / 8.148314082864863) *
                            SizeConfig.heightMultiplier,
                        color: Color(0xffffb900),
                        filledIconData: Icons.star,
                        defaultIconData: Icons.star_border,
                        starCount: 5,
                        allowHalfRating: true,
                        spacing: 0.5,
                      )),
                    ],
                  ),
                ],
              ),
            ),
            Container(
              alignment: Alignment.centerRight,
              child: checkbox,
            ),
          ],
        ),
      ),
    ),
  );
}
