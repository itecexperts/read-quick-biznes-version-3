import 'package:biznes/res/size.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:biznes/res/color.dart';
import 'package:biznes/res/style.dart';
class CustomTextField extends StatelessWidget {
  FocusNode _focusNode;
  Color color1;
  IconData iconWidget;
  Function onchange;
  String hints;
  Widget trailingIcon;
  double borderwidth;
  TextInputType textInputType;
  TextEditingController textEditingController;
  bool obscureText;
  bool isEnable;
  double fontSize;
  CustomTextField({this.fontSize,this.isEnable=true,this.obscureText,this.textInputType,this.borderwidth,this.trailingIcon,this.hints,this.color1, this.iconWidget,this.textEditingController});
  @override
  Widget build(BuildContext context) {
    return Material(
      shadowColor: Color(0xffd4d4d4),
      elevation: 5,
      borderRadius: BorderRadius.circular(25),
      child: Container(
        decoration: BoxDecoration(
          border: Border.all(
            color: color1 ?? borderColor.withOpacity(0.50),
          ),
          borderRadius: BorderRadius.circular(25),
        ),
        child: Row(
          children: <Widget>[
            iconWidget == null ? SizedBox(width: 0,) :SizedBox(width: 10,),
             iconWidget == null ? Container() : Icon(iconWidget,size: size.convert(context,17),color: Colors.grey,),
            iconWidget == null ? SizedBox(width: 10,) :SizedBox(width: 10,),
            Expanded(
              child: TextFormField(
                focusNode: _focusNode,
                enabled: isEnable,
                obscureText: obscureText ?? false,
                keyboardType: textInputType == null  ? TextInputType.text: textInputType,
                controller: textEditingController,
                style: TextStyle(
                  fontSize: fontSize?? 16,
                  fontFamily: "LatoRegular",
                  color: Colors.black
                ),
                decoration: InputDecoration(
                  focusColor: color1,
                  disabledBorder: InputBorder.none,
                  border: InputBorder.none,
                  hintText: hints==null ? "": hints,
                  hintStyle: style.QuicksandRegular(
                    color: Colors.black.withOpacity(0.50),
                    fontSize: size.convert(context, 14)
                  )
                ),
              ),
            ),
            trailingIcon == null ? Container() : trailingIcon,
          ],
        ),
      ),
    );
  }
}
