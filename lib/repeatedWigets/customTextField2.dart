import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:biznes/res/style.dart';
import 'package:biznes/res/size.dart';
import 'package:biznes/res/color.dart';

class CustomTextFieldSecond extends StatelessWidget {
  TextAlign textalign;
  num colorCode;
  Color color1;
  int maxLine;
  Widget iconWidget;
  Function onchange;
  Function onSubmit;
  Function focusChange;
  String hints;
  Widget trailingIcon;
  double borderwidth;
  TextInputType textInputType;
  TextEditingController textEditingController;
  bool obscureText;
  bool isEnable;
  double fontSize;
  int maxLength;
  bool isborder = false;
  CustomTextFieldSecond(
      {this.fontSize,
      this.isEnable = true,
      this.obscureText,
      this.textInputType,
      this.borderwidth,
      this.trailingIcon,
      this.hints,
      this.color1,
      this.iconWidget,
      this.textEditingController,
      this.colorCode,
      this.onSubmit,
      this.focusChange,
      this.textalign = TextAlign.start,
      this.maxLength,
      this.onchange,
      this.isborder = false,
      this.maxLine});
  @override
  Widget build(BuildContext context) {
    return Container(
      // padding: EdgeInsets.only(right: 5, left: 5),
      decoration: BoxDecoration(
        border: Border(
            bottom: isborder
                ? BorderSide.none
                : BorderSide(
                    width: borderwidth ?? 2,
                    color: color1 ?? borderColor.withOpacity(0.50))),
//        borderRadius: BorderRadius.circular(5)
      ),
      child: Row(
        children: <Widget>[
          iconWidget == null ? Container() : iconWidget,
          iconWidget == null
              ? SizedBox(
                  width: 0,
                )
              : SizedBox(
                  width: 10,
                ),
          Expanded(
            child: Focus(
              onFocusChange: focusChange ?? (val) {},
              child: TextFormField(
                onChanged: onchange ?? (val) {},

                // onEditingComplete: (){
                //   print("competer");
                // },
                maxLength: maxLength ?? 1000,
                maxLines: maxLine ?? 1,
                textAlign: textalign,
                onFieldSubmitted: onSubmit ?? (val) {},
                enabled: isEnable,
                obscureText: obscureText ?? false,
                keyboardType:
                    textInputType == null ? TextInputType.text : textInputType,
                controller: textEditingController,
                style: TextStyle(
                    fontSize: fontSize ?? 16,
                    fontFamily: "LatoRegular",
                    color: Colors.black),
                decoration: InputDecoration(
                  disabledBorder: InputBorder.none,
                  border: InputBorder.none,
                  hintText: hints == null ? "" : hints,
                  hintStyle: style.QuicksandRegular(
                      color: isborder
                          ? Colors.white
                          : Colors.black.withOpacity(0.50),
                      fontSize: size.convert(context, 14)),
                  counterText: "",
                ),
                //autofocus: true,
//              style: TextStyle(
//
//              ),
              ),
            ),
          ),
          trailingIcon == null ? Container() : trailingIcon,
        ],
      ),
    );
  }
}
