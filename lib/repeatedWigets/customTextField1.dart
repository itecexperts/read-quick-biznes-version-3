import 'package:flutter/material.dart';
import 'package:biznes/res/color.dart';
import 'package:biznes/res/style.dart';
import 'package:biznes/res/size.dart';
class customTextField1 extends StatefulWidget {
  Color color1;
  Widget iconWidget;
  Function onchange;
  String hints;
  Widget trailingIcon ;
  double borderwidth;
  double height;
  TextInputType textInputType;
  TextEditingController textEditingController;
  bool obscureText;
  bool isEnable;
  double fontSize;
  customTextField1({this.fontSize,this.isEnable=true,this.obscureText,this.textInputType,this.borderwidth,this.trailingIcon,this.hints,this.color1, this.iconWidget,this.textEditingController,this.height});
  @override
  _customTextField1State createState() => _customTextField1State();
}

class _customTextField1State extends State<customTextField1> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: widget.height ?? size.convert(context, 42),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(size.convert(context, 30)),
        color: whiteColor,
        boxShadow: [
          BoxShadow(
            color: Color(0xff29000000),
            blurRadius: 6,
            offset: Offset(0,3),
          )
        ]
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          widget.iconWidget == null ? Container() : widget.iconWidget,
          //widget.iconWidget == null ? SizedBox(width: 0,) :SizedBox(width: 10,),
          Expanded(
            child: Container(
              //color: Colors.amberAccent,
              alignment: Alignment.centerLeft,
              child: TextFormField(
                enabled: widget.isEnable,
                obscureText: widget.obscureText ?? false,
                keyboardType: widget.textInputType == null  ? TextInputType.text: widget.textInputType,
                controller: widget.textEditingController,
                style: style.PoppinsRegular(
                  fontSize: size.convert(context, 15),
                ),
                decoration: InputDecoration(
                    disabledBorder: InputBorder.none,
                    border: InputBorder.none,
                    hintText: widget.hints==null ? "": widget.hints,
                    hintStyle: style.PoppinsRegular(
                        color: Colors.black.withOpacity(0.50),
                        fontSize: size.convert(context, 15)
                    )
                ),
                //autofocus: true,
//              style: TextStyle(
//
//              ),

              ),
            ),
          ),
          widget.trailingIcon == null ? Container() : widget.trailingIcon,
        ],
      ),
    );
  }
}
