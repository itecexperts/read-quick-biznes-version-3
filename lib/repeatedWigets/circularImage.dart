import 'dart:io';

import 'package:biznes/res/color.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:biznes/res/size.dart';

class circularImage extends StatelessWidget {

  String imageUrl;
  double h;
  double w;
  double borderRadius;
  bool assetImage;
  bool assetSvg;
  bool isCircleImage;
  bool fileImage;
  File file;

  circularImage(
      {this.file,
      this.imageUrl,
      this.fileImage,
      this.h,
      this.w,
      this.assetImage = false,
        this.isCircleImage = true,
        this.assetSvg = false,
        this.borderRadius
      });

  @override
  Widget build(BuildContext context) {
    return Container(
      height: h == null ? 24 : h,
      width: w == null ? 24 : w,
      decoration: BoxDecoration(
          color: appColor,
          shape: isCircleImage ? BoxShape.circle : BoxShape.rectangle,
          borderRadius: isCircleImage ? null : BorderRadius.circular(borderRadius??13),
          image: imageUrl == null
              ? null
              : DecorationImage(
                  image: fileImage??false
                      ? FileImage(file)
                      : assetImage
                          ? AssetImage(imageUrl)
                          : NetworkImage(imageUrl),
                  fit: BoxFit.cover)),
    );
  }
}

class circularSvgImage extends StatelessWidget {
  String imageUrl;
  double h;
  double w;
  bool isCircleImage;
  Color color;

  circularSvgImage({this.imageUrl, this.h, this.w,this.isCircleImage = true,this.color});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: h == null ? 24 : h,
      width: w == null ? 24 : w,

      decoration: BoxDecoration(
          color: color??appColor,
        shape: isCircleImage ? BoxShape.circle : BoxShape.rectangle,
        borderRadius: isCircleImage ? null : BorderRadius.circular(20),
      ),
        child: Center(child: SvgPicture.asset(imageUrl,
          width: size.convert(context, 20),
          height: size.convert(context, 20),
        )),

    );
  }
}
