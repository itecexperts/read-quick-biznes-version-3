import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:biznes/res/size.dart';
class centerButton extends StatelessWidget {
  Function onFloatingPress;
  centerButton({this.onFloatingPress});
  @override
  Widget build(BuildContext context) {
    return Container(
      child: GestureDetector(
        onTap: () {
          print("floatingActionButton");
          onFloatingPress();
        },
        child: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: [Color(0xff6984ff),Color(0xff7b65d6)],
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter
            ),
            shape: BoxShape.circle
          ),
          //padding: EdgeInsets.only(right: 10),
          height: size.convert(context, 60),
          width: size.convert(context, 60),
          child: Center(child: SvgPicture.asset("assets/icons/bottomBarIcons/plusIcon.svg")),
        ),
      ),
    );
  }
}
