import 'package:biznes/res/color.dart';
import 'package:flutter/material.dart';
import 'package:biznes/res/size.dart';

class ChatCard extends StatelessWidget {
  final String title;
  final int count;
  final String id;
  final bool isShowBuyerSeller;
  final String buyerSeller;
  ChatCard(
      {this.title,
      this.count,
      this.id,
      this.isShowBuyerSeller = false,
      this.buyerSeller});
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.only(
                left: size.convert(context, 10),
                right: size.convert(context, 10)),
            margin: EdgeInsets.only(
                left: size.convert(context, 10),
                right: size.convert(context, 10)),
            height: size.convert(context, 87),
            width: size.convert(context, 390),
            decoration: BoxDecoration(
              boxShadow: [
                BoxShadow(
                  color: shadow,
                  offset: Offset(0, 3),
                  blurRadius: 6,
                ),
              ],
              color: Color(0xffffffff),
              borderRadius: BorderRadius.circular(19),
            ),
            child: Row(
              children: [
                Container(
                  height: size.convert(context, 52),
                  width: size.convert(context, 52),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    gradient: LinearGradient(
                      colors: [Color(0xff69bef7), Color(0xff7595f4)],
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                    ),
                  ),
                  child: Icon(
                    Icons.message,
                    size: size.convert(context, 17),
                    color: Colors.white,
                  ),
                ),
                SizedBox(
                  width: size.convert(context, 10),
                ),
                Expanded(
                  flex: 8,
                  child: Row(
                    children: [
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            title ?? "",
                            style: TextStyle(
                              fontFamily: fontfaimly,
                              color: appbarfontandiconcolor,
                              fontWeight: FontWeight.w600,
                              fontSize: size.convert(context, 16),
                            ),
                          ),
                          Text(
                            "${count ?? "0"} Messages has been Recieved",
                            style: TextStyle(
                              fontFamily: fontfaimlyregular,
                              fontSize: size.convert(context, 10),
                            ),
                          ),
                          isShowBuyerSeller
                              ? Text(
                                  buyerSeller ?? "",
                                  style: TextStyle(
                                    fontFamily: fontfaimly,
                                    color: appbarfontandiconcolor,
                                    fontSize: size.convert(context, 8),
                                  ),
                                )
                              : Container(),
                        ],
                      ),
                    ],
                  ),
                ),
                Expanded(
                  flex: 2,
                  child: Container(
                    height: size.convert(context, 15),
                    width: size.convert(context, 15),
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: tabbarcolor,
                    ),
                    child: Center(
                      child: Text(
                        "${count ?? "0"}",
                        style: TextStyle(
                          fontSize: size.convert(context, 8),
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
