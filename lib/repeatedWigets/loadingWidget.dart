import 'package:biznes/res/color.dart';
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';
import 'package:biznes/res/size.dart';

import '../size_config.dart';

Column Colums(context) {
  return Column(
    children: [
      SizedBox(
        height: 20,
      ),
      ListTile(
        leading: CircleAvatar(
          maxRadius: 28,
        ),
        title: Column(
          children: [
            Container(
              color: Colors.red,
              width: double.infinity,
              height: size.convert(context, 10),
            ),
            SizedBox(
              height: 5,
            ),
            Container(
              color: Colors.red,
              width: double.infinity,
              height: size.convert(context, 10),
            ),
            SizedBox(
              height: 5,
            ),
            Container(
              color: Colors.red,
              width: double.infinity,
              height: size.convert(context, 10),
            ),
            SizedBox(
              height: 5,
            ),
            Container(
              color: Colors.red,
              width: double.infinity,
              height: size.convert(context, 10),
            ),
          ],
        ),
      ),
    ],
  );
}

LoadingShimmerWidget(context) {
  return Shimmer.fromColors(
    baseColor: Colors.grey[300],
    highlightColor: Colors.grey[100],
    child: ListView(
      padding: EdgeInsets.zero,
      children: [
        Colums(context),
        Colums(context),
        Colums(context),
        Colums(context),
        Colums(context),
        Colums(context),
        Colums(context),
        Colums(context),
      ],
    ),
  );
}

CategoriesInboxLoading(context) {
  return Column(
    children: [
      ListTile(
        leading: Container(
          height: 50,
          width: 50,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(19),
            color: Colors.red,
          ),
        ),
        trailing: Padding(
          padding: const EdgeInsets.only(right: 22.0),
          child: CircleAvatar(
            maxRadius: 7,
          ),
        ),
        title: Column(
          children: [
            Container(
              color: Colors.red,
              width: double.infinity,
              height: size.convert(context, 10),
            ),
            SizedBox(
              height: 5,
            ),
            Container(
              color: Colors.red,
              width: double.infinity,
              height: size.convert(context, 10),
            ),
            SizedBox(
              height: 5,
            ),
            Container(
              color: Colors.red,
              width: double.infinity,
              height: size.convert(context, 10),
            ),
          ],
        ),
      ),
    ],
  );
}

SearchProductLoading(context) {
  return Column(
    children: [
      Container(
        height: (120 / 8.148314082864863) * SizeConfig.heightMultiplier,
        child: Row(
          children: [
            SizedBox(
              width: 10,
            ),
            Expanded(
              flex: 1,
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: Colors.red,
                ),
                width: (120 / 4.853932272197492) * SizeConfig.widthMultiplier,
                height: (120 / 8.148314082864863) * SizeConfig.heightMultiplier,
              ),
            ),
            SizedBox(
              width: 10,
            ),
            Expanded(
              flex: 4,
              child: Column(
                children: [
                  SizedBox(
                    height: 5,
                  ),
                  Container(
                    color: Colors.red,
                    width: double.infinity,
                    height: size.convert(context, 10),
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Container(
                    color: Colors.red,
                    width: double.infinity,
                    height: size.convert(context, 10),
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Container(
                    color: Colors.red,
                    width: double.infinity,
                    height: size.convert(context, 10),
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Container(
                    color: Colors.red,
                    width: double.infinity,
                    height: size.convert(context, 10),
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Container(
                    color: Colors.red,
                    width: double.infinity,
                    height: size.convert(context, 10),
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Container(
                    color: Colors.red,
                    width: double.infinity,
                    height: size.convert(context, 10),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
      SizedBox(
        height: 10,
      ),
    ],
  );
}

Widget shimmer() {
  return Container(
    width: double.infinity,
    padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
    child: Shimmer.fromColors(
      baseColor: Colors.grey[300],
      highlightColor: Colors.grey[100],
      child: SingleChildScrollView(
        child: Column(
          children: [
            Column(
              children: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
                  .map((_) => Padding(
                        padding: const EdgeInsetsDirectional.only(bottom: 8.0),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              width: 80.0,
                              height: 80.0,
                              color: whiteColor,
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 8.0),
                            ),
                            Expanded(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    width: double.infinity,
                                    height: 18.0,
                                    color: whiteColor,
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 5.0),
                                  ),
                                  Container(
                                    width: double.infinity,
                                    height: 8.0,
                                    color: whiteColor,
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 5.0),
                                  ),
                                  Container(
                                    width: 100.0,
                                    height: 8.0,
                                    color: whiteColor,
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 5.0),
                                  ),
                                  Container(
                                    width: 20.0,
                                    height: 8.0,
                                    color: whiteColor,
                                  ),
                                ],
                              ),
                            )
                          ],
                        ),
                      ))
                  .toList(),
            ),
          ],
        ),
      ),
    ),
  );
}

List<Widget> SearchProloadingWidget(context) {
  List<Widget> list = [];
  for (int i = 0; i < 8; i++) {
    list.add(SearchProductLoading(context));
  }
  return list;
}

ShimmerCategoriesInboxLoading(context) {
  return Shimmer.fromColors(
    baseColor: Colors.grey[300],
    highlightColor: Colors.grey[100],
    child: ListView(
      children: [
        CategoriesInboxLoading(context),
        CategoriesInboxLoading(context),
        CategoriesInboxLoading(context),
        CategoriesInboxLoading(context),
        CategoriesInboxLoading(context),
        CategoriesInboxLoading(context),
        CategoriesInboxLoading(context),
        CategoriesInboxLoading(context),
        CategoriesInboxLoading(context),
      ],
    ),
  );
}

Widget LoadingWidgetBiznes() {
  return Column(
    mainAxisAlignment: MainAxisAlignment.center,
    children: [
      Center(
        child: Image.asset(
          "assets/images/logo2.gif",
          height: 200,
          width: 100,
        ),
      ),
    ],
  );
}
