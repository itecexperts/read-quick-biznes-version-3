import 'package:biznes/Controllers/backendservicescontroller.dart';
import 'package:biznes/Text/messagetext.dart';
import 'package:biznes/bloc/inbox/ChatCategoriestile.dart/Categoriesbloc.dart';
import 'package:biznes/bloc/inbox/ChatCategoriestile.dart/categoriesEvents.dart';
import 'package:biznes/helper/notificationhelper.dart';
import 'package:biznes/model/bulkmodel.dart';
import 'package:biznes/repeatedWigets/filledButton.dart';
import 'package:biznes/res/size.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:biznes/res/style.dart';
import 'package:biznes/services/register_logic.dart';
import 'package:flutter/material.dart';
import 'package:biznes/res/color.dart';
import 'package:provider/provider.dart';

class CustomDialog extends StatelessWidget {
  final String userId;
  final String categoryId;
  final String subCategoryId;
  final Bulk bulk;
  final String createdForId;
  final bool iscurrentpage;
  CustomDialog({
    @required this.userId,
    @required this.categoryId,
    @required this.subCategoryId,
    this.bulk,
    @required this.iscurrentpage,
    this.createdForId,
  }) : assert(userId != null && categoryId != null && subCategoryId != null,
            bulk != null);

  @override
  Widget build(BuildContext context) {
    var ismessagesend = Provider.of<Notificationgetting>(context);
    return Container(
      child: Dialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(12.0),
        ),
        child: SingleChildScrollView(
          child: Container(
            margin: EdgeInsets.all(10),
            padding: EdgeInsets.all(10),
            child: Column(children: <Widget>[
              Text(
                "Send Message",
                style: style.MontserratMedium(
                    color: buttonColor, fontSize: size.convert(context, 20)),
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                alignment: Alignment.bottomLeft,
                decoration: BoxDecoration(
                  color: Color(0xfff5f5f5),
                  borderRadius: BorderRadius.circular(15),
                ),
                padding: EdgeInsets.all(5.0),
                child: Text(
                  "$messageText",
                  style: style.MontserratRegular(
                      color: dialogTextColor,
                      fontSize: size.convert(context, 14)),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Stack(
                children: [
                  filledButton(
                    shadowColor:
                        ismessagesend.ismessagesend ? Colors.grey : null,
                    endColor: ismessagesend.ismessagesend ? Colors.grey : null,
                    startColor:
                        ismessagesend.ismessagesend ? Colors.grey : null,
                    txt: ismessagesend.ismessagesend
                        ? "Message Sending..."
                        : "Send Message",
                    onTap: () {
                      ismessagesend.setmessagetrue(value: true);
                      if (iscurrentpage) {
                        checkboxmessagemodel(
                          context: context,
                          message: "$messageText",
                          bulk: bulk ?? "",
                          categoryId: categoryId ?? "",
                          subCategoryId: subCategoryId ?? "",
                          userId: userId ?? "",
                        ).then((data) {
                          ismessagesend.setmessagetrue(value: false);
                          context
                              .bloc<CategoriesInboxBloc>()
                              .add(FetchMessagesbyCategories(userId: userId));
                          Navigator.of(context).pop();
                        });
                      } else {
                        singlemessage(
                          createdby: userId,
                          createdfor: createdForId,
                          categoryid: categoryId,
                          subcategoryid: subCategoryId,
                          message: "$messageText",
                          context: context,
                        ).then((data) {
                          ismessagesend.setmessagetrue(value: false);
                          context
                              .bloc<CategoriesInboxBloc>()
                              .add(FetchMessagesbyCategories(userId: userId));
                          Navigator.of(context).pop();
                        });
                      }
                    },
                  ),
                ],
              )
            ]),
          ),
        ),
      ),
    );
  }
}

CustomFloatingActionButton({@required Function onPressed}) {
  return Padding(
    padding: const EdgeInsets.all(15.0),
    child: Container(
      alignment: Alignment.bottomRight,
      child: FloatingActionButton(
        onPressed: onPressed,
        child: Container(
          height: 68,
          width: 68,
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            gradient: LinearGradient(
              begin: const Alignment(0.7, -0.5),
              end: const Alignment(0.6, 0.5),
              colors: [
                Color(0xff69bef7),
                Color(0xff7595f4),
              ],
            ),
          ),
          child: Icon(
            Icons.send,
          ),
        ),
      ),
    ),
  );
}
