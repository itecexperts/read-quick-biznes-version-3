import 'package:biznes/res/size.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class checkBox extends StatefulWidget {
  Function ontap;
  bool val;
  checkBox({this.ontap, this.val});
  @override
  _checkBoxState createState() => _checkBoxState();
}

class _checkBoxState extends State<checkBox> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        widget.ontap();
        setState(() {
          widget.val;
          print(widget.val);
        });
      },
      child: Container(
        child: widget.val
            ? Container(
                padding: EdgeInsets.all(size.convert(context, 5)),
                decoration: BoxDecoration(
                  color: Color(0xff2dcc71),
                  shape: BoxShape.circle,
                ),
                child: Center(
                  child: SvgPicture.asset("assets/icons/tick.svg"),
                ),
              )
            : Container(
                width: size.convert(context, 16),
                height: size.convert(context, 16),
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    border: Border.all(
                      color: Color(0xff7595f0),
                      width: size.convert(context, 3),
                    )),
              ),
      ),
    );
  }
}
