import 'package:biznes/res/size.dart';
import 'package:biznes/size_config.dart';
import 'package:flutter/material.dart';
import '../model/getguestmessges.dart';
import '../services/register_logic.dart';
import 'package:flutter/services.dart';
import 'package:biznes/res/color.dart' as fontandcolor;
// import 'package:clipboard_manager/clipboard_manager.dart';
import 'package:provider/provider.dart';
import '../helper/login_helper.dart';

class ChatGuest extends StatefulWidget {
  @override
  _ChatGuestState createState() => _ChatGuestState();
}

class _ChatGuestState extends State<ChatGuest> {
  @override
  Widget build(BuildContext context) {
    var userid = Provider.of<LoginHelper>(context).user.sId;
    return Scaffold(
      body: OrientationBuilder(builder: (context, orientation) {
        return SingleChildScrollView(
          child: Container(
            decoration: BoxDecoration(
                image: DecorationImage(
                  fit: BoxFit.cover,
                  image: AssetImage("assets/images/newback.jpg"),
                ),
                color: Colors.white,
                boxShadow: <BoxShadow>[
                  BoxShadow(
                      color: Colors.grey,
                      blurRadius: 10.3,
                      offset: Offset(1.0, 0.5),
                      spreadRadius: 5)
                ],
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(20.5),
                  bottomRight: Radius.circular(20.5),
                )),
            width: orientation == Orientation.portrait
                ? (485.3932272197492 / 4.853932272197492) *
                    SizeConfig.widthMultiplier
                : (814.8314082864863 / 8.148314082864863) *
                    SizeConfig.heightMultiplier,
            height: MediaQuery.of(context).size.height,
            child: SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  Container(
                    height: orientation == Orientation.portrait
                        ? ((814.8314082864863 / 8.148314082864863) / 1.44) *
                            SizeConfig.heightMultiplier
                        : ((814.8314082864863 / 8.148314082864863) / 1.8) *
                            SizeConfig.heightMultiplier,
                    child: FutureBuilder(
                      future: getmessagesofguest(userid, context),
                      builder: (BuildContext context,
                          AsyncSnapshot<List<GuestMessages>> snapshot) {
                        List<GuestMessages> messages = snapshot.data;
                        return snapshot.hasData
                            ? ListView.builder(
                                itemCount: messages.length,
                                itemBuilder: (BuildContext context, int index) {
                                  return Padding(
                                    padding: const EdgeInsets.only(
                                        left: 8.0, right: 8.0, bottom: 2.0),
                                    child: Container(
                                      child: Column(
                                        children: <Widget>[
                                          Container(
                                            color: Colors.white,
                                            height: orientation ==
                                                    Orientation.portrait
                                                ? ((200.8314082864863 /
                                                            8.148314082864863) /
                                                        1.44) *
                                                    SizeConfig.heightMultiplier
                                                : ((200.8314082864863 /
                                                            8.148314082864863) /
                                                        1.8) *
                                                    SizeConfig.heightMultiplier,
                                            child: Row(
                                              children: <Widget>[
                                                Stack(
                                                  children: <Widget>[
                                                    Container(
                                                      height: size.convert(
                                                          context, 40),
                                                      width: size.convert(
                                                          context, 40),
                                                      decoration: BoxDecoration(
                                                        color: fontandcolor
                                                            .tabbarcolor,
                                                        shape: BoxShape.circle,
                                                      ),
                                                      child: Icon(
                                                        Icons.email,
                                                        color: Colors.white,
                                                        size: size.convert(
                                                            context, 20),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                                SizedBox(
                                                    width: size.convert(
                                                        context, 10)),
                                                Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.start,
                                                  children: <Widget>[
                                                    Container(
                                                      padding: EdgeInsets.only(
                                                        top: 6.0,
                                                        left: 3.0,
                                                      ),
                                                      height: orientation ==
                                                              Orientation
                                                                  .portrait
                                                          ? ((40.8314082864863 /
                                                                      8.148314082864863) /
                                                                  1.44) *
                                                              SizeConfig
                                                                  .heightMultiplier
                                                          : ((40.8314082864863 /
                                                                      8.148314082864863) /
                                                                  1.8) *
                                                              SizeConfig
                                                                  .heightMultiplier,
                                                      child: Text(
                                                        '${messages[index].cityId.title}',
                                                        style: TextStyle(
                                                            fontFamily:
                                                                fontandcolor
                                                                    .fontfaimly,
                                                            color: Color(
                                                                0xff394c81),
                                                            fontSize: orientation ==
                                                                    Orientation
                                                                        .portrait
                                                                ? (17.8314082864863 /
                                                                        8.148314082864863) *
                                                                    SizeConfig
                                                                        .heightMultiplier
                                                                : (17.8314082864863 /
                                                                        8.148314082864863) *
                                                                    SizeConfig
                                                                        .heightMultiplier),
                                                      ),
                                                    ),
                                                    Padding(
                                                      padding: EdgeInsets.only(
                                                          top: 4.0),
                                                    ),
                                                    Row(
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .start,
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .start,
                                                      children: <Widget>[
                                                        Row(
                                                          children: <Widget>[
                                                            Text(
                                                              'Category: ',
                                                              style: TextStyle(
                                                                fontFamily:
                                                                    fontandcolor
                                                                        .fontfaimlyregular,
                                                                fontSize: orientation == Orientation.portrait
                                                                    ? ((17.8314082864863 /
                                                                                8.148314082864863) /
                                                                            1.44) *
                                                                        SizeConfig
                                                                            .heightMultiplier
                                                                    : ((17.8314082864863 /
                                                                                8.148314082864863) /
                                                                            1.8) *
                                                                        SizeConfig
                                                                            .heightMultiplier,
                                                              ),
                                                            ),
                                                            Text(
                                                              '${messages[index].categoryId.title}',
                                                              style: TextStyle(
                                                                fontFamily:
                                                                    fontandcolor
                                                                        .fontfaimly,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .bold,
                                                                fontSize: orientation == Orientation.portrait
                                                                    ? ((17.8314082864863 /
                                                                                8.148314082864863) /
                                                                            1.44) *
                                                                        SizeConfig
                                                                            .heightMultiplier
                                                                    : ((17.8314082864863 /
                                                                                8.148314082864863) /
                                                                            1.8) *
                                                                        SizeConfig
                                                                            .heightMultiplier,
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                        Padding(
                                                          padding:
                                                              EdgeInsets.only(
                                                                  right: 10),
                                                        ),
                                                      ],
                                                    ),
                                                    Padding(
                                                      padding: EdgeInsets.only(
                                                          top: 4.0),
                                                    ),
                                                    Row(
                                                      children: <Widget>[
                                                        Text(
                                                          "SubCategory: ",
                                                          style: TextStyle(
                                                            fontFamily: fontandcolor
                                                                .fontfaimlyregular,
                                                            fontSize: orientation ==
                                                                    Orientation
                                                                        .portrait
                                                                ? ((17.8314082864863 /
                                                                            8.148314082864863) /
                                                                        1.44) *
                                                                    SizeConfig
                                                                        .heightMultiplier
                                                                : ((17.8314082864863 /
                                                                            8.148314082864863) /
                                                                        1.8) *
                                                                    SizeConfig
                                                                        .heightMultiplier,
                                                          ),
                                                        ),
                                                        Text(
                                                            '${messages[index].subCategoryId.title}',
                                                            style: TextStyle(
                                                              fontFamily:
                                                                  fontandcolor
                                                                      .fontfaimly,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold,
                                                              fontSize: orientation == Orientation.portrait
                                                                  ? ((17.8314082864863 /
                                                                              8.148314082864863) /
                                                                          1.44) *
                                                                      SizeConfig
                                                                          .heightMultiplier
                                                                  : ((17.8314082864863 /
                                                                              8.148314082864863) /
                                                                          1.8) *
                                                                      SizeConfig
                                                                          .heightMultiplier,
                                                            )),
                                                      ],
                                                    ),
                                                    Row(
                                                      children: <Widget>[
                                                        Container(
                                                          padding:
                                                              EdgeInsets.all(
                                                                  8.0),
                                                          decoration:
                                                              BoxDecoration(
                                                            color: Color(
                                                                0xffEbf3fa),
                                                            borderRadius: BorderRadius.only(
                                                                topLeft: Radius
                                                                    .circular(
                                                                        15),
                                                                bottomLeft: Radius
                                                                    .circular(
                                                                        15)),
                                                          ),
                                                          height: orientation == Orientation.portrait
                                                              ? ((90.8314082864863 /
                                                                          8.148314082864863) /
                                                                      1.4) *
                                                                  SizeConfig
                                                                      .heightMultiplier
                                                              : ((90.8314082864863 /
                                                                          8.148314082864863) /
                                                                      1.8) *
                                                                  SizeConfig
                                                                      .heightMultiplier,
                                                          width: orientation ==
                                                                  Orientation
                                                                      .portrait
                                                              ? ((150.8314082864863 /
                                                                          4.148314082864863) /
                                                                      0.6) *
                                                                  SizeConfig
                                                                      .widthMultiplier
                                                              : ((150.8314082864863 /
                                                                          4.148314082864863) /
                                                                      0.3) *
                                                                  SizeConfig
                                                                      .widthMultiplier,
                                                          child: ListView(
                                                            children: <Widget>[
                                                              Text(
                                                                messages[index]
                                                                    .message,
                                                                style:
                                                                    TextStyle(
                                                                  fontFamily:
                                                                      fontandcolor
                                                                          .fontfaimlyregular,
                                                                  fontSize: orientation == Orientation.portrait
                                                                      ? ((25.8314082864863 / 8.148314082864863) /
                                                                              1.44) *
                                                                          SizeConfig
                                                                              .heightMultiplier
                                                                      : ((25.8314082864863 / 8.148314082864863) /
                                                                              1.8) *
                                                                          SizeConfig
                                                                              .heightMultiplier,
                                                                  color: Color(
                                                                      0xff394c81),
                                                                ),
                                                              ),
                                                            ],
                                                          ),
                                                        ),
                                                        Container(
                                                            height: orientation ==
                                                                    Orientation
                                                                        .portrait
                                                                ? ((90.8314082864863 /
                                                                            8.148314082864863) /
                                                                        1.4) *
                                                                    SizeConfig
                                                                        .heightMultiplier
                                                                : ((90.8314082864863 /
                                                                            8.148314082864863) /
                                                                        1.8) *
                                                                    SizeConfig
                                                                        .heightMultiplier,
                                                            decoration:
                                                                BoxDecoration(
                                                              color: Color(
                                                                  0xffEbf3fa),
                                                              borderRadius: BorderRadius.only(
                                                                  topRight: Radius
                                                                      .circular(
                                                                          15),
                                                                  bottomRight: Radius
                                                                      .circular(
                                                                          15)),
                                                            ),
                                                            child: Padding(
                                                              padding:
                                                                  const EdgeInsets
                                                                      .all(8.0),
                                                              child: IconButton(
                                                                icon: Icon(Icons
                                                                    .content_copy),
                                                                onPressed: () {
                                                                  Clipboard.setData(new ClipboardData(
                                                                          text: messages[index]
                                                                              .message))
                                                                      .then(
                                                                          (_) {
                                                                    Scaffold.of(
                                                                            context)
                                                                        .showSnackBar(SnackBar(
                                                                            content:
                                                                                Text("Message copy to clipboard")));
                                                                  });
                                                                  //             ClipboardManager.copyToClipBoard(messages[index].message).then((result) {
                                                                  //            final snackBar = SnackBar(
                                                                  //            content: Text('Copied to Clipboard'),
                                                                  //            action: SnackBarAction(
                                                                  //            label: 'Undo',
                                                                  //           onPressed: () {},
                                                                  //     ),
                                                                  //   );
                                                                  //   Scaffold.of(context).showSnackBar(snackBar);
                                                                  // });
                                                                },
                                                              ),
                                                            )),
                                                      ],
                                                    ),
                                                  ],
                                                ),
                                              ],
                                            ),
                                          ),
                                          Divider(),
                                        ],
                                      ),
                                    ),
                                  );
                                })
                            : Container(
                                child: Center(
                                  child: CircularProgressIndicator(),
                                ),
                              );
                      },
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      }),
    );
  }
}
