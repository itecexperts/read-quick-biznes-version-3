// import 'package:biznes/helper/login_helper.dart';
// import 'package:biznes/products_Detail/extra.dart';
// import 'package:biznes/res/size.dart';
// import 'package:biznes/services/register_logic.dart';
// import 'package:cached_network_image/cached_network_image.dart';
// import 'package:flutter/material.dart';
// import 'package:biznes/services/server.dart';
// import 'package:provider/provider.dart';
// import 'package:biznes/res/color.dart' as fontandcolor;
// import 'package:biznes/setting/notification.dart';
// import 'package:page_transition/page_transition.dart';
// import 'package:biznes/helper/counthelper.dart';

// class InquiresAccepted extends StatefulWidget {
//   final String category;
//   final String status;
//   final String userid;
//   InquiresAccepted(this.category, this.status, this.userid);

//   @override
//   _InquiresAcceptedState createState() => _InquiresAcceptedState();
// }

// class _InquiresAcceptedState extends State<InquiresAccepted> {
//   @override
//   void initState() {
//     super.initState();
//     // gettinginquiriesusers(context: context,status: widget.status,userid:widget.userid );
//   }

//   @override
//   Widget build(BuildContext context) {
//     var counters = Provider.of<CountHelper>(context).counter;
//     var counterset = Provider.of<CountHelper>(context);
//     var user = Provider.of<LoginHelper>(context).user;
//     return Scaffold(
//       backgroundColor: Colors.white,
//       body: Column(
//         children: [
//           CustomAppBar(
//               icon: Icon(Icons.arrow_back),
//               IconPressed: () {
//                 Navigator.of(context).pop();
//               },
//               context: context,
//               text: widget.status,
//               length: counters.toString(),
//               onTap: () {
//                 counterset.zerocunter();
//                 Navigator.of(context).push(PageTransition(
//                     child: MyNotification(),
//                     duration: Duration(milliseconds: 700),
//                     type: PageTransitionType.leftToRightWithFade));
//               }),
//           Divider(
//             thickness: 1,
//             height: 3,
//           ),
//           Expanded(
//             child: Container(
//               child: FutureBuilder(
//                 // future: gettinginquiriesusers(
//                 //     context: context, userid: user.sId, status: widget.status),
//                 builder: (BuildContext context, AsyncSnapshot snapshot) {
//                   Map data = snapshot.data;
//                   return snapshot.hasData
//                       ? ListView.builder(
//                           itemCount: data["Inquiries"].length,
//                           itemBuilder: (BuildContext context, int index) {
//                             return Column(children: [
//                               ListTile(
//                                 leading: CircleAvatar(
//                                     backgroundImage: CachedNetworkImageProvider(
//                                         "$base_url/${data["Inquiries"][index]["participants"][1]["profilePic"]}")),
//                                 title: Text(
//                                   "${data["Inquiries"][index]["participants"][1]["username"]}",
//                                   style: TextStyle(
//                                     color: fontandcolor.appbarfontandiconcolor,
//                                     fontFamily: fontandcolor.fontfaimly,
//                                     fontSize: 19,
//                                     fontWeight: FontWeight.w300,
//                                   ),
//                                 ),
//                                 subtitle: Column(
//                                   crossAxisAlignment: CrossAxisAlignment.start,
//                                   children: [
//                                     Text(
//                                       "${data["Inquiries"][index]["categoryId"]["title"]}",
//                                       style: TextStyle(
//                                         fontSize: size.convert(context, 10),
//                                         fontWeight: FontWeight.w400,
//                                         color: Colors.black,
//                                         fontFamily: 'MontserratRegular',
//                                       ),
//                                     ),
//                                     Text(
//                                       "${data["Inquiries"][index]["subCategoryId"]["title"]}",
//                                       style: TextStyle(
//                                         fontSize: size.convert(context, 10),
//                                         fontWeight: FontWeight.w400,
//                                         color: Colors.black,
//                                         fontFamily: 'MontserratRegular',
//                                       ),
//                                     ),
//                                     Container(
//                                       height: 20,
//                                       width: size.convert(context, 200),
//                                       child: ListView.builder(
//                                           padding: EdgeInsets.zero,
//                                           scrollDirection: Axis.horizontal,
//                                           itemCount: data["Inquiries"][0]
//                                                   ["participants"][1]["roleId"]
//                                               .length,
//                                           itemBuilder: (BuildContext context,
//                                               int indexs) {
//                                             return FittedBox(
//                                               fit: BoxFit.scaleDown,
//                                               child: Row(
//                                                 children: [
//                                                   Text(
//                                                     "${data["Inquiries"][0]["participants"][1]["roleId"][indexs]["title"].toString()},",
//                                                     textAlign: TextAlign.left,
//                                                     overflow: TextOverflow.clip,
//                                                     style: TextStyle(
//                                                       fontWeight:
//                                                           FontWeight.w400,
//                                                       fontFamily: fontandcolor
//                                                           .fontfaimly,
//                                                       color: Color(0xff394C81),
//                                                       fontSize: size.convert(
//                                                           context, 10),
//                                                     ),
//                                                   ),
//                                                 ],
//                                               ),
//                                             );
//                                           }),
//                                     ),
//                                   ],
//                                 ),
//                               ),
//                               Divider(
//                                 thickness: 1,
//                                 height: 3,
//                               ),
//                             ]);
//                           })
//                       : Container(
//                           child: Center(child: CircularProgressIndicator()),
//                         );
//                 },
//               ),
//             ),
//           ),
//         ],
//       ),
//     );
//   }
// }
