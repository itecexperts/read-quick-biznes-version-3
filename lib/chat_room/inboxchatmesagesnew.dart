import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:provider/provider.dart';
import 'package:biznes/helper/conversationhelper.dart';
import 'dart:convert';
class InboxDart extends StatefulWidget {
  @override
  _InboxDartState createState() => _InboxDartState();
}

class _InboxDartState extends State<InboxDart> {
  Box unreadsBox;
  @override
  void initState() {
    unreadsBox = Hive.box('conversationsGeneration');
    // Provider.of<NewMessagesHlper>(context, listen: false)
    //     .setNewMessages(jsonDecode(unreadsBox.get("601293a26de0972f141cf528")));
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    var data=Provider.of<NewMessagesHlper>(context);
    return Scaffold(
      appBar: AppBar(
        title: Column(
          children: [
            Text(unreadsBox.get("601293a26de0972f141cf528").toString()),
          ],
        ),
      ),
      body: Text(unreadsBox.get("601293a26de0972f141cf528").toString()),
    );
  }
}
