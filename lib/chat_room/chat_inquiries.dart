import 'package:biznes/bloc/inbox/ChatCategoriestile.dart/Categoriesbloc.dart';
import 'package:biznes/bloc/inbox/ChatCategoriestile.dart/categoriesEvents.dart';
import 'package:biznes/bloc/inbox/bloc.dart';
import 'package:biznes/bloc/inbox/events.dart';
import 'package:biznes/bloc/inbox/states.dart';
import 'package:biznes/helper/allmessageshelper.dart';
import 'package:biznes/helper/categoryInboxHelper.dart';
import 'package:biznes/helper/counthelper.dart';
import 'package:biznes/repeatedWigets/loadingWidget.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hive/hive.dart';
import 'package:biznes/model/inboxlistmessages.dart';
import 'package:flutter/services.dart';
import 'package:biznes/products_Detail/extra.dart';
import 'package:biznes/res/size.dart';
import 'package:biznes/setting/notification.dart';
import 'package:page_transition/page_transition.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:biznes/res/style.dart';
import 'package:biznes/res/color.dart' as font;
import 'package:biznes/services/server.dart';
import 'gettingchat.dart';
import 'package:biznes/chat_room/chat_room.dart';
import 'package:biznes/helper/login_helper.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:biznes/services/register_logic.dart';
import 'package:biznes/size_config.dart';
import 'package:intl/intl.dart' as intl;
import 'package:biznes/res/color.dart' as fontandcolor;
import 'package:flutter/material.dart';
import 'package:biznes/helper/tab_bar.dart';
import 'package:provider/provider.dart';

class ChatTile extends StatefulWidget {
  final String categoryId;
  final String userId;
  final String roleName;
  final String page;
  final String status;
  ChatTile(
      {this.categoryId, this.userId, this.roleName, this.status, this.page});

  @override
  _ChatTileState createState() => _ChatTileState();
}

class _ChatTileState extends State<ChatTile> {
  static const platform = const MethodChannel('samples.flutter.dev/battery');
  Box unreadsBox;
  String query = "";
  final _controller = TextEditingController();
  var formKey = GlobalKey<FormState>();
  var volume = Icons.volume_up;
  var firstValue;
  var secondValue;
  final primary = Color(0xff696b9e);
  final secondary = Color(0xfff29a94);
  bool orientation = false;
  Map datas;
  bool isloading = true;
  List<AllMessages> data = List();

  @override
  void didChangeDependencies() {
    Provider.of<SocketMessaging>(context, listen: false).init();
    super.didChangeDependencies();
  }

  @override
  void deactivate() {
    Provider.of<SocketMessaging>(context, listen: false).disconnect();
    super.deactivate();
  }

  gettinginboxmessagesfilters() async {
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    String userid = _prefs.getString("userid");
    // getAllinboxmessagesfrommodel(
    //         userid: userid, catid: widget.categoryId, context: context)
    //     .then((value) {
    //   if (value != null) {
    //     Provider.of<AllmessagesHelper>(context, listen: false)
    //         .onmessagesrecieved(value);
    //   }

    //   print(userid + 'ffff');
    //   // data=data1;
    //   setState(() {
    //     isloading = false;
    //   });
    // });
  }

  getcategoriesMessages() async {
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    context
        .bloc<CategoriesInboxBloc>()
        .add(FetchMessagesbyCategories(userId: _prefs.getString("userid")));
  }

  _fetchAlbum(String userId, String catId, String query) async {
    context.bloc<AlbumBloc>().add(FetchMessage(
        userId: userId, catId: catId, query: query, roleName: widget.roleName));
  }

  @override
  void initState() {
    print(widget.status + "ffffffffffffffffffffffffffffff");
    _fetchAlbum(widget.userId, widget.categoryId, query);
    unreadsBox = Hive.box('conversationsGeneration');
    print(widget.categoryId);
    gettinginboxmessagesfilters();
    super.initState();
  }

  List<Message> messages = List<Message>();

  @override
  Widget build(BuildContext context) {
    var counters = Provider.of<CountHelper>(context).counter;
    var counterSet = Provider.of<CountHelper>(context);
    return Scaffold(
      backgroundColor: Colors.white,
      body: OrientationBuilder(builder: (context, orientation) {
        return Column(
          children: <Widget>[
            CustomAppBar(
                isShowBackground: true,
                icon: Icon(Icons.arrow_back),
                IconPressed: () {
                  Navigator.of(context).pop();
                },
                context: context,
                text: "Chat",
                length: counters.toString(),
                onTap: () {
                  counterSet.zerocunter();
                  Navigator.of(context).push(PageTransition(
                      child: MyNotification(),
                      duration: Duration(milliseconds: 700),
                      type: PageTransitionType.leftToRightWithFade));
                }),
            Container(
              decoration: BoxDecoration(
                color: Colors.white,
                image: DecorationImage(
                  fit: BoxFit.cover,
                  image: AssetImage("assets/images/newback.jpg"),
                ),
              ),
              child: TextField(
                controller: _controller,
                onChanged: (string) {
                  setState(() {
                    query = string.trim();
                    _fetchAlbum(widget.userId, widget.categoryId, query);
                  });
                  // Provider.of<AllmessagesHelper>(context, listen: false)
                  //     .changeSearchString(string.trim());
                },
                decoration: InputDecoration(
                    hintText: 'Search by username or role name',
                    hintStyle: TextStyle(
                        fontFamily: 'MontserratRegular',
                        fontSize: (15 / 8.148314082864863) *
                            SizeConfig.heightMultiplier),
                    prefixIcon: Icon(
                      Icons.search,
                      size: (20 / 8.148314082864863) *
                          SizeConfig.heightMultiplier,
                    ),
                    border: InputBorder.none),
              ),
            ),
            Divider(
              height: 0,
            ),
            Expanded(
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  image: DecorationImage(
                    fit: BoxFit.cover,
                    image: AssetImage("assets/images/newback.jpg"),
                  ),
                ),
                child: Padding(
                  padding: EdgeInsets.only(top: 2.0),
                  child: listItems(context),
                ),
              ),
            ),
          ],
        );
      }),
    );
  }

  Widget listItems(context) {
    var user = Provider.of<LoginHelper>(context).user;
    return BlocBuilder<AlbumBloc, AllMessageStates>(
        builder: (BuildContext context, AllMessageStates states) {
      if (states is AlbumErrorState) {
        final errror = states.error;
        return RefreshIndicator(
          color: font.buttonColor,
          onRefresh: () => _fetchAlbum(widget.userId, widget.categoryId, query),
          child: ListView(
            children: [
              Center(
                  child: Text(
                "${errror.message}",
                style: style.MontserratMedium(
                  color: font.buttonColor,
                  fontSize: size.convert(context, 18),
                ),
              )),
            ],
          ),
        );
      }
      if (states is AllMessagesLoadedState) {
        List<AllMessages> data = states.allmessages;
        return data.length == 0
            ? Container(
                child: Center(
                  child: Text('No Chats found',
                      style: style.MontserratMedium(
                          color: font.buttonColor,
                          fontSize: size.convert(context, 18))),
                ),
              )
            : RefreshIndicator(
                color: font.buttonColor,
                onRefresh: () =>
                    _fetchAlbum(widget.userId, widget.categoryId, query),
                child: ListView.builder(
                  padding: EdgeInsets.only(top: 10),
                  itemCount: data?.length ?? 0,
                  physics: AlwaysScrollableScrollPhysics(),
                  itemBuilder: (BuildContext context, int index) {
                    DateTime dateTime1 = DateTime.parse(data[index].createdAt);
                    String formattedTime1 = intl.DateFormat()
                        .format(dateTime1.add(Duration(hours: 5)));
                    _data() {
                      return Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(bottom: 3.0),
                            child: GestureDetector(
                              onLongPress: () {
                                showdialogfordeleting(
                                  context: context,
                                  title: "Delete Chat ",
                                  text: "Are you Sure to delete chat??",
                                  onPressed: () {
                                    unreadsBox.delete(data[index].conversation);
                                    deleteconversation(
                                            data[index].conversation, context)
                                        .then((value) {
                                      gettinginboxmessagesfilters();
                                    });
                                  },
                                );
                              },
                              onTap: () async {
                                print(user.sId);
                                print(data[index].conversationCreatedBy);
                                if (data[index].status == "Rejected" &&
                                    data[index].conversationCreatedBy ==
                                        user.sId) {
                                  return;
                                } else {
                                  platform
                                      .invokeMethod('closeAllNotifications');
                                  final status = await Navigator.of(context,
                                          rootNavigator: true)
                                      .push(MaterialPageRoute(
                                          builder: (context) => ChatRoom(
                                                conversationid:
                                                    data[index].conversation ??
                                                        "",
                                                createdforid: data[index]
                                                            .createdFor
                                                            .sId ==
                                                        user.sId
                                                    ? data[index].createdBy.sId
                                                    : data[index]
                                                            .createdFor
                                                            .sId ??
                                                        "",
                                                categoryid: data[index]
                                                        .categoryId
                                                        .sId ??
                                                    "",
                                                subcategoryid: data[index]
                                                        .subCategoryId
                                                        .sId ??
                                                    "",
                                                userid: user.sId ?? "",
                                                status:
                                                    data[index].status ?? "",
                                                username: user.fullName ?? "",
                                                image: data[index]
                                                            .createdFor
                                                            .sId ==
                                                        user.sId
                                                    ? '$base_url/${data[index].createdBy.profilePic}' ??
                                                        ""
                                                    : '$base_url/${data[index].createdFor.profilePic}' ??
                                                        "",
                                                createdbyid:
                                                    data[index].createdBy.sId,
                                                messageusername: data[index]
                                                            .createdFor
                                                            .sId ==
                                                        user.sId
                                                    ? data[index]
                                                            .createdBy
                                                            .username ??
                                                        ""
                                                    : data[index]
                                                            .createdFor
                                                            .username ??
                                                        "",
                                                onlinestatus: data[index]
                                                            .createdBy
                                                            .sId ==
                                                        user.sId
                                                    ? data[index]
                                                                .createdFor
                                                                .onlineStatus ==
                                                            true
                                                        ? "online"
                                                        : "offline"
                                                    : data[index]
                                                                .createdBy
                                                                .onlineStatus ==
                                                            true
                                                        ? "online"
                                                        : "offline",
                                                organizationname: data[index]
                                                            .createdBy
                                                            .sId ==
                                                        user.sId
                                                    ? data[index]
                                                            .createdFor
                                                            .organizationName ??
                                                        ""
                                                    : data[index]
                                                            .createdBy
                                                            .organizationName ??
                                                        "",
                                                description:
                                                    data[index].createdBy.sId ==
                                                            user.sId
                                                        ? data[index]
                                                            .createdFor
                                                            .description
                                                        : data[index]
                                                                .createdBy
                                                                .description ??
                                                            "",
                                                city:
                                                    data[index].createdBy.sId ==
                                                            user.sId
                                                        ? data[index]
                                                                .createdFor
                                                                .cityId
                                                                .title ??
                                                            ""
                                                        : data[index]
                                                                .createdBy
                                                                .cityId
                                                                .title ??
                                                            "",
                                                country:
                                                    data[index].createdBy.sId ==
                                                            user.sId
                                                        ? data[index]
                                                                .createdFor
                                                                .countryId
                                                                .title ??
                                                            ""
                                                        : data[index]
                                                                .createdBy
                                                                .countryId
                                                                .title ??
                                                            "",
                                                rolename:
                                                    data[index].createdBy.sId ==
                                                            user.sId
                                                        ? data[index]
                                                                .createdFor
                                                                .roleId[0]
                                                                .title ??
                                                            ""
                                                        : data[index]
                                                                .createdBy
                                                                .roleId[0]
                                                                .title ??
                                                            "",
                                              )));
                                  setState(() {
                                    data[index].status = status;
                                    getcategoriesMessages();
                                    _fetchAlbum(widget.userId,
                                        widget.categoryId, query);
                                    // getCategoriesForInbox(user.sId, context)
                                    //     .then((value) {
                                    //   Provider.of<CategoryHelper>(context,
                                    //           listen: false)
                                    //       .setCategoryInbox(value);
                                    // });
                                    // getAllinboxmessagesfrommodel(
                                    //         userid: user.sId,
                                    //         catid: widget.categoryId,
                                    //         context: context)
                                    //     .then((value) {
                                    //   Provider.of<AllmessagesHelper>(context,
                                    //           listen: false)
                                    //       .onmessagesrecieved(value);
                                    // });
                                  });
                                }
                              },
                              child: Stack(
                                children: [
                                  Container(
                                    // color: Colors.white,
                                    child: ListTile(
                                      leading: Stack(
                                        alignment: Alignment.topRight,
                                        overflow: Overflow.visible,
                                        children: [
                                          CircleAvatar(
                                            maxRadius: 28,
                                            backgroundImage:
                                                CachedNetworkImageProvider(data[
                                                                index]
                                                            .createdFor
                                                            .sId ==
                                                        user.sId
                                                    ? '$base_url/${data[index].createdBy.profilePic}'
                                                    : '$base_url/${data[index].createdFor.profilePic}'),
                                          ),
                                          Padding(
                                            padding: EdgeInsets.all(0.5 *
                                                SizeConfig.heightMultiplier),
                                            child: CircleAvatar(
                                              radius: 5,
                                              backgroundColor: data[index]
                                                          .createdBy
                                                          .sId ==
                                                      user.sId
                                                  ? data[index]
                                                              .createdFor
                                                              .onlineStatus ==
                                                          true
                                                      ? Colors.green
                                                      : Colors.grey
                                                  : data[index]
                                                              .createdBy
                                                              .onlineStatus ==
                                                          true
                                                      ? Colors.green
                                                      : Colors.grey,
                                            ),
                                          ),
                                        ],
                                      ),
                                      title: Text(
                                        data[index].createdFor.sId == user.sId
                                            ? data[index].createdBy.username
                                            : data[index].createdFor.username,
                                        style: TextStyle(
                                          fontFamily: 'MontserratMedium',
                                          color: Color(0xff394c81),
                                          fontSize: 18,
                                          fontWeight: FontWeight.w400,
                                        ),
                                      ),
                                      subtitle: Column(
                                        children: <Widget>[
                                          Row(
                                            children: <Widget>[
                                              Flexible(
                                                child: Container(
                                                  child: data[index].message !=
                                                          ""
                                                      ? Text(
                                                          data[index].message ==
                                                                  ""
                                                              ? "Image"
                                                              : data[index]
                                                                  .message,
                                                          style: TextStyle(
                                                            fontSize: 12.0,
                                                            color: Colors.black,
                                                            fontWeight: data[index]
                                                                            .unRead
                                                                            .toString() ==
                                                                        "" ||
                                                                    data[index].unRead ==
                                                                            0 &&
                                                                        data[index].createdBy.sId ==
                                                                            user
                                                                                .sId
                                                                ? FontWeight
                                                                    .normal
                                                                : FontWeight
                                                                    .bold,
                                                            fontFamily:
                                                                'MontserratRegular',
                                                          ),
                                                          overflow: TextOverflow
                                                              .ellipsis,
                                                        )
                                                      : Row(
                                                          children: [
                                                            Image.asset(
                                                              "assets/images/imagemessage.png",
                                                              height:
                                                                  size.convert(
                                                                      context,
                                                                      16),
                                                            ),
                                                            Text(
                                                              "Photo",
                                                              style: style.MontserratRegular(
                                                                  fontSize: size
                                                                      .convert(
                                                                          context,
                                                                          12)),
                                                            )
                                                          ],
                                                        ),
                                                ),
                                              ),
                                            ],
                                          ),
                                          SizedBox(
                                            height: 1,
                                          ),
                                          Row(
                                            children: [
                                              SizedBox(
                                                height: 3,
                                                width: 60,
                                                child: Divider(
                                                  height: 2,
                                                  thickness: 1.0,
                                                ),
                                              ),
                                            ],
                                          ),
                                          SizedBox(
                                            height: 1,
                                          ),
                                          Column(
                                            children: [
                                              Row(
                                                children: [
                                                  Text(
                                                    '${data[index]?.subCategoryId?.title ?? ""}',
                                                    style: TextStyle(
                                                        fontSize: size.convert(
                                                            context, 7),
                                                        fontWeight:
                                                            FontWeight.w400,
                                                        color: Colors.black,
                                                        fontFamily:
                                                            'MontserratRegular'),
                                                  ),
                                                ],
                                              ),
                                              Row(
                                                children: [
                                                  Text(
                                                    "Role: ",
                                                    style: TextStyle(
                                                      letterSpacing: .3,
                                                      fontWeight:
                                                          FontWeight.w400,
                                                      fontFamily:
                                                          font.fontfaimly,
                                                      color: Color(0xff394C81),
                                                      fontSize: size.convert(
                                                          context, 7),
                                                    ),
                                                  ),
                                                  Wrap(
                                                    children:
                                                        data[index]
                                                                    .createdBy
                                                                    .sId ==
                                                                user.sId
                                                            ? data[index]
                                                                .createdFor
                                                                .roleId
                                                                .map(
                                                                  (e) => Text(
                                                                    "${e.title.toString()} ",
                                                                    textAlign:
                                                                        TextAlign
                                                                            .left,
                                                                    overflow:
                                                                        TextOverflow
                                                                            .clip,
                                                                    style:
                                                                        TextStyle(
                                                                      letterSpacing:
                                                                          .3,
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .w400,
                                                                      fontFamily:
                                                                          font.fontfaimly,
                                                                      color: Color(
                                                                          0xff394C81),
                                                                      fontSize:
                                                                          size.convert(
                                                                              context,
                                                                              7),
                                                                    ),
                                                                  ),
                                                                )
                                                                .toList()
                                                            : data[index]
                                                                .createdBy
                                                                .roleId
                                                                .map(
                                                                  (role) =>
                                                                      Text(
                                                                    "${role.title.toString()} ",
                                                                    textAlign:
                                                                        TextAlign
                                                                            .left,
                                                                    overflow:
                                                                        TextOverflow
                                                                            .clip,
                                                                    style:
                                                                        TextStyle(
                                                                      letterSpacing:
                                                                          .3,
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .w400,
                                                                      fontFamily:
                                                                          font.fontfaimly,
                                                                      color: Color(
                                                                          0xff394C81),
                                                                      fontSize:
                                                                          size.convert(
                                                                              context,
                                                                              7),
                                                                    ),
                                                                  ),
                                                                )
                                                                .toList(),
                                                  ),
                                                ],
                                              ),
                                              TextForInbox(
                                                  "Country: ",
                                                  data[index].createdFor.sId ==
                                                          user.sId
                                                      ? data[index]
                                                          .createdFor
                                                          .countryId
                                                          .title
                                                      : data[index]
                                                          .createdFor
                                                          .countryId
                                                          .title,
                                                  context),
                                              TextForInbox(
                                                  "City: ",
                                                  data[index].createdFor.sId ==
                                                          user.sId
                                                      ? data[index]
                                                          .createdFor
                                                          .cityId
                                                          .title
                                                      : data[index]
                                                          .createdFor
                                                          .cityId
                                                          .title,
                                                  context),
                                            ],
                                          ),
                                        ],
                                      ),
                                      trailing: Column(
                                        children: <Widget>[
                                          Padding(
                                            padding: EdgeInsets.only(
                                                right: 18.0,
                                                top: 1.5 *
                                                    SizeConfig
                                                        .heightMultiplier),
                                            child: data[index].status ==
                                                    "Pending"
                                                ? Text(
                                                    data[index].status == null
                                                        ? "loading.."
                                                        : data[index].status,
                                                    style: TextStyle(
                                                      fontFamily: fontandcolor
                                                          .fontfaimly,
                                                      fontSize: 1.5 *
                                                          SizeConfig
                                                              .textMultiplier,
                                                    ),
                                                  )
                                                : Container(
                                                    height: 1,
                                                    width: 1,
                                                  ),
                                          ),
                                          SizedBox(
                                            height: (5 / 8.148314082864863) *
                                                SizeConfig.heightMultiplier,
                                          ),
                                          data[index].unRead.toString() == "" ||
                                                  data[index].unRead == 0
                                              ? Container(
                                                  height: 1.0,
                                                  width: 1.0,
                                                )
                                              : data[index].createdBy.sId ==
                                                      user.sId
                                                  ? Container(
                                                      height: 1,
                                                      width: 1,
                                                    )
                                                  : CircleAvatar(
                                                      backgroundColor:
                                                          Colors.green,
                                                      child: Center(
                                                        child: Text(
                                                            data[index]
                                                                .unRead
                                                                .toString(),
                                                            textAlign: TextAlign
                                                                .center,
                                                            style: TextStyle(
                                                                fontSize: 9.0)),
                                                      ),
                                                      maxRadius: (12 /
                                                              8.148314082864863) *
                                                          SizeConfig
                                                              .heightMultiplier,
                                                    ),
                                        ],
                                      ),
                                    ),
                                  ),
                                  Positioned(
                                    child: TextForInbox(
                                        "", formattedTime1, context),
                                    bottom: 5,
                                    right: 10,
                                  ),
                                ],
                              ),
                            ),
                          ),
                          SizedBox(
                            width: MediaQuery.of(context).size.width / 1.1,
                            child: Divider(
                              thickness: 1,
                              height: 3,
                            ),
                          ),
                        ],
                      );
                    }

                    if (widget.page == "ChatTileCat" &&
                        data[index].status == "Rejected" &&
                        data.length == 1 &&
                        data[index].conversationCreatedBy != user.sId) {
                      return Container(
                        padding: EdgeInsets.all(8.0),
                        alignment: Alignment.center,
                        child: Center(
                          child: Text(
                              'You have one rejected user in inquires.Please go to send Inquiries to Accept him Again',
                              style: style.MontserratMedium(
                                  color: font.buttonColor,
                                  fontSize: size.convert(context, 18))),
                        ),
                      );
                    }
                    if (widget.page == "ChatTileCat") {
                      return data[index].status == "Rejected" ||
                              data[index].status == "Pending"
                          ? Container()
                          : _data();
                    } else {
                      return data[index].status == widget.status
                          ? _data()
                          : Container();
                    }
                  },
                ),
              );
      }
      return Center(child: LoadingShimmerWidget(context));
    });
  }
}
