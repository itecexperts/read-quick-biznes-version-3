import 'package:biznes/bloc/inbox/ChatCategoriestile.dart/Categoriesbloc.dart';
import 'package:biznes/bloc/inbox/ChatCategoriestile.dart/categoriesEvents.dart';
import 'package:biznes/bloc/inbox/ChatCategoriestile.dart/categoriesState.dart';
import 'package:biznes/helper/login_helper.dart';
import 'package:biznes/model/categoryinbox.dart';
import 'package:biznes/repeatedWigets/buyerseller.dart';
import 'package:biznes/repeatedWigets/chatCategoriesCard.dart';
import 'package:biznes/repeatedWigets/loadingWidget.dart';
import 'package:biznes/res/color.dart';
import 'package:biznes/res/size.dart';
import 'package:biznes/res/style.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:biznes/services/server.dart';
import 'chat_inquiries.dart';

class ChatCategoriesTile extends StatefulWidget {
  @override
  _ChatCategoriesTileState createState() => _ChatCategoriesTileState();
}

class _ChatCategoriesTileState extends State<ChatCategoriesTile> {
  getcategoriesMessages() async {
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    context
        .bloc<CategoriesInboxBloc>()
        .add(FetchMessagesbyCategories(userId: _prefs.getString("userid")));
  }

  @override
  void initState() {
    getcategoriesMessages();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var selectedTIle = Provider.of<LoginHelper>(context);
    var userId = Provider.of<LoginHelper>(context).user.sId;

    return Scaffold(
      body: BlocBuilder<CategoriesInboxBloc, AllCategoriesMessageStates>(
          builder: (BuildContext context, AllCategoriesMessageStates states) {
        if (states is AllCategoriesMessageErrorState) {
          final errror = states.error;
          return Text(errror.message);
        }
        if (states is AllCategoriesMessageStatesLoadedState) {
          CategoryInbox categoryInbox = states.categoryInbox;
          return RefreshIndicator(
            color: buttonColor,
            onRefresh: () => getcategoriesMessages(),
            child: Container(
              decoration: BoxDecoration(
                color: Colors.white,
                image: DecorationImage(
                  fit: BoxFit.cover,
                  image: AssetImage("assets/images/newback.jpg"),
                ),
              ),
              child: Column(
                children: [
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    children: [
                      SizedBox(
                        width: 5,
                      ),
                      Text(
                        "Chat Filter by",
                        style: style.MontserratMedium(
                            fontSize: size.convert(context, 12)),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Expanded(
                        child: BuyerSellerContainer(
                            context: context,
                            isselected: selectedTIle.isSelected1,
                            onTap: () {
                              selectedTIle.setIsSelectedOne(true);
                            },
                            title: 'Buyer'),
                      ),
                      Expanded(
                        child: BuyerSellerContainer(
                            context: context,
                            isselected: selectedTIle.isSelected2,
                            onTap: () {
                              selectedTIle.setIsSelectedTwo(true);
                            },
                            title: 'Seller'),
                      ),
                      Expanded(
                        child: BuyerSellerContainer(
                            context: context,
                            isselected: selectedTIle.isSelected3,
                            onTap: () {
                              selectedTIle.setIsSelectedThree(true);
                            },
                            title: 'All'),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Expanded(
                    child: ListView.builder(
                        padding: EdgeInsets.zero,
                        itemCount: selectedTIle.isSelected3
                            ? categoryInbox.allMessages.length == 0
                                ? categoryInbox.buyerArray.length
                                : categoryInbox.buyerArray.length
                            : selectedTIle.isSelected2
                                ? categoryInbox.buyerArray.length
                                : categoryInbox.allMessages.length,
                        itemBuilder: (BuildContext context, int index) {
                          return Padding(
                            padding: const EdgeInsets.symmetric(vertical: 8.0),
                            child: selectedTIle.getisSelected3
                                ? Column(
                                    children: [
                                      categoryInbox.buyerArray.length == 0
                                          ? Container()
                                          : GestureDetector(
                                              onTap: () {
                                                Navigator.of(context).push(
                                                    MaterialPageRoute(
                                                        builder: (context) =>
                                                            ChatTile(
                                                              status: "",
                                                              page:
                                                                  "ChatTileCat",
                                                              categoryId:
                                                                  categoryInbox
                                                                      .buyerArray[
                                                                          index]
                                                                      .id,
                                                              userId: userId,
                                                              roleName: "All",
                                                            )));
                                              },
                                              child: ChatCard(
                                                title: categoryInbox
                                                    .buyerArray[index].title,
                                                count: categoryInbox
                                                    .buyerArray[index]
                                                    .totalCount,
                                              ),
                                            ),
                                      SizedBox(
                                        height: 5,
                                      ),
                                      categoryInbox.allMessages.length == 0
                                          ? Container()
                                          : GestureDetector(
                                              onTap: () {
                                                Navigator.of(context).push(
                                                    MaterialPageRoute(
                                                        builder: (context) =>
                                                            ChatTile(
                                                              status: "",
                                                              page:
                                                                  "ChatTileCat",
                                                              categoryId:
                                                                  categoryInbox
                                                                      .allMessages[
                                                                          index]
                                                                      .id,
                                                              userId: userId,
                                                              roleName: "All",
                                                            )));
                                              },
                                              child: ChatCard(
                                                title: categoryInbox
                                                    .allMessages[index].title,
                                                count: categoryInbox
                                                    .allMessages[index]
                                                    .totalCount,
                                              ),
                                            )
                                    ],
                                  )
                                : GestureDetector(
                                    onTap: () async {
                                      if (await checkInternetConnection()) {
                                        Navigator.of(context)
                                            .push(MaterialPageRoute(
                                                builder: (context) => ChatTile(
                                                      status: "",
                                                      page: "ChatTileCat",
                                                      categoryId: selectedTIle
                                                              .isSelected2
                                                          ? categoryInbox
                                                              .buyerArray[index]
                                                              .id
                                                          : categoryInbox
                                                              .allMessages[
                                                                  index]
                                                              .id,
                                                      userId: userId,
                                                      roleName: selectedTIle
                                                              .getisSelected1
                                                          ? "Seller"
                                                          : selectedTIle
                                                                  .getisSelected2
                                                              ? "Buyer"
                                                              : "All",
                                                    )));
                                      } else {
                                        // Get.snackbar("No Connection", "No Internet Connection");
                                      }
                                    },
                                    child: ChatCard(
                                      title: selectedTIle.isSelected2
                                          ? categoryInbox
                                              .buyerArray[index].title
                                          : categoryInbox
                                              .allMessages[index].title,
                                      count: selectedTIle.isSelected2
                                          ? categoryInbox
                                              .buyerArray[index].totalCount
                                          : categoryInbox
                                              .allMessages[index].totalCount,
                                    ),
                                  ),
                          );
                        }),
                  ),
                ],
              ),
            ),
          );
        }
        return Center(
          child: ShimmerCategoriesInboxLoading(context),
        );
      }),
    );
  }
}
