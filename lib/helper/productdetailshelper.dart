import 'package:flutter/cupertino.dart';
import 'package:biznes/model/single_Product.dart';

class ProductDetailsHelperData extends ChangeNotifier {
  SingleProductdetails _singleProductdetails;
  SingleProductdetails get singleproduct => _singleProductdetails;
  void productdetailsfunction(SingleProductdetails singleProductList) {
    _singleProductdetails = singleProductList;
    notifyListeners();
  }
}
