import 'package:flutter/cupertino.dart';
import 'package:biznes/model/notifications.dart';

class Notificationgetting extends ChangeNotifier {
  int unreadcount = 0;
  bool ismessagesend = false;
  String currentPage = "Page3";
  int selectedIndex = 2;

  int get unreadCount => unreadcount;
  String get currentpage => currentPage;
  int get selectIndex => selectedIndex;
  bool get ismessagesent => ismessagesend;

  setmessagetrue({@required bool value}) {
    ismessagesend = value;
    notifyListeners();
  }

  void updatenotification(
    currentpage,
    selectedindex,
  ) {
    currentPage = currentpage;
    selectedIndex = selectedindex;
    notifyListeners();
  }

  setunreadcount(count) {
    unreadcount = count;
    notifyListeners();
  }

  NewNotification _newNotificationdata;
  NewNotification get newnotification => _newNotificationdata;
  void gettingNotificationData(NewNotification value) {
    _newNotificationdata = value;
    notifyListeners();
  }
}
