import 'package:flutter/material.dart';
import 'package:biznes/model/conversationmodel.dart';

class NewMessagesHlper extends ChangeNotifier {
  bool isblockingUser = true;
  bool isloading = true;
  List<NewMessages> _newmessages;
  bool get getIsBlockingUser => isblockingUser;
  List<NewMessages> get newmessages => _newmessages;
  setisBlockingUser(bool setblock) {
    print("is calling this function for setting it true");
    isblockingUser = setblock;
    print(isblockingUser);
    notifyListeners();
  }

  void setNewMessages(List<NewMessages> newmessage) {
    _newmessages = newmessage;
    isloading = false;
    notifyListeners();
  }
}
