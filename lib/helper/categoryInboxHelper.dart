import 'package:flutter/material.dart';
import 'package:biznes/model/categoryinbox.dart';
class CategoryHelper extends ChangeNotifier{
  bool isloading=true;
  CategoryInbox _categoryInbox;
  CategoryInbox get categoryInbox=>_categoryInbox;
  void setCategoryInbox(CategoryInbox categoryInbox){
    _categoryInbox=categoryInbox;
    isloading=false;
    notifyListeners();
  }
}