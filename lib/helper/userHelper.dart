import 'dart:convert';

import 'package:biznes/model/UserModel.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class UserHelper with ChangeNotifier {
  User _user;
  String _token;

  String get token => _token;

  User get user => _user;

  set token(String t) {
    _token = t;
    notifyListeners();
  }

  set user(User u) {
    _user = u;
    notifyListeners();
  }

  bool _isLoggedIn = false;

  bool get isLoggedIn => isLoggedIn;

  set isLoggedIn(bool value) {
    _isLoggedIn = value;
    notifyListeners();
  }

  loginHelper() {
    isUserLogin().then((loggedIn) {
      if (loggedIn) {
        _isLoggedIn = true;
        getUser();
      } else {
        _isLoggedIn = false;
      }
    });
  }

  Future<bool> isUserLogin() async {
    try {
      SharedPreferences loginPref = await SharedPreferences.getInstance();
      var value = loginPref.getBool("is_login");
      if (value == null) {
        return false;
      } else {
        return true;
      }
    } catch (error) {
      print(error.message);
      return false;
    }
  }

  onLogin(String t, User user) async {
    _isLoggedIn = true;
    _user = user;
    _token = t;
    saveUser();
  }

  onSignOut() async {
    SharedPreferences signOutPref = await SharedPreferences.getInstance();
    signOutPref.remove("user");
    signOutPref.remove("token");
    signOutPref.remove("is_login");
    _isLoggedIn = false;
    notifyListeners();
  }

  saveUser() async {
    SharedPreferences savePref = await SharedPreferences.getInstance();
    savePref.setBool("is_login", true);
    savePref.setString("token", _token);
    savePref.setString("user", jsonEncode(_user.toJson()));
    notifyListeners();
  }

  getUser() async {
    print("getting user");
    SharedPreferences getPref = await SharedPreferences.getInstance();
    String user = getPref.getString("user");
    _user = User.fromJson(jsonDecode(user));
  }
}
