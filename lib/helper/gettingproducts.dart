import 'package:biznes/model/companycurrenproducts.dart';
import 'package:biznes/model/productsbyid.dart';
import 'package:flutter/cupertino.dart';
class GettingProducts extends ChangeNotifier{
List<Products> _products;
List<Products> get products=>_products;
set products(List<Products> p){
  _products = p;       
  notifyListeners();
}
 onLogIn(List<Products> products) async {
    _products = products;
    notifyListeners();
  }
  List<CompanyProducts> _comanyproducts;
List<CompanyProducts> get companyproducts=>_comanyproducts;
set companyproducts(List<CompanyProducts> p){
  _comanyproducts = p;       
  notifyListeners();
}
 oncompanycurrentproduct(List<CompanyProducts> companyproducts) async {
    _comanyproducts = companyproducts;
    notifyListeners();
  }
 }