import 'package:biznes/model/caegoryModel.dart';
import 'package:flutter/material.dart';

class CategoriesData extends ChangeNotifier {
  List<CategoriesModel> _categories = [];
  List<CategoriesModel> get categories => _categories;
  void setCategories(List<CategoriesModel> data) {
    _categories = data;
    notifyListeners();
  }
}
