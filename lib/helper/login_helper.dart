import 'dart:convert';
import 'package:biznes/model/userModel.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

LoginHelper lofginher;

class LoginHelper with ChangeNotifier {
  bool isSelected1 = true;
  bool isSelected2 = false;
  bool isSelected3 = false;
  bool isLoadinIndicator = false;
  bool isselectedSendInquires = true;
  bool isselectedRecieveInquires = false;
  bool isshowDropdown = false;
  bool get getIsShowDropDown => isshowDropdown;
  bool get getisselectedSendInquires => isselectedSendInquires;
  bool get getisselectedRecieveInquires => isselectedRecieveInquires;
  bool get loadingIndicator => isLoadinIndicator;
  setIsShowDropDown(bool isShowDropDown) {
    isshowDropdown = isShowDropDown;
    notifyListeners();
  }

  setLoadingIndicator(bool setLoading) {
    isLoadinIndicator = setLoading;
    notifyListeners();
  }

  setIsselectedSendInquires(bool isselected) {
    isselectedSendInquires = isselected;
    notifyListeners();
  }

  setIsselectedRecieveInquires(bool isselected) {
    isselectedRecieveInquires = isselected;
    notifyListeners();
  }

  bool _isvisible = false;
  bool get isvisible => _isvisible;
  set loading(bool isvisible) {
    _isvisible = isvisible;
    notifyListeners();
  }

  bool get getisSelected1 => isSelected1;
  bool get getisSelected2 => isSelected2;
  bool get getisSelected3 => isSelected3;

  setIsSelectedOne(bool isSelectedone) {
    isSelected1 = isSelectedone;
    isSelected2 = false;
    isSelected3 = false;
    notifyListeners();
  }

  setIsSelectedTwo(bool isSelectedTwo) {
    isSelected2 = isSelectedTwo;
    isSelected1 = false;
    isSelected3 = false;
    notifyListeners();
  }

  setIsSelectedThree(bool isSelectedThree) {
    isSelected3 = isSelectedThree;
    isSelected1 = false;
    isSelected2 = false;
    notifyListeners();
  }

  User _user;
  List<User> _users;
  Widget widget;
  String _token;
  String get token => _token;
  User get user => _user;
  List<User> get users => _users;
  set users(List<User> user) {
    _users = user;
    notifyListeners();
  }

  set token(String t) {
    _token = t;
    notifyListeners();
  }

  set user(User u) {
    _user = u;
    notifyListeners();
  }

  bool _isshowbottom = true;
  bool _isLoggedIn = false;

  bool get isLoggedIn => _isLoggedIn;
  bool get isshowbottom => _isshowbottom;
  set isLoggedIn(bool value) {
    _isLoggedIn = value;
    notifyListeners();
  }

  set isshowbottom(bool value) {
    _isshowbottom = value;
    notifyListeners();
  }

  LoginHelper() {
    isUserLogin().then((loggedIn) {
      if (loggedIn) {
        _isLoggedIn = true;
        print("saving");
        getUser();
      } else {
        isLoggedIn = false;
      }
    });
    notifyListeners();
  }

  Future<bool> isUserLogin() async {
    try {
      SharedPreferences pref = await SharedPreferences.getInstance();
      String userString = pref.getString('user');
      if (userString == null && userString == "") {
        return false;
      } else {
        return true;
      }
    } catch (err) {
      print(err);
      return false;
    }
  }

  onLogIn(String t, User user) async {
    _isLoggedIn = true;
    _user = user;
    _token = t;
    saveUser();
  }

  onprofilechange(User user) async {
    _isLoggedIn = true;
    _user = user;
    saveUser();
  }

  onSignOut() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove('user');
    prefs.remove("token");
    prefs.remove("is_login");
    _isLoggedIn = false;
    notifyListeners();
  }

  saveUser() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString("token", _token);
    prefs.setBool("is_login", true);
    prefs.setString('user', jsonEncode(_user.toJson()));
    notifyListeners();
  }

  getUser() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String userString = prefs.getString('user');
    if (userString == null) {
      return;
    } else {
      _user = User.fromJson(jsonDecode(userString));
    }
  }
}
