import 'dart:ui';

import 'package:biznes/login_UI/newpincode.dart';
import 'package:biznes/size_config.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter/material.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:biznes/services/register_logic.dart';
import 'package:biznes/products_Detail/extra.dart';
import 'package:toast/toast.dart';

class ConfirmationPinCode extends StatefulWidget {
  final int phoneNumber;
  ConfirmationPinCode({this.phoneNumber});
  @override
  _ConfirmationPinCodeState createState() => _ConfirmationPinCodeState();
}

class _ConfirmationPinCodeState extends State<ConfirmationPinCode> {
  var formKey = GlobalKey<FormState>();
  String phoneNumber;
  int verificationCode;
  bool isloading = false;
  bool validateAndSave() {
    final form = formKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    } else {
      return false;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Scaffold(body: OrientationBuilder(
          builder: (context, orientation) {
            return Container(
              decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage("assets/images/app_backgroudnn.jpg"),
                    fit: BoxFit.cover),
              ),
              child: Column(
                children: <Widget>[
                  SizedBox(
                    height: orientation == Orientation.portrait
                        ? (230 / 8.148314082864863) *
                            SizeConfig.heightMultiplier
                        : (80 / 8.148314082864863) *
                            SizeConfig.heightMultiplier,
                    child: Center(
                      child: Image.asset(
                        "assets/images/logo.png",
                        height: (170 / 8.148314082864863) *
                            SizeConfig.heightMultiplier,
                        width: (270 / 4.853932272197492) *
                            SizeConfig.widthMultiplier,
                      ),
                    ),
                  ),
                  Expanded(
                    child: Container(
                      decoration: BoxDecoration(
                          boxShadow: [
                            BoxShadow(
                              color: Color(0xff69BEF7),
                              spreadRadius: 5,
                              blurRadius: 7,
                              offset:
                                  Offset(0, 3), // changes position of shadow
                            ),
                          ],
                          color: Colors.white,
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(44.5),
                            topRight: Radius.circular(44.5),
                          )),
                      child: ListView(
                        children: <Widget>[
                          Center(
                            child: Padding(
                              padding: const EdgeInsets.only(
                                  left: 8.0, right: 8.0, top: 8.0),
                              child: Text(
                                "Confirmation",
                                style: TextStyle(
                                  color: Color.fromRGBO(57, 76, 129, 1),
                                  fontSize: orientation == Orientation.portrait
                                      ? (35 / 8.148314082864863) *
                                          SizeConfig.heightMultiplier
                                      : (20 / 8.148314082864863) *
                                          SizeConfig.heightMultiplier,
                                ),
                              ),
                            ),
                          ),
                          Center(
                            child: Padding(
                              padding: const EdgeInsets.all(2.0),
                              child: Container(
                                  height: orientation == Orientation.portrait
                                      ? (150 / 8.148314082864863) *
                                          SizeConfig.heightMultiplier
                                      : (80 / 8.148314082864863) *
                                          SizeConfig.heightMultiplier,
                                  width: orientation == Orientation.portrait
                                      ? (150 / 4.853932272197492) *
                                          SizeConfig.widthMultiplier
                                      : (80 / 4.853932272197492) *
                                          SizeConfig.widthMultiplier,
                                  child: SvgPicture.asset("assets/images/phones.svg")),
                            ),
                          ),
                          Center(
                            child: FittedBox(
                              child: Text(
                                'Enter Verification Code',
                                style: TextStyle(
                                  fontSize: orientation == Orientation.portrait
                                      ? (15 / 8.148314082864863) *
                                          SizeConfig.heightMultiplier
                                      : (10 / 8.148314082864863) *
                                          SizeConfig.heightMultiplier,
                                ),
                              ),
                            ),
                          ),
                          SizedBox(
                            height: orientation == Orientation.portrait
                                ? (18 / 8.148314082864863) *
                                    SizeConfig.heightMultiplier
                                : (6 / 8.148314082864863) *
                                    SizeConfig.heightMultiplier,
                          ),
                          SizedBox(
                            height: orientation == Orientation.portrait
                                ? (12 / 8.148314082864863) *
                                    SizeConfig.heightMultiplier
                                : (4 / 8.148314082864863) *
                                    SizeConfig.heightMultiplier,
                          ),
                          Padding(
                            padding:
                                const EdgeInsets.symmetric(horizontal: 88.0),
                            child: Container(
                                height: (60 / 8.148314082864863) *
                                    SizeConfig.heightMultiplier,
                                padding: EdgeInsets.only(
                                    left: 12.0,
                                    right: 12.0,
                                    top: 2.0,
                                    bottom: 2.0),
                                decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.circular(15.0),
                                    border: Border.all(color: Colors.black)),
                                child: PinCodeTextField(
                                  appContext: context,
                                  onChanged: (val) {},
                                  length: 4,
                                  onCompleted: (value) {
                                    verificationCode = int.parse(value);
                                  },
                                  keyboardType: TextInputType.number,
                                  obscureText: true,
                                  // obsecureText: true,
                                  // textInputType: TextInputType.number,
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                  animationType: AnimationType.fade,
                                  pinTheme: PinTheme(
                                    shape: PinCodeFieldShape.underline,
                                    borderRadius: BorderRadius.circular(5),
                                    fieldHeight: (50 / 8.148314082864863) *
                                        SizeConfig.heightMultiplier,
                                    fieldWidth: (30 / 4.853932272197492) *
                                        SizeConfig.widthMultiplier,
                                    activeFillColor: Colors.blue,
                                  ),
                                )),
                          ),
                          SizedBox(
                            height: orientation == Orientation.portrait
                                ? (40 / 8.148314082864863) *
                                    SizeConfig.heightMultiplier
                                : (10 / 8.148314082864863) *
                                    SizeConfig.heightMultiplier,
                          ),
                          Center(
                            child: MaterialButton(
                              onPressed: () {
                                setState(() {
                                  isloading = true;
                                });
                                resetPincode(
                                        code: verificationCode.toString(),
                                        contactNumber:
                                            widget.phoneNumber.toString(),
                                        context: context)
                                    .then((value) {
                                  setState(() {
                                    isloading = false;
                                  });
                                  if (value["Success"] == true) {
                                    Navigator.of(context).push(
                                        MaterialPageRoute(
                                            builder: (context) => NewPinCode(
                                                userid: value['message'])));
                                  } else {
                                    setState(() {
                                      isloading = false;
                                    });
                                    Toast.show(value["message"], context,
                                        gravity: Toast.TOP);
                                  }
                                  print("return value is $value");
                                });
                              },
                              child: Container(
                                alignment: Alignment.center,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(25.0),
                                  gradient: LinearGradient(
                                    begin: Alignment.topCenter,
                                    end: Alignment.bottomCenter,
                                    colors: [
                                      Color(0xFF69bef7),
                                      Color(0xFF2a65ff),
                                    ],
                                  ),
                                  boxShadow: [
                                    BoxShadow(
                                      color: Color(0xFF69bef7),
                                      spreadRadius: 1,
                                      blurRadius: 12,
                                      offset: Offset(0, 2),
                                    ),
                                  ],
                                ),
                                width: (370 / 4.853932272197492) *
                                    SizeConfig.widthMultiplier,
                                height: 50,
                                child: Text(
                                  "Confirm",
                                  style: TextStyle(
                                      fontSize: (22 / 8.148314082864863) *
                                          SizeConfig.heightMultiplier,
                                      color: Colors.white),
                                ),
                              ),
                              shape: StadiumBorder(),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            );
          },
        )),
        isloading ? stack() : Container(),
      ],
    );
  }
}
