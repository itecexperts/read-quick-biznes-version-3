import 'dart:ui';
import 'package:biznes/login_UI/signIn.dart';
import 'package:biznes/repeatedWigets/filledButton.dart';
import 'package:biznes/res/color.dart';
import 'package:biznes/services/register_logic.dart';
import 'package:biznes/size_config.dart';
import 'package:biznes/products_Detail/extra.dart';
import 'package:biznes/res/size.dart';
import 'package:biznes/res/style.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:page_transition/page_transition.dart';

class ForgotNewPassword extends StatefulWidget {
  final String userid;
  ForgotNewPassword({this.userid});
  @override
  _ForgotNewPasswordState createState() => _ForgotNewPasswordState();
}

class _ForgotNewPasswordState extends State<ForgotNewPassword> {
  var formKey = GlobalKey<FormState>();

  String newpassword;
  String confirmpassword;
  bool isPasswordShow1 = false;
  bool isPasswordShow2 = false;
  bool validateAndSave() {
    final form = formKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    } else {
      return false;
    }
  }

  Widget buildForm({Orientation orientation}) {
    return Padding(
      padding: EdgeInsets.all(12.2),
      child: Form(
          key: formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 5, vertical: 10),
                child: Text(
                  "Enter New Password",
                      style: style.MontserratRegular(
                          fontSize: size.convert(context, 12)),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 10.0, right: 10.0),
                child: Stack(
                  children: [
                    material(),
                    TextFormField(
                      onSaved: (value) {
                        newpassword = value;
                      },
                      obscureText: !isPasswordShow1,
                      validator: (value) {
                        if (value.length == 0) {
                          return 'required field';
                        }
                        return null;
                      },
                      decoration: InputDecoration(
                        focusedErrorBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(25),
                          borderSide: BorderSide(color: Colors.red),
                        ),
                        errorBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(25),
                          borderSide: BorderSide(color: Colors.red),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(25),
                          borderSide: BorderSide(color: Color(0xff69BEF7)),
                        ),
                        hintStyle: TextStyle(
                          fontSize: 2 * SizeConfig.textMultiplier,
                        ),
                        contentPadding: EdgeInsets.symmetric(
                            vertical: 10.0, horizontal: 10.0),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(30.0),
                        ),
                        hintText: 'Enter your new password',
                        prefixIcon: Icon(
                          Icons.lock,
                          size: 15,
                        ),
                        suffixIcon: InkWell(
                            onTap: () {
                              setState(() {
                                isPasswordShow1 = !isPasswordShow1;
                              });
                            },
                            child: isPasswordShow1
                                ? Icon(
                                    Icons.visibility_off,
                                    size: 15,
                                  )
                                : Icon(
                                    Icons.visibility,
                                    size: 15,
                                  )),
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 5, vertical: 10),
                child: Text(
                  "Re-Enter New password",
                  style: style.MontserratRegular(
                      fontSize: size.convert(context, 12)),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 10.0, right: 10.0),
                child: Stack(
                  children: [
                    material(),
                    TextFormField(
                      validator: (value) {
                        if (value.isEmpty && value.length < 6) {
                          return 'required field';
                        }
                        return null;
                      },
                      onSaved: (value) {
                        confirmpassword = value;
                      },
                      decoration: InputDecoration(
                        focusedErrorBorder: outlineInputBorder(),
                        errorBorder: outlineInputBorder(),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(25),
                          borderSide: BorderSide(color: Color(0xff69BEF7)),
                        ),
                        hintStyle: TextStyle(
                          fontSize: 2 * SizeConfig.textMultiplier,
                        ),
                        contentPadding: EdgeInsets.symmetric(
                            vertical: 10.0, horizontal: 10.0),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(30.0),
                        ),
                        hintText: 'Re-Enter password',
                        prefixIcon: Icon(
                          Icons.lock,
                          size: 15,
                        ),
                        suffixIcon: InkWell(
                            onTap: () {
                              setState(() {
                                isPasswordShow2 = !isPasswordShow2;
                              });
                            },
                            child: isPasswordShow2
                                ? Icon(
                                    Icons.visibility_off,
                                    size: 15,
                                  )
                                : Icon(
                                    Icons.visibility,
                                    size: 15,
                                  )),
                      ),
                      obscureText: !isPasswordShow2,
                    ),
                  ],
                ),
              ),
            ],
          )),
    );
  }

  TextStyle textStyle() {
    return TextStyle(
        color: Colors.black87,
        fontWeight: FontWeight.bold,
        fontSize: (13.9 / 8.148314082864863) * SizeConfig.heightMultiplier);
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusScope.of(context).requestFocus(new FocusNode()),
      child: Scaffold(
        body: newPassword(),
      ),
    );
  }

  oldBody() {
    return OrientationBuilder(
      builder: (context, orientation) {
        return Container(
          decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage("assets/images/app_backgroudnn.jpg"),
                fit: BoxFit.cover),
          ),
          child: Column(
            children: <Widget>[
              SizedBox(
                height: orientation == Orientation.portrait
                    ? (230 / 8.148314082864863) * SizeConfig.heightMultiplier
                    : (80 / 8.148314082864863) * SizeConfig.heightMultiplier,
                child: Center(
                  child: Image.asset(
                    "assets/images/logo.png",
                    height:
                        (130 / 8.148314082864863) * SizeConfig.heightMultiplier,
                    width:
                        (220 / 4.853932272197492) * SizeConfig.widthMultiplier,
                  ),
                ),
              ),
              Expanded(
                child: Container(
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(44.5),
                        topRight: Radius.circular(44.5),
                      )),
                  child: ListView(
                    children: <Widget>[
                      Align(
                        alignment: Alignment(0.0, 25),
                        child: Text(
                          "Enter New Password",
                          style: TextStyle(
                            color: Color.fromRGBO(57, 76, 129, 1),
                            fontSize: orientation == Orientation.portrait
                                ? (26.8 / 8.148314082864863) *
                                    SizeConfig.textMultiplier
                                : (30.8 / 8.148314082864863) *
                                    SizeConfig.textMultiplier,
                          ),
                        ),
                      ),
                      Center(child: SvgPicture.asset('assets/images/lock.svg')),
                      buildForm(orientation: orientation),
                      Center(
                        child: MaterialButton(
                          onPressed: () {
                            FocusScope.of(context)
                                .requestFocus(new FocusNode());
                            if (validateAndSave()) {
                              resetnewpasswordafterotp(newpassword,
                                      confirmpassword, widget.userid, context)
                                  .then((value) {
                                if (value['Success'] = true) {
                                  showDialog(
                                      context: context,
                                      builder: (context) {
                                        return WillPopScope(
                                          onWillPop: () async {
                                            return Future.value(false);
                                          },
                                          child: AlertDialog(
                                            shape: RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(
                                                        20.0)),
                                            title: Text(
                                              "Password Changed Successfully",
                                              style: style.MontserratRegular(
                                                  fontSize: size.convert(context, 12)),
                                            ),
                                            content: Text(
                                              "${value['message']}",
                                              style: TextStyle(
                                                fontFamily: fontfaimlyregular,
                                              ),
                                            ),
                                            actions: <Widget>[
                                              FlatButton(
                                                onPressed: () {
                                                  Navigator.pop(context);
                                                  Navigator.pushAndRemoveUntil(
                                                      context,
                                                      PageTransition(
                                                        child: SignIn(),
                                                        type: PageTransitionType
                                                            .bottomToTop,
                                                        duration: Duration(
                                                            milliseconds: 500),
                                                      ),
                                                      (route) => false);
                                                },
                                                child: Text(
                                                  "OK",
                                                  style: TextStyle(
                                                    fontFamily:
                                                        fontfaimlyregular,
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        );
                                      });
                                } else {
                                  print(value['message']);
                                }
                              });
                            }
                          },
                          child: Container(
                            alignment: Alignment.center,
                            decoration: decoration(),
                            width: ((500.3932272197492 / 1.5) /
                                    4.853932272197492) *
                                SizeConfig.widthMultiplier,
                            height: 50,
                            child: Text(
                              "DONE",
                              style: TextStyle(
                                  fontSize: (22.2 / 8.148314082864863) *
                                      SizeConfig.textMultiplier,
                                  color: Colors.white),
                            ),
                          ),
                          shape: StadiumBorder(),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  newPassword() {
    return SingleChildScrollView(
      child: Column(
        children: [
          Container(
            //color: Colors.red,
            margin: EdgeInsets.symmetric(horizontal: size.convert(context, 16)),
            child: Column(
              children: [
                SizedBox(height: size.convert(context, 40)),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    InkWell(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: Icon(
                        Icons.arrow_back_sharp,
                        color: Colors.black,
                      ),
                    ),
                    Container(),
                  ],
                ),
                // SizedBox(height: size.convert(context, 20),),
//                Container(
//                  child: Image.asset(
//                    "assets/images/logo.png",
//                    width: size.convertWidth(context, 240),
//                    height: size.convert(context, 101),
//                  ),
//                ),
              ],
            ),
          ),
          SizedBox(
            height: size.convert(context, 15),
          ),
          Container(
            padding: EdgeInsets.symmetric(vertical: 20),
            margin: EdgeInsets.symmetric(
                horizontal: size.convertWidth(context, 21)),
            //alignment: Alignment.bottomLeft,
            decoration: BoxDecoration(
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                  color: Color(0xffd4d4d4),
                  blurRadius: 6,
                  offset: Offset(0, 3),
                ),
              ],
              borderRadius: BorderRadius.circular(size.convert(context, 20)),
            ),
            child: Column(
              children: <Widget>[
                Container(
                  child: SvgPicture.asset(
                    "assets/icons/loginIcon.svg",
                    width: size.convertWidth(context, 250),
                    height: size.convert(context, 150),
                  ),
                ),
                Center(
                  child: Padding(
                    padding: const EdgeInsets.only(top: 12.0),
                    child: Text(
                      "New Password ",
                      style: style.QuicksandBold(
                          fontSize: size.convert(context, 30),
                          color: Color(0xff394c81)),
                    ),
                  ),
                ),
                Container(
                  child: RichText(
                    textAlign: TextAlign.center,
                    text: TextSpan(
                        text: "Please select your new password",
                        style: style.MontserratRegular(
                            fontSize: size.convert(context, 12))),
                  ),
                ),
                SizedBox(
                  height: size.convert(context, 10),
                ),
                buildForm(orientation: MediaQuery.of(context).orientation),
                SizedBox(
                  height: size.convert(context, 20),
                ),
                filledButton(
                  txt: "Confirm",
                  onTap: () {
                    FocusScope.of(context).requestFocus(new FocusNode());
                    if (validateAndSave()) {
                      resetnewpasswordafterotp(newpassword, confirmpassword,
                              widget.userid, context)
                          .then((value) {
                        if (value['Success'] = true) {
                          showDialog(
                              context: context,
                              builder: (context) {
                                return WillPopScope(
                                  onWillPop: () async {
                                    return Future.value(false);
                                  },
                                  child: AlertDialog(
                                    shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(20.0)),
                                    title: Text("Successful !",
                                      style: style.QuicksandBold(
                                          fontSize: size.convert(context, 30),
                                          color: Color(0xff394c81)),),
                                    content: Text("${value['message']}",style: style.MontserratRegular(
                                        fontSize: size.convert(context, 12)),),
                                    actions: <Widget>[
                                      FlatButton(
                                        onPressed: () {
                                          Navigator.pop(context);
                                          Navigator.pushAndRemoveUntil(
                                              context,
                                              PageTransition(
                                                child: SignIn(),
                                                type:
                                                    PageTransitionType.bottomToTop,
                                                duration:
                                                    Duration(milliseconds: 500),
                                              ),
                                              (route) => false);
                                        },
                                        child: Text("OK"),
                                      ),
                                    ],
                                  ),
                                );
                              });
                        } else {
                          print(value['message']);
                        }
                      });
                    }
                  },
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
