import 'dart:convert';
import 'dart:ui';
import 'package:biznes/res/color.dart';
import 'package:biznes/services/server.dart';
import 'package:biznes/login_UI/forgotPassword.dart';
import 'package:biznes/repeatedWigets/filledButton.dart';
import 'package:biznes/size_config.dart';
import 'package:biznes/res/size.dart';
import 'package:biznes/res/style.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:biznes/model/regiterModel.dart';
import 'package:page_transition/page_transition.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:http/http.dart' as http;
// import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter_svg/flutter_svg.dart';

class PinCode extends StatefulWidget {
  final RegisterModel regModel;
  final int countryCode;
  final String phone;
  PinCode({this.regModel, this.countryCode, this.phone});
  @override
  _PinCodeState createState() => _PinCodeState();
}

class _PinCodeState extends State<PinCode> {
  FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  String curruntPinCode, rePinCode;
  Orientation orientation;
  String token;
  bool isvisible = false;
  @override
  void initState() {
    super.initState();
    Future.delayed(Duration.zero, () {
      this.firebaseCloudMessagingListeners(context);
    });
  }

  void firebaseCloudMessagingListeners(BuildContext context) {
    _firebaseMessaging.getToken().then((deviceToken) {
      print("Firebase Device token: $deviceToken");
      setState(() {
        token = deviceToken;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: selectPinCode(),
    );
  }

  selectPinCode() {
    return Stack(
      children: [
        SingleChildScrollView(
          child: Column(
            children: [
              Container(
                //color: Colors.red,
                margin:
                    EdgeInsets.symmetric(horizontal: size.convert(context, 16)),
                child: Column(
                  children: [
                    SizedBox(height: size.convert(context, 40)),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        InkWell(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: Icon(
                            Icons.arrow_back_sharp,
                            color: Colors.black,
                          ),
                        ),
                        Container(),
                      ],
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: size.convert(context, 15),
              ),
              Container(
                padding: EdgeInsets.symmetric(vertical: 20),
                margin: EdgeInsets.symmetric(
                    horizontal: size.convertWidth(context, 21)),
                decoration: BoxDecoration(
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      color: Color(0xffd4d4d4),
                      blurRadius: 6,
                      offset: Offset(0, 3),
                    ),
                  ],
                  borderRadius:
                      BorderRadius.circular(size.convert(context, 20)),
                ),
                child: Column(
                  children: <Widget>[
                    Container(
                      child: SvgPicture.asset(
                        "assets/icons/loginIcon.svg",
                        width: size.convertWidth(context, 250),
                        height: size.convert(context, 150),
                      ),
                    ),
                    Center(
                      child: Padding(
                        padding: const EdgeInsets.only(top: 12.0),
                        child: Text(
                          "Select Your PIN",
                          style: style.QuicksandBold(
                              fontSize: size.convert(context, 30),
                              color: Color(0xff394c81)),
                        ),
                      ),
                    ),
                    Container(
                      child: RichText(
                        textAlign: TextAlign.center,
                        text: TextSpan(
                            text: "4-Digits Code shall be used to use your\n"
                                "account after registration",
                            style: style.MontserratRegular(
                                fontSize: size.convert(context, 12))),
                      ),
                    ),
                    SizedBox(
                      height: size.convert(context, 10),
                    ),
                    Center(
                      child: Text(
                        'Please enter 4-Digits PIN-CODE',
                        style: TextStyle(
                          fontFamily: fontfaimlyregular,
                          fontSize: (16.6 / 8.148314082864863) *
                              SizeConfig.heightMultiplier,
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 100.0, vertical: 9.0),
                      child: Container(
                          height:
                              ((814.8314082864863 / 8.148314082864863) / 12) *
                                  SizeConfig.heightMultiplier,
                          width:
                              ((485.3932272197492 / 4.853932272197492) / 1.8) *
                                  SizeConfig.widthMultiplier,
                          padding: EdgeInsets.only(left: 10.0, right: 10),
                          decoration: BoxDecoration(
                              color: Colors.white.withOpacity(0.7),
                              borderRadius: BorderRadius.circular(14.0),
                              border: Border.all(color: Color(0xff5e5c5c))),
                          child: PinCodeTextField(
                            textStyle: TextStyle(
                              color: Color(0xff394c81),
                            ),
                            pinTheme: PinTheme(
                              inactiveColor: Color(0xffc7c7c7),
                              shape: PinCodeFieldShape.underline,
                              borderRadius: BorderRadius.circular(5),
                              fieldHeight: (50 / 8.148314082864863) *
                                  SizeConfig.heightMultiplier,
                              fieldWidth: (30 / 4.853932272197492) *
                                  SizeConfig.widthMultiplier,
                              activeFillColor: Colors.blue,
                            ),
                            appContext: context,
                            length: 4,
                            onChanged: (value) {
                              curruntPinCode = value;
                            },
                            
                                  keyboardType: TextInputType.number,
                                  obscureText: true,
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            animationType: AnimationType.fade,
                          )),
                    ),
                    SizedBox(
                      height: (10 / 8.148314082864863) *
                          SizeConfig.heightMultiplier,
                    ),
                    Center(
                      child: Text(
                        'Please Re_enter 4-Digits PIN-CODE',
                        style: TextStyle(
                          fontFamily: fontfaimlyregular,
                          fontSize: (16.6 / 8.148314082864863) *
                              SizeConfig.heightMultiplier,
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 100.0, vertical: 9.0),
                      child: Container(
                          height:
                              ((814.8314082864863 / 8.148314082864863) / 12) *
                                  SizeConfig.heightMultiplier,
                          width:
                              ((485.3932272197492 / 4.853932272197492) / 1.8) *
                                  SizeConfig.widthMultiplier,
                          padding: EdgeInsets.only(left: 10.0, right: 10),
                          decoration: BoxDecoration(
                              color: Colors.white.withOpacity(0.7),
                              borderRadius: BorderRadius.circular(14.0),
                              border: Border.all(color: Color(0xff5e5c5c))),
                          child: PinCodeTextField(
                            textStyle: TextStyle(
                              color: Color(0xff394c81),
                            ),
                            appContext: context,
                            obscureText: true,
                            keyboardType: TextInputType.number,
                            onChanged: (v) {
                              rePinCode = v;
                            },
                            length: 4,
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            animationType: AnimationType.fade,
                            pinTheme: PinTheme(
                              inactiveColor: Color(0xffc7c7c7),
                              shape: PinCodeFieldShape.underline,
                              borderRadius: BorderRadius.circular(5),
                              fieldHeight: (50 / 8.148314082864863) *
                                  SizeConfig.heightMultiplier,
                              fieldWidth: (30 / 4.853932272197492) *
                                  SizeConfig.widthMultiplier,
                              activeFillColor: Colors.blue,
                            ),
                          )),
                    ),
                    SizedBox(
                      height: size.convert(context, 20),
                    ),
                    filledButton(
                      txt: "Confirm",
                      onTap: () async {
                        if (curruntPinCode != null &&
                            rePinCode != null &&
                            curruntPinCode == rePinCode) {
                          setState(() {
                            isvisible = true;
                          });
                          widget.regModel.roleName == 'Broker'
                              ? registerUser(context)
                              : registeruserotherthanbroker(context);
                        } else if (curruntPinCode != rePinCode) {
                          showDialog(
                              context: context,
                              builder: (context) {
                                return AlertDialog(
                                  title: Text("Oops !!!"),
                                  content: Text("your pin was not same ..."),
                                  actions: <Widget>[
                                    FlatButton(
                                        onPressed: () {
                                          Navigator.of(context).pop();
                                        },
                                        child: Text("Okay"))
                                  ],
                                );
                              });
                        }
                        if (curruntPinCode == null && rePinCode == null) {
                          showDialog(
                              context: context,
                              builder: (context) {
                                return AlertDialog(
                                  title: Text("Oops !!!"),
                                  content: Text("pin can not be empty ..."),
                                  actions: <Widget>[
                                    FlatButton(
                                        onPressed: () {
                                          Navigator.of(context).pop();
                                        },
                                        child: Text("Okay"))
                                  ],
                                );
                              });
                        }
                      },
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        isvisible
            ? Stack(
                children: <Widget>[
                  Container(
                    color: Color(0xffffffff).withOpacity(0.6),
                    height: double.infinity,
                    width: double.infinity,
                    child: Opacity(
                      opacity: 1.0,
                      child: Center(
                          child: Image.asset(
                        "assets/images/logo2.gif",
                        height: 200,
                        width: 100,
                      )),
                    ),
                  ),
                ],
              )
            : Container(),
      ],
    );
  }

  registeruserotherthanbroker(context) async {
    print('roleId ${widget.regModel.description}');
    print('roleId ${widget.regModel.roleName}');
    String url = '$base_url/register/user';
    Map data = {
      'fullName': widget.regModel.fullName,
      'username': widget.regModel.username,
      'organizationName': widget.regModel.organizationName,
      'email': widget.regModel.email,
      'contactNumber': widget.countryCode.toString() +
          widget.regModel.contactNumber.toString(),
      'countryId': widget.regModel.countryId,
      'description': widget.regModel.description,
      'cityId': widget.regModel.cityId,
      'password': widget.regModel.password,
      'role': widget.regModel.role,
      'roleName': 'multiple',
      'userPinCode': rePinCode.toString(),
      'mobileToken': token,
    };
    Map<String, String> headers = {
      'Content-type': 'application/json',
      'Accept': 'application/json'
    };
    var request =
        await http.post(url, body: jsonEncode(data), headers: headers);
    var response = jsonDecode(request.body);
    if (request.body != null) {
      setState(() {
        isvisible = false;
      });
      Navigator.push(
          context,
          PageTransition(
              type: PageTransitionType.bottomToTop,
              duration: Duration(seconds: 1),
              child: ConfirmationPassword(
                countryCode: widget.countryCode,
                phone: widget.phone,
                registerconfirmation: true,
              )));
    }
    print(response);
  }

  registerUser(context) async {
    print('roleId ${widget.regModel.role}');
    print('roleId ${widget.regModel.contactNumber}');
    print('roleId ${widget.regModel.roleidforbroker}');
    print('roleId ${widget.regModel.roleName}');
    print('roleId ${widget.regModel.cityId}');
    print('roleId ${widget.regModel.countryId}');
    print('roleId ${widget.regModel.cnicCopy}');
    print('roleId ${widget.regModel.contactNumber}');
    print('roleId ${widget.regModel.homeBill}');
    print('roleId ${widget.regModel.mobileToken}');
    print('roleId ${widget.regModel.email}');
    print('roleId ${widget.regModel.fullName}');
    print('roleId ${widget.regModel.description}');
    print('roleId ${widget.regModel.organizationName}');
    print('roleId ${widget.regModel.password}');
    print('roleId ${widget.regModel.username}');
    print('roleId ${widget.regModel.countryCode}');
    String registerUrl = '$base_url/register/user';
    var request = http.MultipartRequest('POST', Uri.parse(registerUrl))
      ..fields['fullName'] = widget.regModel.fullName
      ..fields['username'] = widget.regModel.username
      ..fields['organizationName'] = widget.regModel.organizationName
      ..fields['email'] = widget.regModel.email
      ..fields['contactNumber'] = widget.countryCode.toString() +
          widget.regModel.contactNumber.toString()
      ..fields['countryId'] = widget.regModel.countryId
      ..fields['cityId'] = widget.regModel.cityId
      ..fields['password'] = widget.regModel.password
      ..fields['description'] = widget.regModel.description
      ..fields['role'] = widget.regModel.roleidforbroker
      ..fields['roleName'] = 'Broker'
      ..fields['userPinCode'] = rePinCode.toString()
      ..fields['mobileToken'] = token;
    if (widget.regModel.roleName == 'Broker') {
      print('adding files');
      request.files.add(await http.MultipartFile.fromPath(
          'cnicCopy', widget.regModel.cnicCopy));
      request.files.add(await http.MultipartFile.fromPath(
          'homeBill', widget.regModel.homeBill));
    }
    http.Response productResponse = await http.Response.fromStream(
      await request.send(),
    );
    print(productResponse.body);
    if (productResponse.body != null) {
      Navigator.push(
          context,
          PageTransition(
              type: PageTransitionType.bottomToTop,
              duration: Duration(seconds: 1),
              child: ConfirmationPassword(
                countryCode: widget.countryCode,
                phone: widget.phone,
                registerconfirmation: true,
              )));
    } else {
      print("error in registration");
    }
  }
}
