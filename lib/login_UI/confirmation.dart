// import 'dart:ui';
// import 'package:biznes/services/register_logic.dart';
// import 'package:biznes/size_config.dart';
// import '../login_UI/signIn.dart';
// import 'package:flutter/material.dart';
// import 'package:biznes/model/regiterModel.dart';
// import 'package:flutter_svg/flutter_svg.dart';
// import 'package:page_transition/page_transition.dart';
// import 'package:pin_code_fields/pin_code_fields.dart';

// class Confirmation extends StatefulWidget {
//   final String phone;
//   final int countryCode;
//   Confirmation({this.phone, this.countryCode});

//   @override
//   _ConfirmationState createState() => _ConfirmationState();
// }

// class _ConfirmationState extends State<Confirmation> {
//   String code;
//   String token;
//   bool isvisible = false;

//   @override
//   Widget build(BuildContext context) {
//     return Stack(
//       children: [
//         Scaffold(
//             body: Container(
//                 width: (485.3932272197492 / 4.853932272197492) *
//                     SizeConfig.widthMultiplier,
//                 height: (814.8314082864863 / 8.148314082864863) *
//                     SizeConfig.heightMultiplier,
//                 decoration: BoxDecoration(
//                   image: DecorationImage(
//                       image: AssetImage("assets/images/app_backgroudnn.jpg"),
//                       fit: BoxFit.cover),
//                 ),
//                 child: Container(
//                   width: (485.3932272197492 / 4.853932272197492) *
//                       SizeConfig.widthMultiplier,
//                   height: (814.8314082864863 / 8.148314082864863) *
//                       SizeConfig.heightMultiplier,
//                   child: ListView(
//                     children: <Widget>[
//                       SizedBox(
//                         height: (100 / 8.148314082864863) *
//                             SizeConfig.heightMultiplier,
//                       ),
//                       Padding(
//                         padding: const EdgeInsets.all(3.0),
//                         child: Container(
//                           width: (485.3932272197492 / 4.853932272197492) *
//                               SizeConfig.widthMultiplier,
//                           height:
//                               ((814.8314082864863 / 8.148314082864863) / 1.2) *
//                                   SizeConfig.heightMultiplier,
//                           decoration: BoxDecoration(
//                               boxShadow: [
//                                 BoxShadow(
//                                   color: Color(0xff69BEF7),
//                                   spreadRadius: 5,
//                                   blurRadius: 7,
//                                   offset: Offset(
//                                       0, 3), // changes position of shadow
//                                 ),
//                               ],
//                               color: Colors.white,
//                               borderRadius: BorderRadius.only(
//                                 topLeft: Radius.circular(44.5),
//                                 topRight: Radius.circular(44.5),
//                               )),
//                           child: ListView(
//                             children: <Widget>[
//                               Center(
//                                 child: Padding(
//                                   padding: const EdgeInsets.only(
//                                       left: 8.0, right: 8.0, top: 8.0),
//                                   child: Text(
//                                     "Confirmation",
//                                     style: TextStyle(
//                                       color: Color.fromRGBO(57, 76, 129, 1),
//                                       fontSize: (40 / 8.148314082864863) *
//                                           SizeConfig.heightMultiplier,
//                                     ),
//                                   ),
//                                 ),
//                               ),
//                               Center(
//                                 child: Padding(
//                                   padding: const EdgeInsets.all(44.0),
//                                   child: SvgPicture.asset(
//                                       "assets/images/lock.svg"),
//                                 ),
//                               ),
//                               Center(
//                                 child: Text(
//                                   'please confirm your Mobile number \n   by Entering Verification Code',
//                                   style: TextStyle(
//                                     fontSize: (16.6 / 8.148314082864863) *
//                                         SizeConfig.heightMultiplier,
//                                   ),
//                                 ),
//                               ),
//                               SizedBox(
//                                 height: (18 / 8.148314082864863) *
//                                     SizeConfig.heightMultiplier,
//                               ),
//                               Center(
//                                 child: Text(
//                                   'code sent to +${widget.countryCode}${widget.phone}',
//                                   style: TextStyle(
//                                     fontSize: (16.6 / 8.148314082864863) *
//                                         SizeConfig.heightMultiplier,
//                                   ),
//                                 ),
//                               ),
//                               SizedBox(
//                                 height: (20 / 8.148314082864863) *
//                                     SizeConfig.heightMultiplier,
//                               ),
//                               Padding(
//                                 padding: const EdgeInsets.symmetric(
//                                     horizontal: 70.0),
//                                 child: Container(
//                                   height:
//                                       ((814.8314082864863 / 8.148314082864863) /
//                                               12) *
//                                           SizeConfig.heightMultiplier,
//                                   width:
//                                       ((485.3932272197492 / 4.853932272197492) /
//                                               1.8) *
//                                           SizeConfig.widthMultiplier,
//                                   padding:
//                                       EdgeInsets.only(left: 10.0, right: 10),
//                                   decoration: BoxDecoration(
//                                       color: Colors.white.withOpacity(0.7),
//                                       borderRadius: BorderRadius.circular(14.0),
//                                       border: Border.all(color: Colors.black)),
//                                   child: PinCodeTextField(
//                                     textInputType: TextInputType.number,
//                                     appContext: context,
//                                     enabled: true,
//                                     length: 4,
//                                     obsecureText: false,
//                                     mainAxisAlignment:
//                                         MainAxisAlignment.spaceEvenly,
//                                     animationType: AnimationType.fade,
//                                     pinTheme: PinTheme(
//                                       shape: PinCodeFieldShape.underline,
//                                       borderRadius: BorderRadius.circular(5),
//                                       fieldHeight: (50 / 8.148314082864863) *
//                                           SizeConfig.heightMultiplier,
//                                       fieldWidth: (30 / 4.853932272197492) *
//                                           SizeConfig.widthMultiplier,
//                                       activeFillColor: Colors.blue,
//                                     ),
//                                     onChanged: (value) {
//                                       code = value;
//                                     },
//                                   ),
//                                 ),
//                               ),
//                               SizedBox(
//                                 height: (29 / 8.148314082864863) *
//                                     SizeConfig.heightMultiplier,
//                               ),
//                               Center(
//                                 child: MaterialButton(
//                                   onPressed: () {
//                                     setState(() {
//                                       isvisible = true;
//                                     });
//                                     if (code.length == 4) {

//                                     }
//                                   },
//                                   child: Container(
//                                     height: (60 / 8.148314082864863) *
//                                         SizeConfig.heightMultiplier,
//                                     width: (300 / 4.853932272197492) *
//                                         SizeConfig.widthMultiplier,
//                                     alignment: Alignment.center,
//                                     decoration: BoxDecoration(
//                                       borderRadius: BorderRadius.circular(25.0),
//                                       gradient: LinearGradient(
//                                         begin: Alignment.topCenter,
//                                         end: Alignment.bottomCenter,
//                                         colors: [
//                                           Color(0xFF69bef7),
//                                           Color(0xFF2a65ff),
//                                         ],
//                                       ),
//                                       boxShadow: [
//                                         BoxShadow(
//                                           color: Color(0xFF69bef7),
//                                           spreadRadius: 1,
//                                           blurRadius: 12,
//                                           offset: Offset(0,
//                                               2), // changes position of shadow
//                                         ),
//                                       ],
//                                     ),
//                                     child: Text(
//                                       "Confirm",
//                                       style: TextStyle(
//                                           fontSize: (22 / 8.148314082864863) *
//                                               SizeConfig.heightMultiplier,
//                                           color: Colors.white),
//                                     ),
//                                   ),
//                                   shape: StadiumBorder(),
//                                 ),
//                               ),
//                             ],
//                           ),
//                         ),
//                       ),
//                     ],
//                   ),
//                 ))),
//         isvisible
//             ? Stack(
//                 children: <Widget>[
//                   Container(
//                     color: Color(0xffffffff).withOpacity(0.6),
//                     height: double.infinity,
//                     width: double.infinity,
//                     child: Opacity(
//                       opacity: 1.0,
//                       child: Center(
//                           child: Image.asset(
//                         "assets/images/logo2.gif",
//                         height: 200,
//                         width: 100,
//                       )),
//                     ),
//                   ),
//                 ],
//               )
//             : Container(),
//       ],
//     );
//   }
// }
