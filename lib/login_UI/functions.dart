import 'package:biznes/res/color.dart';
import 'package:biznes/res/size.dart';
import 'package:biznes/services/register_logic.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:biznes/size_config.dart';
import 'signIn.dart';
import 'package:biznes/repeatedWigets/filledButton.dart';

registeruser({int countrycode, String phone, String code, context}) {
  verifyPhone(countrycode.toString() + phone, code, context).then((value) {
    if (value == true) {
      showDialog(
          context: context,
          barrierDismissible: false,
          builder: (context) {
            return WillPopScope(
              onWillPop: () async {
                return Future.value(false);
              },
              child: AlertDialog(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15),
                ),
                title: Center(
                    child: Text(
                  "Registered Successfully",
                  style: TextStyle(
                    fontSize: size.convert(context, 18),
                    fontFamily: fontfaimly,
                  ),
                )),
                content: SingleChildScrollView(
                  child: Column(
                    children: <Widget>[
                      Container(
                        child: Image.asset(
                          "assets/images/backround_image.png",
                          fit: BoxFit.fitWidth,
                          width: 20 * SizeConfig.heightMultiplier,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 18.0),
                        child: Text(
                          "Congratulation you are successfully register in Quick Biznes.",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontSize: size.convert(context, 10),
                            fontFamily: fontfaimlyregular,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: size.convert(context, 13),
                      ),
                      filledButton(
                        txt: "Get Started",
                        onTap: () {
                          Navigator.of(context).pushAndRemoveUntil(
                              PageTransition(
                                  child: SignIn(),
                                  duration: Duration(milliseconds: 600),
                                  type: PageTransitionType.bottomToTop),
                              (route) => false);
                        },
                      ),
                    ],
                  ),
                ),
              ),
            );
          });
    }
  });
}
