import 'dart:ui';
import 'package:biznes/helper/login_helper.dart';
import 'package:biznes/login_UI/forgotPassword.dart';
import 'package:biznes/login_UI/pinCodeVerification.dart';
import 'package:biznes/register/register.dart';
import 'package:biznes/products_Detail/extra.dart';
import 'package:biznes/repeatedWigets/CustomTextField.dart';
import 'package:biznes/repeatedWigets/filledButton.dart';
import 'package:biznes/services/register_logic.dart';
import 'package:biznes/size_config.dart';
import 'package:biznes/res/size.dart';
import 'package:biznes/res/style.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:page_transition/page_transition.dart';
import 'package:provider/provider.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:shared_preferences/shared_preferences.dart';
class SignIn extends StatefulWidget {
  @override
  _SignInState createState() => _SignInState();
}

class _SignInState extends State<SignIn> {
  var loginHelper;
  FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  String token;
  bool isemailempty = false;
  bool ispasswordempty = false;
  @override
  void initState() {
    super.initState();
    Future.delayed(Duration.zero, () {
      this.firebaseCloudMessagingListeners(context);
    });
  }

  void firebaseCloudMessagingListeners(BuildContext context) {
    _firebaseMessaging.getToken().then((deviceToken) {
      print("Firebase Device token: $deviceToken");
      setState(() {
        token = deviceToken;
        print(token);
      });
    });
  }

  var formKey = GlobalKey<FormState>();
  bool isvisible = false;
  String email;
  String password;
  bool isPasswordShow = false;

  bool validateAndSave() {
    final form = formKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    } else {
      return false;
    }
  }
  Widget buildForm({Orientation orientation}) {
    final focus = FocusNode();
    final focus2 = FocusNode();
    return Form(
        key: formKey,
        autovalidateMode: AutovalidateMode.onUserInteraction,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: orientation == Orientation.portrait
                      ? 3.5 * SizeConfig.heightMultiplier
                      : 10 * SizeConfig.widthMultiplier,
                  vertical: orientation == Orientation.portrait
                      ? 1.5 * SizeConfig.heightMultiplier
                      : 2 * SizeConfig.widthMultiplier),
              child: Text(
                "Email",
                style: style.MontserratMedium(
                  fontSize: size.convert(context, 14),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: orientation == Orientation.portrait
                      ? 3.5 * SizeConfig.heightMultiplier
                      : 10 * SizeConfig.widthMultiplier),
              child: Stack(
                children: [
                  material(),
                  TextFormField(

                    textInputAction: TextInputAction.done,
                  
                    onFieldSubmitted: (v) {
                      FocusScope.of(context).requestFocus(focus);
                    },
                    onChanged: (value) {
                      email = value;
                    },
                    validator: (value) {
                      if (value.length == 0) {
                        return 'Email is required';
                      }
                      return null;
                    },
                    decoration: InputDecoration(
                      errorStyle:style.MontserratRegular(
                        color: Colors.red, fontSize: size.convert(context, 12)),
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(25),
                        borderSide: BorderSide(color: Color(0xff69BEF7)),
                      ),
                      errorBorder: outlineInputBorder(),
                      focusedErrorBorder: outlineInputBorder(),
                      focusColor: Color(0xff69BEF7),
                      contentPadding: EdgeInsets.symmetric(
                          vertical: 10.0, horizontal: 10.0),
                      border: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.red),
                        borderRadius: BorderRadius.circular(30.0),
                      ),
                      hintStyle: style.MontserratRegular(
                          fontSize: size.convert(context, 12)),
                      hintText: 'Enter email or username',
                      prefixIcon: Icon(Icons.person, size: 17),
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: orientation == Orientation.portrait
                      ? 3.5 * SizeConfig.heightMultiplier
                      : 10 * SizeConfig.widthMultiplier,
                  vertical: orientation == Orientation.portrait
                      ? 1.5 * SizeConfig.heightMultiplier
                      : 4 * SizeConfig.widthMultiplier),
              child: Text(
                "Password",
                style: style.MontserratMedium(
                  fontSize: size.convert(context, 14),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: orientation == Orientation.portrait
                      ? 3.5 * SizeConfig.heightMultiplier
                      : 10 * SizeConfig.widthMultiplier),
              child: Stack(
                children: [
                  material(),
                  TextFormField(
                    onFieldSubmitted: (v) {
                      FocusScope.of(context).requestFocus(focus2);
                    },
                    validator: (value) {
                      if (value.isEmpty && value.length < 6) {
                        return 'Password is required';
                      }
                      return null;
                    },
                    onChanged: (value) {
                      password = value;
                    },
                    decoration: InputDecoration(

                      errorStyle:style.MontserratRegular(
                          color: Colors.red, fontSize: size.convert(context, 12)),
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(25),
                        borderSide: BorderSide(color: Color(0xff69BEF7)),
                      ),
                      errorBorder: outlineInputBorder(),
                      contentPadding: EdgeInsets.symmetric(
                          vertical: 10.0, horizontal: 10.0),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(30.0),
                      ),
                      hintText: 'Password',
                      focusedErrorBorder: outlineInputBorder(),
                      hintStyle: style.MontserratRegular(
                          fontSize: size.convert(context, 12)),
                      prefixIcon: Icon(Icons.lock, size: 17),
                      suffixIcon: InkWell(
                          onTap: () {
                            setState(() {
                              isPasswordShow = !isPasswordShow;
                            });
                          },
                          child: isPasswordShow
                              ? Icon(
                                  Icons.visibility_off,
                                  size: 15,
                                )
                              : Icon(
                                  Icons.visibility,
                                  size: 15,
                                )),
                    ),
                    obscureText: !isPasswordShow,
                  ),
                ],
              ),
            ),
          ],
        ));
  }

  TextStyle textStyle() {
    return TextStyle(
        color: Colors.black87,
        fontWeight: FontWeight.w500,
        fontSize: (13.9 / 8.148314082864863) * SizeConfig.heightMultiplier);
  }

  @override
  Widget build(BuildContext context) {
    loginHelper = Provider.of<LoginHelper>(context);
    return Builder(
      builder: (BuildContext context){
        return Scaffold(
        body: GestureDetector(
            onTap: () => FocusScope.of(context).requestFocus(new FocusNode()),
            child: Stack(
              children: [
                _loginBody(context),
                _loadingBody(),
              ],
            )),
      );
      },
    );
  }

  _loadingBody() {
    return isvisible
        ? Stack(
            children: <Widget>[
              Container(
                color: Color(0xffffffff).withOpacity(0.4),
                height: double.infinity,
                width: double.infinity,
                child: Opacity(
                  opacity: 1.0,
                  child: Center(
                      child: Image.asset(
                    "assets/images/logo2.gif",
                    height: 200,
                    width: 100,
                  )),
                ),
              ),
            ],
          )
        : Container();
  }

  _loginBody(context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      decoration: BoxDecoration(
          ),
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Container(
              margin:
                  EdgeInsets.symmetric(horizontal: size.convert(context, 16)),
              child: Column(
                children: [
                  SizedBox(height: size.convert(context, 40)),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      InkWell(
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: Icon(
                          Icons.arrow_back_sharp,
                          color: Colors.black,
                        ),
                      ),
                      Container(),
                    ],
                  ),
                  Container(
                    child: Image.asset(
                      "assets/images/logo.png",
                      width: size.convertWidth(context, 240),
                      height: size.convert(context, 101),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.symmetric(vertical: 20),
              margin: EdgeInsets.symmetric(
                  horizontal: size.convertWidth(context, 21)),
              alignment: Alignment.bottomLeft,
              decoration: BoxDecoration(
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                    color: Color(0xffd4d4d4),
                    blurRadius: 6,
                    offset: Offset(0, 3),
                  ),
                ],
                borderRadius: BorderRadius.circular(size.convert(context, 20)),
              ),
              child: SingleChildScrollView(
                child: Column(
                  children: <Widget>[
                    Container(
                      child: SvgPicture.asset(
                        "assets/icons/loginIcon.svg",
                        width: size.convertWidth(context, 250),
                        height: size.convert(context, 150),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(
                        top: size.convert(context, 5),
                      ),
                      child: Align(
                        alignment: Alignment(0.0, 25),
                        child: Text(
                          "Login",
                          style: style.QuicksandBold(
                              fontSize: size.convert(context, 30),
                              color: Color(0xff394c81)),
                        ),
                      ),
                    ),
                    // Padding(
                      
              // padding: EdgeInsets.symmetric(
              //     horizontal:MediaQuery.of(context).orientation == Orientation.portrait
              //         ? 3.5 * SizeConfig.heightMultiplier
              //         : 10 * SizeConfig.widthMultiplier),
              //         child: CustomTextField(
              //           color1: Colors.red,
              //           iconWidget: Icons.person,
              //           hints: "Enter email or username",
              //         ),
              //       ),
                    buildForm(orientation: MediaQuery.of(context).orientation),
                    Container(
                      margin: EdgeInsets.symmetric(
                          horizontal: size.convertWidth(context, 20)),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Expanded(
                            flex: 6,
                            child: Container(
                              alignment: Alignment.centerLeft,
                              child: FlatButton(
                                  onPressed: () => Navigator.push(
                                      context,
                                      PageTransition(
                                          type: PageTransitionType.bottomToTop,
                                          duration: Duration(milliseconds: 500),
                                          child: ForgotPassword())),
                                  child: Text(
                                    "Forgot Password?",
                                    style: style.MontserratRegular(
                                        fontSize: size.convert(context, 12)),
                                  )),
                            ),
                          ),
                          Expanded(
                            flex: 4,
                            child: Container(
                              alignment: Alignment.centerRight,
                              child: FlatButton(
                                  onPressed: () => Navigator.push(
                                      context,
                                      PageTransition(
                                          type: PageTransitionType.bottomToTop,
                                          duration: Duration(milliseconds: 500),
                                          child: EnterYourRole())),
                                  child: Text(
                                    "Register",
                                    style: style.MontserratRegular(
                                        fontSize: size.convert(context, 12)),
                                  )),
                            ),
                          ),
                        ],
                      ),
                    ),

                    SizedBox(
                      height: size.convert(context, 20),
                    ),
                    filledButton(
                      onTap: () async {
                        if (formKey.currentState.validate()) {
                          SharedPreferences _prefs =
                              await SharedPreferences.getInstance();
                          setState(() {
                            isvisible = true;
                          });
                          signIn(email, password, token, context).then((map) {
                            setState(() {
                              isvisible = false;
                            });
                            if(map!=null){
                            if (map['success']) {
                              loginHelper.onLogIn(map['token'], map['user']);
                              _prefs.setString("userid", map["userid"]);
                              Navigator.of(context).pushAndRemoveUntil(
                                  MaterialPageRoute(builder: (context) => PinVerification()),
                                  (route) => false);
                            }
                            }
                          });
                        }
                      },
                      txt: "Login",
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
