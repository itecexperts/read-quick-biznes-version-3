import 'dart:ui';
import 'package:biznes/register/register.dart';
import 'package:biznes/login_UI/signIn.dart';
import 'package:biznes/repeatedWigets/filledButton.dart';
import 'package:biznes/res/style.dart';
import 'package:biznes/services/register_logic.dart';
import 'package:biznes/services/server.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:biznes/size_config.dart';
import 'package:biznes/res/size.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();
    Future<bool> _onbackpress() {
      return showDialog(
          context: context,
          barrierDismissible: false,
          builder: (BuildContext context) {
            return AlertDialog(
                  key: navigatorKey,
                  title: Text("Are you sure you want to exit"),
                  actions: [
                    RaisedButton(
                      child: Text("Cancel"),
                      onPressed: () {
                        Navigator.of(
                          context,
                        ).pop(false);
                      },
                    ),
                    RaisedButton(
                      child: Text("ok"),
                      onPressed: () {
                        print("true");
                        Navigator.of(context).pop(true);
                      },
                    ),
                  ],
                ) ??
                false;
          });
    }

    return WillPopScope(
      onWillPop: () async {
        return true;
      },
      child: Scaffold(body: OrientationBuilder(
        builder: (context, orientation) {
          return InkWell(
              onTap: () {
                Navigator.of(context, rootNavigator: true)
                    .popUntil((route) => route.isFirst);
                print("hellow");

                Navigator.of(context).push(
                    MaterialPageRoute(builder: (context) => LoginScreen()));
              },
              child: Container(
                width: orientation == Orientation.portrait
                    ? (485.3932272197492 / 4.853932272197492) *
                        SizeConfig.widthMultiplier
                    : (814.8314082864863 / 8.148314082864863) *
                        SizeConfig.heightMultiplier,
                height: orientation == Orientation.portrait
                    ? (814.8314082864863 / 8.148314082864863) *
                        SizeConfig.heightMultiplier
                    : (485.3932272197492 / 4.853932272197492) *
                        SizeConfig.widthMultiplier,
                child: Image.asset(
                  "assets/images/splash.JPG",
                  fit: BoxFit.fill,
                ),
              ));
        },
      )),
    );
  }
}

class LoginScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        return Future.value(true);
      },
      child: Scaffold(
        body: _body(context),
      ),
    );
  }

  _body(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 24)),
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(
              height: size.convert(context, 80),
            ),
            Container(
              alignment: Alignment.center,
              child: Image.asset(
                "assets/images/logo.png",
                width: size.convert(context, 220),
                height: size.convert(context, 92),
              ),
            ),
            Container(
              alignment: Alignment.center,
              child: SvgPicture.asset(
                "assets/icons/businessMan.svg",
                width: size.convert(context, 310),
                height: size.convert(context, 186),
              ),
            ),
            SizedBox(
              height: size.convert(context, 15),
            ),
            Container(
              child: Text(
                "Welcome",
                style: style.QuicksandBold(
                    fontSize: size.convert(context, 28),
                    color: Color(0xff394c81)),
              ),
            ),
            SizedBox(
              height: MediaQuery.of(context).orientation == Orientation.portrait
                  ? size.convert(context, 15)
                  : size.convert(context, 15),
            ),
            Container(
                margin: EdgeInsets.symmetric(
                    horizontal: size.convertWidth(context, 10)),
                child: Center(
                  child: RichText(
                    textAlign: TextAlign.center,
                    text: TextSpan(
                        text:
                            "Quick Biznes is an online trading messenger. A platform that will help you to buy and sell your business stock / products instantly. Register yourself to gain and chat with new customers and expand your business portfolio.",
                        style: style.MontserratRegular(
                            fontSize: size.convert(context, 12))),
                  ),
                )),
            SizedBox(
              height: MediaQuery.of(context).orientation == Orientation.portrait
                  ? size.convert(context, 70)
                  : size.convert(context, 40),
            ),
            Container(
              //margin: EdgeInsets.symmetric(horizontal: size.convert(context, 30)),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  filledButton(
                    w: size.convertWidth(context, 160),
                    txt: "Login",
                    //shadowColor: Color(0xff298facff),
                    onTap: () async {
                      if (await checkInternetConnection()) {
                        Navigator.push(
                            context,
                            PageTransition(
                                type: PageTransitionType.bottomToTop,
                                duration: Duration(milliseconds: 500),
                                child: SignIn()));
                      } else {
                        // Get.snackbar("No Connection","No Internet Connection");
                      }
                    },
                  ),
                  filledButton(
                    w: size.convertWidth(context, 160),
                    txt: "Register",
                    startColor: Color(0xffffcd02),
                    endColor: Color(0xffffcd02),
                    shadowColor: Color(0xffffde66),
                    onTap: () {
                      Navigator.push(
                          context,
                          PageTransition(
                              type: PageTransitionType.bottomToTop,
                              duration: Duration(milliseconds: 500),
                              child: EnterYourRole()));
                    },
                  ),
                ],
              ),
            ),
            SizedBox(
              height: size.convert(context, 20),
            ),
            InkWell(
              onTap: () {
                signinForGuest(context);
                // Navigator.of(context)
                //     .push(MaterialPageRoute(builder: (_) => GuestHome()));
              },
              child: Text(
                "Login as a guest",
                style: style.MontserratMedium(
                    fontSize: size.convert(context, 14),
                    color: Color(0xff394c81)),
              ),
            ),
            SizedBox(
              height: MediaQuery.of(context).orientation == Orientation.portrait
                  ? size.convert(context, 0)
                  : size.convert(context, 20),
            ),
          ],
        ),
      ),
    );
  }
}
