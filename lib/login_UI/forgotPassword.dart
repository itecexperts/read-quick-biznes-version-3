import 'dart:ui';
import 'package:biznes/login_UI/newpincode.dart';
import 'package:biznes/repeatedWigets/filledButton.dart';
import 'package:biznes/services/register_logic.dart';
import 'functions.dart';
import 'package:biznes/products_Detail/extra.dart';
import 'package:biznes/login_UI/signIn.dart';
import 'package:biznes/size_config.dart';
import 'package:biznes/res/style.dart';
import 'package:biznes/res/size.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter_svg/flutter_svg.dart';
import '../login_UI/forgetnewpassword.dart';
import 'package:pin_code_fields/pin_code_fields.dart';

class ForgotPassword extends StatefulWidget {
  String title;
  bool Forgotpin;

  ForgotPassword({this.title, this.Forgotpin = false});

  @override
  _ForgotPasswordState createState() => _ForgotPasswordState();
}

class _ForgotPasswordState extends State<ForgotPassword> {
  var formKey = GlobalKey<FormState>();
  String phoneNumber;

  bool validateAndSave() {
    final form = formKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    } else {
      return false;
    }
  }

  //form made here
  Widget buildForm({Orientation orientation}) {
    return Padding(
      padding: EdgeInsets.all(16.2),
      child: Form(
          key: formKey,
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
//                Center(
//                  child: Padding(
//                    padding: const EdgeInsets.only(left: 22.0, right: 22.0),
//                    child: SvgPicture.asset(
//                      "assets/images/lock.svg",
//                      width: orientation == Orientation.portrait
//                          ? (150 / 4.853932272197492) *
//                              SizeConfig.widthMultiplier
//                          : (80 / 8.148314082864863) *
//                              SizeConfig.heightMultiplier,
//                      height: orientation == Orientation.portrait
//                          ? (150 / 8.148314082864863) *
//                              SizeConfig.heightMultiplier
//                          : (80 / 4.853932272197492) *
//                              SizeConfig.widthMultiplier,
//                    ),
//                  ),
//                ),
                SizedBox(
                  height: orientation == Orientation.portrait
                      ? (18 / 8.148314082864863) * SizeConfig.heightMultiplier
                      : (4 / 8.148314082864863) * SizeConfig.heightMultiplier,
                ),

                SizedBox(
                  height: orientation == Orientation.portrait
                      ? (18 / 8.148314082864863) * SizeConfig.heightMultiplier
                      : (4 / 8.148314082864863) * SizeConfig.heightMultiplier,
                ),
                Padding(
                  padding: const EdgeInsets.all(6.0),
                  child: Stack(
                    children: [
                      material(),
                      TextFormField(
                        onSaved: (value) {
                          phoneNumber = value;
                        },
                        validator: (value) {
                          if (value.isEmpty && value.length < 12) {
                            return 'required field';
                          }
                          return null;
                        },
                        keyboardType: TextInputType.phone,
                        decoration: InputDecoration(
                          errorBorder: outlineInputBorder(),
                          focusedErrorBorder: outlineInputBorder(),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(25),
                            borderSide: BorderSide(color: Color(0xff69BEF7)),
                          ),
                          contentPadding: EdgeInsets.symmetric(
                              vertical: 10.0, horizontal: 10.0),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(30.0),
                          ),
                          hintText: '3130037752',
                          prefixIcon: Icon(
                            Icons.send_to_mobile,
                            color: Color(0xff394c81).withOpacity(0.7),
                          ),
                        ),
                        obscureText: false,
                        maxLength: 10,
                      ),
                    ],
                  ),
                )
              ],
            ),
          )),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(backgroundColor: Colors.white, body: _forgotPasswordBody());
  }

  _forgotPasswordBody() {
    return SingleChildScrollView(
      child: Column(
        children: [
          Container(
            //color: Colors.red,
            margin: EdgeInsets.symmetric(horizontal: size.convert(context, 16)),
            child: Column(
              children: [
                SizedBox(height: size.convert(context, 40)),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    InkWell(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: Icon(
                        Icons.arrow_back_sharp,
                        color: Colors.black,
                      ),
                    ),
                    Container(),
                  ],
                ),
                // SizedBox(height: size.convert(context, 20),),
//                Container(
//                  child: Image.asset(
//                    "assets/images/logo.png",
//                    width: size.convertWidth(context, 240),
//                    height: size.convert(context, 101),
//                  ),
//                ),
              ],
            ),
          ),
          SizedBox(
            height: MediaQuery.of(context).orientation == Orientation.landscape
                ? size.convert(context, 15)
                : size.convert(context, 80),
          ),
          Container(
            padding: EdgeInsets.symmetric(vertical: 20),
            margin: EdgeInsets.symmetric(
                horizontal: size.convertWidth(context, 21)),
            //alignment: Alignment.bottomLeft,
            decoration: BoxDecoration(
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                  color: Color(0xffd4d4d4),
                  blurRadius: 6,
                  offset: Offset(0, 3),
                ),
              ],
              borderRadius: BorderRadius.circular(size.convert(context, 20)),
            ),
            child: Column(
              children: <Widget>[
                Container(
                  child: SvgPicture.asset(
                    "assets/icons/ForgetPasswrd.svg",
                    width: size.convertWidth(context, 250),
                    height: size.convert(context, 150),
                  ),
                ),
                Center(
                  child: Padding(
                    padding: const EdgeInsets.only(top: 12.0),
                    child: Text(
                      widget.Forgotpin ? widget.title : "Forgot Password ",
                      style: style.QuicksandBold(
                          fontSize: size.convert(context, 30),
                          color: Color(0xff394c81)),
                    ),
                  ),
                ),
                Container(
                  child: RichText(
                    text: TextSpan(
                        text:
                            ' Enter Your phone number For \nVerification and Reset Password',
                        style: style.MontserratRegular(
                            fontSize: size.convert(context, 12))),
                  ),
                ),
                buildForm(orientation: MediaQuery.of(context).orientation),
                filledButton(
                  txt: "Send",
                  onTap: () async {
                    if (validateAndSave()) {
                      // forgetPassword(phoneNumber, context);
                      if (await forgetPassword(phoneNumber, context)) {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (_) => ConfirmationPassword(
                                  phoneNumber: int.parse(phoneNumber),
                                  forgotPin: widget.Forgotpin,
                                )));
                      }
                    }
                  },
                ),
//                  Center(
//                    child: MaterialButton(
//                      onPressed: () {
//                        if (validateAndSave()) {
//                          forgetPassword(phoneNumber, context);
//                          Navigator.of(context).push(MaterialPageRoute(
//                              builder: (_) => ConfirmationPassword(
//                                phoneNumber: int.parse(phoneNumber),
//                              )));
//                        }
//                      },
//                      child: Container(
//                        width: ((485.3932272197492 / 4.853932272197492) / 1.5) *
//                            SizeConfig.widthMultiplier,
//                        height: (60 / 8.148314082864863) * SizeConfig.heightMultiplier,
//                        decoration: decoration(),
//                        alignment: Alignment.center,
//                        child: Text(
//                          "Send",
//                          style: TextStyle(
//                              fontSize:
//                              (22 / 8.148314082864863) * SizeConfig.heightMultiplier,
//                              color: Colors.white),
//                        ),
//                      ),
//                      shape: StadiumBorder(),
//                    ),
//                  ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class ConfirmationPassword extends StatefulWidget {
  final int phoneNumber;
  bool forgotPin;
  bool registerconfirmation;
  int countryCode;
  String phone;

  ConfirmationPassword({
    this.phoneNumber,
    this.forgotPin = false,
    this.registerconfirmation = false,
    this.countryCode,
    this.phone,
  });

  @override
  _ConfirmationPasswordState createState() => _ConfirmationPasswordState();
}

class _ConfirmationPasswordState extends State<ConfirmationPassword> {
  int verificationCode;
  String newPassword;
  var formKey = GlobalKey<FormFieldState>();
  TextEditingController password = TextEditingController();
  bool isvisible = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(body: OrientationBuilder(
      builder: (context, orientation) {
        return _confirmationBody();
      },
    ));
  }

  _confirmationBody() {
    return SingleChildScrollView(
      child: Column(
        children: [
          Container(
            margin: EdgeInsets.symmetric(horizontal: size.convert(context, 16)),
            child: Column(
              children: [
                SizedBox(height: size.convert(context, 40)),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    InkWell(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: Icon(
                        Icons.arrow_back_sharp,
                        color: Colors.black,
                      ),
                    ),
                    Container(),
                  ],
                ),
              ],
            ),
          ),
          SizedBox(
            height: MediaQuery.of(context).orientation == Orientation.landscape
                ? size.convert(context, 15)
                : size.convert(context, 80),
          ),
          Container(
            padding: EdgeInsets.symmetric(vertical: 20),
            margin: EdgeInsets.symmetric(
                horizontal: size.convertWidth(context, 21)),
            //alignment: Alignment.bottomLeft,
            decoration: BoxDecoration(
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                  color: Color(0xffd4d4d4),
                  blurRadius: 6,
                  offset: Offset(0, 3),
                ),
              ],
              borderRadius: BorderRadius.circular(size.convert(context, 20)),
            ),
            child: Column(
              children: <Widget>[
                Container(
                  child: SvgPicture.asset(
                    "assets/icons/ConfirmationCode.svg",
                    width: size.convertWidth(context, 250),
                    height: size.convert(context, 150),
                  ),
                ),
                Center(
                  child: Padding(
                    padding: const EdgeInsets.only(top: 12.0),
                    child: Text(
                      "Confirmation ",
                      style: style.QuicksandBold(
                          fontSize: size.convert(context, 30),
                          color: Color(0xff394c81)),
                    ),
                  ),
                ),
                Container(
                  child: RichText(
                    textAlign: TextAlign.center,
                    text: TextSpan(
                        text: " A four Digits code has been sent to\n"
                            "your contact number",
                        style: style.MontserratRegular(
                            fontSize: size.convert(context, 12))),
                  ),
                ),
                SizedBox(
                  height: size.convert(context, 10),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 88.0),
                  child: Container(
                      height: (60 / 8.148314082864863) *
                          SizeConfig.heightMultiplier,
                      padding: EdgeInsets.only(
                          left: 12.0, right: 12.0, top: 2.0, bottom: 2.0),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(15.0),
                          border: Border.all(color: Colors.black)),
                      child: PinCodeTextField(
                        textStyle: TextStyle(
                          color: Color(0xff394c81),
                        ),
                        keyboardType: TextInputType.number,
                        obscureText: true,
                        appContext: context,
                        onChanged: (val) {},
                        length: 4,
                        onCompleted: (value) {
                          verificationCode = int.parse(value);
                        },
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        animationType: AnimationType.fade,
                        pinTheme: PinTheme(
                            shape: PinCodeFieldShape.underline,
                            borderRadius: BorderRadius.circular(5),
                            fieldHeight: (50 / 8.148314082864863) *
                                SizeConfig.heightMultiplier,
                            fieldWidth: (30 / 4.853932272197492) *
                                SizeConfig.widthMultiplier,
                            activeFillColor: Colors.blue,
                            inactiveColor: Colors.grey),
                      )),
                ),
                SizedBox(
                  height: size.convert(context, 20),
                ),
                filledButton(
                  txt: "Confirm",
                  onTap: () {
                    if (widget.forgotPin == true &&
                        widget.registerconfirmation == false) {
                      resetPincode(
                              code: verificationCode.toString(),
                              contactNumber: widget.phoneNumber.toString(),
                              context: context)
                          .then((value) {
                        if (value['Success'] == true) {
                          Navigator.of(context).pushAndRemoveUntil(
                              MaterialPageRoute(
                                  builder: (context) => NewPinCode(
                                        userid: value['message'],
                                        forgotPin: widget.forgotPin,
                                      )),
                              (route) => false);
                        }
                      });
                    } else if (widget.forgotPin == false &&
                        widget.registerconfirmation == true) {
                      registeruser(
                        code: verificationCode.toString(),
                        context: context,
                        countrycode: widget.countryCode,
                        phone: widget.phone,
                      );
                    } else if (widget.forgotPin == false &&
                        widget.registerconfirmation == false) {
                      resetPassword(
                              code: verificationCode.toString(),
                              contactNumber: widget.phoneNumber.toString(),
                              context: context)
                          .then((value) {
                        if (value['Success'] == true) {
                          Navigator.of(context).pushAndRemoveUntil(
                              MaterialPageRoute(
                                  builder: (context) => ForgotNewPassword(
                                      userid: value['message'])),
                              (route) => false);
                        } else {
                          print("invalid code");
                        }
                        print("return value is $value");
                      });
                    }
                  },
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class NewPassword extends StatefulWidget {
  @override
  _NewPasswordState createState() => _NewPasswordState();
}

class _NewPasswordState extends State<NewPassword> {
  var formKey = GlobalKey<FormState>();

  //form made here
  Widget buildForm() {
    return Padding(
      padding: EdgeInsets.all(12.2),
      child: Form(
          key: formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 5, vertical: 2),
                child: Text(
                  "New Password",
                  style: TextStyle(fontSize: 22.9),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(6.0),
                child: TextFormField(
                  obscureText: true,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(30.0),
                    ),
                    hintText: 'enter new password',
                    suffixIcon: Icon(Icons.remove_red_eye),
                    prefixIcon: Icon(Icons.lock),
                  ),
                ),
              ),
              SizedBox(
                height: 12.2,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 5, vertical: 2),
                child: Text(
                  "Confirm New Password",
                  style: TextStyle(fontSize: 22.9),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(6.0),
                child: TextFormField(
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(30.0),
                    ),
                    hintText: 'password',
                    prefixIcon: Icon(Icons.lock),
                    suffixIcon: Icon(Icons.remove_red_eye),
                  ),
                  obscureText: true,
                ),
              ),
              SizedBox(
                height: 17.7,
              )
            ],
          )),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage("assets/images/app_backgroudnn.jpg"),
                  fit: BoxFit.cover),
            ),
            child: Container(
              decoration: BoxDecoration(),
              child: ListView(
                children: <Widget>[
                  SizedBox(
                    height: 150,
                  ),
                  Padding(
                    padding: const EdgeInsets.all(6.0),
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      height: MediaQuery.of(context).size.height / 1.3,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(44.5),
                            topRight: Radius.circular(44.5),
                          )),
                      child: Wrap(
                        children: <Widget>[
                          SizedBox(
                            height: 16.6,
                          ),
                          Center(
                            child: Padding(
                              padding: const EdgeInsets.only(
                                  left: 22.0, top: 44.2, right: 22.2),
                              child: Text(
                                "Enter New Password",
                                style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 40.8,
                                    fontStyle: FontStyle.italic),
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 12.3,
                          ),
                          Center(
                            child: Padding(
                              padding: const EdgeInsets.only(
                                  left: 2.0, top: 12.2, right: 2.4),
                              child: SvgPicture.asset("assets/images/new.svg"),
                            ),
                          ),
                          buildForm(),
                          Center(
                            child: MaterialButton(
                              minWidth: MediaQuery.of(context).size.width / 1.5,
                              height: 62,
                              color: Color.fromRGBO(45, 204, 113, 1),
                              onPressed: () => Navigator.of(context).push(
                                  MaterialPageRoute(builder: (_) => SignIn())),
                              child: Text(
                                "Confirm",
                                style: TextStyle(
                                    fontSize: 22.2, color: Colors.white),
                              ),
                              shape: StadiumBorder(),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            )));
  }
}
