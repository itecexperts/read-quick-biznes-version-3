import 'dart:ui';

import 'package:biznes/helper/login_helper.dart';
import 'package:biznes/login_UI/confirmationpincode.dart';
import 'package:biznes/services/register_logic.dart';
import 'package:biznes/size_config.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ForgetPinResquest extends StatefulWidget {
  @override
  _ForgetPinResquestState createState() => _ForgetPinResquestState();
}

class _ForgetPinResquestState extends State<ForgetPinResquest> {
  var formKey = GlobalKey<FormState>();
  String phoneNumber;
  bool validateAndSave() {
    final form = formKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    } else {
      return false;
    }
  }

  Widget buildForm({Orientation orientation}) {
    return Padding(
      padding: EdgeInsets.all(12.2),
      child: Form(
          key: formKey,
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Center(
                  child: Padding(
                    padding: const EdgeInsets.only(left: 22.0, right: 22.0),
                    child: SvgPicture.asset(
                      "assets/images/lock.svg",
                      width: orientation == Orientation.portrait
                          ? (150 / 4.853932272197492) *
                              SizeConfig.widthMultiplier
                          : (80 / 8.148314082864863) *
                              SizeConfig.heightMultiplier,
                      height: orientation == Orientation.portrait
                          ? (150 / 8.148314082864863) *
                              SizeConfig.heightMultiplier
                          : (80 / 4.853932272197492) *
                              SizeConfig.widthMultiplier,
                    ),
                  ),
                ),
                SizedBox(
                  height: orientation == Orientation.portrait
                      ? (18 / 8.148314082864863) * SizeConfig.heightMultiplier
                      : (4 / 8.148314082864863) * SizeConfig.heightMultiplier,
                ),
                Center(
                  child: FittedBox(
                    child: Text(
                      '   Enter Your phone number For \nVerification and Reset Pin Code',
                      style: TextStyle(
                        fontSize: orientation == Orientation.portrait
                            ? (16 / 8.148314082864863) *
                                SizeConfig.heightMultiplier
                            : (12 / 8.148314082864863) *
                                SizeConfig.heightMultiplier,
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: orientation == Orientation.portrait
                      ? (18 / 8.148314082864863) * SizeConfig.heightMultiplier
                      : (4 / 8.148314082864863) * SizeConfig.heightMultiplier,
                ),
                Padding(
                  padding: const EdgeInsets.all(6.0),
                  child: TextFormField(
                    onSaved: (value) {
                      phoneNumber = value;
                    },
                    validator: (value) {
                      if (value.isEmpty && value.length < 12) {
                        return 'required field';
                      }
                      return null;
                    },
                    keyboardType: TextInputType.phone,
                    decoration: InputDecoration(
                      contentPadding: EdgeInsets.symmetric(
                          vertical: 10.0, horizontal: 10.0),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(30.0),
                      ),
                      hintText: '3130037752',
                      prefixIcon: Icon(
                        Icons.send_to_mobile,
                        color: Color(0xff394c81),
                      ),
                    ),
                    obscureText: false,
                    maxLength: 12,
                  ),
                )
              ],
            ),
          )),
    );
  }

  @override
  Widget build(BuildContext context) {
    var token = Provider.of<LoginHelper>(context).token;
    return Scaffold(
      body: OrientationBuilder(builder: (context, orientation) {
        return Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage("assets/images/app_backgroudnn.jpg"),
                  fit: BoxFit.cover),
            ),
            child: Container(
              decoration: BoxDecoration(),
              child: ListView(
                children: <Widget>[
                  SizedBox(
                    height: orientation == Orientation.portrait
                        ? (190 / 8.148314082864863) *
                            SizeConfig.heightMultiplier
                        : (65 / 8.148314082864863) *
                            SizeConfig.heightMultiplier,
                  ),
                  Padding(
                    padding: EdgeInsets.all(3.0),
                    child: Container(
                      width: double.infinity,
                      height: orientation == Orientation.portrait
                          ? (814.8314082864863 / 8.148314082864863) *
                              SizeConfig.heightMultiplier
                          : (485.3932272197492 / 4.853932272197492) *
                              SizeConfig.widthMultiplier,
                      decoration: BoxDecoration(
                          boxShadow: [
                            BoxShadow(
                              color: Color(0xff69BEF7),
                              spreadRadius: 5,
                              blurRadius: 7,
                              offset:
                                  Offset(0, 3), // changes position of shadow
                            ),
                          ],
                          color: Colors.white,
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(44.5),
                            topRight: Radius.circular(44.5),
                          )),
                      child: ListView(
                        children: <Widget>[
                          Center(
                            child: Padding(
                              padding: const EdgeInsets.only(top: 12.0),
                              child: Text(
                                "Forget Pin Code ",
                                style: TextStyle(
                                  color: Color.fromRGBO(57, 76, 129, 1),
                                  fontSize: orientation == Orientation.portrait
                                      ? (24 / 8.148314082864863) *
                                          SizeConfig.heightMultiplier
                                      : (18 / 8.148314082864863) *
                                          SizeConfig.heightMultiplier,
                                ),
                              ),
                            ),
                          ),
                          buildForm(orientation: orientation),
                          Center(
                            child: MaterialButton(
                              onPressed: () {
                                if (validateAndSave()) {
                                  forgotpincoderequest(
                                      int.parse(phoneNumber), context, token);
                                  Navigator.of(context).push(MaterialPageRoute(
                                      builder: (_) => ConfirmationPinCode(
                                            phoneNumber: int.parse(phoneNumber),
                                          )));
                                }
                              },
                              child: Container(
                                alignment: Alignment.center,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(25.0),
                                  gradient: LinearGradient(
                                    begin: Alignment.topCenter,
                                    end: Alignment.bottomCenter,
                                    colors: [
                                      Color(0xFF69bef7),
                                      Color(0xFF2a65ff),
                                    ],
                                  ),
                                  boxShadow: [
                                    BoxShadow(
                                      color: Color(0xFF69bef7),
                                      spreadRadius: 1,
                                      blurRadius: 12,
                                      offset: Offset(0, 2),
                                    ),
                                  ],
                                ),
                                width: (370 / 4.853932272197492) *
                                    SizeConfig.widthMultiplier,
                                height: 50,
                                child: Text(
                                  "Send",
                                  style: TextStyle(
                                      fontSize: (22 / 8.148314082864863) *
                                          SizeConfig.heightMultiplier,
                                      color: Colors.white),
                                ),
                              ),
                              shape: StadiumBorder(),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ));
      }),
    );
  }
}
