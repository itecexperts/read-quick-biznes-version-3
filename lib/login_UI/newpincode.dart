import 'dart:ui';

import 'package:biznes/res/color.dart';
import 'package:biznes/size_config.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import '../services/register_logic.dart';
import 'package:biznes/products_Detail/extra.dart';
import 'package:biznes/login_UI/pinCodeVerification.dart';
import 'package:biznes/res/size.dart';
import 'package:biznes/res/style.dart';
import 'package:biznes/repeatedWigets/filledButton.dart';

class NewPinCode extends StatefulWidget {
  final String userid;
  bool forgotPin;
  NewPinCode({this.userid, this.forgotPin = false});
  @override
  _NewPinCodeState createState() => _NewPinCodeState();
}

class _NewPinCodeState extends State<NewPinCode> {
  int curruntPinCode, rePinCode;
  Orientation orientation;
  bool isloading = false;
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Stack(
        children: [
          Scaffold(
            body: _newBody(),
          ),
          isloading ? stack() : Container(),
        ],
      ),
    );
  }

  _newBody() {
    return Container(
      child: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              //color: Colors.red,
              margin:
                  EdgeInsets.symmetric(horizontal: size.convert(context, 16)),
              child: Column(
                children: [
                  SizedBox(height: size.convert(context, 40)),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      InkWell(
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: Icon(
                          Icons.arrow_back_sharp,
                          color: Colors.black,
                        ),
                      ),
                      Container(),
                    ],
                  ),
                  // SizedBox(height: size.convert(context, 20),),
//                Container(
//                  child: Image.asset(
//                    "assets/images/logo.png",
//                    width: size.convertWidth(context, 240),
//                    height: size.convert(context, 101),
//                  ),
//                ),
                ],
              ),
            ),
            SizedBox(
              height: size.convert(context, 15),
            ),
            Container(
              padding: EdgeInsets.symmetric(vertical: 20),
              margin: EdgeInsets.symmetric(
                  horizontal: size.convertWidth(context, 21)),
              //alignment: Alignment.bottomLeft,
              decoration: BoxDecoration(
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                    color: Color(0xffd4d4d4),
                    blurRadius: 6,
                    offset: Offset(0, 3),
                  ),
                ],
                borderRadius: BorderRadius.circular(size.convert(context, 20)),
              ),
              child: Column(
                children: <Widget>[
                  Container(
                    child: SvgPicture.asset(
                      "assets/icons/loginIcon.svg",
                      width: size.convertWidth(context, 250),
                      height: size.convert(context, 150),
                    ),
                  ),
                  Center(
                    child: Padding(
                      padding: const EdgeInsets.only(top: 12.0),
                      child: Text(
                        "Select New Pin-Code",
                        style: style.QuicksandBold(
                            fontSize: size.convert(context, 30),
                            color: Color(0xff394c81)),
                      ),
                    ),
                  ),
                  Container(
                    child: RichText(
                      textAlign: TextAlign.center,
                      text: TextSpan(
                          text: "4-Digits Code shall be used to use your\n"
                              "account after registration",
                          style: style.MontserratRegular(
                              fontSize: size.convert(context, 12))),
                    ),
                  ),
                  SizedBox(
                    height: size.convert(context, 10),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 100.0, vertical: 9.0),
                    child: Container(
                        height: (60 / 8.148314082864863) *
                            SizeConfig.heightMultiplier,
                        padding: EdgeInsets.only(
                            left: 12.0, right: 12.0, top: 2.0, bottom: 2.0),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(15.0),
                            border: Border.all(color: Colors.black)),
                        child: PinCodeTextField(
                          appContext: context,
                          length: 4,
                          onChanged: (v){},
                                  keyboardType: TextInputType.number,
                                  obscureText: true,
                          textStyle: TextStyle(
                            color: Color(0xff394c81),
                          ),
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          animationType: AnimationType.fade,
                          pinTheme: PinTheme(
                            inactiveColor: Colors.grey,
                            shape: PinCodeFieldShape.underline,
                            borderRadius: BorderRadius.circular(5),
                            fieldHeight: (50 / 8.148314082864863) *
                                SizeConfig.heightMultiplier,
                            fieldWidth: (30 / 4.853932272197492) *
                                SizeConfig.widthMultiplier,
                            activeFillColor: Colors.blue,
                          ),
                          onCompleted: (value) {
                            curruntPinCode = int.parse(value);
                          },
                        )),
                  ),
                  Center(
                    child: Text(
                      'Please Re-Enter 4 Digits PIN',
                      style: TextStyle(
                        fontFamily: fontfaimlyregular,
                        fontSize: (16.6 / 8.148314082864863) *
                            SizeConfig.heightMultiplier,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 100.0, vertical: 9.0),
                    child: Container(
                        height: (60 / 8.148314082864863) *
                            SizeConfig.heightMultiplier,
                        padding: EdgeInsets.only(
                            left: 12.0, right: 12.0, top: 2.0, bottom: 2.0),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(15.0),
                            border: Border.all(color: Colors.black)),
                        child: PinCodeTextField(
                          appContext: context,
                          // autoFocus: ,
                          onChanged: (v) {},
                          
                                  keyboardType: TextInputType.number,
                                  obscureText: true,
                          length: 4,

                          textStyle: TextStyle(
                            color: Color(0xff394c81),
                          ),
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          animationType: AnimationType.fade,
                          pinTheme: PinTheme(
                            inactiveColor: Colors.grey,
                            shape: PinCodeFieldShape.underline,
                            borderRadius: BorderRadius.circular(5),
                            fieldHeight: (50 / 8.148314082864863) *
                                SizeConfig.heightMultiplier,
                            fieldWidth: (30 / 4.853932272197492) *
                                SizeConfig.widthMultiplier,
                            activeFillColor: Colors.blue,
                          ),
                          onCompleted: (value) {
                            rePinCode = int.parse(value);
                          },
                        )),
                  ),
                  Center(
                    child: Text(
                      'Confirm your New PIN-Code',
                      style: TextStyle(
                        fontSize: (16.6 / 8.148314082864863) *
                            SizeConfig.heightMultiplier,
                        fontFamily: fontfaimlyregular,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: size.convert(context, 10),
                  ),
                  filledButton(
                    txt: "Confirm",
                    onTap: () async {
                      setState(() {
                        isloading = true;
                      });
                      if (widget.forgotPin) {
                        resetpincodeafterotp(
                                curruntPinCode, rePinCode, widget.userid)
                            .then((value) {
                          if (value == true) {
                            setState(() {
                              isloading = false;
                            });
                            Navigator.of(context).pushAndRemoveUntil(
                                MaterialPageRoute(
                                    builder: (context) => PinVerification()),
                                (route) => false);
                          }
                        });
                      } else {
                        if (curruntPinCode == rePinCode) {
                          resetpincodeafterotp(
                                  curruntPinCode, rePinCode, widget.userid)
                              .then((value) {
                            if (value == true) {
                              setState(() {
                                isloading = false;
                              });
                              Navigator.of(context).pop();
                              Navigator.of(context).pop();
                              Navigator.of(context).pop();
                            }
                          });
                        } else {
                          setState(() {
                            isloading = false;
                          });
                          showDialog(
                              context: context,
                              builder: (context) {
                                return AlertDialog(
                                  title: Text("Oops !!!"),
                                  content: Text("your pin is not same ..."),
                                  actions: <Widget>[
                                    FlatButton(
                                        onPressed: () {
                                          Navigator.of(context).pop();
                                        },
                                        child: Text("Okay"))
                                  ],
                                );
                              });
                        }
                      }
                    },
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
