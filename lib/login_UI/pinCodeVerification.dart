import 'dart:ui';
import 'package:biznes/login_UI/forgotPassword.dart';
import 'package:biznes/login_UI/home.dart';
import 'package:biznes/Controllers/pincontroller.dart';
import 'package:biznes/repeatedWigets/filledButton.dart';
import 'package:biznes/res/loadingWidget.dart';
import 'package:biznes/res/size.dart';
import 'package:biznes/res/style.dart';
import 'package:flutter/cupertino.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../helper/login_helper.dart';
import 'package:provider/provider.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:biznes/size_config.dart';
import 'package:flutter/material.dart';
import 'package:pin_code_fields/pin_code_fields.dart';

import '../services/register_logic.dart';

class PinVerification extends StatefulWidget {
  @override
  _PinVerificationState createState() => _PinVerificationState();
}

class _PinVerificationState extends State<PinVerification> {
  var formKey = GlobalKey<FormState>();
  String pinCodeTest;

  bool isvisible = false;
  @override
  Widget build(BuildContext context) {
    var loginHelper = Provider.of<LoginHelper>(context);
    return Stack(
      alignment: Alignment.center,
      children: [
        Scaffold(
          backgroundColor: Colors.white,
          body: pinCode(),
        ),
        loadingBody(loginHelper.isLoadinIndicator),
      ],
    );
  }

  pinCode() {
    var loginHelper = Provider.of<LoginHelper>(context);
    var userid = Provider.of<LoginHelper>(context).user.sId;
    return Container(
      height: MediaQuery.of(context).size.height,
      alignment: Alignment.center,
      child: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              child: SvgPicture.asset(
                "assets/images/NewUpdatedPIN.svg",
              ),
            ),
            SizedBox(
              height: size.convert(context, 15),
            ),
            Container(
              padding: EdgeInsets.symmetric(vertical: 20),
              margin: EdgeInsets.symmetric(
                  horizontal: size.convertWidth(context, 21)),
              decoration: BoxDecoration(
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                    color: Color(0xffd4d4d4),
                    blurRadius: 6,
                    offset: Offset(0, 3),
                  ),
                ],
                borderRadius: BorderRadius.circular(size.convert(context, 20)),
              ),
              child: Column(
                children: <Widget>[
                  Center(
                    child: Padding(
                      padding: const EdgeInsets.only(top: 12.0),
                      child: Text(
                        "Enter your PIN",
                        style: style.QuicksandBold(
                            fontSize: size.convert(context, 30),
                            color: Color(0xff394c81)),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: size.convert(context, 5),
                  ),
                  Container(
                    child: RichText(
                      text: TextSpan(
                          text: 'Please enter 4 Digits PIN-CODE',
                          style: style.MontserratRegular(
                              fontSize: size.convert(context, 12))),
                    ),
                  ),
                  SizedBox(
                    height: size.convert(context, 15),
                  ),
                  Container(
                    height: ((814.8314082864863 / 8.148314082864863) / 12) *
                        SizeConfig.heightMultiplier,
                    width: ((485.3932272197492 / 4.853932272197492) / 1.8) *
                        SizeConfig.widthMultiplier,
                    padding: EdgeInsets.only(left: 10.0, right: 10),
                    decoration: BoxDecoration(
                        color: Colors.white.withOpacity(0.7),
                        borderRadius: BorderRadius.circular(14.0),
                        border: Border.all(color: Color(0xff5e5c5c))),
                    child: PinCodeTextField(
                      keyboardType: TextInputType.number,
                      obscureText: true,
                      textStyle: TextStyle(
                        color: Color(0xff394c81),
                      ),
                      pinTheme: PinTheme(
                        inactiveColor: Color(0xffc7c7c7),
                        shape: PinCodeFieldShape.underline,
                        borderRadius: BorderRadius.circular(5),
                        fieldHeight: (50 / 8.148314082864863) *
                            SizeConfig.heightMultiplier,
                        fieldWidth: (30 / 4.853932272197492) *
                            SizeConfig.widthMultiplier,
                        activeFillColor: Colors.blue,
                      ),
                      length: 4,
                      appContext: context,
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      backgroundColor: Colors.white.withOpacity(0.7),
                      animationType: AnimationType.fade,
                      animationDuration: Duration(milliseconds: 300),
                      onChanged: (value) {
                        pinCodeTest = value;
                      },
                    ),
                  ),
                  SizedBox(
                    height: size.convert(context, 5),
                  ),
                  InkWell(
                    onTap: () {
                      print("forgot pin");

                      Navigator.of(context, rootNavigator: true)
                          .pushAndRemoveUntil(
                              MaterialPageRoute(
                                  builder: (context) => ForgotPassword(
                                        title: "Forgot PIN-CODE",
                                        Forgotpin: true,
                                      )),
                              (route) => false);
                    },
                    child: Container(
                      child: RichText(
                        text: TextSpan(
                            text: 'Forgot your PIN-CODE?',
                            style: style.MontserratMedium(
                                fontSize: size.convert(context, 12))),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: size.convert(context, 5),
                  ),
                  InkWell(
                    onTap: () async {
                      setState(() {
                        isvisible = true;
                      });
                      SharedPreferences _prefs =
                          await SharedPreferences.getInstance();
                      loginHelper.onSignOut();
                      loginHelper.removeListener(() {});
                      _prefs.remove("userid");
                      _prefs.remove("channelid");
                      _prefs.remove("channelname");
                      _prefs.remove("channeldescription");
                      setState(() {
                        isvisible = false;
                      });
                      Navigator.of(context, rootNavigator: true)
                          .pushAndRemoveUntil(
                              MaterialPageRoute(
                                  builder: (context) => LoginScreen()),
                              (route) => false);
                    },
                    child: Container(
                      //color: Colors.red,
                      child: RichText(
                        text: TextSpan(
                            text: 'Logout',
                            style: style.MontserratRegular(
                                fontSize: size.convert(context, 12))),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: size.convert(context, 10),
                  ),
                  filledButton(
                    txt: "Done",
                    onTap: () async {
                      verifyPincode(userid, pinCodeTest, context);
                      // print(userid);
                      // print(pinCodeTest);
                      // pinCodeTest == null
                      //     ? print("null")
                      //     : setState(() {
                      //         isvisible = true;
                      //       });
                      // verifyPincodeafterlogin(userid, pinCodeTest, context)
                      //     .then((value) {
                      //   setState(() {
                      //     isvisible = false;
                      //   });
                      // if (value == true) {
                      //   print(value);
                      //   Navigator.of(context)
                      //       .pushReplacement(MaterialPageRoute(
                      //           builder: (_) => ProductHome(
                      //                 userid: userid,
                      //               )));
                      // } else {
                      //     print("your pin was incorrect..... !!!");
                      //   }
                      // });
                    },
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
