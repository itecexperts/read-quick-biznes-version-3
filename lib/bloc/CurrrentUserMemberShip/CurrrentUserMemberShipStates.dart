import 'package:biznes/model/membershitpdetailmodel.dart';
import 'package:equatable/equatable.dart';

abstract class CurrrentUserMemberShipStates extends Equatable {
  @override
  List<Object> get props => [];
}

class CurrrentUserMemberShipStatesInitailState
    extends CurrrentUserMemberShipStates {}

class CurrrentUserMemberShipStatessLoadingState
    extends CurrrentUserMemberShipStates {}

class CurrrentUserMemberShipStatesLoadedState
    extends CurrrentUserMemberShipStates {
  final List<Membershipdetailmodel> memberShipDetails;
  CurrrentUserMemberShipStatesLoadedState({this.memberShipDetails});
}

class CurrrentUserMemberShipErrorState extends CurrrentUserMemberShipStates {
  final error;
  CurrrentUserMemberShipErrorState({this.error});
}
