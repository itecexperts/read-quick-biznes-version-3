import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

abstract class FetchCurrrentUserMemberShip extends Equatable {}

class FetchMembershipCurrrentUserMemberShip
    extends FetchCurrrentUserMemberShip {
  final String userId;
  FetchMembershipCurrrentUserMemberShip({
    @required this.userId,
  });
  List<Object> get props => [userId];
}
