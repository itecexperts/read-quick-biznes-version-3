import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

abstract class SearchProductEvents extends Equatable {}

class FetchProduct extends SearchProductEvents {
  final String categoryId;
  final String subcategoryId;
  final String countryId;
  final String cityId;
  final String roleId;
  final String userId;
  final String query;
  FetchProduct(
      {@required this.categoryId,
      @required this.subcategoryId,
      @required this.countryId,
      @required this.cityId,
      @required this.roleId,
      this.query = '',
      @required this.userId});
  List<Object> get props => [
        categoryId +
            subcategoryId +
            countryId +
            cityId +
            roleId +
            userId +
            query
      ];
}
