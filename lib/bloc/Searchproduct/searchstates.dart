import 'dart:collection';

import 'package:biznes/model/inboxlistmessages.dart';
import 'package:biznes/model/searchmodel.dart';
import 'package:equatable/equatable.dart';

abstract class SearchProductStates extends Equatable {
  @override
  List<Object> get props => [];
}

class SearchProductInitailState extends SearchProductStates {}

class SearchProductLoadingState extends SearchProductStates {}

class SearchProductLoadedState extends SearchProductStates {
  final UnmodifiableListView<ProductSearchModel> searchProduct;
  SearchProductLoadedState({this.searchProduct});
}

class SearchProductErrorState extends SearchProductStates {
  final error;
  SearchProductErrorState({this.error});
}
