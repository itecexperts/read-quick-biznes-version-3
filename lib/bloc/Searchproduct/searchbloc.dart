import 'dart:collection';
import 'dart:io';
import 'package:biznes/Controllers/backendservicescontroller.dart';
import 'package:biznes/bloc/Exceptions.dart';
import 'package:biznes/bloc/Searchproduct/searchevents.dart';
import 'package:biznes/bloc/Searchproduct/searchstates.dart';
import 'package:biznes/model/searchmodel.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SearchProductBloc extends Bloc<SearchProductEvents, SearchProductStates> {
  List<ProductSearchModel> search;
  UnmodifiableListView<ProductSearchModel> newsearch;
  SearchProductBloc() : super(SearchProductInitailState());
  @override
  Stream<SearchProductStates> mapEventToState(
      SearchProductEvents event) async* {
    if (event is FetchProduct) {
      yield SearchProductLoadingState();
      try {
        search = await searchProductFunction(
            categoryId: event.categoryId,
            subcategoryId: event.subcategoryId,
            countryId: event.countryId,
            cityId: event.cityId,
            userId: event.userId,
            roleId: event.roleId);
        print("heelojjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj");
        print(search.length);
        newsearch = UnmodifiableListView(search.where((message) => message.userRole.length ==
                1
            ? message.userRole[0].title.toLowerCase().startsWith(event.query.toLowerCase()) ||
                message.title
                    .toString()
                    .toLowerCase()
                    .startsWith(event.query.toString().toLowerCase()) ||
                message.moq
                    .toString()
                    .toLowerCase()
                    .startsWith(event.query.toString().toLowerCase()) ||
                message.price.toString().startsWith(event.query)
            : message.userRole.length == 2
                ? message.userRole[0].title.toLowerCase().startsWith(event.query) ||
                    message.userRole[1].title
                        .toLowerCase()
                        .startsWith(event.query.toLowerCase()) ||
                    message.title
                        .toString()
                        .toLowerCase()
                        .startsWith(event.query.toString().toLowerCase()) ||
                    message.moq
                        .toString()
                        .toLowerCase()
                        .startsWith(event.query.toString().toLowerCase()) ||
                    message.price.toString().startsWith(event.query)
                : message.userRole.length == 3
                    ? message.userRole[0].title.toLowerCase().startsWith(event.query.toLowerCase()) ||
                        message.userRole[1].title
                            .toLowerCase()
                            .startsWith(event.query.toLowerCase()) ||
                        message.userRole[2].title
                            .toLowerCase()
                            .startsWith(event.query.toLowerCase()) ||
                        message.title
                            .toString()
                            .toLowerCase()
                            .startsWith(event.query.toString().toLowerCase()) ||
                        message.moq.toString().toLowerCase().startsWith(event.query.toString().toLowerCase()) ||
                        message.price.toString().startsWith(event.query)
                    : message.userRole.length == 4
                        ? message.userRole[0].title.toLowerCase().startsWith(event.query.toLowerCase()) || message.userRole[1].title.toLowerCase().startsWith(event.query.toLowerCase()) || message.userRole[2].title.toLowerCase().startsWith(event.query.toLowerCase()) || message.userRole[3].title.toLowerCase().startsWith(event.query.toLowerCase()) || message.title.toString().toLowerCase().startsWith(event.query.toString().toLowerCase()) || message.moq.toString().toLowerCase().startsWith(event.query.toString().toLowerCase()) || message.price.toString().startsWith(event.query)
                        : message.price.toString().startsWith(event.query) || message.moq.toString().toLowerCase().startsWith(event.query.toString().toLowerCase())));
        yield SearchProductLoadedState(searchProduct: newsearch);
      } on SocketException {
        yield SearchProductErrorState(
            error: NoInterNetException(message: "No Internet"));
      } on HttpException {
        yield SearchProductErrorState(
            error:
                NoServiceFoundException(message: "No Service Found Exception"));
      } on FormatException {
        yield SearchProductErrorState(
            error: InvalidFormatException(message: "Invalid Format Exception"));
      } catch (e) {
        yield SearchProductErrorState(
            error: UnknownException(message: "Unknown Error Message"));
      }
    }
  }
}
