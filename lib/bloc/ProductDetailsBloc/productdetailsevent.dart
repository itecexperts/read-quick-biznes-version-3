import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

abstract class ProductDetailsEvents extends Equatable {}

class FetchProductDetails extends ProductDetailsEvents {
  final String productId;
  final String userId;
  FetchProductDetails({@required this.productId, @required this.userId});
  List<Object> get props => [productId + userId];
}
