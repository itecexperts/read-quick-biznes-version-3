import 'package:biznes/model/single_Product.dart';
import 'package:equatable/equatable.dart';

abstract class ProductDetailStates extends Equatable {
  @override
  List<Object> get props => [];
}

class ProductDetailInitailState extends ProductDetailStates {}

class ProductDetailLoadingState extends ProductDetailStates {}

class ProductDetailLoadedState extends ProductDetailStates {
  final SingleProductdetails singleProductdetails;
  ProductDetailLoadedState({this.singleProductdetails});
}

class ProductDetailErrorState extends ProductDetailStates {
  final error;
  ProductDetailErrorState({this.error});
}
