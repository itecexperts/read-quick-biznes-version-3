import 'dart:collection';

import 'package:biznes/model/inboxlistmessages.dart';
import 'package:equatable/equatable.dart';

abstract class AllMessageStates extends Equatable {
  @override
  List<Object> get props => [];
}

class AllMessagesInitailState extends AllMessageStates {}

class AllMessagesLoadingState extends AllMessageStates {}

class AllMessagesLoadedState extends AllMessageStates {
  final UnmodifiableListView<AllMessages> allmessages;
  AllMessagesLoadedState({this.allmessages});
}

class AlbumErrorState extends AllMessageStates {
  final error;
  AlbumErrorState({this.error});
}
