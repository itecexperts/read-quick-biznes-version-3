// import 'package:biznes/model/inboxlistmessages.dart';
import 'package:biznes/model/categoryinbox.dart';
import 'package:equatable/equatable.dart';

abstract class AllCategoriesMessageStates extends Equatable {
  @override
  List<Object> get props => [];
}

class AllCategoriesMessageStatesInitailState extends AllCategoriesMessageStates {}

class AllCategoriesMessageStatessLoadingState extends AllCategoriesMessageStates {}

class AllCategoriesMessageStatesLoadedState extends AllCategoriesMessageStates {
  final CategoryInbox categoryInbox;
  AllCategoriesMessageStatesLoadedState({this.categoryInbox});
}

class AllCategoriesMessageErrorState extends AllCategoriesMessageStates {
  final error;
  AllCategoriesMessageErrorState({this.error});
}
