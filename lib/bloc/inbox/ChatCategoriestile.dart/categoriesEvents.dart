import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
abstract class FetchCategories extends Equatable {}
class FetchMessagesbyCategories extends FetchCategories {
  final String userId;
  FetchMessagesbyCategories({@required this.userId,});
  List<Object> get props => [userId];
}