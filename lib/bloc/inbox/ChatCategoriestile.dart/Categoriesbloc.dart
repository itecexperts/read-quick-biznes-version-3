import 'dart:io';
import 'package:biznes/Controllers/backendservicescontroller.dart';
import 'package:biznes/bloc/Exceptions.dart';
import 'package:biznes/bloc/inbox/ChatCategoriestile.dart/categoriesEvents.dart';
import 'package:biznes/bloc/inbox/ChatCategoriestile.dart/categoriesState.dart';
import 'package:biznes/model/categoryinbox.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CategoriesInboxBloc
    extends Bloc<FetchCategories, AllCategoriesMessageStates> {
  CategoryInbox categoryInbox;
  CategoriesInboxBloc() : super(AllCategoriesMessageStatesInitailState());
  @override
  Stream<AllCategoriesMessageStates> mapEventToState(
      FetchCategories event) async* {
    if (event is FetchMessagesbyCategories) {
      yield AllCategoriesMessageStatessLoadingState();
      try {
        categoryInbox = await getAllInboxMessagesByCategories(event.userId);
        print("heelo");
        print(categoryInbox);
        yield AllCategoriesMessageStatesLoadedState(
            categoryInbox: categoryInbox);
      } on SocketException {
        yield AllCategoriesMessageErrorState(
            error: NoInterNetException(message: "No Internet Connection"));
      } on HttpException {
        yield AllCategoriesMessageErrorState(
            error:
                NoServiceFoundException(message: "No Service Found Exception"));
      } on FormatException {
        yield AllCategoriesMessageErrorState(
            error: InvalidFormatException(message: "Invalid Format Exception"));
      } catch (e) {
        yield AllCategoriesMessageErrorState(
            error: UnknownException(message: "Unknown Error Message"));
      }
    }
  }
}
