import 'package:biznes/helper/login_helper.dart';
import 'package:biznes/login_UI/home.dart';
import 'package:biznes/services/register_logic.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:toast/toast.dart';
appUserLogOut(message,context)async{
  var loginHelper = Provider.of<LoginHelper>(context,listen: false);
  SharedPreferences _prefs =
  await SharedPreferences.getInstance();
  if (message== "Token Expired or Invalid" || message == "UnAuthorized User") {
    Toast.show("Your Token has been expired", context,gravity: Toast.TOP);
    appuserlogout(loginHelper.user.sId)
        .then((value) {
      if (value["Success"] == true) {
        loginHelper.onSignOut();
        loginHelper.removeListener(() {});
        _prefs.remove("userid");
        _prefs.remove("token");
        print("this is before logout");
        Navigator.of(context).pop();
        Navigator.of(context,rootNavigator: true).pushAndRemoveUntil(MaterialPageRoute(builder: (context)=>LoginScreen()), (route) => false);
      }
    });
  }else{
    return;
  }
}