import 'dart:convert';
import 'dart:core';
import 'package:biznes/guest/guestdata.dart';
import 'package:biznes/helper/login_helper.dart';
import 'package:biznes/login_UI/home.dart';
import 'package:biznes/model/companycurrenproducts.dart';
import 'package:biznes/model/inboxlistmessages.dart';
import 'package:biznes/product_UI/product_home.dart';
import 'package:provider/provider.dart';
import 'logoutFunction.dart';
import 'server.dart';
import 'package:flutter/material.dart';
import 'package:biznes/model/productsunits.dart';
import '../model/bannersmodel.dart';
import '../model/bulkmodel.dart';
import 'package:biznes/model/conversationmodel.dart';
import '../model/inquiresmodel.dart';
import '../model/notifications.dart';
import 'package:biznes/model/membershitpdetailmodel.dart';
import 'package:biznes/model/productsbyid.dart';
import '../model/notificationcount.dart';
import 'package:biznes/model/searchmodel.dart';
import '../model/getguestmessges.dart';
import 'dart:io';
import 'package:biznes/model/caegoryModel.dart';
import 'package:biznes/model/citiesModel.dart';
import '../model/guesttobrokermodel.dart';
import 'package:biznes/model/countriesModel.dart';
import 'package:biznes/model/planCategories.dart';
import 'package:biznes/model/planModel.dart';
import '../model/editprofilemodel.dart';
import 'package:biznes/model/productModel.dart';
import 'package:biznes/model/roleModel.dart';
import 'package:biznes/model/search_product.dart';
import 'package:biznes/model/send_to_all_message_model.dart';
import 'package:biznes/model/single_Product.dart';
import 'package:biznes/model/subCategoryModel.dart';
import 'package:biznes/model/userModel.dart';
import 'package:biznes/model/userPlanSubscrition.dart';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'package:biznes/Toasts.dart/toasts.dart';
import 'package:toast/toast.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:biznes/model/categoryinbox.dart';

// All plane
Future<List<PlanModel>> getAllPlan(String userid, context) async {
  SharedPreferences _prefs = await SharedPreferences.getInstance();
  String tokens = _prefs.getString("token");
  var urlPlan = '$base_url/get/all/plans/$userid';
  try {
    var response = await http.get(Uri.encodeFull(urlPlan),
        headers: {HttpHeaders.authorizationHeader: tokens});
    var planData = jsonDecode(response.body);
    List<PlanModel> getPlansList = [];
    planData['plans'].forEach((plan) {
      getPlansList.add(PlanModel.fromJson(plan));
    });
    return getPlansList;
  } on SocketException {
    showToast(context, "No Internet Connection");
  } catch (e) {
    showToast(context, "$e");
  }
}

Future<List<PlanCategoriesModel>> getAllPlanCategories(
    String userid, context) async {
  print("data loading");
  SharedPreferences _prefs = await SharedPreferences.getInstance();
  String tokens = _prefs.getString("token");
  var getAllPlanCategoryUrl = '$base_url/get/product-parent/category/$userid';
  try {
    var response = await http.get(getAllPlanCategoryUrl,
        headers: {HttpHeaders.authorizationHeader: tokens});
    Map responseData = jsonDecode(response.body);
    List<PlanCategoriesModel> listPlanCategories = [];
    responseData['categories'].forEach((f) {
      listPlanCategories.add(PlanCategoriesModel.fromJson(f));
    });
    return listPlanCategories;
  } on SocketException {
    showToast(context, "No Internet Connection");
  } catch (e) {
    showToast(context, "$e");
  }
}

Future<bool> getSubscription(
    UserPlanSubscription userPlanSubscription, BuildContext context) async {
  SharedPreferences _prefs = await SharedPreferences.getInstance();
  String tokens = _prefs.getString("token");
  var subscriptionUrl = '$base_url/user/plan/subscription';
  Map datas = {
    "planId": "${userPlanSubscription.planId}",
    "userId": "${userPlanSubscription.userId}",
    "categoryId": "${userPlanSubscription.categoryId}",
    "subcategories": userPlanSubscription.subcategories,
  };
  Map<String, String> headers = {
    'Content-type': 'application/json',
    'Accept': 'application/json',
    HttpHeaders.authorizationHeader: tokens,
  };
  var response = await http.post(subscriptionUrl,
      body: jsonEncode(datas), headers: headers);
  var getResponse = jsonDecode(
    response.body,
  );
  print(getResponse.toString());
  if (getResponse['Success'] == true) {
    Toast.show("You have successfully got membership", context,
        duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
    print(getResponse.toString());
    print("succees");
    return true;
  } else {
    Toast.show(getResponse['message'], context,
        duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
    print("false");
    return false;
  }
}

Future<bool> brokerplansubscription(
    UserPlanSubscription userPlanSubscription, context) async {
  String url = "$base_url/broker/plan/subscription";
  SharedPreferences _prefs = await SharedPreferences.getInstance();
  String token = _prefs.getString("token");
  Map data = {
    "planId": userPlanSubscription.planId,
    "userId": userPlanSubscription.userId,
    "categoryId": userPlanSubscription.categoryId,
  };
  var request = await http
      .post(url, body: data, headers: {HttpHeaders.authorizationHeader: token});
  var response = jsonDecode(request.body);
  if (response['Success'] == true) {
    print("succees");
    return true;
  } else {
    Toast.show(response['message'], context,
        duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
    print("false");
    return false;
  }
}

Future<bool> checkboxmessagemodel(
    {String userId,
    String categoryId,
    String subCategoryId,
    String message,
    Bulk bulk,
    context}) async {
  print(userId);
  SharedPreferences _prefs = await SharedPreferences.getInstance();
  String token = _prefs.getString("token");
  String url = "$base_url/create/messages/bulk";
  Map data = {
    "userId": userId,
    "categoryId": categoryId,
    "subCategoryId": subCategoryId,
    "message": message,
    "createdBy": bulk.createdbyid,
  };
  Map<String, String> headers = {
    'Content-type': 'application/json',
    'Accept': 'application/json',
    HttpHeaders.authorizationHeader: token,
  };
  var response = await http.post(url, body: jsonEncode(data), headers: headers);
  var getResponse = jsonDecode(response.body);
  print(getResponse);
  if (getResponse['success'] == true) {
    Toast.show(getResponse['message'], context,
        duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
    print("succees");
    return true;
  } else {
    Toast.show(getResponse['message'], context,
        duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
    print("false");
    return false;
  }
}

Future<bool> resetpincodeafterotp(
    int newpincode, int confirmpincode, String userid) async {
  Map data = {
    "newPinCode": newpincode.toString(),
    "confirmPinCode": confirmpincode.toString(),
    "userId": userid,
  };
  String url = "$base_url/reset-pincode/confirm-otp";
  var request = await http.post(url, body: data);
  var responsedata = jsonDecode(request.body);
  print(responsedata);
  return responsedata["Success"];
}

Future<Map> signIn(String email, String password, String token, context) async {
  FocusScope.of(context).unfocus();
  var signInUrl = '$base_url/login/user';
  Map data = {
    'email': email,
    'password': password,
    'mobileToken': token,
  };
  try {
    http.Response response = await http.post(signInUrl, body: data);
    var map = await jsonDecode(response.body);
    print(response.statusCode);
    if (response.statusCode == 200) {
      if (map['Success']) {
        showToast(context, "Logged In Successfully");
        User user = User.fromJson(map['user']);
        return {
          "user": user,
          "success": true,
          "token": map['token'],
          "userid": user.sId
        };
      } else {
        showToast(context, "Login failed");
        return {"success": false};
      }
    }
  } on SocketException {
    showToast(context, "No Internet Connection");
  } on HttpException {
    showToast(context, "Server Connection Not found");
  } on FormatException {
    showToast(context, "Invalid Format Exception");
  } catch (e) {
    showToast(context, e);
  }
}

signinForGuest(context) {
  String token = "hidden";
  var loginhelper = Provider.of<LoginHelper>(context, listen: false);
  User user = User.fromJson(guestData['user']);
  loginhelper.onLogIn(token, user);
  Navigator.of(context)
      .pushReplacement(MaterialPageRoute(builder: (_) => ProductHome()));
  print(user.cityId);
}

Future<Map> profileupdateforbroker(
    EditProfileModel editProfileModel, String userId, context) async {
  SharedPreferences _prefs = await SharedPreferences.getInstance();
  String token = _prefs.getString("token");
  String url = "$base_url/user/edit/profile";
  var request2 = http.MultipartRequest('POST', Uri.parse(url))
    ..fields['userId'] = userId
    ..fields['username'] = editProfileModel.username
    ..fields['email'] = editProfileModel.email
    ..fields['countryId'] = editProfileModel.countryId
    ..fields['cityId'] = editProfileModel.city;
  request2.headers['authorization'] = token;
  if (editProfileModel.profilePic == "") {
    print("hello");
  } else {
    request2.files.add(await http.MultipartFile.fromPath(
        'profilePic', editProfileModel.profilePic));
  }

  http.Response productResponse = await http.Response.fromStream(
    await request2.send(),
  );
  var data = jsonDecode(productResponse.body);
  User user = User.fromJson(data["user"]);
  print(productResponse.body);
  return {"user": user};
}

Future<Map> profileupdate(EditProfileModel editProfileModel, String userId,
    context, List<String> userrole) async {
  String url = "$base_url/user/edit/profile";
  SharedPreferences _prefs = await SharedPreferences.getInstance();
  String token = _prefs.getString("token");
  if (editProfileModel.profilePic == "") {
    Map data = {
      "organizationName": editProfileModel.organizationName,
      "userId": userId,
      "username": editProfileModel.username,
      "email": editProfileModel.email,
      "countryId": editProfileModel.countryId,
      "cityId": editProfileModel.city,
      "role": editProfileModel.addingrole,
    };
    Map<String, String> headers = {
      'Content-type': 'application/json',
      'Accept': 'application/json',
      HttpHeaders.authorizationHeader: token,
    };
    http.Response request =
        await http.post(url, body: jsonEncode(data), headers: headers);
    var map = jsonDecode(request.body);
    print(map['user']);

    User user = User.fromJson(map['user']);
    return {"user": user};
  } else {
    var request2 = http.MultipartRequest('POST', Uri.parse(url))
      ..fields['organizationName'] = editProfileModel.organizationName
      ..fields['userId'] = userId
      ..fields['username'] = editProfileModel.username
      ..fields['email'] = editProfileModel.email
      ..fields['countryId'] = editProfileModel.countryId
      ..fields['cityId'] = editProfileModel.city;
    request2.headers['authorization'] = token;
    request2.files.add(await http.MultipartFile.fromPath(
        'profilePic', editProfileModel.profilePic));
    http.Response productResponse = await http.Response.fromStream(
      await request2.send(),
    );
    Map data = {
      "organizationName": editProfileModel.organizationName,
      "userId": userId,
      "username": editProfileModel.username,
      "email": editProfileModel.email,
      "countryId": editProfileModel.countryId,
      "cityId": editProfileModel.city,
      "role": editProfileModel.addingrole,
    };
    Map<String, String> headers = {
      'Content-type': 'application/json',
      'Accept': 'application/json',
      HttpHeaders.authorizationHeader: token,
    };
    http.Response request =
        await http.post(url, body: jsonEncode(data), headers: headers);
    print(productResponse.body);
    var map = jsonDecode(request.body);
    print(map['user']);

    User user = User.fromJson(map['user']);
    return {"user": user};
  }
}

Future<Map> resetPassword(
    {String code, String contactNumber, BuildContext context}) async {
  String urlResetPassword = '$base_url/resetPassword';
  Map data = {
    "code": code,
    "contactNumber": "92$contactNumber",
  };
  var response = await http.post(urlResetPassword, body: data);
  print(response.body);
  Map dataBody = await jsonDecode(response.body);

  if (dataBody['success'] == false) {
    Toast.show(dataBody['message'], context,
        duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
    return dataBody;
  } else {
    return dataBody;
  }
}

Future<Map> resetPincode(
    {String code, String contactNumber, BuildContext context}) async {
  String urlResetPassword = '$base_url/resetPinCode';
  Map data = {
    "code": code,
    "contactNumber": "92$contactNumber",
  };
  print(contactNumber);
  print(code);
  print(data.length);
  var response = await http.post(urlResetPassword, body: data);
  var datarespon = jsonDecode(response.body);

  return jsonDecode(response.body);
}

Future deleteproductimage(String productid, String image, context) async {
  String url = "$base_url/remove/product-image";
  SharedPreferences _prefs = await SharedPreferences.getInstance();
  String token = _prefs.getString("token");
  Map data = {
    "productId": productid,
    "imagePath": image,
  };
  var request = await http
      .post(url, body: data, headers: {HttpHeaders.authorizationHeader: token});
  var response = jsonDecode(
    request.body,
  );
  appUserLogOut(response['message'], context);
  print(response);
}

Future<Map> resetnewpasswordafterotp(
    String newPassword, String confirmPassword, String userId, context) async {
  String url = '$base_url/reset-password/confirm-otp';
  Map data = {
    'newPassword': newPassword,
    'confirmPassword': confirmPassword,
    'userId': userId,
  };
  var response = await http.post(url, body: data);
  Map returndata = jsonDecode(response.body);
  return returndata;
}

Future forgetPassword(String phoneNumber, context) async {
  var forgetUrl = '$base_url/forgot-password/request';
  Map data = {'contactNumber': "92$phoneNumber"};
  var response = await http.post(forgetUrl, body: data);
  Map responseback = jsonDecode(response.body);
  Toast.show(responseback['message'], context,
      duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
  print(response.body);
  if (responseback["Success"]) {
    return true;
  } else {
    return false;
  }
}

Future forgotpincoderequest(int phoneNumber, context, String token) async {
  String url = "$base_url/forgot-pincode/request";
  Map data = {
    "contactNumber": "92${phoneNumber.toString()}",
  };
  var response = await http
      .post(url, body: data, headers: {HttpHeaders.authorizationHeader: token});
  print(response.body);
  var returningdata = jsonDecode(response.body);
  Toast.show(returningdata["message"], context,
      duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
}

Future<bool> verifyPhone(String contactNumber, String code, context) async {
  var verifyUrl = '$base_url/user/verify/OTP';
  print('contact $contactNumber code $code');
  String number = "$contactNumber";
  print(number);
  Map verifyCode = {
    'contactNumber': number,
    'code': code,
  };

  var response = await http.post(verifyUrl, body: verifyCode);
  print('response : ${response.body}');
  Map map = await jsonDecode(response.body);
  if (map['message'] == 'Verification successfull!') {
    Toast.show(map['message'], context,
        duration: Toast.LENGTH_SHORT, gravity: Toast.TOP);

    return true;
  } else if (map['message'] == 'Invalid code') {
    Toast.show(map['message'], context,
        duration: Toast.LENGTH_SHORT, gravity: Toast.TOP);
    return false;
  }
  return false;
}

Future<bool> verifyPincodeafterlogin(
    String userid, String code, context) async {
  var verifycodeUrl = '$base_url/veify/user-pin-code';
  Map verifyCode = {
    'userId': userid.toString(),
    'userPinCode': code.toString(),
  };
  try {
    var response = await http.post(verifycodeUrl, body: verifyCode);
    print('response : ${response.body}');
    Map map = await jsonDecode(response.body);
    if (map['message'] == 'Verified successfully') {
      Toast.show(map['message'], context,
          duration: Toast.LENGTH_SHORT, gravity: Toast.TOP);
      return true;
    } else if (map['message'] == 'Invalid user id or Pin Code') {
      Toast.show(map['message'], context,
          duration: Toast.LENGTH_SHORT, gravity: Toast.TOP);
      return false;
    }
  } on SocketException {
    showToast(context, "No Internet Connection");
  } on HttpException {
    showToast(context, "Server Connection Not found");
  } on FormatException {
    showToast(context, "Invalid Format Exception");
  } catch (e) {
    return showToast(context, e);
  }
  return false;
}

Future<List<CountriesModel>> getCountries() async {
  var countryUrl = '$base_url/get/countries';
  var countryResponse = await http.get(countryUrl);
  var data = jsonDecode(countryResponse.body);

  List<CountriesModel> _list = [];
  data['countries'].forEach((f) {
    _list.add(CountriesModel.fromJson(f));
  });
  return _list;
}

Future<Map> getproductcount(
    SendMessageToAllMessageModel countProductModel, userid, context) async {
  print(countProductModel.userRole + "newid");
  print(countProductModel.userId);
  SharedPreferences _prefs = await SharedPreferences.getInstance();
  String token = _prefs.getString("token");
  Map data = {
    "category": countProductModel.categoryId,
    "subcategory": countProductModel.subCategoryId,
    "country": countProductModel.countryId,
    "city": countProductModel.cityId,
    'userId': userid,
    "roleId": countProductModel.userRole,
  };
  var productcount = '$base_url/product/count/allMessages';
  var countryResponse = await http.post(productcount,
      body: data, headers: {HttpHeaders.authorizationHeader: token});
  var response = jsonDecode(countryResponse.body);
  print(response);
  appUserLogOut(response['message'], context);
  return jsonDecode(countryResponse.body);
}

Future<Map> brokercount(GuestToBrokermodel countProductModel, context) async {
  SharedPreferences _prefs = await SharedPreferences.getInstance();
  String tokens = _prefs.getString("token");
  Map data = {
    "country": countProductModel.country,
    "city": countProductModel.city,
    "category": countProductModel.category,
  };
  var guestcount = '$base_url/broker/count/allBroker';
  var countryResponse = await http.post(guestcount,
      body: data, headers: {HttpHeaders.authorizationHeader: tokens});
  var response = jsonDecode(countryResponse.body);
  appUserLogOut(response['message'], context);
  return jsonDecode(countryResponse.body);
}

// Future<List<Membershipdetailmodel>> getmembershipdetails(
//     String userid, context) async {
//   String url = "$base_url/get/current/user/membership/$userid";
//   SharedPreferences _prefs = await SharedPreferences.getInstance();
//   String tokens = _prefs.getString("token");
//   var response =
//       await http.get(url, headers: {HttpHeaders.authorizationHeader: tokens});
//   print(response.body);
//   var data = jsonDecode(response.body);
//   appUserLogOut(data[0]['message'].toString() ?? "", context);
//   List<Membershipdetailmodel> details;
//   details = (json.decode(response.body) as List)
//       .map((i) => Membershipdetailmodel.fromJson(i))
//       .toList();
//   return details;
// }

Future<List<CitiesModel>> getCities(countryId) async {
  var cityUrl = '$base_url/get/city/$countryId';
  var cityResponse = await http.get(cityUrl);
  var data = jsonDecode(cityResponse.body);
  print(data);
  List<CitiesModel> cities = [];
  data['cities'].forEach((f) {
    cities.add(CitiesModel.fromJson(f));
  });

  return cities;
}

Future<List<Banners>> getallbanners() async {
  String url = "$base_url/get/all/banners";
  var response = await http.get(url);
  var data = jsonDecode(response.body);
  List<Banners> banners = [];
  data['banners'].forEach((f) {
    banners.add(Banners.fromJson(f));
  });
  return banners;
}

Future<NewNotification> gettingnotification(
    String userid, String token, context) async {
  String url = "$base_url/get/user/notifications/$userid";
  SharedPreferences _prefs = await SharedPreferences.getInstance();
  String tokens = _prefs.getString("token");
  var response =
      await http.get(url, headers: {HttpHeaders.authorizationHeader: tokens});
  var data = jsonDecode(response.body);
  appUserLogOut(data['message'], context);
  print(data);
  NewNotification newNotification = NewNotification.fromJson(data);
  print("hello");
  return newNotification;
}

Future<AllInquiries> getallinquires(String userid, context) async {
  String url = "$base_url/all/inquiries/get/$userid";
  print(url);
  SharedPreferences _prefs = await SharedPreferences.getInstance();
  String token = _prefs.getString("token");
  var response = await http.get(url, headers: {
    HttpHeaders.authorizationHeader: token,
  });
  var data = jsonDecode(response.body);
  print(data);
  appUserLogOut(data['message'], context);
  AllInquiries inquiries = AllInquiries.fromJson(data);
  return inquiries;
}

Future<List<RoleModel>> getRoles({String registerpage}) async {
  var rolesUrl = '$base_url/get/roles';
  var response = await http.get(rolesUrl);
  var data = jsonDecode(response.body);
  List<RoleModel> roles = [];
  data['roles'].forEach((f) {
    if (registerpage == "Register") {
      roles.add(RoleModel.fromJson(f));
    } else {
      if (f['title'] == "Broker") {
        return null;
      } else {
        roles.add(RoleModel.fromJson(f));
      }
    }
  });
  return roles;
}

Future<List<CategoriesModel>> getCategories() async {
  var categoryUrl = '$base_url/get/categories';
  var categoryResponse = await http.get(
    categoryUrl,
  );
  var data = jsonDecode(categoryResponse.body);
  List<CategoriesModel> _listCategory = [];
  data['categories'].forEach((f) {
    _listCategory.add(CategoriesModel.fromJson(f));
  });
  return _listCategory;
}

Future<List<SubCategories>> getSubCategories({
  String catId,
}) async {
  SharedPreferences _prefs = await SharedPreferences.getInstance();
  String token = _prefs.getString("token");
  var subCategoryUrl = '$base_url/get/subcategories/$catId';
  var subCategoryResponse = await http
      .get(subCategoryUrl, headers: {HttpHeaders.authorizationHeader: token});
  var data = jsonDecode(subCategoryResponse.body);
  print(subCategoryResponse.body);
  List<SubCategories> _listSubCategory = [];
  data['subcategories'].forEach((f) {
    _listSubCategory.add(SubCategories.fromJson(f));
  });
  return _listSubCategory;
}

Future<Map> getAllCurrentUserProducts(String userId, context) async {
  String getAllUserProductApi = '$base_url/get/product/userId/$userId';
  SharedPreferences _prefs = await SharedPreferences.getInstance();
  String token = _prefs.getString("token");
  var getAllProducts = await http.get(getAllUserProductApi, headers: {
    HttpHeaders.authorizationHeader: token,
  });
  var data = jsonDecode(getAllProducts.body);
  appUserLogOut(data['message'], context);
  if (data["Success"] == false && data["message"] == "UnAuthorized User") {
    Navigator.of(context, rootNavigator: true).pushAndRemoveUntil(
        MaterialPageRoute(builder: (context) => Home()), (route) => false);
  }
  print(data);
  List<Products> listData = [];
  data['products'].forEach((f) {
    listData.add(Products.fromJson(f));
  });
  return {"products": listData};
}

// Future gettinginquiriesusers({String userid, String status, context}) async {
//   SharedPreferences _prefs = await SharedPreferences.getInstance();
//   String token = _prefs.getString("token");
//   String url = "$base_url/selected/inquiries/get/$userid/$status";
//   var request = await http.get(url, headers: {
//     HttpHeaders.authorizationHeader: token,
//   });

//   var response = jsonDecode(request.body);
//   appUserLogOut(response['message'], context);
//   return response;
// }

Future<Map> getAllCurrentUserProductsinopeningcompany(
    String userId, context) async {
  String getAllUserProductApi = '$base_url/get/product/userId/$userId';
  SharedPreferences _prefs = await SharedPreferences.getInstance();
  String token = _prefs.getString("token");
  try {
    var getAllProducts = await http.get(getAllUserProductApi, headers: {
      HttpHeaders.authorizationHeader: token,
    });
    var data = jsonDecode(getAllProducts.body);
    appUserLogOut(data['message'], context);
    List<CompanyProducts> listData = [];
    data['products'].forEach((f) {
      listData.add(CompanyProducts.fromJson(f));
    });
    return {"products": listData};
  } on SocketException {
    showToast(context, "No Internet Connection");
  } on HttpException {
    showToast(context, "Server Connection Not found");
  } on FormatException {
    showToast(context, "Invalid Format Exception");
  } catch (e) {
    return {"error": showToast(context, e)};
  }
}

Future getProductUnits(context) async {
  String url = "$base_url/get/product/units";
  SharedPreferences _prefs = await SharedPreferences.getInstance();
  String tokens = _prefs.getString("token");
  try {
    var request =
        await http.get(url, headers: {HttpHeaders.authorizationHeader: tokens});
    var response = jsonDecode(request.body);
    appUserLogOut(response['message'], context);
    ProductUnits productUnits = ProductUnits.fromJson(response);
    return productUnits;
  } on SocketException {
    showToast(context, "No Internet Connection");
  } catch (e) {
    showToast(context, e);
  }
}

Future<SingleProductdetails> getProductDetails(
    {BuildContext context, String productId, String userid}) async {
  String getProductApi = '$base_url/get/product-detail/$productId/$userid';
  try {
    var getAllProducts = await http.get(getProductApi);
    var data = jsonDecode(getAllProducts.body);
    appUserLogOut(data['message'], context);
    SingleProductdetails singleProductdetails =
        SingleProductdetails.fromJson(data);
    return singleProductdetails;
  } on SocketException {
    showToast(context, "No Internet Connection");
  } on HttpException {
    showToast(context, "Server Connection Not found");
  } on FormatException {
    showToast(context, "Invalid Format Exception");
  } catch (e) {
    return showToast(context, e);
  }
}

Future<List<Ratings>> getdata(String productid, String userid, context) async {
  var rolesUrl = '$base_url/get/product-detail/$productid/$userid';
  var response = await http.get(rolesUrl);
  var data = jsonDecode(response.body);
  appUserLogOut(data['message'], context);
  List<Ratings> roles = [];
  print(data['ratings']);
  data['ratings'].forEach((f) {
    roles.add(Ratings.fromJson(f));
  });
  return roles;
}

Future<Map> appuserlogout(String userid) async {
  String url = "$base_url/app-user-logout/$userid";
  var request = await http.get(url);
  print(request.body);
  return jsonDecode(request.body);
}

Future<bool> deleteProductById(String productId, context) async {
  SharedPreferences _prefs = await SharedPreferences.getInstance();
  String tokens = _prefs.getString("token");
  String deleteProductApi = '$base_url/delete/product/$productId';
  var responseDelete = await http.get(deleteProductApi,
      headers: {HttpHeaders.authorizationHeader: tokens});
  var dataDelete = jsonDecode(responseDelete.body);
  appUserLogOut(dataDelete['message'], context);
  print(dataDelete);
  return true;
}

Future<Map> checkifuserhasmembershipornot(
    SendMessageToAllMessageModel sendMessageToAllMessageModel,
    String userid,
    context) async {
  SharedPreferences _prefs = await SharedPreferences.getInstance();
  String tokens = _prefs.getString("token");
  String url = "$base_url/message/send/all/verification";
  Map searchMapData = {
    'categoryId': sendMessageToAllMessageModel.categoryId,
    'subCategoryId': sendMessageToAllMessageModel.subCategoryId,
    'countryId': sendMessageToAllMessageModel.countryId,
    'cityId': sendMessageToAllMessageModel.cityId,
    'userId': userid,
  };
  var request = await http.post(url,
      body: searchMapData, headers: {HttpHeaders.authorizationHeader: tokens});
  var response = jsonDecode(request.body);
  appUserLogOut(response['message'], context);
  print(response);
  return jsonDecode(request.body);
}

// Future<CategoryInbox> getCategoriesForInbox(String userid, context) async {
//   String url = "$base_url/get/user/inbox-category/messages/$userid";
//   SharedPreferences _prefs = await SharedPreferences.getInstance();
//   String tokens = _prefs.getString("token");
//   var request =
//       await http.get(url, headers: {HttpHeaders.authorizationHeader: tokens});
//   var response = jsonDecode(request.body);
//   print(response);
//   appUserLogOut(response['message'], context);
//   CategoryInbox inbox = categoryInboxFromJson(request.body);
//   return inbox;
// }

// Future<List<ProductSearchModel>> searchedProduct(
//     {SearchProductModel search, context, String userid}) async {
//   print('before convert ${userid}');
//   Map searchMapData = {
//     'category': search.category,
//     'subcategory': search.subcategory,
//     'country': search.country,
//     'city': search.city,
//     'roleId': search.userRole,
//     'userId': userid,
//   };
//   try {
//     var searchProductUrl = '$base_url/product/search';
//     var searchProduct = await http.post(searchProductUrl, body: searchMapData);
//     var searchedProductData = jsonDecode(searchProduct.body);
//     appUserLogOut(searchedProductData['message'], context);
//     List<ProductSearchModel> _getProducts = [];
//     searchedProductData['products'].forEach((f) {
//       _getProducts.add(ProductSearchModel.fromJson(f));
//     });
//     return _getProducts;
//   } on SocketException {} on HttpException {
//     showToast(context, "Server Connection Not found");
//   } on FormatException {
//     showToast(context, "Invalid Format Exception");
//   } catch (e) {
//     return showToast(context, e);
//   }
// }

// Future<bool> singlemessage({
//   String createdby,
//   String createdfor,
//   String categoryid,
//   String subcategoryid,
//   String message,
//   String offer,
//   context,
//   image,
// }) async {
//   print(image.toString());
//   String url = "$base_url/create/message/single";
//   SharedPreferences _prefs = await SharedPreferences.getInstance();
//   String tokens = _prefs.getString("token");
//   print(createdby + "createdby");
//   print(createdfor + "createdfor");
//   print(categoryid + "category");
//   print(subcategoryid + "subcategory");
//   print(offer);
//   try {
//     var request = http.MultipartRequest(
//       'POST',
//       Uri.parse(url),
//     )
//       ..fields['createdBy'] = createdby
//       ..fields['createdFor'] = createdfor
//       ..fields['categoryId'] = categoryid
//       ..fields['subCategoryId'] = subcategoryid
//       ..fields['offer'] = "true"
//       ..fields['message'] = message;
//     request.headers['authorization'] = tokens;
//     image == null
//         ? null
//         : request.files.add(await http.MultipartFile.fromPath('image', image));
//     http.Response response = await http.Response.fromStream(
//       await request.send(),
//     );
//     var getData = jsonDecode(response.body);
//     appUserLogOut(getData['message'], context);
//     if (getData['success'] == false) {
//       Toast.show(getData['message'], context,
//           duration: Toast.LENGTH_SHORT, gravity: Toast.TOP);
//     } else if (getData['success'] == true) {
//       // Toast.show('Message send successfully', context,
//       //     duration: Toast.LENGTH_SHORT, gravity: Toast.TOP);
//     }
//     return getData['success'];
//   } on SocketException {
//     showToast(context, "No Internet Connection");
//   } on HttpException {
//     showToast(context, "Server Connection Not found");
//   } on FormatException {
//     showToast(context, "Invalid Format Exception");
//   } catch (e) {
//     showToast(context, e);
//   }
// }

Future messagetoguest(GuestToBrokermodel guestToBrokermodel, context) async {
  String url = '$base_url/guest/message-to-broker';
  print(guestToBrokermodel.city + "city");
  SharedPreferences _prefs = await SharedPreferences.getInstance();
  String tokens = _prefs.getString("token");
  Map data = {
    'category': guestToBrokermodel.category,
    'subcategory': guestToBrokermodel.subcategory,
    'country': guestToBrokermodel.country,
    'city': guestToBrokermodel.city,
    'message': guestToBrokermodel.message,
  };
  var response = await http.post(url,
      body: data, headers: {HttpHeaders.authorizationHeader: tokens});
  var getresponse = jsonDecode(response.body);
  appUserLogOut(getresponse['message'], context);
  if (getresponse['success'] == false) {
    Toast.show(getresponse['message'], context,
        duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
  } else {
    Toast.show(getresponse['message'], context,
        duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
  }
  print(getresponse);
}

Future<List<GuestMessages>> getmessagesofguest(String userid, context) async {
  String url = "$base_url/get/guest-messages/$userid";
  SharedPreferences _prefs = await SharedPreferences.getInstance();
  String tokens = _prefs.getString("token");
  var response =
      await http.get(url, headers: {HttpHeaders.authorizationHeader: tokens});
  var responsedata = jsonDecode(response.body);
  appUserLogOut(responsedata['message'], context);
  print(responsedata);
  List<GuestMessages> guestmessage = [];
  responsedata['guestMessages'].forEach((f) {
    guestmessage.add(GuestMessages.fromJson(f));
  });
  return guestmessage;
}

// Future<List<AllMessages>> getAllinboxmessagesfrommodel(
//     {String userid, String catid, context}) async {
//   String url = "$base_url/get/current/user/messages/$userid/$catid";
//   SharedPreferences _prefs = await SharedPreferences.getInstance();
//   String tokens = _prefs.getString("token");
//   try {
//     var response =
//         await http.get(url, headers: {HttpHeaders.authorizationHeader: tokens});
//     List<AllMessages> allmessages = [];
//     var responseback = jsonDecode(
//       response.body,
//     );
//     print(responseback);
//     appUserLogOut(responseback['message'], context);
//     responseback["AllMessages"].forEach((value) {
//       allmessages.add(AllMessages.fromJson(value));
//     });
//     print(allmessages);
//     if (allmessages != null) {
//       return allmessages;
//     }
//   } on SocketException {
//     showToast(context, "No Internet Connection");
//   } catch (e) {
//     showToast(context, e);
//   }
// }

Future<Map> gettingnotificationcount(
    String userid, String token, context) async {
  String url = "$base_url/get/user/allnotification/count/$userid";
  try {
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    String tokens = _prefs.getString("token");
    var data =
        await http.get(url, headers: {HttpHeaders.authorizationHeader: tokens});
    var response = jsonDecode(data.body);
    appUserLogOut(response['message'], context);
    print(data.body);
    List<NotificationCount> notificationscount = [];
    notificationscount
        .add(NotificationCount(notifications: response['notifications']));
    return jsonDecode(data.body);
  } on SocketException {
    print("Socket");
  } on HttpException {
    print("htpp");
  } on FormatException {} catch (e) {
    print(e);
  }
}

// Future<Map> gettingratingsbyuserid(
//     [String userid, String token, context]) async {
//   String url = "$base_url/get/user/average/ratings/$userid";
//   SharedPreferences _prefs = await SharedPreferences.getInstance();
//   String tokens = _prefs.getString("token");
//   var data =
//       await http.get(url, headers: {HttpHeaders.authorizationHeader: tokens});
//   var responsedata = jsonDecode(data.body);
//   appUserLogOut(responsedata['message'], context);
//   print(responsedata);
//   if (responsedata["Success"] == false) {
//     throw new Exception(
//         "Token is Expired"); // you may want to implement different exception class
//   }
//   print(responsedata);
//   return jsonDecode(data.body);
// }

Future<List<NewMessages>> getconversation(
    String conversationid, String userid, context) async {
  SharedPreferences _prefs = await SharedPreferences.getInstance();
  String tokens = _prefs.getString("token");
  String getAllSendMessage =
      '$base_url/get/current/user/all-chat/$conversationid/$userid';
  print(getAllSendMessage);
  var response = await http.get(getAllSendMessage,
      headers: {HttpHeaders.authorizationHeader: tokens});
  var getData = jsonDecode(response.body);
  appUserLogOut(getData['message'], context);
  print(getData);
  List<NewMessages> newmessages = [];
  getData['newMessages'].forEach((e) {
    newmessages.add(NewMessages.fromJson(e));
  });
  return newmessages;
}

Future<Map> getsubscriptionstatus(
    String conversationid, String userid, context) async {
  String url = '$base_url/get/current/user/all-chat/$conversationid/$userid';
  SharedPreferences _prefs = await SharedPreferences.getInstance();
  String tokens = _prefs.getString("token");
  var request =
      await http.get(url, headers: {HttpHeaders.authorizationHeader: tokens});
  var response = jsonDecode(request.body);
  appUserLogOut(response['message'], context);
  return jsonDecode(request.body);
}

Future deleteMessagebySelecting(String id, List messageids, context) async {
  SharedPreferences _prefs = await SharedPreferences.getInstance();
  String tokens = _prefs.getString("token");
  Map data = {
    "messageId": messageids,
  };
  Map<String, String> headers = {
    'Content-type': 'application/json',
    'Accept': 'application/json',
    HttpHeaders.authorizationHeader: tokens
  };
  String url = "$base_url/user/message/delete/$id";
  var request = await http.post(url, body: jsonEncode(data), headers: headers);
  var response = jsonDecode(request.body);
  appUserLogOut(response['message'], context);
  print(response);
}

Future conversationstatus(String conversationid, String status, context) async {
  SharedPreferences _prefs = await SharedPreferences.getInstance();
  String tokens = _prefs.getString("token");
  String url = "$base_url/message/update/status";
  Map datas = {
    "conversationId": conversationid,
    "status": status,
  };
  var response = await http.post(url,
      body: datas, headers: {HttpHeaders.authorizationHeader: tokens});
  var data = jsonDecode(response.body);
  appUserLogOut(data['message'], context);
  print(data);
}

Future<Map> checkingusername(String username) async {
  String url = "$base_url/register/check-username/$username";
  var requset = await http.get(url);
  return jsonDecode(requset.body);
}

Future<Map> checkingemail(String email) async {
  String url = "$base_url/register/check-email/$email";
  var requset = await http.get(url);
  return jsonDecode(requset.body);
}

Future<Map> checkingphonenumber(String phone) async {
  String url = "$base_url/register/check-phonenumber/$phone";
  var requset = await http.get(url);
  return jsonDecode(requset.body);
}

Future<Map> verifiyPassword(String userid, String password, context) async {
  FocusScope.of(context).requestFocus(new FocusNode());
  String url = "$base_url/profile/user/verify-password";
  Map data = {
    "userId": userid,
    "password": password.trim(),
  };
  var request = await http.post(url, body: data);
  var response = jsonDecode(request.body);
  print(response);
  if (response["Success"] == false) {
    Toast.show(response["message"].toString(), context, gravity: Toast.TOP);
  } else {
    Toast.show(response["message"].toString(), context, gravity: Toast.TOP);
  }
  return jsonDecode(request.body);
}

Future deletenotification(String id, context) async {
  print(id);
  SharedPreferences _prefs = await SharedPreferences.getInstance();
  String tokens = _prefs.getString("token");
  String url = "$base_url/user/notification/delete/$id";
  print(url);
  var request =
      await http.get(url, headers: {HttpHeaders.authorizationHeader: tokens});
  var response = jsonDecode(
    request.body,
  );
  appUserLogOut(response['message'], context);
  print(response);
}

Future deleteconversation(String id, context) async {
  print(id);
  SharedPreferences _prefs = await SharedPreferences.getInstance();
  String tokens = _prefs.getString("token");
  String url = "$base_url/user/conversation/delete/$id";
  print(url);
  var request =
      await http.get(url, headers: {HttpHeaders.authorizationHeader: tokens});
  var response = jsonDecode(request.body);
  appUserLogOut(response['message'], context);
  print(response);
}

Future<Map> changenewpasswordinprofile(
    String userid, String password, String cPassword, context) async {
  print(userid);
  SharedPreferences _prefs = await SharedPreferences.getInstance();
  String tokens = _prefs.getString("token");
  String url = "$base_url/profile/user/change-password";
  Map data = {
    "userId": userid,
    "password": password.trim(),
    "cPassword": cPassword.trim(),
  };
  var request = await http.post(url,
      body: data, headers: {HttpHeaders.authorizationHeader: tokens});
  var response = jsonDecode(request.body);
  appUserLogOut(response['message'], context);
  print(response);
  if (response["Success"] == false) {
    Toast.show(response["message"].toString(), context, gravity: Toast.TOP);
  } else {
    Toast.show(response["message"].toString(), context, gravity: Toast.TOP);
  }
  return jsonDecode(request.body);
}
