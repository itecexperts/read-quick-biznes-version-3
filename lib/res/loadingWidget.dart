import 'package:flutter/material.dart';

loadingBody(isvisible) {
  return isvisible
      ? Stack(
          children: <Widget>[
            Container(
              color: Color(0xffffffff).withOpacity(0.4),
              height: double.infinity,
              width: double.infinity,
              child: Opacity(
                opacity: 1.0,
                child: Center(
                    child: Image.asset(
                  "assets/images/logo2.gif",
                  height: 200,
                  width: 100,
                )),
              ),
            ),
          ],
        )
      : Container();
}
