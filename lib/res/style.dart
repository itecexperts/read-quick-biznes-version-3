import 'dart:ui';

import 'package:flutter/material.dart';
class style{
  static TextStyle QuicksandBold({double fontSize = 30 , String fontFamily = "QuicksandBold", Color color = Colors.black}){
    return TextStyle(
      fontSize: fontSize,
      fontFamily: fontFamily,
      color: color,
    );
  }
  static TextStyle QuicksandRegular({double fontSize = 20 , String fontFamily = "QuicksandRegular", Color color = Colors.black}){
    return TextStyle(
      fontSize: fontSize,
      fontFamily: fontFamily,
      color: color,
    );
  }

  static TextStyle PoppinsRegular({double fontSize = 9 , String fontFamily = "PoppinsRegular", Color color = Colors.black}){
    return TextStyle(
      fontSize: fontSize,
      fontFamily: fontFamily,
      color: color,
    );
  }

  static TextStyle PoppinsSemiBold({double fontSize = 9 , String fontFamily = "PoppinsSemiBold", Color color = Colors.black}){
    return TextStyle(
      fontSize: fontSize,
      fontFamily: fontFamily,
      color: color,
    );
  }

  static TextStyle MontserratRegular({double fontSize = 9 , String fontFamily = "MontserratRegular", Color color = Colors.black}){
    return TextStyle(
      fontSize: fontSize,
      fontFamily: fontFamily,
      color: color,
    );
  }

  static TextStyle PoppinsBold({double fontSize = 9 , String fontFamily = "PoppinsBold", Color color = Colors.black}){
    return TextStyle(
      fontSize: fontSize,
      fontFamily: fontFamily,
      color: color,
    );
  }

  static TextStyle MontserratMedium({double fontSize = 9 , String fontFamily = "MontserratMedium", Color color = Colors.black}){
    return TextStyle(
      fontSize: fontSize,
      fontFamily: fontFamily,
      color: color,
    );
  }

  static TextStyle JosefinSansRegular({double fontSize = 18 , String fontFamily = "JosefinSansRegular", Color color = Colors.black}){
    return TextStyle(
      fontSize: fontSize,
      fontFamily: fontFamily,
      color: color,
    );
  }

  static TextStyle MontserratBold({double fontSize = 18 , String fontFamily = "MontserratBold", Color color = Colors.black}){
    return TextStyle(
      fontSize: fontSize,
      fontFamily: fontFamily,
      color: color,
    );
  }


}