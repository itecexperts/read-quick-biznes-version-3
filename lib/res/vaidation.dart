bool validEmail(String text) {
  RegExp exp = RegExp('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+\$');
  return exp.hasMatch(text);
}

bool validNumber(String text) {
  return text.length == 9 && text.substring(0, 1) != 0;
}

bool validPassword(String text) {
  return text.length >= 6;
}

bool validConformPassword(String password,String conform){
  return password == conform;
}

bool validName(String text) {
  return text.length >= 3;
}
bool validTitle(String text) {
  return text.length >= 3;
}
