import 'package:biznes/helper/login_helper.dart';
import 'package:biznes/products_Detail/extra.dart';
import 'package:flutter/material.dart';
import 'notification.dart';
import 'package:biznes/CardWidget/TermsAndConditionText.dart';
import 'package:page_transition/page_transition.dart';
import 'package:provider/provider.dart';
import 'package:biznes/Text/TermsAndConditionData.dart';
import 'package:biznes/helper/counthelper.dart';
class TermsAndCondition extends StatefulWidget {
  final String role;
  TermsAndCondition(this.role);
  @override
  _TermsAndConditionState createState() => _TermsAndConditionState();
}

class _TermsAndConditionState extends State<TermsAndCondition> {
  @override
  Widget build(BuildContext context) {
    var counters = Provider.of<CountHelper>(context).counter;
    var counterset = Provider.of<CountHelper>(context);
    var guestUser = Provider.of<LoginHelper>(context).user.roleName;
    return Scaffold(
      backgroundColor: Colors.white,
      body: Column(
        children: [
          CustomAppBar(
              role: guestUser,
              icon: Icon(Icons.arrow_back),
              IconPressed: (){
                Navigator.of(context).pop();
              },
              context: context,
              text: "Terms & Condition",
              length: counters.toString(),
              onTap: () {
                counterset.zerocunter();
                Navigator.of(context).push(PageTransition(
                    child: MyNotification(),
                    duration: Duration(milliseconds: 700),
                    type: PageTransitionType
                        .leftToRightWithFade));
              }),
          Expanded(
            child: ListView(
              physics: BouncingScrollPhysics(),
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 4.0, right: 4.0),
                  child: Container(
                    margin: EdgeInsets.only(left: 10, right: 10),
                    decoration: BoxDecoration(
                        color: Color(0xffEbf3fa),
                        borderRadius: BorderRadius.circular(20)),
                    width: double.infinity,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        TermsAndConditionText(context: context,heading: firstheading,subheading: secondheading,textAlign: TextAlign.center,left: 0.0,right: 0.0),
                        TermsAndConditionText(context: context,heading: accpetandapplication,subheading: accpetandapplicationdetails,textAlign: TextAlign.start,),
                        TermsAndConditionText(context: context,heading: serviceProvision,subheading: serviceProvisiondetails,textAlign: TextAlign.start,),
                        TermsAndConditionText(context: context,heading: buyersAndSellers,subheading: serviceProvisiondetails,textAlign: TextAlign.start,),
                        TermsAndConditionText(context: context,heading: generalGuaidanceforusers,subheading: serviceProvisiondetails,textAlign: TextAlign.start,),
                        TermsAndConditionText(context: context,heading: Accountholder,subheading: AccountholderDetails,textAlign: TextAlign.start,),
                        SizedBox(height: 10,),
                      ],
                    ),
                  ),
                ),

              ],
            ),
          ),
        ],
      ),
    );
  }
}
