import 'package:biznes/Text/privacypolicyText.dart';
import 'package:biznes/helper/login_helper.dart';
import 'package:biznes/products_Detail/extra.dart';
import 'package:flutter/material.dart';
import 'notification.dart';
import 'package:page_transition/page_transition.dart';
import 'package:provider/provider.dart';
import 'package:biznes/helper/counthelper.dart';
import 'package:biznes/res/color.dart' as fontandcolor;
import 'package:biznes/CardWidget/TermsAndConditionText.dart';
class Privacy extends StatefulWidget {
  final String role;
  Privacy({this.role});
  @override
  _PrivacyState createState() => _PrivacyState();
}

class _PrivacyState extends State<Privacy> {
  @override
  Widget build(BuildContext context) {
    var counters = Provider.of<CountHelper>(context).counter;
    var counterset = Provider.of<CountHelper>(context);
    var roleName=Provider.of<LoginHelper>(context).user.roleName;
    return Scaffold(
      backgroundColor: Colors.white,
      body: Column(
        children: [
          CustomAppBar(
              role: roleName,
              icon: Icon(Icons.arrow_back),
              IconPressed: (){
                Navigator.of(context).pop();
              },
              context: context,
              text: "Privacy Policy",
              length: counters.toString(),
              onTap: () {
                counterset.zerocunter();
                Navigator.of(context).push(PageTransition(
                    child: MyNotification(),
                    duration: Duration(milliseconds: 700),
                    type: PageTransitionType
                        .leftToRightWithFade));
              }),
          Expanded(
            child: ListView(
              physics: BouncingScrollPhysics(),
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 4.0, right: 4.0),
                  child: Container(
                    margin: EdgeInsets.only(left: 10, right: 10),
                    decoration: BoxDecoration(
                        color: Color(0xffEbf3fa),
                        borderRadius: BorderRadius.circular(20)),
                    width: double.infinity,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        TermsAndConditionText(context: context,heading: firstheadingprivacy,subheading: firstheadingprivacyDetails,left: 0.0,right: 0.0),
                        TermsAndConditionText(context: context,heading: variety,subheading: varietyDetails),
                        TermsAndConditionText(context: context,heading: personalinforaccess,subheading: personalinforaccessdetails),
                        TermsAndConditionText(context: context,heading: personalinfo,subheading: personalinfodetails),
                        TermsAndConditionText(context: context,heading: credentials,subheading: credentialsdetails),
                        TermsAndConditionText(context: context,heading: paymentKnowlege,subheading: paymentknowlegeDetails),
                        TermsAndConditionText(context: context,heading: autocollectedinfo,subheading: autocollectedinfoDetails),
                        TermsAndConditionText(context: context,heading: onlineidentifier,subheading: onlineidentifierdetails),
                        TermsAndConditionText(context: context,heading: formAlternativeSources,subheading: formalternativesourcesDetails),
                        TermsAndConditionText(context: context,heading: howQuickBiznes,subheading: howQuickBiznesDetails),
                        TermsAndConditionText(context: context,heading: toFacilitate,subheading: toFacilitateDetails),
                        TermsAndConditionText(context: context,heading: communiction,subheading: communictionDetails),
                        TermsAndConditionText(context: context,heading: bodyinfo,subheading: bodyinfoDetails),
                        TermsAndConditionText(context: context,heading: yourOrders,subheading: yourOrdersDetails),
                        TermsAndConditionText(context: context,heading: testimonals,subheading: testimonalsDetails),
                        TermsAndConditionText(context: context,heading: advertising,subheading: advertasingdetails),
                        TermsAndConditionText(context: context,heading: competitions,subheading: competitionsDetails),
                        TermsAndConditionText(context: context,heading: requestFeedback,subheading: requestFeedBackDetails),
                        TermsAndConditionText(context: context,heading: protectservices,subheading: protectservicesDetails),
                        TermsAndConditionText(context: context,heading: usertousercommuntion,subheading: usertousercommunicationDetails),
                        TermsAndConditionText(context: context,heading: legalRequestHarm,subheading: legalRequestDetails),
                        TermsAndConditionText(context: context,heading: userAccount,subheading: userAccountDetails),
                        TermsAndConditionText(context: context,heading: deliverservicestuser,subheading: deliverservicestuserDetails),
                        TermsAndConditionText(context: context,heading: inquiries,subheading: inquiriesDetails),
                        TermsAndConditionText(context: context,heading: bussinessFunction,subheading: functionDetails),
                        TermsAndConditionText(context: context,heading: sharingpolicy,subheading: sharingpolicyDetails),
                        TermsAndConditionText(context: context,heading: consent,subheading: concentDetails),
                        TermsAndConditionText(context: context,heading: legimateIntersets,subheading: legimateIntersetsDetails),
                        TermsAndConditionText(context: context,heading: performance,subheading: performanceDetails),
                        TermsAndConditionText(context: context,heading: obligations,subheading: obligationsDetails),
                        TermsAndConditionText(context: context,heading: intersets,subheading: intersetsDetails),
                        TermsAndConditionText(context: context,heading: vernors,subheading: vendorsDetails),
                        TermsAndConditionText(context: context,heading: bussinessTransfer,subheading: bussinessTransferDetail),
                        TermsAndConditionText(context: context,heading: thridpartyAdvertisers,subheading: thirdPartyAdvertiser),
                        TermsAndConditionText(context: context,heading: affiliates,subheading: affiliatesDetails),
                        TermsAndConditionText(context: context,heading: bussinessPartners,subheading: bussinessPartnerDetails),
                        TermsAndConditionText(context: context,heading: infowillbeshared,subheading: infowillbeshareddetails),
                        TermsAndConditionText(context: context,heading: dmarketing,subheading: dmarketingdetails),
                        TermsAndConditionText(context: context,heading: cloudcompting,subheading: cloudcomputingdetails),
                        TermsAndConditionText(context: context,heading: cloudoptimization,subheading: cloudoptimizationdetails),
                        TermsAndConditionText(context: context,heading: backUpSecurity,subheading: backupdetails),
                        TermsAndConditionText(context: context,heading: invoice,subheading: invoicedetails),
                        TermsAndConditionText(context: context,heading: retargeting,subheading: retargetting),
                        TermsAndConditionText(context: context,heading: socialMediaAdvert,subheading: socialMediaAdDetails),
                        TermsAndConditionText(context: context,heading: wmaa,subheading: wmaad),
                        TermsAndConditionText(context: context,heading: performanceMonitoring,subheading: performanceMonitoringdetails),
                        TermsAndConditionText(context: context,heading: marketingautomation,subheading: markitingautomationDetails),
                        TermsAndConditionText(context: context,heading: webinar,subheading: webinarde),
                        TermsAndConditionText(context: context,heading: areyou,subheading: areyouDetails),
                        TermsAndConditionText(context: context,heading: pointofview,subheading: pointofViewDetails),
                        TermsAndConditionText(context: context,heading: howlong,subheading: howlongDetails),
                        TermsAndConditionText(context: context,heading: datasafe,subheading: datasafeDetails),
                        TermsAndConditionText(context: context,heading: collectinformatio,subheading: collectinfoDetails),
                        TermsAndConditionText(context: context,heading: privacyRight,subheading: privacyRightDetails),
                        TermsAndConditionText(context: context,heading: databreach,subheading: databreachdetails),
                        TermsAndConditionText(context: context,heading: updatespolicy,subheading: updatesPolicyDetails),
                        SizedBox(height: 10,),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
