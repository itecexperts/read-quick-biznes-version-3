import 'dart:convert';
import 'dart:io';

import 'package:biznes/helper/counthelper.dart';
import 'package:biznes/helper/login_helper.dart';
import 'package:biznes/main.dart';
import 'package:biznes/products_Detail/extra.dart';
import 'package:biznes/repeatedWigets/customTextField2.dart';
import 'package:biznes/repeatedWigets/filledButton.dart';
import 'package:biznes/services/server.dart';
import 'package:biznes/setting/notification.dart';
import 'package:flutter/material.dart';
import 'package:biznes/res/style.dart';
import 'package:biznes/res/size.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:image_picker/image_picker.dart';
import 'package:multi_image_picker/multi_image_picker.dart';
import 'package:image_picker/image_picker.dart';
import 'package:http/http.dart' as http;
import 'package:page_transition/page_transition.dart';
import 'package:path_provider/path_provider.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:toast/toast.dart';

List<File> imagesFiles = [];

int desLength = 500;
bool Publish = false;
String userid_new = "";
var response;

class Report extends StatefulWidget {
  @override
  _ReportState createState() => _ReportState();
}

class _ReportState extends State<Report> {
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  bool loading = false;
  TextEditingController description = TextEditingController();
  @override
  Widget build(BuildContext context) {
    var counters = Provider.of<CountHelper>(context).counter;
    var counterset = Provider.of<CountHelper>(context);
    return Scaffold(
      key: _scaffoldKey,
      body: Stack(
        children: [
          CustomAppBar(
              icon: Icon(Icons.arrow_back),
              IconPressed: () {
                Navigator.of(context).pop();
              },
              context: context,
              text: "Report",
              length: counters.toString(),
              onTap: () {
                counterset.zerocunter();
                Navigator.of(context).push(PageTransition(
                    child: MyNotification(),
                    duration: Duration(milliseconds: 700),
                    type: PageTransitionType.leftToRightWithFade));
              }),
          Publish == false
              ? _body()
              : Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Center(
                      child: Container(
                        padding: EdgeInsets.symmetric(
                          vertical: 80,
                        ),
                        // top: 200,
                        // bottom: 1,
                        // bottom: 1,
                        // alignment: Alignment.bottomCenter,
                        child: !Publish ? doneBody() : Container(),
                      ),
                    ),
                  ],
                ),
        ],
      ),
    );
  }

  _body() {
    return Stack(
      children: [
        Container(
          padding: EdgeInsets.symmetric(
            vertical: 80,
          ),
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  padding:
                      EdgeInsets.symmetric(vertical: size.convert(context, 10)),
                  margin: EdgeInsets.symmetric(
                      horizontal: size.convert(context, 16)),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SvgPicture.asset(
                        "assets/images/immigration (1).svg",
                        width: size.convert(context, 58),
                        // height: size.convert(context, 34),
                      ),
                      SizedBox(
                        width: size.convert(context, 13),
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            child: Text(
                              "Report",
                              style: style.QuicksandBold(
                                  fontSize: size.convert(context, 16)),
                            ),
                          ),
                          Text(
                            "Report any bug or error",
                            style: style.PoppinsSemiBold(
                              fontSize: size.convert(context, 10),
                              color: Colors.black.withOpacity(0.30),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                Container(
                  padding: EdgeInsets.symmetric(
                    horizontal: 30,
                    vertical: 20,
                  ),
                  // margin: EdgeInsets.symmetric(horizontal: size.convert(context, 16)),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "What's went Wrong?",
                        style: style.PoppinsSemiBold(
                            fontSize: size.convert(context, 14)),
                      ),
                      SizedBox(
                        height: size.convert(context, 5),
                      ),
                      CustomTextFieldSecond(
                        hints: 'Description',
                        maxLine: 3,
                        maxLength: 1500,
                        onchange: (val) {
                          setState(() {
                            desLength = 500 - val.length == null ||
                                    500 - val.length == 0
                                ? 500
                                : 500 - val.length;
                          });
                        },
                        textEditingController: description,
                        color1: Color(0xff76b9fd),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Expanded(
                            flex: 1,
                            child: Container(
                              alignment: Alignment.topRight,
                              child: Text(
                                "$desLength Words",
                                style: style.QuicksandRegular(
                                  fontSize: size.convert(context, 12),
                                  color: Color(0xff76b9fd),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: size.convert(context, 10),
                      ),
                      RichText(
                        text: TextSpan(
                            text: "Attachement",
                            style: style.PoppinsSemiBold(
                                fontSize: size.convert(context, 14)),
                            children: [
                              TextSpan(
                                text: "    (Optional)",
                                style: style.PoppinsSemiBold(
                                  fontSize: size.convert(context, 14),
                                  color: Color(0xff76b9fd),
                                ),
                              ),
                            ]),
                      ),
                      SizedBox(
                        height: size.convert(context, 5),
                      ),
                      Text(
                        "Attach any screenshot or image",
                        style: style.PoppinsSemiBold(
                          fontSize: size.convert(context, 10),
                          color: Colors.black.withOpacity(0.30),
                        ),
                      ),

                      SizedBox(
                        height: size.convert(context, 10),
                      ),

                      ///image parttttttttttttttttttttttttttttttttttt

                      Container(
                        child: StaggeredGridView.countBuilder(
                          padding: EdgeInsets.all(0),
                          scrollDirection: Axis.vertical,
                          physics: ScrollPhysics(),
                          staggeredTileBuilder: (int index) =>
                              new StaggeredTile.fit(1),
                          mainAxisSpacing: 14.0,
                          crossAxisSpacing: 16.0,
                          shrinkWrap: true,
                          crossAxisCount: 3,
                          itemBuilder: (BuildContext context, int index) {
                            if (index == 0) {
                              return InkWell(
                                onTap: () {
                                  _showPicker(context);
                                },
                                child: Container(
                                  width: size.convertWidth(context, 106),
                                  height: size.convert(context, 92),
                                  decoration: BoxDecoration(
                                      border: Border.all(
                                          color:

                                              // error.length != 0
                                              //     ? error[2] == "image"
                                              //         ? Colors.red
                                              //         : Colors.transparent
                                              //     :
                                              //

                                              Colors.transparent,
                                          width: size.convert(context, 1.5)),
                                      color: Colors.white,
                                      boxShadow: [
                                        BoxShadow(
                                          color: Color(0xff8e7b65d6),
                                          blurRadius: 6,
                                          offset: Offset(0, 3),
                                        )
                                      ],
                                      borderRadius: BorderRadius.circular(
                                          size.convert(context, 13))),
                                  child: Center(
                                      child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      SvgPicture.asset(
                                          "assets/images/addphotos.svg"),
                                      SizedBox(
                                        height: size.convert(context, 10),
                                      ),
                                      Container(
                                        child: Text(
                                          "Add image *",
                                          style: style.QuicksandRegular(
                                              fontSize:
                                                  size.convert(context, 10)),
                                        ),
                                      ),
                                    ],
                                  )),
                                ),
                              );
                            }
                            return Stack(
                              children: [
                                Column(
                                  children: [
                                    Container(
                                      // width: size.convertWidth(context, 106),
                                      // height: size.convert(context, 92),
                                      width: size.convertWidth(context, 156),
                                      height: size.convert(context, 122),

                                      decoration: BoxDecoration(
                                        color: Colors.white,
                                        boxShadow: [
                                          BoxShadow(
                                            color: Color(0xff8e7b65d6),
                                            blurRadius: 6,
                                            offset: Offset(0, 3),
                                          )
                                        ],
                                        borderRadius: BorderRadius.circular(
                                            size.convert(context, 13)),
                                      ),
                                      child: Center(
                                          child: Image.file(
                                              imagesFiles[index - 1])),
                                    ),
                                    // FloatingActionButton(onPressed: () {
                                    //   print("++++++++++++++++++++++++++++++++++++++++");
                                    //   print(imagesFiles[index - 1]);
                                    //   print("++++++++++++++++++++++++++++++++++++++++");
                                    // }),
                                  ],
                                ),
                                Positioned(
                                  bottom: 1,
                                  right: 1,
                                  left: 1,
                                  child: IconButton(
                                    icon: Icon(Icons.delete_forever),
                                    color: Colors.red,
                                    onPressed: () {
                                      setState(() {
                                        imagesFiles.removeAt(index - 1);
                                      });
                                    },
                                  ),
                                ),
                              ],
                            );
                          },
                          itemCount: imagesFiles.length + 1,
                        ),
                      ),

                      ///image parttttttttttttttttttttttttttttttttttt

                      SizedBox(
                        height: size.convert(context, 10),
                      ),
                      Container(
                        alignment: Alignment.center,
                        child: filledButton(
                          radius: 5,
                          onTap: () {
                            setState(() {
                              loading = true;
                            });
                            reportsomethings(context);
                          },
                          txt: "Submit",
                          fontfamily: "PoppinsRegular",
                        ),
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
        loading
            ? Stack(
                children: <Widget>[
                  Container(
                    color: Color(0xffffffff).withOpacity(0.4),
                    height: double.infinity,
                    width: double.infinity,
                    child: Opacity(
                      opacity: 1.0,
                      child: Center(
                          child: Image.asset(
                        "assets/images/logo2.gif",
                        height: 200,
                        width: 100,
                      )),
                    ),
                  ),
                ],
              )
            : Container(),
      ],
    );
  }

  void _showPicker(context) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return SafeArea(
            child: Container(
              child: new Wrap(
                children: <Widget>[
                  new ListTile(
                      leading: new Icon(Icons.photo_library),
                      title: new Text('Photo Library'),
                      onTap: () {
                        loadAssets();
                        Navigator.of(context).pop();
                      }),
                  new ListTile(
                    leading: new Icon(Icons.photo_camera),
                    title: new Text('Camera'),
                    onTap: () {
                      _takeImage();
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              ),
            ),
          );
        });
  }

  final picker = ImagePicker();
  Future _takeImage() async {
    final pickedFile = await picker.getImage(source: ImageSource.camera);

    setState(() {
      if (pickedFile != null) {
        imagesFiles.add(File(pickedFile.path));
      } else {
        print('No image selected.');
      }
    });
  }

  List<Asset> images = List<Asset>();

  Future<void> loadAssets() async {
    List<Asset> resultList = List<Asset>();
    String error = 'No Error Dectected';
    try {
      resultList = await MultiImagePicker.pickImages(
        maxImages: 10,
        enableCamera: true,
        selectedAssets: images,
        cupertinoOptions: CupertinoOptions(takePhotoIcon: "chat"),
        materialOptions: MaterialOptions(
          actionBarColor: "#abcdef",
          actionBarTitle: "Example App",
          allViewTitle: "All Photos",
          useDetailsView: false,
          selectCircleStrokeColor: "#000000",
        ),
      );
      getImageFileFromAssets(resultList);
    } on Exception catch (e) {
      error = e.toString();
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    setState(() {
      images = resultList;
      // _error = error;
    });
    getImageFileFromAssets(resultList);
  }

  doneBody() {
    return Container(
      child: Column(
        children: [
          AlertDialog(
            backgroundColor: Colors.transparent,
            insetPadding: EdgeInsets.all(0),
            elevation: 0.0,
            actions: <Widget>[
              Container(
                padding: EdgeInsets.only(
                  top: size.convert(context, 50),
                  bottom: size.convert(context, 25),
                  right: size.convertWidth(context, 18),
                  left: size.convertWidth(context, 18),
                ),
                width: size.convertWidth(context, 380),
                decoration: BoxDecoration(
                    color: Colors.white,
                    boxShadow: [
                      BoxShadow(
                        color: Color(0xff707070).withOpacity(0.34),
                        blurRadius: 1100,
                        offset: Offset(0, 3),
                      )
                    ],
                    borderRadius:
                        BorderRadius.circular(size.convert(context, 40))),
                child: Column(
                  children: [
                    Container(
                      child: SvgPicture.asset("assets/images/doneLogo.svg"),
                    ),
                    SizedBox(
                      height: size.convert(context, 15),
                    ),
                    Container(
                      child: Text(
                        "Bug Report Successfully",
                        style: style.QuicksandBold(
                          fontSize: size.convert(context, 16),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: size.convert(context, 20),
                    ),
                    Container(
                      child: filledButton(
                        txt: "Done",
                        onTap: () {},
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
        ],
      ),
    );
  }

  getImageFileFromAssets(List<Asset> assets) async {
    List<File> _files = [];
    assets.forEach((f) async {
      var byteData = await f.getByteData();
      final file = File('${(await getTemporaryDirectory()).path}/${f.name}');
      await file.writeAsBytes(byteData.buffer
          .asUint8List(byteData.offsetInBytes, byteData.lengthInBytes));
      _files.add(file);
    });

    imagesFiles = _files;

    Future.delayed(Duration(milliseconds: 2000)).then((t) {
      setState(() {});
    });
  }

  Future<Map> reportsomethings(context) async {
    var user = Provider.of<LoginHelper>(context, listen: false).user;
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    String token = _prefs.getString("token");
    List<String> images = [];
    imagesFiles.forEach((element) {
      images.add(element.path);
    });
    String url = "$base_url/create/userReport";
    var request = http.MultipartRequest('POST', Uri.parse(url))
      ..fields['email'] = user.email
      ..fields['message'] = description.text;
    request.headers['authorization'] = token;
    images.forEach((f) async {
      request.files.add(await http.MultipartFile.fromPath('images', f));
    });
    http.Response response =
        await http.Response.fromStream(await request.send());
    var data = jsonDecode(response.body);
    print(data);
    if (data["Success"] == true) {
      Navigator.of(context).pop();
      print("posted");
      Toast.show("Report Sent", context, gravity: Toast.TOP);
    } else {}
    return {"data": data};
  }
}
