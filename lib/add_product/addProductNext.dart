import 'dart:convert';
import 'package:biznes/product_UI/bottomBar_UI/DropDownWidget.dart';
import 'package:biznes/repeatedWigets/filledButton.dart';
import 'dart:io';
import 'package:biznes/helper/counthelper.dart';
import 'package:biznes/res/color.dart' as fontandcolor;
import 'package:biznes/products_Detail/extra.dart';
import 'package:biznes/helper/gettingproducts.dart';
import 'package:biznes/helper/login_helper.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:biznes/memberShip/Membership.dart';
import 'package:biznes/services/register_logic.dart';
import 'package:biznes/model/caegoryModel.dart';
import 'package:biznes/services/server.dart';
import 'package:biznes/model/productModel.dart';
import 'package:biznes/model/subCategoryModel.dart';
import 'package:biznes/setting/notification.dart';
import 'package:page_transition/page_transition.dart';
import 'package:biznes/size_config.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:multi_image_picker/multi_image_picker.dart';
import 'package:path_provider/path_provider.dart';
import 'package:provider/provider.dart';
import 'package:http/http.dart' as http;
import 'package:toast/toast.dart';

class AddProductNext extends StatefulWidget {
  final ProductModel productModel;
  final String title;
  AddProductNext({this.productModel, this.title});

  @override
  _AddProductNextState createState() => _AddProductNextState();
}

class _AddProductNextState extends State<AddProductNext> {
  var formKey = GlobalKey<FormState>();
  List<CategoriesModel> categories = [];
  List<SubCategories> subCategory = [];
  String errorMessage = 'No Error';
  List<Asset> images;
  List<Asset> resultList = List<Asset>();

  List<File> imagesFiles = [];

  int maxImageNo = 10;
  bool loading = true;
  bool isUploading = false;
  String selectedCategory;
  String selectedSubCategory;
  bool selectSingleImage = false;

  bool validateAndSave() {
    final form = formKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    } else {
      return false;
    }
  }

  productPicture() async {
    setState(() {
      images = [];
      errorMessage = 'No Error';
    });
    String error;
    try {
      resultList = await MultiImagePicker.pickImages(
        maxImages: maxImageNo,
        enableCamera: true,
        selectedAssets: images,
        cupertinoOptions: CupertinoOptions(takePhotoIcon: "chat"),
        materialOptions: MaterialOptions(
          actionBarColor: "#abcdef",
          actionBarTitle: "Example App",
          allViewTitle: "All Photos",
          useDetailsView: false,
          selectCircleStrokeColor: "#000000",
        ),
      );
      images = resultList;
      getImageFileFromAssets(resultList);
      // setState(() {});
    } on PlatformException catch (e) {
      error = e.message;
      print(error);
    }

    if (!mounted) return;
  }

  convertAssetsToFiles(List<Asset> _images) {
    setState(() async {

    });
  }

  @override
  void initState() {
    print(widget.title);
    getCategories().then((_list) {
      setState(() {
        categories = _list;
        loading = false;
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var user = Provider.of<LoginHelper>(context).user;
    var token = Provider.of<LoginHelper>(context).token;
    var counters = Provider.of<CountHelper>(context).counter;
    var counterset = Provider.of<CountHelper>(context);
    Widget buildForm(String token) {
      return Padding(
        padding: const EdgeInsets.all(3.0),
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(25.9),
              topRight: Radius.circular(25.9),
              bottomLeft: Radius.circular(25.9),
              bottomRight: Radius.circular(25.9),
            ),
          ),
          child: Padding(
            padding: EdgeInsets.all(10.2),
            child: Form(
                key: formKey,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    TextFieldAndText(
                      context: context,
                      title: "Add Organization Name",
                      hintText: "Enter organization name",
                      onSaved: (value) {
                        widget.productModel.organizationName = value;
                      },
                      validator: (value) {
                        if (value.isEmpty && value.length < 5) {
                          return 'Enter company/organization name';
                        }
                        return null;
                      },
                    ),
                    SizedBox(
                      height: (10 / 8.148314082864863) *
                          SizeConfig.heightMultiplier,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 5, vertical: 5),
                      child: Text(
                        "Select Category",
                        style: TextStyle(
                            fontFamily: fontandcolor.fontfaimly,
                            fontSize: (18 / 8.148314082864863) *
                                SizeConfig.textMultiplier),
                      ),
                    ),
                    DropDownWidget(
                      context: context,
                      image: "assets/icons/Category.svg",
                      value: selectedCategory,
                      categories: categories,
                      hintext: "Category",
                      selectedCategory: selectedCategory,
                      onChanged: (value){
                        setState(() {
                          if (selectedSubCategory != null) {
                            selectedSubCategory = null;
                          }
                          subCategory = [];
                        });
                        categories.forEach((f) async {
                          if (f.title == value) {
                            getSubCategories(catId: f.sId,)
                                .then((list) {
                              // regModel;
                              setState(() {
                                subCategory = list;
                                selectedCategory = value;
                                widget.productModel.category = f.sId;
                              });
                            });
                          }
                        });
                      }
                    ),
                    SizedBox(
                      height:
                          (10 / 8.148314082864863) * SizeConfig.textMultiplier,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 5, vertical: 5),
                      child: Text(
                        "Select Sub Category",
                        style: TextStyle(
                            fontFamily: fontandcolor.fontfaimly,
                            fontSize: (18 / 8.148314082864863) *
                                SizeConfig.textMultiplier),
                      ),
                    ),
                    DropDownWidget(
                      image: "assets/icons/subCategory.svg",
                      hintext: "Sub-Category",
                      selectedCategory: selectedSubCategory,
                      value: selectedSubCategory,
                      context: context,
                      categories: subCategory,
                      onChanged: (value){
                        subCategory.forEach((f) {
                          if (f.title == value) {
                            widget.productModel.subCategory = f.sId;
                            // widget.regModel.
                          }
                        });
                        setState(() {
                          selectedSubCategory = value;
                        });
                      }

                    ),
                    SizedBox(
                        height: (10 / 8.148314082864863) *
                            SizeConfig.textMultiplier),
                    Padding(
                      padding: const EdgeInsets.only(
                          top: 8.0, left: 18.0, bottom: 8.0),
                      child: InkWell(
                        // onTap: productPicture,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              "Select product pictures from Gallery",
                              style: TextStyle(
                                fontFamily: fontandcolor.fontfaimlyregular,
                                fontSize: (16 / 8.148314082864863) *
                                    SizeConfig.textMultiplier,
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    images == null
                        ? InkWell(
                            onTap: productPicture,
                            child: new Container(
                              height: (100 / 8.148314082864863) *
                                  SizeConfig.heightMultiplier,
                              width: (100 / 4.853932272197492) *
                                  SizeConfig.widthMultiplier,
                              decoration: BoxDecoration(
                                  gradient: LinearGradient(
                                    colors: [
                                      Color(0xff69bef7),
                                      Color(0xff7595f4)
                                    ],
                                    begin: Alignment.topCenter,
                                    end: Alignment.bottomCenter,
                                  ),
                                  borderRadius: BorderRadius.circular(20),
                                  border: Border.all(color: Colors.blue)),
                              child: new Icon(
                                Icons.add_to_photos,
                                size: (30 / 8.148314082864863) *
                                    SizeConfig.heightMultiplier,
                                color: Colors.white,
                              ),
                            ),
                          )
                        : new Container(
                            height: (200 / 8.148314082864863) *
                                SizeConfig.heightMultiplier,
                            width: MediaQuery.of(context).size.width,
                            child: new ListView.builder(
                              scrollDirection: Axis.horizontal,
                              itemBuilder: (BuildContext context, int index) =>
                                  new Container(
                                      margin: EdgeInsets.all(2.0),
                                      decoration: BoxDecoration(
                                        border: Border.all(color: Colors.blue),
                                        borderRadius: BorderRadius.circular(25),
                                        shape: BoxShape.rectangle,
                                      ),
                                      padding: const EdgeInsets.all(10.0),
                                      child: ClipRRect(
                                          borderRadius:
                                              BorderRadius.circular(25),
                                          child: Image.file(
                                            imagesFiles[index],
                                            height: (200 / 8.148314082864863) *
                                                SizeConfig.heightMultiplier,
                                            width: (150 / 4.853932272197492) *
                                                SizeConfig.widthMultiplier,
                                          ))),
                              itemCount: imagesFiles.length,
                            ),
                          ),
                    SizedBox(
                      height: 30,
                    ),
                    Center(
                      child: filledButton(
                        onTap: () {
                          widget.productModel.createdBy = user.sId;
                          if (validateAndSave() && imagesFiles.isNotEmpty) {
                            setState(() {
                              isUploading = true;
                            });
                            postProducts(context, user.sId).then((value) {
                              if (value["Success"] == true) {
                                membershipandproductdialog(
                                    contexts:context,
                                    text:"Product Added Successfully",
                                    subtitle:"""Your Proudct is added and under Review.Your Product will be publish soon.
                                          """,
                                    image:"assets/images/ProductAddedSuccessfully.svg");
                                if (widget.title == "membership") {
                                  getAllCurrentUserProducts(user.sId,context)
                                      .then((value) {
                                    Provider.of<GettingProducts>(context,
                                            listen: false)
                                        .onLogIn(value["products"]);
                                    setState(() {
                                      isUploading = false;
                                    });
                                    Navigator.of(context).pop();
                                    Navigator.of(context).pop();
                                    Navigator.of(context).pop();
                                    print(value["products"]);
                                  });
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (cotnext) =>
                                              Membership(userid: user.sId)));
                                } else {
                                  getAllCurrentUserProducts(
                                    user.sId,context
                                  ).then((value) {
                                    Provider.of<GettingProducts>(context,
                                            listen: false)
                                        .onLogIn(value["products"]);
                                    print(value["products"]);
                                    setState(() {
                                      isUploading = false;
                                    });
                                    Navigator.of(context).pop();
                                    Navigator.of(context).pop();
                                  });
                                }
                              } else if (value["Success"] == false) {
                                setState(() {
                                  isUploading = false;
                                });
                                Toast.show(value['mesasage'], context,
                                    duration: Toast.LENGTH_SHORT,
                                    gravity: Toast.BOTTOM);
                              }
                            });
                          } 
                          // else if (formKey.currentState.validate() &&
                          //     imagesFiles.length == 0) {
                          //   Toast.show("Select Images", context,
                          //       duration: Toast.LENGTH_SHORT,
                          //       gravity: Toast.BOTTOM);
                          // }
                        },
                        txt: "Done",
                      ),
                    ),
                    SizedBox(
                      height: (30 / 8.148314082864863) *
                          SizeConfig.heightMultiplier,
                    ),
                  ],
                )),
          ),
        ),
      );
    }

    return GestureDetector(
      onTap: () => FocusScope.of(context).requestFocus(new FocusNode()),
      child: Stack(
        children: [
          Scaffold(
            body:Column(
                    children: [
                      CustomAppBar(
                        icon: Icon(Icons.arrow_back),
                        IconPressed: (){
                          Navigator.of(context).pop();
                        },
                        context: context,
                        onTap: (){
                          counterset.zerocunter();
                          Navigator.of(context).push(PageTransition(
                              child: MyNotification(),
                              duration: Duration(milliseconds: 700),
                              type: PageTransitionType
                                  .leftToRightWithFade));
                        },
                        length: counters.toString(),
                        text: "Add Product",
                      ),
                      loading
                          ? Expanded(
                            child: Container(
                        child: Center(
                            child: Image.asset(
                              "assets/images/logo2.gif",
                              height: 200,
                              width: 100,
                            ),
                        ),
                      ),
                          )
                          : Expanded(
                        child: Container(
                          child: ListView(
                            padding: EdgeInsets.zero,
                            children: <Widget>[
                              SizedBox(
                                height: (20 / 8.148314082864863) *
                                    SizeConfig.heightMultiplier,
                              ),
                              buildForm(token)
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
          ),
          isUploading
              ? Stack(
                  children: <Widget>[
                    Container(
                      color: Color(0xffffffff).withOpacity(0.6),
                      height: double.infinity,
                      width: double.infinity,
                      child: Opacity(
                        opacity: 1.0,
                        child: Center(
                            child: Image.asset(
                          "assets/images/logo2.gif",
                          height: 200,
                          width: 100,
                        )),
                      ),
                    ),
                  ],
                )
              : Container(),
        ],
      ),
    );
  }

  getImageFileFromAssets(List<Asset> assets) async {
    List<File> _files = [];
    assets.forEach((f) async {
      var byteData = await f.getByteData();
      final file = File('${(await getTemporaryDirectory()).path}/${f.name}');
      await file.writeAsBytes(byteData.buffer
          .asUint8List(byteData.offsetInBytes, byteData.lengthInBytes));
      _files.add(file);
    });

    imagesFiles = _files;

    Future.delayed(Duration(milliseconds: 2000)).then((t) {
      setState(() {});
    });
  }

  Future<Map<dynamic, dynamic>> postProducts(
      BuildContext context, userid) async {
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    String token = _prefs.getString("token");
    List<String> images = [];
    imagesFiles.forEach((f) {
      images.add(f.path);
    });
    String productPostUrl = '$base_url/add/product';

    var request = http.MultipartRequest(
      'POST',
      Uri.parse(productPostUrl),
    )
      ..fields['title'] = widget.productModel.title
      ..fields['quantityUnit']=widget.productModel.quantityUnit
      ..fields['description'] = widget.productModel.description
      ..fields['quantity'] = widget.productModel.quantity.toString()
      ..fields['moq'] = widget.productModel.moq.toString()
      ..fields['category'] = widget.productModel.category
      ..fields['subCategory'] = widget.productModel.subCategory
      ..fields['createdBy'] = widget.productModel.createdBy
      ..fields['price'] = widget.productModel.price
      ..fields['organizationName'] = widget.productModel.organizationName;
    request.headers['authorization'] = token;
    images.forEach((f) async {
      request.files.add(await http.MultipartFile.fromPath('images', f));
    });
    http.Response productResponse = await http.Response.fromStream(
      await request.send(),
    );
    print(productResponse.body);
    return jsonDecode(productResponse.body);
  }
}
