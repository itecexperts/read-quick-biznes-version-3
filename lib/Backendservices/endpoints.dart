import 'package:biznes/services/server.dart';

String loginUrl = '$base_url/login/user';
String allMessages = "$base_url/get/current/user/messages/";
String inboxByCategories = "$base_url/get/user/inbox-category/messages/";
String verifiyCode = "$base_url/veify/user-pin-code";
String searchProduct = '$base_url/product/search';
String singleProductDetails = "$base_url//get/product-detail/";
String getCurrentUserMemberShip = "$base_url/get/current/user/membership/";
String messageSendToAll = "$base_url/message/send/all";
String sendSingleMessage = "$base_url/create/message/single";
String reportUserinConversation = "$base_url/report-user/in-conversation";
String blockUserInConversation = "$base_url/block/user/why/me";
String getUserRatingsbyId = "$base_url/get/user/average/ratings/";
