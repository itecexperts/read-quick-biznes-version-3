import 'package:flutter/material.dart';
import 'package:dio/dio.dart';
import 'dart:io';

import 'package:shared_preferences/shared_preferences.dart';

const String NO_CONNECTION = 'no_connection';

class BackEndApi {
  static Future postFunction({String url, body}) async {
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    String tokens = _prefs.getString("token");
    print(url + "this is url");
    print(body);
    try {
      bool checkConnection = await _checkConnection();
      if (!checkConnection) {
        return NO_CONNECTION;
      }
      Dio dio = Dio();
      Response response = await dio.post(url,
          data: body,
          options: Options(headers: {HttpHeaders.authorizationHeader: tokens}));
      print(response.statusCode);
      if (response.statusCode == 200) {
        return response.data;
      }
    } catch (e) {
      print(e);
    }
  }

  static Future get({String url}) async {
    print(url);
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    String tokens = _prefs.getString("token");
    try {
      bool checkConnection = await _checkConnection();
      if (!checkConnection) {
        return NO_CONNECTION;
      }
      Dio dio = Dio();
      Response response = await dio.get(url,
          options: Options(headers: {HttpHeaders.authorizationHeader: tokens}));
      return response.data;
    } catch (e) {}
  }
}

_checkConnection() async {
  try {
    await InternetAddress.lookup("www.google.com");
    return true;
  } on SocketException {
    print("Socket Exception");
    return false;
  }
}

void showFloatingFlushbar(BuildContext context) {}

void showFloatingFlushbarSuccess(_scaffoldKey,
    {String title, String subtitle}) {
  try {
    _scaffoldKey.currentState.hideCurrentSnackBar();
  } catch (e) {}
  _scaffoldKey.currentState.showSnackBar(
    SnackBar(
      content: Row(
        children: <Widget>[
          // Icon(
          //   leadingIcon,
          //   color: Colors.white,
          //   size: 18,
          // ),
          SizedBox(
            width: 5,
          ),
          Expanded(
              child: Text(
            title,
            style: TextStyle(fontSize: 14, color: Colors.white),
          )),
        ],
      ),
      duration: Duration(seconds: 3),
      backgroundColor: Colors.green,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(4.0)),
      elevation: 10.0,
      behavior: SnackBarBehavior.floating,
    ),
  );
}

void showFloatingFlushbarForError(_scaffoldKey,
    {String title, String subtitle}) {
  try {
    _scaffoldKey.currentState.hideCurrentSnackBar();
  } catch (e) {}
  _scaffoldKey.currentState.showSnackBar(
    SnackBar(
      content: Row(
        children: <Widget>[
          // Icon(
          //   leadingIcon,
          //   color: Colors.white,
          //   size: 18,
          // ),
          SizedBox(
            width: 5,
          ),
          Expanded(
              child: Text(
            title,
            style: TextStyle(fontSize: 14, color: Colors.white),
          )),
        ],
      ),
      duration: Duration(seconds: 3),
      backgroundColor: Colors.red,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(4.0)),
      elevation: 10.0,
      behavior: SnackBarBehavior.floating,
    ),
  );
}
