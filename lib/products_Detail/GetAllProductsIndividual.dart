import 'package:biznes/helper/login_helper.dart';
import 'package:biznes/products_Detail/productDetails.dart';
import 'package:biznes/services/server.dart';
import 'package:biznes/model/single_Product.dart';
import 'package:biznes/services/register_logic.dart';
import 'package:biznes/helper/productdetailshelper.dart';
import 'package:biznes/memberShip/Membership.dart';
import 'package:biznes/size_config.dart';
import 'package:biznes/res/color.dart' as fontandcolor;
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'currentusehalfrmembershippage.dart';
import 'package:provider/provider.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';
import 'package:biznes/helper/gettingproducts.dart';

class GetAllProducts extends StatefulWidget {
  final String username;
  final String userid;
  final String profilepic;
  final String organizationname;
  final String description;
  final String city;
  final String country;
  final bool subscribtion;
  final String roleName;
  final SingleProductdetails product;
  final String gettingproductid;
  GetAllProducts({
    this.username,
    this.userid,
    this.profilepic,
    this.organizationname,
    this.description,
    this.city,
    this.subscribtion,
    this.country,
    this.roleName,
    this.gettingproductid,
    this.product,
  });
  _GetAllProductsState createState() => _GetAllProductsState();
}

class _GetAllProductsState extends State<GetAllProducts> {
  String rating;
  final TextStyle dropdownMenuItem =
      TextStyle(color: Colors.black, fontSize: 18);
  final primary = Color(0xff696b9e);
  final secondary = Color(0xfff29a94);
  bool longPress = false;
  bool orientation = false;
  bool isloading = true;
  bool ismessagesend = false;
  @override
  void initState() {
    getAllCurrentUserProductsinopeningcompany(widget.userid,context).then((value) {
      if(value!=null){
      Provider.of<GettingProducts>(context, listen: false)
          .oncompanycurrentproduct(value["products"]); 
      setState(() {
        isloading = false;
      });
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var user = Provider.of<LoginHelper>(context).user;
    var token = Provider.of<LoginHelper>(context).token;
    var productList = Provider.of<GettingProducts>(context).companyproducts;
    var productdetails =
        Provider.of<ProductDetailsHelperData>(context).singleproduct;
    return Scaffold(
      backgroundColor: Colors.white,
      body: OrientationBuilder(builder: (context, orientation) {
        return Container(
          child: Column(
            children: [
              productdetails == null
                  ? CurrentProfileMembership(
                      profilepic: widget.profilepic,
                      subscribtionstatus: widget.subscribtion,
                      description: widget.description,
                      organizationname: widget.organizationname,
                      city: widget.city,
                      country: widget.country,
                      orientation: orientation,
                      username: widget.username,
                      token: token,
                      userid: widget.userid,
                      rolename: "Broker",
                    )
                  : CurrentProfileMembership(
                      username: widget.username,
                      description: widget.description,
                      profilepic: widget.profilepic,
                      city: widget.city,
                      country: widget.country,
                      ismessagesend: ismessagesend,
                      organizationname: widget.organizationname,
                      orientation: orientation,
                      subscribtionstatus:
                          productdetails.singleProduct[0].subscriptionStatus,
                      categoryid:
                          productdetails.singleProduct[0].category.sId,
                      subcategory:
                          productdetails.singleProduct[0].subCategory.sId,
                      userid: widget.userid,
                      currentuserid: user.sId,
                      token: token,
                      createdforid:
                          productdetails.singleProduct[0].createdBy.sId,
                      onTap: () {
                        Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => Membership(
                            userid: user.sId,
                            singleproduct: widget.product,
                            gettingproductid: widget.gettingproductid,
                          ),
                        ));
                      },
                    ),
              widget.roleName == "Broker"
                  ? Container()
                  : Expanded(
                      child: Container(
                        padding: EdgeInsets.only(top: 10.0),
                        child: isloading == false
                            ? ListView.builder(
                                padding: EdgeInsets.zero,
                                itemCount: productList.length,
                                itemBuilder:
                                    (BuildContext context, int index) {
                                  return buildList(index, productList, user);
                                })
                            : Container(
                                child: Center(
                                  child: Image.asset(
                                    'assets/images/logo2.gif',
                                    height: 70,
                                    width: 70,
                                  ),
                                ),
                              ),
                      ),
                    ),
            ],
          ),
        );
      }),
    );
  }

  Widget buildList(int index, productList, var user) {
    var productdetails = Provider.of<ProductDetailsHelperData>(context);
    return Stack(
      children: [
        Card(
          child: Container(
            decoration: BoxDecoration(
              color: Color(0xffffffff),
            ),
            width: double.infinity,
            margin: EdgeInsets.symmetric(
                vertical: widget.username == "currentuser" ? 0 : 2,
                horizontal: 8),
            child: InkWell(
              onTap: () async {
                print("hellossj");
                getProductDetails(
                  context: context,
                  userid: user.sId,
                  productId: productList[index].sId,
                ).then((singleProductList) {
                  productdetails.productdetailsfunction(singleProductList);
                  if (singleProductList != null) {
                    Navigator.push(
                        context,
                        PageTransition(
                            child: ProductDetail(
                              gettingproductid: productList[index].sId,
                              userid: user.sId,
                              product: singleProductList,
                            ),
                            type: PageTransitionType.fade,
                            duration: Duration(milliseconds: 800)));
                  }
                });
              },
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    width:
                        (120 / 4.853932272197492) * SizeConfig.widthMultiplier,
                    height:
                        (110 / 8.148314082864863) * SizeConfig.heightMultiplier,
                    margin: EdgeInsets.only(
                        right: 13, top: 1.0 * SizeConfig.heightMultiplier),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(10),
                      child: CachedNetworkImage(
                        fit: BoxFit.fill,
                        imageUrl: '$base_url/${productList[index].images[0]}',
                        placeholder: (context, url) =>
                            Center(child: CircularProgressIndicator()),
                        errorWidget: (context, url, error) => Icon(Icons.error),
                      ),
                    ),
                  ),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          "${productList[index].title}",
                          style: TextStyle(
                            fontWeight: FontWeight.w400,
                            color: Color(0xff394C81),
                            fontFamily: fontandcolor.fontfaimlyquicksandbold,
                            fontSize: (16 / 8.148314082864863) *
                                SizeConfig.heightMultiplier,
                          ),
                        ),
                        SizedBox(
                          height: (2 / 8.148314082864863) *
                              SizeConfig.heightMultiplier,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              "${productList[index].ratings}.0",
                              style: TextStyle(
                                fontFamily: fontandcolor.fontfaimly,
                                color: Color(0xff909090),
                              ),
                            ),
                            Center(
                                child: SmoothStarRating(
                              isReadOnly: true,
                              borderColor: Color(0xffffb900),
                              rating: productList[index].ratings.toString() ==
                                      null
                                  ? 0.0
                                  : double.parse(
                                      productList[index].ratings.toString()),
                              size: (20 / 8.148314082864863) *
                                  SizeConfig.heightMultiplier,
                              color: Color(0xffffb900),
                              filledIconData: Icons.star,
                              defaultIconData: Icons.star_border,
                              starCount: 5,
                              allowHalfRating: true,
                              spacing: 0.5,
                            )),
                          ],
                        ),
                        SizedBox(
                          height: (3 / 8.148314082864863) *
                              SizeConfig.heightMultiplier,
                        ),
                        Text("Category: ${productList[index].category.title??"not mention"}",
                            style: TextStyle(
                                fontFamily: fontandcolor.fontfaimlyregular,
                                color: Colors.black,
                                fontSize: (12 / 8.148314082864863) *
                                    SizeConfig.heightMultiplier,
                                letterSpacing: .3)),
                        SizedBox(
                          height: 1.5,
                        ),
                        Text(
                            "Sub-Category:${productList[index].subCategory.title??"not mention"}",
                            style: TextStyle(
                                color: Colors.black,
                                fontFamily: fontandcolor.fontfaimlyregular,
                                fontSize: (12 / 8.148314082864863) *
                                    SizeConfig.heightMultiplier,
                                letterSpacing: .3)),
                        SizedBox(
                          width: (10 / 4.853932272197492) *
                              SizeConfig.widthMultiplier,
                        ),
                        Text("Price ${productList[index].price??"not mention"}",
                            style: TextStyle(
                                fontFamily: fontandcolor.fontfaimlyregular,
                                color: Colors.black,
                                fontSize: (12 / 8.148314082864863) *
                                    SizeConfig.heightMultiplier,
                                letterSpacing: .3)),
                        SizedBox(
                          height: (1.5 / 8.148314082864863) *
                              SizeConfig.heightMultiplier,
                        ),
                        Text(
                            "Quantity : ${productList[index].quantity.toString()??"not mention"} (${productList[index].quantityUnit??"not mention"})",
                            style: TextStyle(
                                color: Colors.black,
                                fontFamily: fontandcolor.fontfaimlyregular,
                                fontSize: (12 / 8.148314082864863) *
                                    SizeConfig.heightMultiplier,
                                letterSpacing: .3)),
                        Text(
                            "M.O.Q : ${productList[index].moq.toString()??"not mention"} (${productList[index].quantityUnit??"not mention"})",
                            style: TextStyle(
                                color: Colors.black,
                                fontFamily: fontandcolor.fontfaimlyregular,
                                fontSize: (12 / 8.148314082864863) *
                                    SizeConfig.heightMultiplier,
                                letterSpacing: .3)),
                        SizedBox(
                          height: (1.5 / 8.148314082864863) *
                              SizeConfig.heightMultiplier,
                        ),
                        Row(
                          children: <Widget>[
                            Text("Pakistan",
                                style: TextStyle(
                                    color: Colors.green,
                                    fontFamily: fontandcolor.fontfaimly,
                                    fontSize: 11,
                                    letterSpacing: .3)),
                            SizedBox(
                              width: (5 / 4.853932272197492) *
                                  SizeConfig.widthMultiplier,
                            ),
                            Image.asset(
                              'assets/images/flag2.png',
                              height: 12,
                              width: 15,
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }
}
