import 'package:biznes/Controllers/backendservicescontroller.dart';
import 'package:biznes/helper/counthelper.dart';
import 'package:biznes/repeatedWigets/customdialog.dart';
import 'package:flutter/material.dart';
import 'package:biznes/size_config.dart';
import 'package:biznes/res/color.dart' as fontandcolor;
import 'package:page_transition/page_transition.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:biznes/repeatedWigets/filledButton.dart';
import 'package:biznes/setting/notification.dart';
import 'package:biznes/helper/allmessageshelper.dart';
import 'package:provider/provider.dart';
import 'package:biznes/services/register_logic.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';
import 'package:biznes/res/size.dart';

import 'extra.dart';

class CurrentProfileMembership extends StatefulWidget {
  final Orientation orientation;
  bool ismessagesend;
  String profilepic;
  String organizationname;
  bool subscribtionstatus;
  String userid;
  final String description;
  final String username;
  final String country;
  final String city;
  final String currentuserid;
  final String createdforid;
  final String categoryid;
  final String subcategory;
  final String token;
  final String rolename;
  Function onTap;

  CurrentProfileMembership({
    this.orientation,
    this.ismessagesend,
    this.profilepic,
    this.organizationname,
    this.subscribtionstatus,
    this.userid,
    this.description,
    this.username,
    this.country,
    this.city,
    this.currentuserid,
    this.createdforid,
    this.categoryid,
    this.subcategory,
    this.token,
    this.rolename,
    this.onTap,
  });

  @override
  _CurrentProfileMembershipState createState() =>
      _CurrentProfileMembershipState();
}

class _CurrentProfileMembershipState extends State<CurrentProfileMembership> {
  @override
  Widget build(BuildContext context) {
    var counters = Provider.of<CountHelper>(context).counter;
    var counterset = Provider.of<CountHelper>(context);
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        boxShadow: <BoxShadow>[
          BoxShadow(
              color: Color(0xffEbf3fa),
              blurRadius: 10.0,
              offset: Offset(0.0, 5.0),
              spreadRadius: 0.2)
        ],
        borderRadius: BorderRadius.only(
          bottomLeft: Radius.circular(30),
          bottomRight: Radius.circular(30),
        ),
      ),
      child: Column(
        children: [
          CustomAppBar(
              icon: Icon(Icons.arrow_back),
              IconPressed: () {
                Navigator.of(context).pop();
              },
              context: context,
              text: "About Company",
              length: counters.toString(),
              onTap: () {
                counterset.zerocunter();
                Navigator.of(context).push(PageTransition(
                    child: MyNotification(),
                    duration: Duration(milliseconds: 700),
                    type: PageTransitionType.leftToRightWithFade));
              }),
          SizedBox(height: 10),
          Container(
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Padding(
                  padding: EdgeInsets.only(
                    left: widget.orientation == Orientation.portrait
                        ? 2.5 * SizeConfig.heightMultiplier
                        : 5.0 * SizeConfig.widthMultiplier,
                  ),
                  child: CircleAvatar(
                    radius: 45,
                    backgroundColor: Colors.white,
                    backgroundImage:
                        CachedNetworkImageProvider(widget.profilepic),
                  ),
                ),
                Container(
                  child: Column(
                    children: [
                      widget.subscribtionstatus
                          ? Row(
                              children: [
                                Container(
                                  width: size.convertWidth(context, 260),
                                  child: Text(
                                    widget.organizationname,
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        fontFamily: fontandcolor
                                            .fontfaimlyquicksandbold,
                                        color: Color(0xff394c81),
                                        fontWeight: FontWeight.w600,
                                        fontSize: 18),
                                  ),
                                ),
                              ],
                            )
                          : Container(
                              height: (20 / 8.148314082864863) *
                                  SizeConfig.heightMultiplier,
                              width: (130 / 4.853932272197492) *
                                  SizeConfig.widthMultiplier,
                              decoration: BoxDecoration(
                                  color: Color(0xffE8E8E8),
                                  borderRadius: BorderRadius.circular(40)),
                            ),
                      //widget.userid
                      //token
                      widget.rolename == "Broker"
                          ? Container()
                          : FutureBuilder(
                              future: gettingratingsbyuserid(widget.userid),
                              builder: (BuildContext context,
                                  AsyncSnapshot snapshot) {
                                Map data = snapshot.data;
                                return snapshot.hasData
                                    ? Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          Text(
                                            data["averageRating"]
                                                .toStringAsFixed(1),
                                            style: TextStyle(
                                              fontFamily:
                                                  fontandcolor.fontfaimly,
                                            ),
                                          ),
                                          Center(
                                            child: SmoothStarRating(
                                              isReadOnly: true,
                                              borderColor: Color(0xffffb900),
                                              rating: double.parse(
                                                  data["averageRating"]
                                                      .toString()),
                                              size: (25 / 8.148314082864863) *
                                                  SizeConfig.heightMultiplier,
                                              color: Color(0xffffb900),
                                              filledIconData: Icons.star,
                                              halfFilledIconData:
                                                  Icons.star_half,
                                              defaultIconData:
                                                  Icons.star_border,
                                              starCount: 5,
                                              allowHalfRating: true,
                                              spacing: 0.5,
                                            ),
                                          ),
                                        ],
                                      )
                                    : Container();
                              },
                            ),
                      widget.subscribtionstatus
                          ? Container(
                              width: widget.orientation == Orientation.portrait
                                  ? 68 * SizeConfig.widthMultiplier
                                  : 108 * SizeConfig.widthMultiplier,
                              child: Text(
                                  "${widget.description ?? "Description"}",
                                  maxLines: 5,
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    fontFamily: fontandcolor.fontfaimlyregular,
                                    color: Colors.black,
                                    fontSize: 10,
                                  )),
                            )
                          : Container(
                              width: widget.orientation == Orientation.portrait
                                  ? 68 * SizeConfig.widthMultiplier
                                  : 108 * SizeConfig.widthMultiplier,
                            ),
                      Row(
                        children: <Widget>[
                          widget.subscribtionstatus
                              ? Text(widget.country,
                                  style: TextStyle(
                                    fontFamily: fontandcolor.fontfaimly,
                                    color: Color(0xff394c81),
                                    fontSize: 12,
                                  ))
                              : Container(),
                          SizedBox(
                            width: 4,
                          ),
                          widget.subscribtionstatus
                              ? Row(
                                  children: [
                                    Icon(
                                      Icons.location_on,
                                      size: (15 / 8.148314082864863) *
                                          SizeConfig.heightMultiplier,
                                      color: Color(0xff394c81),
                                    ),
                                    Text(widget.city,
                                        style: TextStyle(
                                          fontFamily: fontandcolor.fontfaimly,
                                          color: Color(0xff394c81),
                                          fontSize: 12,
                                        )),
                                  ],
                                )
                              : Container(),
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          SizedBox(
            height: 1.5 * SizeConfig.heightMultiplier,
          ),
          widget.username == "chatuser"
              ? Container()
              : widget.subscribtionstatus
                  ? filledButton(
                      w: size.convert(context, 200),
                      txt: "Chat Now",
                      onTap: () {
                        showDialog(
                            context: context,
                            builder: (context) {
                              return CustomDialog(
                                categoryId: widget.categoryid,
                                subCategoryId: widget.subcategory,
                                iscurrentpage: false,
                                userId: widget.currentuserid,
                                createdForId: widget.createdforid,
                              );
                            });
                      },
                    )
                  : Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        filledButton(
                          txt: "Get Free Trail",
                          onTap: widget.onTap,
                        ),
                      ],
                    ),
          widget.subscribtionstatus == true || widget.username == "chatuser"
              ? Container()
              : Padding(
                  padding: MediaQuery.of(context).orientation ==
                          Orientation.portrait
                      ? EdgeInsets.symmetric(vertical: 6.0, horizontal: 15.0)
                      : EdgeInsets.symmetric(vertical: 1.0, horizontal: 3.0),
                  child: Align(
                    alignment: Alignment.center,
                    child: Text(
                      'Get membership to unblock chat complete and company details',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: Colors.red,
                          fontFamily: fontandcolor.fontfaimly,
                          fontSize: MediaQuery.of(context).orientation ==
                                  Orientation.portrait
                              ? (13.3932272197492 / 4.853932272197492) *
                                  SizeConfig.widthMultiplier
                              : (15.3932272197492 / 4.853932272197492) *
                                  SizeConfig.widthMultiplier),
                    ),
                  ),
                ),
          SizedBox(
            height: 1.5 * SizeConfig.heightMultiplier,
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 8.0),
            child: Container(
              height: 5,
            ),
          ),
        ],
      ),
    );
  }
}
