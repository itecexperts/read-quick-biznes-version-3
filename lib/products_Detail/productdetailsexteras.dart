import 'package:flutter/material.dart';
import 'package:biznes/res/size.dart';
import 'package:biznes/res/color.dart'as font;
DetailsText({String title,String name,context}){
  return Column(
    children: [
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            //This is remaining
            "$title",
            style: TextStyle(
                fontFamily: font.fontfaimly,
                fontSize: size.convert(context, 14),
                color: Color(0xff394c81)),
          ),
          Padding(
            padding: const EdgeInsets.only(right:20.0),
            child: Text("$name",
              style: TextStyle(
                  fontFamily: font.fontfaimlyquicksandbold,
                  fontWeight: FontWeight.w600,
                  fontSize: size.convert(context, 14),
                  color: Color(0xff394c81)),
            ),
          ),
        ],
      ),
      SizedBox(
        height: 2,
      ),
    ],
  );
}