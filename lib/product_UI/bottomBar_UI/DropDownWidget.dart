import 'package:biznes/size_config.dart';
import 'package:flutter/material.dart';
import 'package:biznes/res/style.dart';
import 'package:biznes/res/size.dart';
import 'package:flutter_svg/flutter_svg.dart';

Material DropDownWidget(
    {String value,
    Function onChanged,
    List categories,
    context,
    String hintext,
    String image,
    selectedCategory,
    Color color,
    Color color2}) {
  return Material(
    shadowColor: Color(0xffd4d4d4),
    elevation: 5,
    borderRadius: BorderRadius.circular(
        (28 / 8.148314082864863) * SizeConfig.heightMultiplier),
    child: Container(
      height: 48,
      padding: const EdgeInsets.all(8.0),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(
            (28 / 8.148314082864863) * SizeConfig.heightMultiplier),
        border: Border.all(
            color: selectedCategory == null ? Color(0xffc7c7c7) : Colors.blue),
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Padding(
            padding: EdgeInsets.only(bottom: 1.0),
            child: image == "assets/images/flag2.png"
                ? Image.asset(
                    image,
                    height: 15.0,
                  )
                : SvgPicture.asset(image),
          ),
          SizedBox(
            width: 7.0,
          ),
          Expanded(
            child: DropdownButtonHideUnderline(
              child: DropdownButton(
                icon: selectedCategory == null
                    ? InkWell(
                        child: Image.asset(
                          'assets/images/dropdown.png',
                          height: 15,
                          width: 15,
                        ),
                      )
                    : CircleAvatar(
                        radius: 8,
                        backgroundColor: Color(0xffEEF1F1),
                        child: Icon(
                          Icons.check,
                          color: Colors.blue,
                          size: 10,
                        ),
                      ),
                isDense: true,
                isExpanded: true,
                value: value,
                onChanged: onChanged,
                items: categories.map((f) {
                  return DropdownMenuItem(
                    value: f.title,
                    child: Text(
                      f.title,
                      style: style.MontserratRegular(
                          fontSize: size.convert(context, 14)),
                    ),
                  );
                }).toList(),
                hint: Text(
                  hintext,
                  style: style.MontserratRegular(
                      fontSize: size.convert(context, 14)),
                ),
              ),
            ),
          ),
          SizedBox(
            width: 18.0,
          ),
        ],
      ),
    ),
  );
}

SizedBox Box(double box) {
  return SizedBox(
    height: (box / 8.148314082864863) * SizeConfig.heightMultiplier,
  );
}
