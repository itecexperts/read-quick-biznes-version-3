
import 'package:biznes/helper/counthelper.dart';
import 'package:biznes/product_UI/bottomBar_UI/changepassword.dart';
import 'package:biznes/products_Detail/extra.dart';
import 'package:biznes/services/register_logic.dart';
import 'package:biznes/res/color.dart' as font;
import 'package:biznes/setting/notification.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:provider/provider.dart';
import '../../helper/login_helper.dart';
import '../../size_config.dart';
import 'package:biznes/repeatedWigets/filledButton.dart';

class ChangePassword extends StatefulWidget {
  @override
  _ChangePasswordState createState() => _ChangePasswordState();
}

class _ChangePasswordState extends State<ChangePassword> {
  String _password;
  bool isvisible = false;
  var formKey = GlobalKey<FormState>();
  TextEditingController textEditingController = TextEditingController();
  @override
  void dispose() {
    textEditingController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var counters = Provider.of<CountHelper>(context, listen: false).counter;
    var counterset = Provider.of<CountHelper>(context);
    var user = Provider.of<LoginHelper>(context).user;
    return Stack(
      children: [
        Scaffold(
          backgroundColor: Colors.white,
          body: Form(
            key: formKey,
            child: Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CustomAppBar(
                      icon: Icon(Icons.arrow_back),
                      IconPressed: (){
                        Navigator.of(context).pop();
                      },
                      context: context,
                      text: "Change Password",
                      length: counters.toString(),
                      onTap: () {
                        counterset.zerocunter();
                        Navigator.of(context).push(PageTransition(
                            child: MyNotification(),
                            duration: Duration(milliseconds: 700),
                            type: PageTransitionType
                                .leftToRightWithFade));
                      }),
                  SizedBox(
                    height: 4 * SizeConfig.heightMultiplier,
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      'Enter Your Current Password',
                      style: TextStyle(
                          fontFamily: font.fontfaimly,
                          fontSize: (18.9 / 8.148314082864863) *
                              SizeConfig.textMultiplier),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(4.0),
                    child: Stack(
                      children: [
                        material(),
                        TextFormField(
                          obscureText: true,
                          controller: textEditingController,
                          validator: (value) {
                            if (value.isEmpty && value.length < 6) {
                              return 'Enter your current password';
                            }
                            return null;
                          },
                          decoration: InputDecoration(

                              focusedErrorBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.red),
                                borderRadius: BorderRadius.circular(30.0),
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(25),
                                borderSide:
                                    BorderSide(color: Color(0xff69BEF7)),
                              ),
                              focusColor: Color(0xff69BEF7),
                              contentPadding: EdgeInsets.symmetric(
                                  vertical: 10.0, horizontal: 10.0),
                              border: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.red),
                                borderRadius: BorderRadius.circular(30.0),
                              ),
                              hintStyle: TextStyle(
                                fontFamily: font.fontfaimlyregular,
                                fontSize: 12,
                              ),
                              hintText: 'Current Password'),
                          enabled: true,
                          onChanged: (value) {
                            _password = value;
                          },
                        ),
                      ],
                    ),
                  ),
                  Center(
                    child: Padding(
                      padding: const EdgeInsets.only(top: 18.0),
                      child: filledButton(
                        txt: "Next",
                        onTap: () {
                          if (formKey.currentState.validate()) {
                            setState(() {
                              isvisible = true;
                            });
                            verifiyPassword(user.sId, _password, context)
                                .then((value) {
                              if (value["Success"] == true) {
                                setState(() {
                                  isvisible = false;
                                });
                                Navigator.of(context).push(MaterialPageRoute(
                                    builder: (context) => NewPassword()));
                              } else {
                                setState(() {
                                  isvisible = false;
                                });
                              }
                            });
                          }
                        },
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
        isvisible
            ? Stack(
                children: <Widget>[
                  Container(
                    color: Color(0xffffffff).withOpacity(0.4),
                    height: double.infinity,
                    width: double.infinity,
                    child: Opacity(
                      opacity: 1.0,
                      child: Center(
                          child: Image.asset(
                        "assets/images/logo2.gif",
                        height: 200,
                        width: 100,
                      )),
                    ),
                  ),
                ],
              )
            : Container(),
      ],
    );
  }
}
