import 'package:biznes/Controllers/backendservicescontroller.dart';
import 'package:biznes/add_product/addProduct.dart';
import 'package:biznes/bloc/CurrrentUserMemberShip/CurrrentUserMemberShipBLoc.dart';
import 'package:biznes/bloc/CurrrentUserMemberShip/CurrrentUserMemberShipEvent.dart';
import 'package:biznes/bloc/CurrrentUserMemberShip/CurrrentUserMemberShipStates.dart';
import 'package:biznes/helper/counthelper.dart';
import 'package:biznes/helper/productdetailshelper.dart';
import 'package:biznes/helper/gettingproducts.dart';
import 'package:biznes/products_Detail/extra.dart';
import 'package:biznes/products_Detail/productDetails.dart';
import 'package:biznes/helper/login_helper.dart';
import 'package:biznes/services/server.dart';
import 'package:biznes/repeatedWigets/filledButton.dart';
import 'package:biznes/res/color.dart' as fonts;
import 'package:biznes/model/membershitpdetailmodel.dart';
import 'package:biznes/product_UI/bottomBar_UI/editprofile.dart';
import 'package:biznes/services/register_logic.dart';
import 'package:biznes/setting/notification.dart';
import 'package:biznes/res/size.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:biznes/size_config.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:intl/intl.dart';
import 'package:page_transition/page_transition.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';
import 'editproduct.dart';

class Newprofile extends StatefulWidget {
  const Newprofile({Key key}) : super(key: key);
  @override
  _NewprofileState createState() => _NewprofileState();
}

class _NewprofileState extends State<Newprofile> {
  bool isloading = true;
  bool isdeleting = false;
  int indexs = 0;
  gettinguserproducts() async {
    SharedPreferences _pres = await SharedPreferences.getInstance();
    String userid = _pres.getString("userid");
    getAllCurrentUserProducts(userid, context).then((value) {
      Provider.of<GettingProducts>(context, listen: false)
          .onLogIn(value["products"]);
      print(value["products"]);
      setState(() {
        isloading = false;
      });
    });
  }

  getCurrentUserMemberShip() async {
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    context.bloc<CurrrentUserMemberShipBloc>().add(
        FetchMembershipCurrrentUserMemberShip(
            userId: _prefs.getString("userid")));
  }

  @override
  void initState() {
    getCurrentUserMemberShip();
    gettinguserproducts();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var user = Provider.of<LoginHelper>(context).user;
    var token = Provider.of<LoginHelper>(context).token;
    var counters = Provider.of<CountHelper>(context).counter;
    var counterset = Provider.of<CountHelper>(context);

    var productdetails = Provider.of<ProductDetailsHelperData>(context);
    var productList = Provider.of<GettingProducts>(context).products;

    showdialogfordeleting(index) {
      return showDialog(
        context: context,
        builder: (context) {
          return Dialog(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(5),
            ),
            child: SingleChildScrollView(
              child: Column(
                children: [
                  SizedBox(
                    height: size.convert(context, 20),
                  ),
                  SvgPicture.asset("assets/images/Delete.svg"),
                  SizedBox(
                    height: size.convert(context, 5),
                  ),
                  Text(
                    "Delete Your Product",
                    style: TextStyle(
                      fontFamily: fonts.fontfaimlyquicksandbold,
                      color: fonts.appbarfontandiconcolor,
                      fontSize: size.convert(context, 25),
                    ),
                  ),
                  Text(
                    "Are you sure to delete your product?",
                    style: TextStyle(
                      fontFamily: fonts.fontfaimlyregular,
                    ),
                  ),
                  SizedBox(
                    height: size.convert(context, 15),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      filledButton(
                        onTap: () {
                          setState(() {
                            isdeleting = true;
                          });
                          deleteProductById(productList[index].sId, context)
                              .then((value) {
                            if (value != null) {
                              setState(() {
                                isdeleting = false;
                              });
                              getAllCurrentUserProducts(user.sId, context)
                                  .then((value) {
                                Provider.of<GettingProducts>(context,
                                        listen: false)
                                    .onLogIn(value["products"]);
                                Navigator.pop(context);
                              });
                            } else {
                              Center(
                                child: CircularProgressIndicator(),
                              );
                            }
                          });
                        },
                        w: size.convert(context, 80),
                        fontsize: size.convert(context, 12),
                        h: size.convert(context, 30),
                        shadowColor: Colors.white,
                        txt: "Delete",
                      ),
                      filledButton(
                        onTap: () {
                          Navigator.of(context).pop();
                        },
                        txt: "Cancel",
                        startColor: Color(0xffffcd02),
                        endColor: Color(0xffffcd02),
                        shadowColor: Colors.white,
                        w: size.convert(context, 80),
                        fontsize: size.convert(context, 12),
                        h: size.convert(context, 30),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: size.convert(context, 20),
                  ),
                ],
              ),
            ),
          );
        },
        barrierDismissible: false,
      );
    }

    void handleClick(String value, int index) {
      switch (value) {
        case 'Edit product':
          Navigator.of(context).push(MaterialPageRoute(
            builder: (context) => EditProduct(index, productList),
          ));
          break;
        case 'Delete product':
          showdialogfordeleting(index);
          break;
      }
    }

    Widget buildList(BuildContext context, int index) {
      return Stack(
        children: [
          Card(
            child: Padding(
              padding: const EdgeInsets.all(4.0),
              child: Container(
                decoration: BoxDecoration(
                  color: Color(0xffffffff),
                ),
                width: double.infinity,
                margin: EdgeInsets.symmetric(vertical: 0, horizontal: 6),
                child: InkWell(
                  onTap: () {
                    getProductDetails(
                      context: context,
                      userid: user.sId,
                      productId: productList[index].sId,
                    ).then((singleProductList) {
                      productdetails.productdetailsfunction(singleProductList);
                      if (singleProductList != null) {
                        Navigator.push(
                            context,
                            PageTransition(
                                child: ProductDetail(
                                  gettingproductid: productList[index].sId,
                                  userid: user.sId,
                                  product: singleProductList,
                                  currentuser: "currentuser",
                                ),
                                type: PageTransitionType.fade,
                                duration: Duration(milliseconds: 800)));
                      }
                    });
                  },
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        width: (120 / 4.853932272197492) *
                            SizeConfig.widthMultiplier,
                        height: (110 / 8.148314082864863) *
                            SizeConfig.heightMultiplier,
                        margin: EdgeInsets.only(
                            right: 13, top: 1.0 * SizeConfig.heightMultiplier),
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(10),
                          child: productList[index].images.length == 0
                              ? Container()
                              : CachedNetworkImage(
                                  fit: BoxFit.cover,
                                  imageUrl:
                                      '$base_url/${productList[index].images[0]}',
                                  placeholder: (context, url) => Center(
                                      child: CircularProgressIndicator()),
                                  errorWidget: (context, url, error) =>
                                      Icon(Icons.error),
                                ),
                        ),
                      ),
                      Expanded(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              "${productList[index].title}",
                              style: TextStyle(
                                fontWeight: FontWeight.w400,
                                fontFamily: fonts.fontfaimly,
                                color: Color(0xff394C81),
                                fontSize: (16 / 8.148314082864863) *
                                    SizeConfig.heightMultiplier,
                              ),
                            ),
                            SizedBox(
                              height: (2 / 8.148314082864863) *
                                  SizeConfig.heightMultiplier,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  "${productList[index].ratings.toString()}.0",
                                  style: TextStyle(),
                                ),
                                Center(
                                    child: SmoothStarRating(
                                  isReadOnly: true,
                                  borderColor: Color(0xffffb900),
                                  rating:
                                      productList[index].ratings.toString() ==
                                              null
                                          ? 0.0
                                          : double.parse(productList[index]
                                              .ratings
                                              .toString()),
                                  size: (20 / 8.148314082864863) *
                                      SizeConfig.heightMultiplier,
                                  color: Color(0xffffb900),
                                  filledIconData: Icons.star,
                                  defaultIconData: Icons.star_border,
                                  starCount: 5,
                                  allowHalfRating: true,
                                  spacing: 0.5,
                                )),
                              ],
                            ),
                            SizedBox(
                              height: (3 / 8.148314082864863) *
                                  SizeConfig.heightMultiplier,
                            ),
                            Text(
                                "Category: ${productList[index].category.title ?? "not mention"}",
                                style: TextStyle(
                                    color: fonts.appbarfontandiconcolor,
                                    fontSize: (12 / 8.148314082864863) *
                                        SizeConfig.heightMultiplier,
                                    fontFamily: fonts.fontfaimlyregular,
                                    letterSpacing: .3)),
                            SizedBox(
                              height: 1.5,
                            ),
                            Text(
                                "Sub-Category:${productList[index].subCategory.title ?? "not mention"}",
                                style: TextStyle(
                                    color: Colors.black,
                                    fontSize: (10 / 8.148314082864863) *
                                        SizeConfig.heightMultiplier,
                                    fontFamily: fonts.fontfaimlyregular,
                                    letterSpacing: .3)),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                        "Price ${productList[index].price ?? "not mention"}",
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            color: Colors.black,
                                            fontSize: (12 / 8.148314082864863) *
                                                SizeConfig.heightMultiplier,
                                            fontFamily: fonts.fontfaimlyregular,
                                            letterSpacing: .3)),
                                    SizedBox(
                                      height: (1.5 / 8.148314082864863) *
                                          SizeConfig.heightMultiplier,
                                    ),
                                    Text(
                                        "Quantity : ${productList[index].quantity.toString() ?? "not mention"}  (${productList[index].quantityUnit ?? "not mention"})",
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontSize: (12 / 8.148314082864863) *
                                                SizeConfig.heightMultiplier,
                                            fontFamily: fonts.fontfaimlyregular,
                                            letterSpacing: .3)),
                                    Text(
                                        "M.O.Q : ${productList[index].moq.toString() ?? "not mention"}  (${productList[index].quantityUnit ?? "not mention"})",
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontSize: (12 / 8.148314082864863) *
                                                SizeConfig.heightMultiplier,
                                            fontFamily: fonts.fontfaimlyregular,
                                            letterSpacing: .3)),
                                    SizedBox(
                                      height: (1.5 / 8.148314082864863) *
                                          SizeConfig.heightMultiplier,
                                    ),
                                  ],
                                ),
                              ],
                            ),
                            Row(
                              children: <Widget>[
                                Text("Available",
                                    style: TextStyle(
                                        color: Colors.green,
                                        fontSize: 11,
                                        fontFamily: fonts.fontfaimlyregular,
                                        letterSpacing: .3)),
                                SizedBox(
                                  width: (5 / 4.853932272197492) *
                                      SizeConfig.widthMultiplier,
                                ),
                                Image.asset(
                                  'assets/images/flag2.png',
                                  height: 12,
                                  width: 15,
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(right: 2.0, top: 10.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                PopupMenuButton<String>(
                  icon: Icon(
                    Icons.more_vert,
                    color: Colors.grey,
                  ),
                  padding: EdgeInsets.zero,
                  onSelected: (d) {
                    handleClick(d, index);
                  },
                  itemBuilder: (BuildContext context) {
                    return {'Edit product', 'Delete product'}
                        .map((String choice) {
                      return PopupMenuItem<String>(
                        value: choice,
                        child: Text(
                          choice,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontFamily: fonts.fontfaimlyregular,
                            color: choice == "Delete product"
                                ? Colors.red
                                : Colors.black,
                          ),
                        ),
                      );
                    }).toList();
                  },
                ),
              ],
            ),
          ),
        ],
      );
    }

    Widget profilecontainerportratit() {
      return Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              CustomAppBar(
                  context: context,
                  length: counters.toString(),
                  onTap: () {
                    counterset.zerocunter();
                    Navigator.of(context).push(PageTransition(
                        child: MyNotification(),
                        duration: Duration(milliseconds: 700),
                        type: PageTransitionType.leftToRightWithFade));
                  }),
            ],
          ),
          Stack(
            alignment: Alignment.topRight,
            overflow: Overflow.visible,
            children: <Widget>[
              CircleAvatar(
                backgroundColor: Colors.blue,
                radius: (55 / 8.148314082864863) * SizeConfig.heightMultiplier,
                backgroundImage: CachedNetworkImageProvider(
                  '$base_url/${user.profilePic}',
                ),
              ),
              Padding(
                padding: EdgeInsets.all(1.0 * SizeConfig.heightMultiplier),
                child: CircleAvatar(
                  radius: 5,
                  backgroundColor: Colors.green,
                ),
              ),
            ],
          ),
          Padding(
            padding: const EdgeInsets.all(1.0),
            child: Text(
              '${user.fullName}',
              style: TextStyle(
                color: Color(0xff394C81),
                fontFamily: fonts.fontfaimlyquicksandbold,
                fontSize: (20 / 8.148314082864863) * SizeConfig.textMultiplier,
              ),
            ),
          ),
          user.roleName == "Broker"
              ? Container()
              : Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      color: Colors.white,
                      width: 300,
                      height: 18,
                      child: Center(
                        child: Row(
                          children: user.roleId.map((e) {
                            return Text(
                              e.title + ",",
                              style: TextStyle(
                                fontSize: 1.7 * SizeConfig.heightMultiplier,
                                fontFamily: fonts.fontfaimly,
                              ),
                            );
                          }).toList(),
                        ),
                      ),
                      // child: ListView.builder(
                      //     scrollDirection: Axis.horizontal,
                      //     physics: NeverScrollableScrollPhysics(),
                      //     itemCount: user.roleId.length,
                      //     itemBuilder: (BuildContext context, int index) {
                      //       return Row(
                      //         mainAxisAlignment: MainAxisAlignment.center,
                      //         children: [
                      //           Text(
                      //             user.roleId[index].title,
                      //             textAlign: TextAlign.center,
                      // style: TextStyle(
                      //     fontSize: 1.8*SizeConfig.heightMultiplier,
                      //     ),
                      //           ),
                      //           user.roleId.length == 1 ? Container() : Text(' '),
                      //         ],
                      //       );
                      //     }),
                    ),
                  ],
                ),
          user.roleName == "Broker"
              ? Text(
                  "Broker",
                  style: TextStyle(fontFamily: fonts.fontfaimlyregular),
                )
              : FutureBuilder(
                  future: gettingratingsbyuserid(user.sId),
                  builder: (BuildContext context, AsyncSnapshot snapshot) {
                    Map data = snapshot.data;
                    return snapshot.hasData
                        ? Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                data["averageRating"].toStringAsFixed(1),
                                style: TextStyle(fontFamily: fonts.fontfaimly),
                              ),
                              Center(
                                child: SmoothStarRating(
                                  isReadOnly: true,
                                  borderColor: Color(0xffffb900),
                                  rating: double.parse(
                                      data["averageRating"].toString()),
                                  size: (25 / 8.148314082864863) *
                                      SizeConfig.heightMultiplier,
                                  color: Color(0xffffb900),
                                  filledIconData: Icons.star,
                                  halfFilledIconData: Icons.star_half,
                                  defaultIconData: Icons.star_border,
                                  starCount: 5,
                                  allowHalfRating: true,
                                  spacing: 0.5,
                                ),
                              ),
                            ],
                          )
                        : Container();
                  },
                ),
          SizedBox(
            height: 0.4 * SizeConfig.heightMultiplier,
          ),
          InkWell(
            onTap: () {
              print("user.roleId.length ${user.roleId.length}");
              if (user.roleId.length == 1) {
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => EditProfile(
                          beforeroles1: user.roleId[0].sId,
                          username: user.username,
                          organizationname: user.organizationName,
                          email: user.email,
                        )));
              } else if (user.roleId.length == 2) {
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => EditProfile(
                          beforeroles1: user.roleId[0].sId,
                          beforeroles2: user.roleId[1].sId,
                          username: user.username,
                          organizationname: user.organizationName,
                          email: user.email,
                        )));
              } else if (user.roleId.length == 3) {
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => EditProfile(
                          username: user.username,
                          organizationname: user.organizationName,
                          email: user.email,
                          beforeroles1: user.roleId[0].sId,
                          beforeroles2: user.roleId[1].sId,
                          beforeroles3: user.roleId[2].sId,
                        )));
              } else if (user.roleId.length == 4) {
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => EditProfile(
                          username: user.username,
                          organizationname: user.organizationName,
                          email: user.email,
                          beforeroles1: user.roleId[0].sId,
                          beforeroles2: user.roleId[1].sId,
                          beforeroles3: user.roleId[2].sId,
                          beforerole4: user.roleId[3].sId,
                        )));
              }
            },
            child: filledButton(
              radius: 5,
              h: size.convert(context, 20),
              w: size.convert(context, 100),
              txt: "Edit Profile",
              shadowColor: Colors.white,
              fontsize: size.convert(context, 10),
            ),
          ),
        ],
      );
    }

    Widget profileContainerlandscape() {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Stack(
                alignment: Alignment.topRight,
                overflow: Overflow.visible,
                children: <Widget>[
                  CircleAvatar(
                    backgroundColor: Colors.blue,
                    radius:
                        (35 / 8.148314082864863) * SizeConfig.heightMultiplier,
                    backgroundImage: CachedNetworkImageProvider(
                      '$base_url/${user.profilePic}',
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.all(1.0 * SizeConfig.heightMultiplier),
                    child: CircleAvatar(
                      radius: 5,
                      backgroundColor: Colors.green,
                    ),
                  ),
                ],
              ),
              Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(1.0),
                    child: Text(
                      '${user.fullName}',
                      style: TextStyle(
                        color: Color(0xff394C81),
                        fontSize: (20 / 8.148314082864863) *
                            SizeConfig.textMultiplier,
                        fontFamily: fonts.fontfaimlyquicksandbold,
                      ),
                    ),
                  ),
                  user.roleName == "Broker"
                      ? Container()
                      : Container(
                          width: 200,
                          height: 15,
                          child: ListView.builder(
                              scrollDirection: Axis.horizontal,
                              physics: NeverScrollableScrollPhysics(),
                              itemCount: user.roleId.length,
                              itemBuilder: (BuildContext context, int index) {
                                return FittedBox(
                                  child: Row(
                                    children: [
                                      Text(
                                        user.roleId[index].title,
                                        style: TextStyle(
                                          fontFamily: fonts.fontfaimly,
                                          fontSize:
                                              1.8 * SizeConfig.heightMultiplier,
                                        ),
                                        textAlign: TextAlign.center,
                                      ),
                                      user.roleId.length == 1
                                          ? Container()
                                          : Text(' '),
                                    ],
                                  ),
                                );
                              }),
                        ),
                  user.roleName == "Broker"
                      ? Text(
                          "Broker",
                          style: TextStyle(fontWeight: FontWeight.bold),
                        )
                      : FutureBuilder(
                          future: gettingratingsbyuserid(user.sId),
                          builder:
                              (BuildContext context, AsyncSnapshot snapshot) {
                            Map data = snapshot.data;
                            return snapshot.hasData
                                ? Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Text(
                                        data["averageRating"]
                                            .toStringAsFixed(1),
                                        style: TextStyle(
                                          fontFamily: fonts.fontfaimly,
                                        ),
                                      ),
                                      Center(
                                        child: SmoothStarRating(
                                          isReadOnly: true,
                                          borderColor: Color(0xffffb900),
                                          rating: double.parse(
                                              data["averageRating"].toString()),
                                          size: (25 / 8.148314082864863) *
                                              SizeConfig.heightMultiplier,
                                          color: Color(0xffffb900),
                                          filledIconData: Icons.star,
                                          halfFilledIconData: Icons.star_half,
                                          defaultIconData: Icons.star_border,
                                          starCount: 5,
                                          allowHalfRating: true,
                                          spacing: 0.5,
                                        ),
                                      ),
                                    ],
                                  )
                                : Container();
                          },
                        ),
                  InkWell(
                    onTap: () {
                      if (user.roleId.length == 1) {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) => EditProfile(
                                  beforeroles1: user.roleId[0].sId,
                                  username: user.username,
                                  organizationname: user.organizationName,
                                  email: user.email,
                                )));
                      } else if (user.roleId.length == 2) {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) => EditProfile(
                                  beforeroles1: user.roleId[0].sId,
                                  beforeroles2: user.roleId[1].sId,
                                  username: user.username,
                                  organizationname: user.organizationName,
                                  email: user.email,
                                )));
                      } else if (user.roleId.length == 3) {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) => EditProfile(
                                  beforeroles1: user.roleId[0].sId,
                                  beforeroles2: user.roleId[1].sId,
                                  beforeroles3: user.roleId[2].sId,
                                  username: user.username,
                                  organizationname: user.organizationName,
                                  email: user.email,
                                )));
                      } else if (user.roleId.length == 3) {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) => EditProfile(
                                  beforeroles1: user.roleId[0].sId,
                                  beforeroles2: user.roleId[1].sId,
                                  beforeroles3: user.roleId[2].sId,
                                  beforerole4: user.roleId[3].sId,
                                  username: user.username,
                                  organizationname: user.organizationName,
                                  email: user.email,
                                )));
                      }
                    },
                    child: filledButton(
                      radius: 0.1,
                      h: size.convert(context, 20),
                      w: size.convert(context, 100),
                      txt: "Edit Profile",
                      fontsize: size.convert(context, 10),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ],
      );
    }

    Widget membership(orientation) {
      return Column(
        children: [
          Container(
            decoration: BoxDecoration(
              color: Colors.white,
              image: DecorationImage(
                fit: BoxFit.cover,
                image: AssetImage("assets/images/newback.jpg"),
              ),
            ),
            alignment: Alignment.centerLeft,
            width: double.infinity,
            height: 6 * SizeConfig.heightMultiplier,
            child: Padding(
              padding: EdgeInsets.only(
                  left: 2.0 * SizeConfig.widthMultiplier,
                  right: 2.0 * SizeConfig.widthMultiplier),
              child: Text(
                "All my current memberships",
                style: TextStyle(
                  fontFamily: fonts.fontfaimly,
                ),
              ),
            ),
          ),
          Expanded(
            child: BlocBuilder<CurrrentUserMemberShipBloc,
                CurrrentUserMemberShipStates>(
              builder:
                  (BuildContext context, CurrrentUserMemberShipStates state) {
                if (state is CurrrentUserMemberShipErrorState) {
                  final errror = state.error;
                  return RefreshIndicator(
                      onRefresh: () => getCurrentUserMemberShip(),
                      child: ListView(
                        children: [
                          Text(errror.message),
                        ],
                      ));
                }
                if (state is CurrrentUserMemberShipStatesLoadedState) {
                  List<Membershipdetailmodel> details = state.memberShipDetails;
                  return RefreshIndicator(
                    onRefresh: () => getCurrentUserMemberShip(),
                    child: ListView.builder(
                      padding: EdgeInsets.zero,
                      shrinkWrap: true,
                      itemCount: details.length,
                      physics: AlwaysScrollableScrollPhysics(),
                      itemBuilder: (BuildContext context, int indexs) {
                        DateTime dateTime1 =
                            DateTime.parse(details[indexs].subscriptionDate);
                        DateTime dateTime = DateTime.parse(
                            details[indexs].subscriptionExpiryDate);
                        String formattedTime =
                            DateFormat('dd.MM.yyyy').format(dateTime);
                        String formattedTime1 =
                            DateFormat('dd.MM.yyyy').format(dateTime1);
                        return Container(
                          margin: EdgeInsets.all(4),
                          width: MediaQuery.of(context).size.width,
                          // height: MediaQuery.of(context).orientation ==
                          //         Orientation.portrait
                          //     ? (110 / 8.148314082864863) *
                          //         SizeConfig.heightMultiplier
                          //     : (110 / 8.148314082864863) *
                          //         SizeConfig.heightMultiplier,
                          color: Color(0xffebf3fa),
                          child: Container(
                            margin: EdgeInsets.all(
                              MediaQuery.of(context).orientation ==
                                      Orientation.portrait
                                  ? (10 / 8.148314082864863) *
                                      SizeConfig.heightMultiplier
                                  : (10 / 8.148314082864863) *
                                      SizeConfig.heightMultiplier,
                            ),
                            child: Column(
                              children: [
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        FittedBox(
                                          child: Row(
                                            children: [
                                              Text(
                                                details[indexs].planId.title,
                                                style: TextStyle(
                                                  fontFamily: fonts
                                                      .fontfaimlyquicksandbold,
                                                  color: Color(0xff394C81),
                                                  fontSize: user.roleName ==
                                                          "Broker"
                                                      ? 2.5 *
                                                          SizeConfig
                                                              .textMultiplier
                                                      : orientation ==
                                                              Orientation
                                                                  .portrait
                                                          ? 2.8 *
                                                              SizeConfig
                                                                  .textMultiplier
                                                          : 4 *
                                                              SizeConfig
                                                                  .textMultiplier,
                                                ),
                                                overflow: TextOverflow.ellipsis,
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                    Column(
                                      // mainAxisAlignment: MainAxisAlignment.center,
                                      children: <Widget>[
                                        FittedBox(
                                          child: Row(
                                            children: [
                                              Text(
                                                "Started Date",
                                                style: TextStyle(
                                                  color: Color(0xff51D289),
                                                  fontFamily: fonts
                                                      .fontfaimlyquicksandbold,
                                                  fontSize: orientation ==
                                                          Orientation.portrait
                                                      ? 2.2 *
                                                          SizeConfig
                                                              .textMultiplier
                                                      : 4 *
                                                          SizeConfig
                                                              .textMultiplier,
                                                ),
                                                overflow: TextOverflow.ellipsis,
                                              ),
                                            ],
                                          ),
                                        ),
                                        FittedBox(
                                          child: Text(
                                            formattedTime1,
                                            overflow: TextOverflow.clip,
                                            style: TextStyle(
                                              fontFamily:
                                                  fonts.fontfaimlyregular,
                                              color: Color(0xff51D289),
                                              fontSize: orientation ==
                                                      Orientation.portrait
                                                  ? 1.5 *
                                                      SizeConfig.textMultiplier
                                                  : 2 *
                                                      SizeConfig.textMultiplier,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                    Column(
                                      // mainAxisAlignment: MainAxisAlignment.center,
                                      children: <Widget>[
                                        FittedBox(
                                          child: Row(
                                            children: [
                                              Text(
                                                "Expiry date",
                                                style: TextStyle(
                                                  fontFamily: fonts
                                                      .fontfaimlyquicksandbold,
                                                  color: Color(0xffFD5E5F),
                                                  fontSize: orientation ==
                                                          Orientation.portrait
                                                      ? 2.2 *
                                                          SizeConfig
                                                              .textMultiplier
                                                      : 4 *
                                                          SizeConfig
                                                              .textMultiplier,
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        FittedBox(
                                          child: Row(
                                            children: [
                                              Text(
                                                formattedTime,
                                                style: TextStyle(
                                                  color: Color(0xffFD5E5F),
                                                  fontSize: orientation ==
                                                          Orientation.portrait
                                                      ? 1.5 *
                                                          SizeConfig
                                                              .textMultiplier
                                                      : 2 *
                                                          SizeConfig
                                                              .textMultiplier,
                                                ),
                                                overflow: TextOverflow.ellipsis,
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    FittedBox(
                                      child: Row(
                                        children: [
                                          Text(
                                            "Category: ${details[indexs].categoryId.title}",
                                            textAlign: TextAlign.start,
                                            overflow: TextOverflow.ellipsis,
                                            style: TextStyle(
                                              fontFamily:
                                                  fonts.fontfaimlyregular,
                                              color: Color(0xff707070),
                                              fontSize: orientation ==
                                                      Orientation.portrait
                                                  ? 1.8 *
                                                      SizeConfig.textMultiplier
                                                  : 2 *
                                                      SizeConfig.textMultiplier,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: 0.5 * SizeConfig.heightMultiplier,
                                ),
                                user.roleName == "Broker"
                                    ? Container()
                                    : Container(
                                        alignment: Alignment.topLeft,
                                        child: Text(
                                          "Sub-Category",
                                          style: TextStyle(
                                            fontFamily: fonts.fontfaimly,
                                            color: Color(0xff394C81),
                                            fontSize:
                                                2.2 * SizeConfig.textMultiplier,
                                          ),
                                        ),
                                      ),
                                Row(
                                  children: [
                                    user.roleName == "Broker"
                                        ? Container(
                                            child: Expanded(
                                                flex: 3, child: Container()),
                                          )
                                        : Expanded(
                                            flex: 3,
                                            child: Container(
                                              height: 8 *
                                                  SizeConfig.heightMultiplier,
                                              width: 60 *
                                                  SizeConfig.widthMultiplier,
                                              child: ListView.builder(
                                                  padding: EdgeInsets.zero,
                                                  itemCount: details[indexs]
                                                      .subcategories
                                                      .length,
                                                  itemBuilder:
                                                      (BuildContext context,
                                                          int index) {
                                                    return Column(
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .start,
                                                      children: [
                                                        Text(
                                                          "${details[indexs].subcategories[index].title}",
                                                          style: TextStyle(
                                                            fontFamily: fonts
                                                                .fontfaimlyregular,
                                                            fontSize: 1.8 *
                                                                SizeConfig
                                                                    .textMultiplier,
                                                            color: Color(
                                                                0xff707070),
                                                          ),
                                                        ),
                                                      ],
                                                    );
                                                  }),
                                            ),
                                          ),
                                    Expanded(
                                      flex: 2,
                                      child: filledButton(
                                        txt: "RENEWAL",
                                        fontsize: size.convert(context, 13),
                                        h: size.convert(context, 38),
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        );
                      },
                    ),
                  );
                }
                return Center(child: CupertinoActivityIndicator());
              },
            ),
          ),
          // Expanded(
          //   child: Container(
          //     color: Colors.white,
          //     child: FutureBuilder(
          //       // future: getmembershipdetails(user.sId, context),
          //       builder: (BuildContext context,
          //           AsyncSnapshot<List<Membershipdetailmodel>> snapshot) {
          //         List<Membershipdetailmodel> details = snapshot.data;
          //         return snapshot.hasData
          //             ? details.length == 0
          //                 ? Center(
          //                     child: Container(
          //                       child: Text(
          //                         "No current membership yet",
          //                         style: TextStyle(
          //                           fontFamily: fonts.fontfaimly,
          //                         ),
          //                       ),
          //                     ),
          //                   )
          //                 : ListView.builder(
          //                     padding: EdgeInsets.zero,
          //                     shrinkWrap: true,
          //                     itemCount: details.length,
          //                     physics: BouncingScrollPhysics(),
          //                     itemBuilder: (BuildContext context, int indexs) {
          //                       DateTime dateTime1 = DateTime.parse(
          //                           details[indexs].subscriptionDate);
          //                       DateTime dateTime = DateTime.parse(
          //                           details[indexs].subscriptionExpiryDate);
          //                       String formattedTime =
          //                           DateFormat('dd.MM.yyyy').format(dateTime);
          //                       String formattedTime1 =
          //                           DateFormat('dd.MM.yyyy').format(dateTime1);
          //                       return Container(
          //                         margin: EdgeInsets.all(4),
          //                         width: MediaQuery.of(context).size.width,
          //                         // height: MediaQuery.of(context).orientation ==
          //                         //         Orientation.portrait
          //                         //     ? (110 / 8.148314082864863) *
          //                         //         SizeConfig.heightMultiplier
          //                         //     : (110 / 8.148314082864863) *
          //                         //         SizeConfig.heightMultiplier,
          //                         color: Color(0xffebf3fa),
          //                         child: Container(
          //                           margin: EdgeInsets.all(
          //                             MediaQuery.of(context).orientation ==
          //                                     Orientation.portrait
          //                                 ? (10 / 8.148314082864863) *
          //                                     SizeConfig.heightMultiplier
          //                                 : (10 / 8.148314082864863) *
          //                                     SizeConfig.heightMultiplier,
          //                           ),
          //                           child: Column(
          //                             children: [
          //                               Row(
          //                                 mainAxisAlignment:
          //                                     MainAxisAlignment.spaceBetween,
          //                                 children: <Widget>[
          //                                   Column(
          //                                     crossAxisAlignment:
          //                                         CrossAxisAlignment.start,
          //                                     children: <Widget>[
          //                                       FittedBox(
          //                                         child: Row(
          //                                           children: [
          //                                             Text(
          //                                               details[indexs]
          //                                                   .planId
          //                                                   .title,
          //                                               style: TextStyle(
          //                                                 fontFamily: fonts
          //                                                     .fontfaimlyquicksandbold,
          //                                                 color:
          //                                                     Color(0xff394C81),
          //                                                 fontSize: user
          //                                                             .roleName ==
          //                                                         "Broker"
          //                                                     ? 2.5 *
          //                                                         SizeConfig
          //                                                             .textMultiplier
          //                                                     : orientation ==
          //                                                             Orientation
          //                                                                 .portrait
          //                                                         ? 2.8 *
          //                                                             SizeConfig
          //                                                                 .textMultiplier
          //                                                         : 4 *
          //                                                             SizeConfig
          //                                                                 .textMultiplier,
          //                                               ),
          //                                               overflow: TextOverflow
          //                                                   .ellipsis,
          //                                             ),
          //                                           ],
          //                                         ),
          //                                       ),
          //                                     ],
          //                                   ),
          //                                   Column(
          //                                     // mainAxisAlignment: MainAxisAlignment.center,
          //                                     children: <Widget>[
          //                                       FittedBox(
          //                                         child: Row(
          //                                           children: [
          //                                             Text(
          //                                               "Started Date",
          //                                               style: TextStyle(
          //                                                 color:
          //                                                     Color(0xff51D289),
          //                                                 fontFamily: fonts
          //                                                     .fontfaimlyquicksandbold,
          //                                                 fontSize: orientation ==
          //                                                         Orientation
          //                                                             .portrait
          //                                                     ? 2.2 *
          //                                                         SizeConfig
          //                                                             .textMultiplier
          //                                                     : 4 *
          //                                                         SizeConfig
          //                                                             .textMultiplier,
          //                                               ),
          //                                               overflow: TextOverflow
          //                                                   .ellipsis,
          //                                             ),
          //                                           ],
          //                                         ),
          //                                       ),
          //                                       FittedBox(
          //                                         child: Text(
          //                                           formattedTime1,
          //                                           overflow: TextOverflow.clip,
          //                                           style: TextStyle(
          //                                             fontFamily: fonts
          //                                                 .fontfaimlyregular,
          //                                             color: Color(0xff51D289),
          //                                             fontSize: orientation ==
          //                                                     Orientation
          //                                                         .portrait
          //                                                 ? 1.5 *
          //                                                     SizeConfig
          //                                                         .textMultiplier
          //                                                 : 2 *
          //                                                     SizeConfig
          //                                                         .textMultiplier,
          //                                           ),
          //                                         ),
          //                                       ),
          //                                     ],
          //                                   ),
          //                                   Column(
          //                                     // mainAxisAlignment: MainAxisAlignment.center,
          //                                     children: <Widget>[
          //                                       FittedBox(
          //                                         child: Row(
          //                                           children: [
          //                                             Text(
          //                                               "Expiry date",
          //                                               style: TextStyle(
          //                                                 fontFamily: fonts
          //                                                     .fontfaimlyquicksandbold,
          //                                                 color:
          //                                                     Color(0xffFD5E5F),
          //                                                 fontSize: orientation ==
          //                                                         Orientation
          //                                                             .portrait
          //                                                     ? 2.2 *
          //                                                         SizeConfig
          //                                                             .textMultiplier
          //                                                     : 4 *
          //                                                         SizeConfig
          //                                                             .textMultiplier,
          //                                               ),
          //                                             ),
          //                                           ],
          //                                         ),
          //                                       ),
          //                                       FittedBox(
          //                                         child: Row(
          //                                           children: [
          //                                             Text(
          //                                               formattedTime,
          //                                               style: TextStyle(
          //                                                 color:
          //                                                     Color(0xffFD5E5F),
          //                                                 fontSize: orientation ==
          //                                                         Orientation
          //                                                             .portrait
          //                                                     ? 1.5 *
          //                                                         SizeConfig
          //                                                             .textMultiplier
          //                                                     : 2 *
          //                                                         SizeConfig
          //                                                             .textMultiplier,
          //                                               ),
          //                                               overflow: TextOverflow
          //                                                   .ellipsis,
          //                                             ),
          //                                           ],
          //                                         ),
          //                                       ),
          //                                     ],
          //                                   ),
          //                                 ],
          //                               ),
          //                               Row(
          //                                 mainAxisAlignment:
          //                                     MainAxisAlignment.start,
          //                                 children: [
          //                                   FittedBox(
          //                                     child: Row(
          //                                       children: [
          //                                         Text(
          //                                           "Category: ${details[indexs].categoryId.title}",
          //                                           textAlign: TextAlign.start,
          //                                           overflow:
          //                                               TextOverflow.ellipsis,
          //                                           style: TextStyle(
          //                                             fontFamily: fonts
          //                                                 .fontfaimlyregular,
          //                                             color: Color(0xff707070),
          //                                             fontSize: orientation ==
          //                                                     Orientation
          //                                                         .portrait
          //                                                 ? 1.8 *
          //                                                     SizeConfig
          //                                                         .textMultiplier
          //                                                 : 2 *
          //                                                     SizeConfig
          //                                                         .textMultiplier,
          //                                           ),
          //                                         ),
          //                                       ],
          //                                     ),
          //                                   ),
          //                                 ],
          //                               ),
          //                               SizedBox(
          //                                 height:
          //                                     0.5 * SizeConfig.heightMultiplier,
          //                               ),
          //                               user.roleName == "Broker"
          //                                   ? Container()
          //                                   : Container(
          //                                       alignment: Alignment.topLeft,
          //                                       child: Text(
          //                                         "Sub-Category",
          //                                         style: TextStyle(
          //                                           fontFamily:
          //                                               fonts.fontfaimly,
          //                                           color: Color(0xff394C81),
          //                                           fontSize: 2.2 *
          //                                               SizeConfig
          //                                                   .textMultiplier,
          //                                         ),
          //                                       ),
          //                                     ),
          //                               Row(
          //                                 children: [
          //                                   user.roleName == "Broker"
          //                                       ? Container(
          //                                           child: Expanded(
          //                                               flex: 3,
          //                                               child: Container()),
          //                                         )
          //                                       : Expanded(
          //                                           flex: 3,
          //                                           child: Container(
          //                                             height: 8 *
          //                                                 SizeConfig
          //                                                     .heightMultiplier,
          //                                             width: 60 *
          //                                                 SizeConfig
          //                                                     .widthMultiplier,
          //                                             child: ListView.builder(
          //                                                 padding:
          //                                                     EdgeInsets.zero,
          //                                                 itemCount:
          //                                                     details[indexs]
          //                                                         .subcategories
          //                                                         .length,
          //                                                 itemBuilder:
          //                                                     (BuildContext
          //                                                             context,
          //                                                         int index) {
          //                                                   return Column(
          //                                                     crossAxisAlignment:
          //                                                         CrossAxisAlignment
          //                                                             .start,
          //                                                     children: [
          //                                                       Text(
          //                                                         "${details[indexs].subcategories[index].title}",
          //                                                         style:
          //                                                             TextStyle(
          //                                                           fontFamily:
          //                                                               fonts
          //                                                                   .fontfaimlyregular,
          //                                                           fontSize: 1.8 *
          //                                                               SizeConfig
          //                                                                   .textMultiplier,
          //                                                           color: Color(
          //                                                               0xff707070),
          //                                                         ),
          //                                                       ),
          //                                                     ],
          //                                                   );
          //                                                 }),
          //                                           ),
          //                                         ),
          //                                   Expanded(
          //                                     flex: 2,
          //                                     child: filledButton(
          //                                       txt: "RENEWAL",
          //                                       fontsize:
          //                                           size.convert(context, 13),
          //                                       h: size.convert(context, 38),
          //                                     ),
          //                                   ),
          //                                 ],
          //                               ),
          //                             ],
          //                           ),
          //                         ),
          //                       );
          //                     },
          //                   )
          //             : Container(
          //                 child: Center(
          //                   child: CircularProgressIndicator(),
          //                 ),
          //               );
          //       },
          //     ),
          //   ),
          // ),
        ],
      );
    }

    return OrientationBuilder(
      builder: (BuildContext context, orientation) {
        return user.roleName == "Broker"
            ? Scaffold(
                backgroundColor: Colors.white,
                body: Column(
                  children: [
                    Container(
                      color: Colors.white,
                      height: SizeConfig.isPortrait
                          ? 38 * SizeConfig.heightMultiplier
                          : 20 * SizeConfig.heightMultiplier,
                      child: SizeConfig.isPortrait
                          ? profilecontainerportratit()
                          : profileContainerlandscape(),
                    ),
                    SizedBox(
                      height: 4 * SizeConfig.heightMultiplier,
                      child: Text(
                        "Broker Membership",
                        style: TextStyle(
                          color: Color(0xff394C81),
                          fontFamily: fonts.fontfaimly,
                          fontSize: 2.8 * SizeConfig.textMultiplier,
                        ),
                      ),
                    ),
                    Expanded(
                      child: Container(
                        child: membership(orientation),
                      ),
                    ),
                  ],
                ),
              )
            : DefaultTabController(
                length: 2,
                child: Scaffold(
                  backgroundColor: Colors.white,
                  body: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Container(
                        decoration: BoxDecoration(
                          // color: Colors.red,
                          image: DecorationImage(
                            fit: BoxFit.cover,
                            image: AssetImage("assets/images/newback.jpg"),
                          ),
                        ),
                        height: SizeConfig.isPortrait
                            ? 38 * SizeConfig.heightMultiplier
                            : 20 * SizeConfig.heightMultiplier,
                        child: SizeConfig.isPortrait
                            ? profilecontainerportratit()
                            : profileContainerlandscape(),
                      ),
                      user.roleName == "Broker"
                          ? Container()
                          : Container(
                              height: 8 * SizeConfig.heightMultiplier,
                              child: TabBar(
                                unselectedLabelStyle: TextStyle(
                                  fontWeight: FontWeight.normal,
                                ),
                                unselectedLabelColor: Colors.black87,
                                labelColor: Colors.black,
                                onTap: (index) {
                                  setState(() {
                                    index = index;
                                  });
                                  print(index);
                                },
                                indicatorColor: Color(0xff7595f0),
                                indicatorWeight: 4.0,
                                tabs: [
                                  Tab(
                                    child: Text(
                                      "My Product(s)",
                                      style: TextStyle(
                                        fontFamily: indexs == 0
                                            ? fonts.fontfaimly
                                            : fonts.fontfaimlyregular,
                                      ),
                                    ),
                                  ),
                                  Tab(
                                    child: Text(
                                      "My Membership(s)",
                                      style: TextStyle(
                                        fontFamily: indexs == 1
                                            ? fonts.fontfaimly
                                            : fonts.fontfaimlyregular,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                      Expanded(
                        child: Container(
                          child: TabBarView(children: [
                            Column(
                              children: [
                                Container(
                                  decoration: BoxDecoration(
                                    // color: Colors.red,
                                    image: DecorationImage(
                                      fit: BoxFit.cover,
                                      image: AssetImage(
                                          "assets/images/newback.jpg"),
                                    ),
                                  ),
                                  alignment: Alignment.centerLeft,
                                  width: double.infinity,
                                  height: 6 * SizeConfig.heightMultiplier,
                                  child: Padding(
                                    padding: EdgeInsets.only(
                                        left: 2.0 * SizeConfig.widthMultiplier,
                                        right:
                                            2.0 * SizeConfig.widthMultiplier),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Text("All my products listing",
                                            style: TextStyle(
                                              fontFamily: fonts.fontfaimly,
                                            )),
                                        InkWell(
                                          onTap: () => Navigator.of(context)
                                              .push(MaterialPageRoute(
                                                  builder: (_) =>
                                                      AddProduct('profile'))),
                                          child: Container(
                                            decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(10.0),
                                              color: Color(0xfffaf9fe),
                                            ),
                                            child: Padding(
                                              padding: const EdgeInsets.only(
                                                  left: 10.0,
                                                  right: 10.0,
                                                  top: 4.0,
                                                  bottom: 4.0),
                                              child: Row(
                                                children: [
                                                  Icon(
                                                    Icons.add_circle_outline,
                                                    size: 16,
                                                    color: Color(0xff394c81),
                                                  ),
                                                  SizedBox(
                                                    width: 1.0,
                                                  ),
                                                  Text(
                                                    "Add product",
                                                    style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      fontFamily:
                                                          fonts.fontfaimly,
                                                      color: Color(0xff394c81),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                Expanded(
                                    child: Container(
                                  decoration: BoxDecoration(
                                    color: Colors.white,
                                    image: DecorationImage(
                                      fit: BoxFit.cover,
                                      image: AssetImage(
                                          "assets/images/newback.jpg"),
                                    ),
                                  ),
                                  child: isloading == true
                                      ? Center(
                                          child: CircularProgressIndicator(),
                                        )
                                      : ListView.builder(
                                          physics: BouncingScrollPhysics(),
                                          padding: EdgeInsets.zero,
                                          itemCount: productList.length,
                                          itemBuilder: (BuildContext context,
                                              int index) {
                                            return Column(
                                              children: <Widget>[
                                                buildList(context, index),
                                              ],
                                            );
                                          }),
                                )),
                              ],
                            ),
                            membership(orientation),
                          ]),
                        ),
                      ),
                    ],
                  ),
                ),
              );
      },
    );
  }
}
