import 'dart:io';

import 'package:biznes/Controllers/backendservicescontroller.dart';
import 'package:biznes/helper/categoryInboxHelper.dart';
import 'package:biznes/helper/counthelper.dart';
import 'package:biznes/helper/login_helper.dart';
import 'package:biznes/model/caegoryModel.dart';
import 'package:biznes/model/citiesModel.dart';
import 'package:biznes/model/countriesModel.dart';
import 'package:biznes/products_Detail/extra.dart';
import 'package:biznes/repeatedWigets/filledButton.dart';
import 'package:image_picker/image_picker.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:biznes/model/roleModel.dart';
import 'package:biznes/product_UI/bottomBar_UI/DropDownWidget.dart';
import 'package:biznes/memberShip/Membership.dart';
import 'package:biznes/helper/allmessageshelper.dart';
import 'package:biznes/model/send_to_all_message_model.dart';
import 'package:biznes/res/color.dart' as font;
import 'package:biznes/model/subCategoryModel.dart';
import 'package:biznes/res/style.dart';
import 'package:biznes/services/register_logic.dart';
import 'package:biznes/setting/notification.dart';
import 'package:biznes/size_config.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:page_transition/page_transition.dart';
import 'package:provider/provider.dart';
import 'package:biznes/model/guesttobrokermodel.dart';
import 'package:biznes/res/size.dart';
import 'package:toast/toast.dart';

class WriteMessage extends StatefulWidget {
  const WriteMessage({Key key}) : super(key: key);

  @override
  _WriteMessageState createState() => _WriteMessageState();
}

class _WriteMessageState extends State<WriteMessage>
    with SingleTickerProviderStateMixin {
  List<CategoriesModel> categoriesforguest = [];
  List<SubCategories> subCategoryforguest = [];
  List<CountriesModel> countryListforguest = [];
  List<CitiesModel> citiesListforguest = [];

  String selectedCategoryforguest;
  String selectedSubCategoryforguest;
  String selectedCountryforguest;
  String selectedCityforguest;
  int productcount = 0;
  int countforbroker = 0;
  File _image;
  GuestToBrokermodel guestToBrokermodel;
  TextEditingController messageController;
  List<CategoriesModel> categories = [];
  List<SubCategories> subCategory = [];
  List<CountriesModel> countryList = [];
  List<CitiesModel> citiesList = [];
  List<RoleModel> rolesModelList = [];
  String selectedRole;
  bool textfieldenabled = false;
  String selectedCategory;
  String selectedSubCategory;
  String selectedCountry;
  String selectedCity;
  String currentUserId;

  bool loading = true;
  SendMessageToAllMessageModel sendMessageToAllMessageModel;

  checking() async {
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    Navigator.of(context).push(MaterialPageRoute(
        builder: (context) => Membership(
              page: "writemessage",
              userid: _prefs.getString("userid"),
            )));
  }

  showdialogfornosubscribe() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(25)),
            content: SingleChildScrollView(
              child: Container(
                // height: 35 * SizeConfig.heightMultiplier,
                width: double.infinity,
                alignment: Alignment.center,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(
                      height: 5,
                    ),
                    SvgPicture.asset("assets/images/NewMembership.svg"),
                    Text(
                      "Get Subscription",
                      style: TextStyle(
                          fontFamily: font.fontfaimly,
                          fontSize: (24 / 8.148314082864863) *
                              SizeConfig.heightMultiplier,
                          fontWeight: FontWeight.w400),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 10),
                      child: Text(
                        "Get membership to unblock chat and deal with your vendors for relevent category only",
                        textAlign: TextAlign.center,
                        maxLines: 2,
                        style: TextStyle(
                          fontFamily: font.fontfaimlyregular,
                          fontSize: 14,
                          color: Colors.blueGrey,
                        ),
                      ),
                    ),
                    SizedBox(
                      height: (10 / 8.148314082864863) *
                          SizeConfig.heightMultiplier,
                    ),
                    filledButton(
                      txt: "GET FREE TRAIL",
                      onTap: () {
                        Navigator.of(context).pop();
                        checking();
                        setState(() {
                          sendMessageToAllMessageModel.categoryId = "";
                          sendMessageToAllMessageModel.subCategoryId = "";
                          sendMessageToAllMessageModel.countryId = "";
                          sendMessageToAllMessageModel.cityId = "";
                          selectedCategory = null;
                          selectedSubCategory = null;
                          selectedCountry = null;
                          selectedCity = null;
                          textfieldenabled = false;
                        });
                      },
                    ),
                    SizedBox(
                      height:
                          (2 / 8.148314082864863) * SizeConfig.heightMultiplier,
                    ),
                  ],
                ),
              ),
            ),
          );
        });
  }

  Widget buildForm(String token, orientation, backgroundcolor, String userid,
      Function onpressed, Icon icon) {
    Color color = Colors.grey;
    Color color2 = Colors.blue;
    return ListView(
      physics: BouncingScrollPhysics(),
      padding: EdgeInsets.zero,
      children: [
        Padding(
          padding: const EdgeInsets.all(10.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(left: 16.2, right: 16.2),
                child: Form(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      DropDownWidget(
                          context: context,
                          categories: categories,
                          selectedCategory: selectedCategory,
                          hintext: "Category",
                          value: selectedCategory,
                          image: "assets/icons/Category.svg",
                          onChanged: (value) {
                            setState(() {
                              if (selectedSubCategory != null) {
                                selectedSubCategory = null;
                              }
                              subCategory = [];
                            });
                            categories.forEach((f) async {
                              if (f.title == value) {
                                getSubCategories(
                                  catId: f.sId,
                                ).then((list) {
                                  // regModel;
                                  setState(() {
                                    sendMessageToAllMessageModel.categoryId =
                                        f.sId;
                                    subCategory = list;
                                    sendMessageToAllMessageModel.subCategoryId =
                                        "";
                                    sendMessageToAllMessageModel.countryId = "";
                                    sendMessageToAllMessageModel.cityId = "";
                                    selectedCountry = null;
                                    selectedCity = null;
                                    selectedCategory = value;
                                    textfieldenabled = false;
                                    //productModel.category = f.sId;
                                  });
                                });
                              }
                            });
                          }),
                      SizedBox(
                        height: (10 / 8.148314082864863) *
                            SizeConfig.heightMultiplier,
                      ),
                      DropDownWidget(
                          image: "assets/icons/subCategory.svg",
                          selectedCategory: selectedSubCategory,
                          context: context,
                          categories: subCategory,
                          hintext: "Sub-Category",
                          value: selectedSubCategory,
                          onChanged: (value) {
                            setState(() {
                              selectedSubCategory = value;
                            });
                            subCategory.forEach((f) {
                              if (f.title == value) {
                                sendMessageToAllMessageModel.subCategoryId =
                                    f.sId;
                                //searchModel.subcategory = f.sId;
                                // widget.regModel.
                              }
                            });
                          }),
                      Box(10),
                      DropDownWidget(
                          context: context,
                          hintext: "Country",
                          color: color,
                          value: selectedCountry,
                          selectedCategory: selectedCountry,
                          categories: countryList,
                          color2: color2,
                          image: selectedCountry != null
                              ? 'assets/images/flag2.png'
                              : 'assets/images/countryicon.svg',
                          onChanged: (value) {
                            setState(() {
                              if (selectedCity != null) {
                                selectedCity = null;
                              }
                              citiesList = [];
                            });
                            countryList.forEach((f) async {
                              if (f.title == value) {
                                sendMessageToAllMessageModel.countryId = f.sId;
                                // searchModel.country = f.sId;
                                getCities(f.sId).then((list) {
                                  // regModel;
                                  setState(() {
                                    citiesList = list;
                                    selectedCountry = value;
                                    //countriesModel.sId=f.sId;
                                  });
                                });
                              }
                            });
                          }),
                      Box(10),
                      DropDownWidget(
                        color2: color2,
                        color: color,
                        context: context,
                        image: 'assets/images/city.svg',
                        selectedCategory: selectedCity,
                        categories: citiesList,
                        hintext: "City",
                        onChanged: (value) {
                          setState(() {
                            selectedCity = value;
                          });
                          citiesList.forEach((f) async {
                            if (f.title == value) {
                              sendMessageToAllMessageModel.cityId = f.sId;

                              checkifuserhasmembershipornot(
                                      sendMessageToAllMessageModel,
                                      userid,
                                      context)
                                  .then((value) {
                                if (value["success"] == false &&
                                    value["message"] ==
                                        "There are no companies in the current Category please select another one") {
                                  Toast.show(value["message"], context,
                                      gravity: Toast.TOP);
                                } else if (value["success"] == false) {
                                  showdialogfornosubscribe();
                                } else {
                                  setState(() {
                                    textfieldenabled = true;
                                  });
                                }
                              });
                            }
                          });
                        },
                        value: selectedCity,
                      ),
                      Box(10),
                      DropDownWidget(
                        value: selectedRole,
                        image: "assets/images/roles.svg",
                        color: color,
                        color2: color2,
                        selectedCategory: selectedRole,
                        onChanged: (value) {
                          setState(() {
                            rolesModelList.forEach((f) async {
                              if (f.title == value) {
                                setState(() {
                                  getproductcount(sendMessageToAllMessageModel,
                                          userid, context)
                                      .then((value) {
                                    print("this is printing");
                                    setState(() {
                                      productcount = value['productss'];
                                      print(productcount);
                                    });
                                  });
                                  sendMessageToAllMessageModel.userRole = f.id;
                                  selectedRole = value;
                                });
                              }
                            });
                          });
                        },
                        context: context,
                        hintext: "Role",
                        categories: rolesModelList,
                      ),
                      SizedBox(
                        height: (5 / 8.148314082864863) *
                            SizeConfig.heightMultiplier,
                      ),
                      Container(
                          child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          Text(
                            "${productcount ?? "0"} number of people exists in this result",
                            style: TextStyle(
                              fontFamily: font.fontfaimly,
                              fontSize: 1.5 * SizeConfig.textMultiplier,
                              color: Colors.red,
                            ),
                          ),
                        ],
                      )),
                      SizedBox(
                        height: 5,
                      ),
                      _image == null
                          ? Container()
                          : Row(
                              children: [
                                Container(
                                  height: 80,
                                  width: 80,
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(15),
                                    child: Image.file(
                                      _image,
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  width: 5,
                                ),
                                IconButton(
                                    icon: Icon(
                                      Icons.delete,
                                      color: Colors.red,
                                    ),
                                    onPressed: () {
                                      setState(() {
                                        _image = null;
                                      });
                                    }),
                                // filledButton(
                                //   w: size.convertWidth(context, 150),
                                //   h: size.convert(context, 40),
                                //   txt: 'Delete',
                                //   onTap: () {
                                //     setState(() {
                                //       _image = null;
                                //     });
                                //   },
                                // ),
                              ],
                            ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          SizedBox(
                            height: (10 / 8.148314082864863) *
                                SizeConfig.heightMultiplier,
                          ),
                          Text(
                            "Write Your Message",
                            style: TextStyle(
                                fontSize: (16 / 8.148314082864863) *
                                    SizeConfig.heightMultiplier,
                                fontFamily: font.fontfaimly,
                                color: Colors.black),
                          ),
                          Padding(
                            padding: EdgeInsets.only(top: 10.0),
                            child: Center(
                              child: Stack(
                                children: [
                                  Container(
                                    width: double.infinity,
                                    height: (270 / 8.148314082864863) *
                                        SizeConfig.heightMultiplier,
                                    margin: EdgeInsets.only(bottom: 12),
                                    padding: EdgeInsets.only(
                                        left: 8.0, right: 8.0, bottom: 10.0),
                                    decoration: BoxDecoration(
                                        color: Color(0xffEbf3fa),
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(25.0))),
                                    child: TextFormField(
                                      enabled: textfieldenabled,
                                      autofocus: false,
                                      onChanged: (value) {
                                        sendMessageToAllMessageModel.message =
                                            value;
                                      },
                                      validator: (value) {
                                        if (value.isEmpty && value.length < 6) {
                                          return 'required field';
                                        }
                                        return null;
                                      },
                                      controller: messageController,
                                      maxLines: 10,
                                      decoration: InputDecoration(
                                        hintText: "Type your message here....",
                                        hintStyle: style.MontserratRegular(
                                            fontSize:
                                                size.convert(context, 12)),
                                        border: InputBorder.none,
                                      ),
                                    ),
                                  ),
                                  _image == null
                                      ? Positioned(
                                          bottom: 0,
                                          left: 15,
                                          child: Row(
                                            children: [
                                              GestureDetector(
                                                onTap: () {
                                                  _showPicker(context);
                                                },
                                                child: Container(
                                                  decoration: BoxDecoration(
                                                    color: font.tabbarcolor,
                                                    shape: BoxShape.circle,
                                                  ),
                                                  child: Icon(
                                                    Icons.upload_sharp,
                                                    color: Colors.white,
                                                  ),
                                                  height:
                                                      size.convert(context, 40),
                                                  width:
                                                      size.convert(context, 40),
                                                ),
                                              ),
                                              SizedBox(
                                                width: 10,
                                              ),
                                              Text(
                                                'Upload Picture',
                                                style: style.MontserratMedium(
                                                    fontSize: size.convert(
                                                        context, 12)),
                                              ),
                                            ],
                                          ),
                                        )
                                      : Container(),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: [
                                      Padding(
                                        padding: EdgeInsets.only(
                                          top: orientation ==
                                                  Orientation.portrait
                                              ? 26 * SizeConfig.heightMultiplier
                                              : 41 * SizeConfig.widthMultiplier,
                                        ),
                                        child: CircleAvatar(
                                          backgroundColor: backgroundcolor,
                                          radius: 26,
                                          child: IconButton(
                                            padding: EdgeInsets.zero,
                                            icon: icon,
                                            // Icon(
                                            //   Icons.send,
                                            //   color: color,
                                            // ),
                                            color: Colors.white,
                                            onPressed: onpressed,
                                            // () {

                                            // },
                                          ),
                                        ),
                                      ),
                                    ],
                                  )
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  _imgFromCamera() async {
    File image = await ImagePicker.pickImage(
        source: ImageSource.camera, imageQuality: 50);
    setState(() {
      _image = image;
      sendMessageToAllMessageModel.image = image.path;
    });
  }

  _imgFromGallery() async {
    File image = await ImagePicker.pickImage(
        source: ImageSource.gallery, imageQuality: 50);
    setState(() {
      _image = image;
      sendMessageToAllMessageModel.image = image.path;
    });
  }

  void _showPicker(context) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return SafeArea(
            child: Container(
              child: new Wrap(
                children: <Widget>[
                  new ListTile(
                      leading: new Icon(Icons.photo_library),
                      title: new Text('Photo Library'),
                      onTap: () {
                        _imgFromGallery();
                        Navigator.of(context).pop();
                      }),
                  new ListTile(
                    leading: new Icon(Icons.photo_camera),
                    title: new Text('Camera'),
                    onTap: () {
                      _imgFromCamera();
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              ),
            ),
          );
        });
  }

  Widget buildFormforguest(String token, orientation, backgroundcolor,
      Function onpressed, Icon icon) {
    Color color = Colors.grey;
    Color color2 = Colors.blue;
    return ListView(
      physics: BouncingScrollPhysics(),
      children: [
        Padding(
          padding: const EdgeInsets.all(10.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(left: 16.2, right: 16.2),
                child: Form(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      DropDownWidget(
                        onChanged: (value) {
                          setState(() {
                            if (selectedSubCategoryforguest != null) {
                              selectedSubCategoryforguest = null;
                            }
                            subCategoryforguest = [];
                          });
                          categoriesforguest.forEach((f) async {
                            if (f.title == value) {
                              getSubCategories(catId: f.sId).then((list) {
                                // regModel;
                                setState(() {
                                  guestToBrokermodel.category = f.sId;
                                  subCategoryforguest = list;
                                  selectedCategoryforguest = value;
                                  //productModel.category = f.sId;
                                });
                              });
                            }
                          });
                        },
                        value: selectedCategoryforguest,
                        hintext: "Category",
                        context: context,
                        image: "assets/icons/Category.svg",
                        categories: categories,
                        selectedCategory: selectedCategoryforguest,
                      ),
                      Box(10),
                      DropDownWidget(
                        onChanged: (value) {
                          setState(() {
                            selectedSubCategoryforguest = value;
                          });
                          subCategoryforguest.forEach((f) {
                            if (f.title == value) {
                              guestToBrokermodel.subcategory = f.sId;
                              //searchModel.subcategory = f.sId;
                              // widget.regModel.
                            }
                          });
                        },
                        value: selectedSubCategoryforguest,
                        hintext: "Sub-Category",
                        context: context,
                        image: "assets/icons/subCategory.svg",
                        categories: subCategoryforguest,
                        selectedCategory: selectedSubCategoryforguest,
                      ),
                      Box(10),
                      DropDownWidget(
                        onChanged: (value) {
                          setState(() {
                            if (selectedCityforguest != null) {
                              selectedCityforguest = null;
                            }
                            citiesListforguest = [];
                          });
                          countryListforguest.forEach((f) async {
                            if (f.title == value) {
                              guestToBrokermodel.country = f.sId;
                              // searchModel.country = f.sId;
                              getCities(f.sId).then((list) {
                                // regModel;
                                setState(() {
                                  citiesListforguest = list;
                                  selectedCountryforguest = value;
                                  //countriesModel.sId=f.sId;
                                });
                              });
                            }
                          });
                        },
                        value: selectedCountryforguest,
                        hintext: "Country",
                        context: context,
                        image: selectedCountry != null
                            ? 'assets/images/flag2.png'
                            : 'assets/images/countryicon.svg',
                        categories: countryListforguest,
                        selectedCategory: selectedCountryforguest,
                      ),
                      Box(10),
                      DropDownWidget(
                        onChanged: (value) {
                          setState(() {
                            selectedCityforguest = value;
                          });
                          citiesListforguest.forEach((f) async {
                            if (f.title == value) {
                              brokercount(guestToBrokermodel, context)
                                  .then((value) {
                                setState(() {
                                  countforbroker = value['brokers'];
                                });
                              });
                              setState(() {
                                guestToBrokermodel.city = f.sId;
                                print(guestToBrokermodel.city);
                              });
                            }
                          });
                        },
                        value: selectedCityforguest,
                        hintext: "City",
                        context: context,
                        image: "assets/images/city.svg",
                        categories: citiesListforguest,
                        selectedCategory: selectedCityforguest,
                      ),
                      Box(10),
                      Container(
                          child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          countforbroker == null
                              ? Text(
                                  "0 number of people exists in this result",
                                  style: TextStyle(
                                    fontFamily: font.fontfaimly,
                                    fontSize: 1.5 * SizeConfig.textMultiplier,
                                    color: Colors.red,
                                  ),
                                )
                              : Text(
                                  "$countforbroker number of people exists in this result",
                                  style: TextStyle(
                                    fontFamily: font.fontfaimly,
                                    fontSize: 1.5 * SizeConfig.textMultiplier,
                                    color: Colors.red,
                                  ),
                                ),
                        ],
                      )),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          SizedBox(
                            height: (10 / 8.148314082864863) *
                                SizeConfig.heightMultiplier,
                          ),
                          Text(
                            "Write Your Message",
                            style: TextStyle(
                                fontFamily: font.fontfaimly,
                                fontSize: (16 / 8.148314082864863) *
                                    SizeConfig.heightMultiplier,
                                color: Colors.black),
                          ),
                          Padding(
                            padding: EdgeInsets.only(top: 10.0),
                            child: Center(
                              child: Stack(
                                children: [
                                  Container(
                                    width: double.infinity,
                                    height: (270 / 8.148314082864863) *
                                        SizeConfig.heightMultiplier,
                                    padding: EdgeInsets.only(
                                        left: 8.0, right: 8.0, bottom: 10.0),
                                    decoration: BoxDecoration(
                                        color: Color(0xffEbf3fa),
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(25.0))),
                                    child: TextFormField(
                                      onChanged: (value) {
                                        guestToBrokermodel.message = value;
                                      },
                                      validator: (value) {
                                        if (value.isEmpty && value.length < 6) {
                                          return 'required field';
                                        }
                                        return null;
                                      },
                                      controller: messageController,
                                      maxLines: 10,
                                      decoration: InputDecoration(
                                        hintText:
                                            "You can send message to broker only for (Free)",
                                        hintStyle: TextStyle(
                                          fontFamily: font.fontfaimlyregular,
                                          fontSize: (14 / 8.148314082864863) *
                                              SizeConfig.heightMultiplier,
                                        ),
                                        border: InputBorder.none,
                                      ),
                                    ),
                                  ),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: [
                                      Padding(
                                        padding: EdgeInsets.only(
                                          top: orientation ==
                                                  Orientation.portrait
                                              ? 26 * SizeConfig.heightMultiplier
                                              : 41 * SizeConfig.widthMultiplier,
                                        ),
                                        child: CircleAvatar(
                                          backgroundColor: backgroundcolor,
                                          radius: 26,
                                          child: IconButton(
                                            padding: EdgeInsets.zero,
                                            icon: icon,
                                            // Icon(
                                            //   Icons.send,
                                            //   color: color,
                                            // ),
                                            color: Colors.white,
                                            onPressed: onpressed,
                                            // () {

                                            // },
                                          ),
                                        ),
                                      ),
                                    ],
                                  )
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  TabController _tabController;
  bool isPlaying = false;

  Animation animation;

  AnimationController controller;

  @override
  void initState() {
    getRoles().then((value) {
      setState(() {
        rolesModelList = value;
      });
    });
    controller = AnimationController(
        duration: const Duration(milliseconds: 500), vsync: this);
    guestToBrokermodel = GuestToBrokermodel(
      category: '',
      subcategory: '',
      country: '',
      city: '',
      message: '',
    );
    sendMessageToAllMessageModel = SendMessageToAllMessageModel(
      cityId: '',
      countryId: '',
      subCategoryId: '',
      categoryId: '',
      userId: '',
      message: '',
      userRole: '',
    );
    super.initState();
    // _tabController = new TabController(length: 2, vsync: this);

    getCategories().then((_list) {
      setState(() {
        categoriesforguest = _list;
        categories = _list;
        loading = false;
      });
    });
    getCountries().then((countries) {
      setState(() {
        countryListforguest = countries;
        countryList = countries;
        loading = false;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    currentUserId = Provider.of<LoginHelper>(context).user.sId;
    var rolname = Provider.of<LoginHelper>(context).user;
    var token = Provider.of<LoginHelper>(context).token;
    var counters = Provider.of<CountHelper>(context).counter;
    var counterset = Provider.of<CountHelper>(context);
    onpressed1() {
      sendMessageToAllMessageModel.userId = currentUserId;
      if (sendMessageToAllMessageModel.categoryId == null) {
        Toast.show("Category is missing", context,
            duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
      } else {
        sendToAllMessage(sendMessageToAllMessageModel, currentUserId, context)
            .then((value) {
          print(value);
          if (value == true) {
            setState(() {
              sendMessageToAllMessageModel.countryId = null;
              sendMessageToAllMessageModel.subCategoryId = null;
              sendMessageToAllMessageModel.categoryId = null;
              sendMessageToAllMessageModel.message = null;
              sendMessageToAllMessageModel.image = null;
              sendMessageToAllMessageModel.userRole = null;
              selectedCountry = null;
              selectedCity = null;
              selectedCategory = null;
              selectedSubCategory = null;
              messageController.clear();
              selectedRole = null;
              textfieldenabled = false;
              _image = null;
            });
            // getCategoriesForInbox(currentUserId,context).then((value) {
            //   Provider.of<CategoryHelper>(context, listen: false)
            //       .setCategoryInbox(value);
            // });
          }
        });
      }
    }

    onpressed2() {
      messagetoguest(guestToBrokermodel, context).then((value) {
        if (value == true) {
          print("message sent");
        }
      });
    }

    Icon icon1() {
      return Icon(Icons.send, color: Color(0xff7595f0));
    }

    Icon icon2() {
      return Icon(Icons.send, color: Colors.white);
    }

    return DefaultTabController(
      length: 2,
      child: GestureDetector(
        onTap: () => FocusScope.of(context).requestFocus(new FocusNode()),
        child: Scaffold(
          backgroundColor: Colors.white,
          body: OrientationBuilder(builder: (context, orientation) {
            return rolname.roleName == 'Broker'
                ? Column(
                    children: [
                      CustomAppBar(
                          isShowBackground: true,
                          context: context,
                          text: "Send Message To All",
                          length: counters.toString(),
                          onTap: () {
                            counterset.zerocunter();
                            Navigator.of(context).push(PageTransition(
                                child: MyNotification(),
                                duration: Duration(milliseconds: 700),
                                type: PageTransitionType.leftToRightWithFade));
                          }),
                      Expanded(
                        child: Container(
                          child: buildForm(token, orientation, Colors.white,
                              currentUserId, onpressed1, icon1()),
                        ),
                      ),
                    ],
                  )
                : Column(
                    children: [
                      CustomAppBar(
                          isShowBackground: true,
                          context: context,
                          text: "Send Message To All",
                          length: counters.toString(),
                          onTap: () {
                            counterset.zerocunter();
                            Navigator.of(context).push(PageTransition(
                                child: MyNotification(),
                                duration: Duration(milliseconds: 700),
                                type: PageTransitionType.leftToRightWithFade));
                          }),
                      Container(
                        decoration: BoxDecoration(
                          color: Colors.white,
                          image: DecorationImage(
                            fit: BoxFit.cover,
                            image: AssetImage("assets/images/newback.jpg"),
                          ),
                        ),
                        child: rolname.roleName != 'Broker'
                            ? TabBar(
                                onTap: (s) {
                                  FocusScope.of(context)
                                      .requestFocus(new FocusNode());
                                },
                                unselectedLabelColor: Colors.black87,
                                labelColor: Color(0xff7595f0),
                                indicatorColor: Color(0xff7595f0),
                                indicatorSize: TabBarIndicatorSize.label,
                                tabs: [
                                  Tab(
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        Icon(
                                          // progress: controller,
                                          // icon: AnimatedIcons.play_pause,
                                          Icons.check_circle_outline,
                                          size: size.convert(context, 17),
                                          color: Color(0xff7595f0),
                                        ),
                                        SizedBox(
                                          width: (10 / 4.853932272197492) *
                                              SizeConfig.widthMultiplier,
                                        ),
                                        Text(
                                          "Write Message",
                                          style: TextStyle(
                                            fontSize: size.convert(context, 12),
                                            fontFamily: font.fontfaimly,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Tab(
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        Icon(
                                          Icons.check_circle,
                                          size: size.convert(context, 17),
                                          color: Color(0xff7595f0),
                                        ),
                                        SizedBox(
                                          width: (10 / 4.853932272197492) *
                                              SizeConfig.widthMultiplier,
                                        ),
                                        Text(
                                          "Message to Broker",
                                          style: TextStyle(
                                            fontFamily: font.fontfaimly,
                                            fontSize: size.convert(context, 10),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                                controller: _tabController,
                              )
                            : null,
                        height: size.convert(context, 40),
                      ),
                      Expanded(
                        child: Container(
                          decoration: BoxDecoration(
                            color: Colors.white,
                            image: DecorationImage(
                              fit: BoxFit.cover,
                              image: AssetImage("assets/images/newback.jpg"),
                            ),
                          ),
                          child: TabBarView(
                            children: [
                              buildForm(token, orientation, Colors.white,
                                  currentUserId, onpressed1, icon1()),
                              buildFormforguest(token, orientation,
                                  Color(0xff7595f0), onpressed2, icon2()),
                            ],
                          ),
                        ),
                      ),
                    ],
                  );
          }),
        ),
      ),
    );
  }
}
