import 'dart:convert';
import 'dart:io';
import 'package:biznes/Toasts.dart/toasts.dart';
import 'package:biznes/repeatedWigets/filledButton.dart';
import 'package:biznes/res/color.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:toast/toast.dart';
import 'package:biznes/products_Detail/extra.dart';
import 'package:biznes/helper/counthelper.dart';
import 'package:biznes/helper/gettingproducts.dart';
import 'package:biznes/helper/login_helper.dart';
import 'package:biznes/model/caegoryModel.dart';
import 'package:biznes/res/size.dart';
import 'package:biznes/model/productsbyid.dart';
import 'package:biznes/model/subCategoryModel.dart';
import 'package:biznes/services/server.dart';
import 'package:biznes/services/register_logic.dart';
import 'package:biznes/res/style.dart';
import 'package:biznes/res/color.dart' as fontandcolor;
import 'package:biznes/setting/notification.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:multi_image_picker/multi_image_picker.dart';
import 'package:page_transition/page_transition.dart';
import 'package:path_provider/path_provider.dart';
import 'package:provider/provider.dart';
import '../../size_config.dart';

class EditProduct extends StatefulWidget {
  final List<Products> products;
  final int index;

  EditProduct(this.index, this.products);

  @override
  _EditProductState createState() => _EditProductState();
}

class _EditProductState extends State<EditProduct> {
  String categoryid;
  String subCategoryid;
  TextEditingController _titleController = TextEditingController();
  TextEditingController _descriptioncontroller = TextEditingController();
  TextEditingController _pricecontroller = TextEditingController();
  TextEditingController _controllerquantity = TextEditingController();
  TextEditingController _moqcontroller = TextEditingController();
  TextEditingController _organizationcontroller = TextEditingController();
  List<CategoriesModel> categories = [];
  List<SubCategories> subCategory = [];
  String errorMessage = 'No Error';
  List<Asset> images;
  List<Asset> resultList = List<Asset>();
  var formKey = GlobalKey<FormState>();
  List<File> imagesFiles = [];
  int maxImageNo = 10;
  bool loading = true;
  bool isUploading = false;
  bool isemptydescription = false;
  bool isdescriptionlength = false;
  String selectedCategory;
  String selectedSubCategory;
  bool selectSingleImage = false;

  productPicture() async {
    setState(() {
      images = [];
      errorMessage = 'No Error';
    });
    String error;
    try {
      resultList = await MultiImagePicker.pickImages(
        maxImages: maxImageNo,
        enableCamera: true,
        selectedAssets: images,
        cupertinoOptions: CupertinoOptions(takePhotoIcon: "chat"),
        materialOptions: MaterialOptions(
          actionBarColor: "#abcdef",
          actionBarTitle: "Example App",
          allViewTitle: "All Photos",
          useDetailsView: false,
          selectCircleStrokeColor: "#000000",
        ),
      );
      images = resultList;
      getImageFileFromAssets(resultList);
      setState(() {});
    } on PlatformException catch (e) {
      error = e.message;
      print(error);
    }

    if (!mounted) return;
  }

  @override
  void initState() {
    getCategories().then((_list) {
      setState(() {
        _titleController.text = widget.products[widget.index].title;
        _descriptioncontroller.text = widget.products[widget.index].description;
        _pricecontroller.text = widget.products[widget.index].price.toString();
        _controllerquantity.text =
            widget.products[widget.index].quantity.toString();
        _moqcontroller.text = widget.products[widget.index].moq.toString();
        _organizationcontroller.text =
            widget.products[widget.index].organizationName.toString();
        selectedCategory = widget.products[widget.index].category.title;
        categoryid = widget.products[widget.index].category.sId;
        categories = _list;

        getSubCategories(catId: widget.products[widget.index].category.sId)
            .then((list) {
          setState(() {
            subCategory = list;
            selectedSubCategory =
                widget.products[widget.index].subCategory.title;
            subCategoryid = widget.products[widget.index].subCategory.sId;
          });
        });
        loading = false;
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var user = Provider.of<LoginHelper>(context).user;
    var counters = Provider.of<CountHelper>(context).counter;
    var counterset = Provider.of<CountHelper>(context);
    var productList = Provider.of<GettingProducts>(context).products;
    return Stack(
      children: [
        WillPopScope(
          onWillPop: () async {
            if (productList[widget.index].images.length == 0 &&
                imagesFiles.length == 0) {
              return showToast(context, "Please Select at least one image");
            } else if (productList[widget.index].images.length == 0 &&
                imagesFiles.length != 0) {
              return showToast(context, "Please Upload the selected images");
            } else {
              return Future.value(true);
            }
          },
          child: Scaffold(
            backgroundColor: Colors.white,
            body: Column(
              children: [
                CustomAppBar(
                    icon: Icon(Icons.arrow_back),
                    IconPressed: () {
                      if (productList[widget.index].images.length == 0 &&
                          imagesFiles.length == 0) {
                        showToast(context, "Please select at least one image");
                      } else if (productList[widget.index].images.length == 0 &&
                          imagesFiles.length != 0) {
                        showToast(context, "Please Upload the selected images");
                      } else {
                        Navigator.of(context).pop();
                      }
                    },
                    context: context,
                    text: "Edit Product",
                    length: counters.toString(),
                    onTap: () {
                      counterset.zerocunter();
                      Navigator.of(context).push(PageTransition(
                          child: MyNotification(),
                          duration: Duration(milliseconds: 700),
                          type: PageTransitionType.leftToRightWithFade));
                    }),
                loading
                    ? Container(
                        child: Center(
                            child: Image.asset(
                          "assets/images/logo2.gif",
                          height: 200,
                          width: 100,
                        )),
                      )
                    : Expanded(
                        child: Container(
                          child: ListView(
                            physics: BouncingScrollPhysics(),
                            padding: EdgeInsets.zero,
                            children: <Widget>[
                              SizedBox(
                                height: (20 / 8.148314082864863) *
                                    SizeConfig.heightMultiplier,
                              ),
                              Padding(
                                padding: const EdgeInsets.all(2.0),
                                child: Container(
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.only(
                                    bottomLeft: Radius.circular(25.9),
                                    bottomRight: Radius.circular(25.9),
                                  )),
                                  child: Padding(
                                    padding: EdgeInsets.only(
                                        left: 16.2, right: 16.2),
                                    child: Form(
                                      key: formKey,
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          Padding(
                                            padding: const EdgeInsets.symmetric(
                                                horizontal: 5, vertical: 5),
                                            child: Text(
                                              "Title of the Product",
                                              style: TextStyle(
                                                  fontFamily: fontfaimly,
                                                  fontSize: (18.9 /
                                                          8.148314082864863) *
                                                      SizeConfig
                                                          .textMultiplier),
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.all(2.0),
                                            child: Stack(
                                              children: [
                                                material(),
                                                TextFormField(
                                                  style: TextStyle(
                                                    fontSize: 14,
                                                    fontFamily:
                                                        fontfaimlyregular,
                                                  ),
                                                  controller: _titleController,
                                                  decoration: InputDecoration(
                                                    focusedBorder:
                                                        OutlineInputBorder(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              35),
                                                      borderSide: BorderSide(
                                                          color: Color(
                                                              0xff69BEF7)),
                                                    ),
                                                    contentPadding:
                                                        EdgeInsets.symmetric(
                                                            vertical: 10.0,
                                                            horizontal: 10.0),
                                                    border: OutlineInputBorder(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              30.0),
                                                    ),
                                                    hintText: 'product name',
                                                  ),
                                                  onSaved: (value) {
                                                    // productModel.title=value;
                                                  },
                                                  validator: (value) {
                                                    if (value.isEmpty) {
                                                      return "field required";
                                                    }
                                                    return null;
                                                  },
                                                ),
                                              ],
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.symmetric(
                                                horizontal: 5, vertical: 5),
                                            child: Text(
                                              "Description",
                                              style: TextStyle(
                                                  fontFamily: fontfaimly,
                                                  fontSize: (18.9 /
                                                          8.148314082864863) *
                                                      SizeConfig
                                                          .textMultiplier),
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.all(2.0),
                                            child: Material(
                                              shadowColor: Color(0xffd4d4d4),
                                              elevation: 5,
                                              borderRadius:
                                                  BorderRadius.circular(30),
                                              child: TextFormField(
                                                style: TextStyle(
                                                  fontSize: 14,
                                                  fontFamily: fontfaimlyregular,
                                                ),
                                                controller:
                                                    _descriptioncontroller,
                                                decoration: InputDecoration(
                                                  errorBorder:
                                                      outlineInputBorder(),
                                                  focusedErrorBorder:
                                                      outlineInputBorder(),
                                                  focusedBorder:
                                                      OutlineInputBorder(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            30),
                                                    borderSide: BorderSide(
                                                        color: isemptydescription
                                                            ? Colors.red
                                                            : Color(
                                                                0xff69BEF7)),
                                                  ),
                                                  enabledBorder:
                                                      OutlineInputBorder(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            30.0),
                                                    borderSide: BorderSide(
                                                        color:
                                                            isemptydescription
                                                                ? Colors.red
                                                                : Colors.grey,
                                                        width: 1.0),
                                                  ),
                                                  border: OutlineInputBorder(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            30.0),
                                                  ),
                                                  hintText: 'Description',
                                                ),
                                                onSaved: (value) {
                                                  // productModel.description=value;
                                                },
                                                maxLines: 5,
                                              ),
                                            ),
                                          ),
                                          Row(
                                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                            children: [
                                              isemptydescription
                                                  ? Padding(
                                                padding: const EdgeInsets.all(8.0),
                                                child: Text(
                                                  "Enter brief explaination about your product",
                                                  style:style.MontserratRegular(
                                                      color: Colors.red, fontSize: size.convert(context, 12)),
                                                ),
                                              ):isdescriptionlength? Padding(
                                                padding: const EdgeInsets.all(8.0),
                                                child: Text(
                                                  "Description length must be 1000 words",
                                                  style:style.MontserratRegular(
                                                      color: Colors.red, fontSize: size.convert(context, 12)),
                                                ),
                                              ): Container(),
                                              Padding(
                                                padding: const EdgeInsets.all(8.0),
                                                child: Text("0/1000",style: TextStyle(
                                                  color: Colors.grey,
                                                  fontSize:size.convert(context, 12),
                                                ),),
                                              ),
                                            ],
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.symmetric(
                                                horizontal: 5, vertical: 5),
                                            child: Text(
                                              "Price",
                                              style: TextStyle(
                                                  fontFamily: fontfaimly,
                                                  fontSize: (18.9 /
                                                          8.148314082864863) *
                                                      SizeConfig
                                                          .textMultiplier),
                                            ),
                                          ),
                                          Stack(
                                            children: [
                                              material(),
                                              TextFormField(
                                                style: TextStyle(
                                                  fontSize: 14,
                                                  fontFamily: fontfaimlyregular,
                                                ),
                                                controller: _pricecontroller,
                                                keyboardType:
                                                    TextInputType.number,
                                                decoration: InputDecoration(
                                                  focusedBorder:
                                                      OutlineInputBorder(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            35),
                                                    borderSide: BorderSide(
                                                        color:
                                                            Color(0xff69BEF7)),
                                                  ),
                                                  contentPadding:
                                                      EdgeInsets.symmetric(
                                                          vertical: 7.0,
                                                          horizontal: 7.0),
                                                  border: OutlineInputBorder(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            25.0),
                                                  ),
                                                  hintText: "Price",
                                                ),
                                                validator: (value) {
                                                  if (value.isEmpty) {
                                                    return "field required";
                                                  }
                                                  return null;
                                                },
                                                onSaved: (value) {
                                                  // productModel.priceFrom=value;
                                                },
                                              ),
                                            ],
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.symmetric(
                                                horizontal: 5, vertical: 5),
                                            child: Text(
                                              "Quantity",
                                              style: TextStyle(
                                                  fontFamily: fontfaimly,
                                                  fontSize: (18.9 /
                                                          8.148314082864863) *
                                                      SizeConfig
                                                          .textMultiplier),
                                            ),
                                          ),
                                          Stack(
                                            children: [
                                              material(),
                                              TextFormField(
                                                style: TextStyle(
                                                  fontSize: 14,
                                                  fontFamily: fontfaimlyregular,
                                                ),
                                                controller: _controllerquantity,
                                                keyboardType:
                                                    TextInputType.number,
                                                decoration: InputDecoration(
                                                  focusedBorder:
                                                      OutlineInputBorder(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            35),
                                                    borderSide: BorderSide(
                                                        color:
                                                            Color(0xff69BEF7)),
                                                  ),
                                                  contentPadding:
                                                      EdgeInsets.symmetric(
                                                          vertical: 10.0,
                                                          horizontal: 10.0),
                                                  border: OutlineInputBorder(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            30.0),
                                                  ),
                                                  hintText: 'quantity',
                                                ),
                                                validator: (value) {
                                                  if (value.isEmpty) {
                                                    return "field required";
                                                  }
                                                  return null;
                                                },
                                                onSaved: (value) {
                                                  // productModel.quantity=int.parse(value);
                                                },
                                              ),
                                            ],
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.symmetric(
                                                horizontal: 5, vertical: 5),
                                            child: Text(
                                              "Minimum Order Quantity",
                                              style: TextStyle(
                                                  fontFamily: fontfaimly,
                                                  fontSize: (18.9 /
                                                          8.148314082864863) *
                                                      SizeConfig
                                                          .textMultiplier),
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.all(2.0),
                                            child: Stack(
                                              children: [
                                                material(),
                                                TextFormField(
                                                  style: TextStyle(
                                                    fontFamily:
                                                        fontfaimlyregular,
                                                    fontSize: 14,
                                                  ),
                                                  controller: _moqcontroller,
                                                  keyboardType:
                                                      TextInputType.number,
                                                  decoration: InputDecoration(
                                                    focusedBorder:
                                                        OutlineInputBorder(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              35),
                                                      borderSide: BorderSide(
                                                          color: Color(
                                                              0xff69BEF7)),
                                                    ),
                                                    contentPadding:
                                                        EdgeInsets.symmetric(
                                                            vertical: 10.0,
                                                            horizontal: 10.0),
                                                    border: OutlineInputBorder(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              30.0),
                                                    ),
                                                    hintText: 'M O Q',
                                                  ),
                                                  validator: (value) {
                                                    if (value.isEmpty) {
                                                      return "field required";
                                                    }
                                                    return null;
                                                  },
                                                  onSaved: (value) {},
                                                ),
                                              ],
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.symmetric(
                                                horizontal: 5, vertical: 5),
                                            child: Text(
                                              "Add Organization Name",
                                              style: TextStyle(
                                                  fontFamily: fontfaimly,
                                                  fontSize:
                                                      (18 / 8.148314082864863) *
                                                          SizeConfig
                                                              .textMultiplier),
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.all(2.0),
                                            child: Stack(
                                              children: [
                                                material(),
                                                TextFormField(
                                                  style: TextStyle(
                                                    fontSize: 14,
                                                    fontFamily:
                                                        fontfaimlyregular,
                                                  ),
                                                  controller:
                                                      _organizationcontroller,
                                                  decoration: InputDecoration(
                                                    focusedBorder:
                                                        OutlineInputBorder(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              35),
                                                      borderSide: BorderSide(
                                                          color: Color(
                                                              0xff69BEF7)),
                                                    ),
                                                    contentPadding:
                                                        EdgeInsets.symmetric(
                                                            vertical: 10.0,
                                                            horizontal: 10.0),
                                                    border: OutlineInputBorder(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              25.0),
                                                    ),
                                                    hintText:
                                                        'organization name',
                                                  ),
                                                  onSaved: (value) {
                                                    // widget.productModel.organizationName = value;
                                                  },
                                                  validator: (value) {
                                                    if (value.isEmpty &&
                                                        value.length < 5) {
                                                      return 'field required';
                                                    }
                                                    return null;
                                                  },
                                                ),
                                              ],
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.symmetric(
                                                horizontal: 5, vertical: 5),
                                            child: Text(
                                              "Select Category",
                                              style: TextStyle(
                                                  fontFamily: fontfaimly,
                                                  fontSize:
                                                      (18 / 8.148314082864863) *
                                                          SizeConfig
                                                              .textMultiplier),
                                            ),
                                          ),
                                          Material(
                                            shadowColor: Color(0xff69BEF7),
                                            elevation: 5,
                                            borderRadius:
                                                BorderRadius.circular(25),
                                            child: Container(
                                              decoration: BoxDecoration(
                                                  border: Border.all(
                                                    color: Colors.blue,
                                                  ),
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          25)),
                                              padding:
                                                  const EdgeInsets.all(8.0),
                                              child:
                                                  DropdownButtonHideUnderline(
                                                child: DropdownButton(
                                                  icon: Icon(
                                                    Icons.arrow_drop_down,
                                                  ),
                                                  isDense: true,
                                                  isExpanded: true,
                                                  value: selectedCategory,
                                                  onChanged: (value) {
                                                    setState(() {
                                                      if (selectedSubCategory !=
                                                          null) {
                                                        selectedSubCategory =
                                                            null;
                                                      }
                                                      subCategory = [];
                                                    });
                                                    categories
                                                        .forEach((f) async {
                                                      if (f.title == value) {
                                                        getSubCategories(
                                                                catId: f.sId)
                                                            .then((list) {
                                                          // regModel;
                                                          setState(() {
                                                            subCategory = list;
                                                            selectedCategory =
                                                                value;
                                                            categoryid = f.sId;
                                                          });
                                                        });
                                                      }
                                                    });
                                                  },
                                                  items: categories.map((f) {
                                                    return DropdownMenuItem(
                                                      value: f.title,
                                                      child: Text(
                                                        f.title,
                                                        style: TextStyle(
                                                          fontFamily:
                                                              fontfaimlyregular,
                                                        ),
                                                      ),
                                                    );
                                                  }).toList(),
                                                  hint: Text(
                                                    'Category',
                                                    style: TextStyle(
                                                        fontFamily:
                                                            fontfaimlyregular,
                                                        color: Colors.black,
                                                        fontSize: 12),
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.symmetric(
                                                horizontal: 5, vertical: 5),
                                            child: Text(
                                              "Select Sub Category",
                                              style: TextStyle(
                                                  fontFamily: fontfaimly,
                                                  fontSize:
                                                      (18 / 8.148314082864863) *
                                                          SizeConfig
                                                              .textMultiplier),
                                            ),
                                          ),
                                          Material(
                                            shadowColor: Color(0xff69BEF7),
                                            elevation: 5,
                                            borderRadius:
                                                BorderRadius.circular(25),
                                            child: Container(
                                              decoration: BoxDecoration(
                                                  border: Border.all(
                                                    color: Colors.blue,
                                                  ),
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          25)),
                                              padding:
                                                  const EdgeInsets.all(8.0),
                                              child:
                                                  DropdownButtonHideUnderline(
                                                child: DropdownButton(
                                                  icon: Icon(
                                                    Icons.arrow_drop_down,
                                                  ),
                                                  isDense: true,
                                                  isExpanded: true,
                                                  value: selectedSubCategory,
                                                  onChanged: (value) {
                                                    subCategory.forEach((f) {
                                                      if (f.title == value) {
                                                        subCategoryid = f.sId;
                                                        // widget.regModel.
                                                      }
                                                    });
                                                    setState(() {
                                                      selectedSubCategory =
                                                          value;
                                                    });
                                                  },
                                                  items: subCategory.map((f) {
                                                    return DropdownMenuItem(
                                                      value: f.title,
                                                      child: Text(
                                                        f.title,
                                                        style: TextStyle(
                                                          fontFamily:
                                                              fontfaimlyregular,
                                                        ),
                                                      ),
                                                    );
                                                  }).toList(),
                                                  hint: Text(
                                                    'Select Sub Category',
                                                    style: TextStyle(
                                                        fontFamily:
                                                            fontfaimlyregular,
                                                        color: Colors.black,
                                                        fontSize: (14 /
                                                                8.148314082864863) *
                                                            SizeConfig
                                                                .textMultiplier),
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.symmetric(
                                                horizontal: 5, vertical: 5),
                                            child: Text(
                                              "Product images",
                                              style: TextStyle(
                                                  fontFamily: fontfaimly,
                                                  fontSize:
                                                      (20 / 8.148314082864863) *
                                                          SizeConfig
                                                              .textMultiplier),
                                            ),
                                          ),
                                          productList[widget.index]
                                                      .images
                                                      .length ==
                                                  0
                                              ? Container()
                                              : Container(
                                                  height: 20 *
                                                      SizeConfig
                                                          .heightMultiplier,
                                                  width: double.infinity,
                                                  child: ListView.builder(
                                                    scrollDirection:
                                                        Axis.horizontal,
                                                    itemCount: productList[
                                                            widget.index]
                                                        .images
                                                        .length,
                                                    itemBuilder:
                                                        (BuildContext context,
                                                            int index) {
                                                      return Card(
                                                        child: Container(
                                                          height: 44 *
                                                              SizeConfig
                                                                  .heightMultiplier,
                                                          width: 28 *
                                                              SizeConfig
                                                                  .widthMultiplier,
                                                          child: Column(
                                                            children: [
                                                              Container(
                                                                height: 15 *
                                                                    SizeConfig
                                                                        .heightMultiplier,
                                                                width: 25 *
                                                                    SizeConfig
                                                                        .widthMultiplier,
                                                                child:
                                                                    CachedNetworkImage(
                                                                  fit: BoxFit
                                                                      .cover,
                                                                  imageUrl:
                                                                      '$base_url/${productList[widget.index].images[index]}',
                                                                ),
                                                              ),
                                                              Expanded(
                                                                  child: Row(
                                                                mainAxisAlignment:
                                                                    MainAxisAlignment
                                                                        .end,
                                                                children: [
                                                                  InkWell(
                                                                    child: Icon(
                                                                      Icons
                                                                          .delete,
                                                                      color: Color(
                                                                          0xfff81818),
                                                                    ),
                                                                    onTap: () {
                                                                      print(productList[widget.index]
                                                                              .images[
                                                                          index]);
                                                                      setState(
                                                                          () {
                                                                        isUploading =
                                                                            true;
                                                                      });
                                                                      deleteproductimage(
                                                                              productList[widget.index].sId,
                                                                              productList[widget.index].images[index],context)
                                                                          .then((value) {
                                                                        getAllCurrentUserProducts(user.sId,
                                                                                context)
                                                                            .then((value) {
                                                                          Provider.of<GettingProducts>(context, listen: false)
                                                                              .onLogIn(value["products"]);
                                                                          setState(
                                                                              () {
                                                                            isUploading =
                                                                                false;
                                                                          });
                                                                          print(
                                                                              value["products"]);
                                                                        });
                                                                      });
                                                                    },
                                                                  ),
                                                                ],
                                                              )),
                                                            ],
                                                          ),
                                                        ),
                                                      );
                                                    },
                                                  )),
                                          images == null
                                              ? new Container()
                                              : new Container(
                                                  height: (150 /
                                                          8.148314082864863) *
                                                      SizeConfig
                                                          .heightMultiplier,
                                                  width: MediaQuery.of(context)
                                                      .size
                                                      .width,
                                                  child: new ListView.builder(
                                                    scrollDirection:
                                                        Axis.horizontal,
                                                    itemBuilder: (BuildContext
                                                                context,
                                                            int index) =>
                                                        new Card(
                                                            child: Container(
                                                      height: 44 *
                                                          SizeConfig
                                                              .heightMultiplier,
                                                      width: 28 *
                                                          SizeConfig
                                                              .widthMultiplier,
                                                      child: Image.file(
                                                          imagesFiles[index]),
                                                    )),
                                                    itemCount:
                                                        imagesFiles.length,
                                                  ),
                                                ),
                                          Container(
                                            height: 13 *
                                                SizeConfig.heightMultiplier,
                                            width:
                                                26 * SizeConfig.widthMultiplier,
                                            child: Card(
                                              child: Center(
                                                child: Column(
                                                  children: [
                                                    IconButton(
                                                      icon: Icon(
                                                        Icons.add_to_photos,
                                                        color: Colors.blue,
                                                      ),
                                                      onPressed: productPicture,
                                                    ),
                                                    Text(
                                                      "add images",
                                                      style: TextStyle(
                                                        fontFamily: fontfaimly,
                                                        fontSize: 1.8 *
                                                            SizeConfig
                                                                .textMultiplier,
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ),
                                          ),
                                          SizedBox(
                                            height: (15 / 8.148314082864863) *
                                                SizeConfig.heightMultiplier,
                                          ),
                                          Center(
                                            child: filledButton(
                                              txt: "SAVE CHANGES",
                                              onTap: () {
                                                if (productList[widget.index]
                                                            .images
                                                            .length ==
                                                        0 &&
                                                    imagesFiles.length == 0) {
                                                  print("1");
                                                  return showToast(context,
                                                      "Please Select at least one image");
                                                } else if (formKey.currentState
                                                        .validate() &&
                                                    _descriptioncontroller
                                                        .text.isNotEmpty&&_descriptioncontroller.text.length<1000) {
                                                print("2");
                                                  setState(() {
                                                    isUploading = true;
                                                  });
                                                  updateproduct()
                                                      .then((values) {
                                                    getAllCurrentUserProducts(
                                                            user.sId, context)
                                                        .then((value) {
                                                      Provider.of<GettingProducts>(
                                                              context,
                                                              listen: false)
                                                          .onLogIn(value[
                                                              "products"]);
                                                      Toast.show(
                                                          values["message"],
                                                          context,
                                                          gravity: Toast.TOP);
                                                      print("$values");
                                                      setState(() {
                                                        isUploading = false;
                                                      });
                                                      Navigator.of(context)
                                                          .pop();
                                                    });
                                                  });
                                                } else if (_descriptioncontroller
                                                    .text.isEmpty) {
                                                  setState(() {
                                                    isemptydescription = true;
                                                  });
                                                }else if(_descriptioncontroller.text.length>1000){
                                                  setState(() {
                                                    isdescriptionlength=true;
                                                  });
                                                }
                                              },
                                            ),
                                          ),
                                          SizedBox(
                                            height: (20 / 8.148314082864863) *
                                                SizeConfig.heightMultiplier,
                                          )
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
              ],
            ),
          ),
        ),
        isUploading
            ? Stack(
                children: <Widget>[
                  Container(
                    color: Color(0xffffffff).withOpacity(0.6),
                    child: Opacity(
                      opacity: 1.0,
                      child: Center(
                          child: Image.asset(
                        "assets/images/logo2.gif",
                        height: 200,
                        width: 100,
                      )),
                    ),
                  ),
                ],
              )
            : Container(),
      ],
    );
  }

  getImageFileFromAssets(List<Asset> assets) async {
    List<File> _files = [];
    assets.forEach((f) async {
      var byteData = await f.getByteData();
      final file = File('${(await getTemporaryDirectory()).path}/${f.name}');
      await file.writeAsBytes(byteData.buffer
          .asUint8List(byteData.offsetInBytes, byteData.lengthInBytes));
      _files.add(file);
    });

    imagesFiles = _files;

    Future.delayed(Duration(milliseconds: 2000)).then((t) {
      setState(() {});
    });
  }

  Future<Map<dynamic, dynamic>> updateproduct() async {
    List<String> images = [];
    imagesFiles.forEach((f) {
      images.add(f.path);
    });
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    String token = _prefs.getString("token");
    String productPostUrl = '$base_url/user/update/product';
    var request = http.MultipartRequest(
      'POST',
      Uri.parse(productPostUrl),
    )
      ..fields['productId'] = widget.products[widget.index].sId
      ..fields['title'] = _titleController.text
      ..fields['description'] = _descriptioncontroller.text
      ..fields['quantity'] = _controllerquantity.text
      ..fields['moq'] = _moqcontroller.text.toString()
      ..fields['category'] = categoryid
      ..fields['subCategory'] = subCategoryid
      ..fields['price'] = _pricecontroller.text
      ..fields['organizationName'] = _organizationcontroller.text;
    request.headers['authorization'] = token;
    images.forEach((f) async {
      request.files.add(await http.MultipartFile.fromPath('images', f));
    });
    http.Response productResponse = await http.Response.fromStream(
      await request.send(),
    );
    print(productResponse.body);
    var data = jsonDecode(productResponse.body);
    return data;
  }
}
