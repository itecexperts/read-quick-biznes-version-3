import 'dart:io';
import 'package:biznes/helper/counthelper.dart';
import 'package:biznes/model/citiesModel.dart';
import 'package:biznes/product_UI/bottomBar_UI/DropDownWidget.dart';
import 'package:biznes/repeatedWigets/filledButton.dart';
import 'package:biznes/model/countriesModel.dart';
import 'package:biznes/model/roleModel.dart';
import 'package:biznes/product_UI/bottomBar_UI/editpassword.dart';
import 'package:biznes/res/global.dart';
import 'package:biznes/services/register_logic.dart';
import 'package:biznes/products_Detail/extra.dart';
import 'package:biznes/setting/notification.dart';
import 'package:biznes/size_config.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:biznes/services/server.dart';
import 'package:biznes/helper/login_helper.dart';
import 'package:flutter/material.dart';
import 'package:biznes/model/editprofilemodel.dart';
import 'package:image_picker/image_picker.dart';
import 'package:page_transition/page_transition.dart';
import 'package:provider/provider.dart';
import 'package:biznes/res/color.dart' as fontandcolor;

class EditProfile extends StatefulWidget {
  final String beforeroles1;
  final String beforeroles2;
  final String beforeroles3;
  final String beforerole4;
  final String username;
  final String organizationname;
  final String email;

  EditProfile(
      {this.beforeroles1,
      this.beforeroles2,
      this.beforeroles3,
      this.beforerole4,
      this.username,
      this.organizationname,
      this.email});

  @override
  _EditProfileState createState() => _EditProfileState();
}

class _EditProfileState extends State<EditProfile> {
  TextEditingController _organizationcontroller = new TextEditingController();
  TextEditingController _usernamecontroller = new TextEditingController();
  TextEditingController _emailcontroller = new TextEditingController();
  bool isvisible = false;
  var formKey = GlobalKey<FormState>();
  List<CountriesModel> countryList = [];
  String selectcountry;
  List<CitiesModel> citiesList = [];
  String selectcity;
  EditProfileModel editProfileModel;
  File profilepicupload;
  List<RoleModel> rolesModelList = [];
  List<String> addingrole = [];
  bool showrole = false;

  @override
  void initState() {
    print(widget.organizationname);
    editProfileModel = EditProfileModel(
      userId: '',
      countryId: '',
      city: '',
      email: '',
      username: '',
      organizationName: '',
      profilePic: '',
      addingrole: [],
    );
    setState(() {
      widget.beforeroles1 != null
          ? editProfileModel.addingrole.add(widget.beforeroles1)
          : print("ok");
      widget.beforeroles2 != null
          ? editProfileModel.addingrole.add(widget.beforeroles2)
          : print("ok");
      widget.beforeroles3 != null
          ? editProfileModel.addingrole.add(widget.beforeroles3)
          : print("ok");
      widget.beforerole4 != null
          ? editProfileModel.addingrole.add(widget.beforerole4)
          : print("ok");
      editProfileModel.organizationName = widget.organizationname;
      editProfileModel.email = widget.email;
      editProfileModel.username = widget.username;
      _organizationcontroller.text = widget.organizationname;
      _usernamecontroller.text = widget.username;
      _emailcontroller.text = widget.email;
    });
    getCountries().then((countries) {
      print(editProfileModel.addingrole);
      setState(() {
        countryList = countries;
      });
    });
    getRoles().then((rolesList) {
      setState(() {
        rolesModelList = rolesList;
        print(rolesModelList.length);
      });
    });
    super.initState();
  }

  imagechangeprofile(BuildContext context) async {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
            title: Text(
              "Choose your file",
              style: TextStyle(
                fontFamily: fontandcolor.fontfaimly,
              ),
            ),
            content: Container(
              alignment: Alignment.center,
              height: (100 / 8.148314082864863) * SizeConfig.heightMultiplier,
              child: Column(
                children: <Widget>[
                  SizedBox(
                    height:
                        (40 / 8.148314082864863) * SizeConfig.heightMultiplier,
                  ),
                  InkWell(
                      onTap: () {
                        pickCamera();
                        Navigator.of(context).pop();
                      },
                      child: Text(
                        "Use Camera",
                        style: TextStyle(
                          fontFamily: fontandcolor.fontfaimlyregular,
                        ),
                      )),
                  SizedBox(
                    height:
                        (5 / 8.148314082864863) * SizeConfig.heightMultiplier,
                  ),
                  InkWell(
                      onTap: () {
                        pickGallery();
                        Navigator.of(context).pop();
                      },
                      child: Text(
                        "Use Gallery",
                        style: TextStyle(
                          fontFamily: fontandcolor.fontfaimlyregular,
                        ),
                      )),
                ],
              ),
            ),
            actions: <Widget>[
              FlatButton(
                child: Text("cancel"),
                onPressed: () => Navigator.of(context).pop(),
              )
            ],
          );
        });
  }

  void pickCamera() async {
    profilepicupload = await ImagePicker.pickImage(source: ImageSource.camera);
    setState(() {
      editProfileModel.profilePic = profilepicupload.path;
    });
  }

  void pickGallery() async {
    profilepicupload = await ImagePicker.pickImage(source: ImageSource.gallery);
    setState(() {
      editProfileModel.profilePic = profilepicupload.path;
    });
  }

  @override
  Widget build(BuildContext context) {
    var user = Provider.of<LoginHelper>(context).user.sId;
    var users = Provider.of<LoginHelper>(context).user;
    var token = Provider.of<LoginHelper>(context).token;
    var profiledata = Provider.of<LoginHelper>(context);
    var counters = Provider.of<CountHelper>(context).counter;
    var counterSet = Provider.of<CountHelper>(context);
    return Stack(
      children: [
        GestureDetector(
          onTap: () => FocusScope.of(context).requestFocus(new FocusNode()),
          child: Scaffold(
            backgroundColor: Colors.white,
            body: Container(
              child: Form(
                key: formKey,
                child: Column(
                  children: [
                    CustomAppBar(
                        IconPressed: () {
                          Navigator.of(context).pop();
                        },
                        icon: Icon(Icons.arrow_back),
                        context: context,
                        text: "Edit Profile",
                        length: counters.toString(),
                        onTap: () {
                          counterSet.zerocunter();
                          Navigator.of(context).push(PageTransition(
                              child: MyNotification(),
                              duration: Duration(milliseconds: 700),
                              type: PageTransitionType.leftToRightWithFade));
                        }),
                    Expanded(
                      child: SingleChildScrollView(
                        physics: BouncingScrollPhysics(),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Stack(
                              alignment: Alignment.bottomRight,
                              children: <Widget>[
                                Container(
                                  width: 140,
                                  height: 140,
                                  decoration: BoxDecoration(),
                                  child: CircleAvatar(
                                    backgroundImage: profilepicupload == null
                                        ? CachedNetworkImageProvider(
                                            '$base_url/${users.profilePic}')
                                        : FileImage(profilepicupload),
                                    backgroundColor: Colors.blue,
                                  ),
                                ),
                                CircleAvatar(
                                  backgroundColor: Color(0xff394c81),
                                  child: IconButton(
                                    icon: Icon(
                                      Icons.photo_camera,
                                      size: 20,
                                      color: Colors.white,
                                    ),
                                    onPressed: () {
                                      print("hello");
                                      imagechangeprofile(context);
                                    },
                                    color: Theme.of(context).accentColor,
                                  ),
                                ),
                              ],
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                users.roleName == "Broker"
                                    ? Container()
                                    : Padding(
                                        padding: const EdgeInsets.only(
                                            left: 18.0, bottom: 8),
                                        child: Text(
                                          'Organization name',
                                          style: TextStyle(
                                            fontFamily: fontandcolor.fontfaimly,
                                            fontSize: MediaQuery.of(context)
                                                        .orientation ==
                                                    Orientation.portrait
                                                ? (18 / 8.148314082864863) *
                                                    SizeConfig.heightMultiplier
                                                : (20 / 8.148314082864863) *
                                                    SizeConfig.heightMultiplier,
                                          ),
                                        ),
                                      ),
                                users.roleName == "Broker"
                                    ? Container()
                                    : Padding(
                                        padding: const EdgeInsets.only(
                                            left: 18.0, bottom: 8, right: 18.0),
                                        child: Stack(
                                          children: [
                                            material(),
                                            TextFormField(
                                              controller:
                                                  _organizationcontroller,
                                              validator: (value) {
                                                if (value.isEmpty &&
                                                    value.length < 6) {
                                                  return 'required field';
                                                }
                                                return null;
                                              },
                                              style: TextStyle(
                                                fontSize: 14,
                                                fontFamily: fontandcolor
                                                    .fontfaimlyregular,
                                              ),
                                              decoration: InputDecoration(
                                                  hintStyle: TextStyle(
                                                    fontFamily: fontandcolor
                                                        .fontfaimlyregular,
                                                  ),
                                                  errorBorder:
                                                      outlineInputBorder(),
                                                  focusedErrorBorder:
                                                      outlineInputBorder(),
                                                  focusedBorder:
                                                      OutlineInputBorder(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            25),
                                                    borderSide: BorderSide(
                                                        color:
                                                            Color(0xff69BEF7)),
                                                  ),
                                                  contentPadding:
                                                      EdgeInsets.symmetric(
                                                          vertical: 10.0,
                                                          horizontal: 10.0),
                                                  border: OutlineInputBorder(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            25.0),
                                                  ),
                                                  hintText:
                                                      'Change oraganization name'),
                                              enabled: true,
                                              onChanged: (value) {
                                                editProfileModel
                                                    .organizationName = value;
                                              },
                                            ),
                                          ],
                                        ),
                                      ),
                                // Padding(
                                //   padding: const EdgeInsets.all(8.0),
                                //   child: Text(
                                //     'Username',
                                //     style: TextStyle(
                                //       fontSize: MediaQuery.of(context).orientation ==
                                //               Orientation.portrait
                                //           ? (18 / 8.148314082864863) *
                                //               SizeConfig.heightMultiplier
                                //           : (20 / 8.148314082864863) *
                                //               SizeConfig.heightMultiplier,
                                //     ),
                                //   ),
                                // ),
                                // TextFormField(
                                //   controller: _usernamecontroller,
                                //   validator: (value) {
                                //     if (value.isEmpty && value.length < 6) {
                                //       return 'required field';
                                //     }
                                //     return null;
                                //   },
                                //     style: TextStyle(
                                //     fontSize:14,
                                //     ),
                                //   decoration: InputDecoration(

                                //       focusedBorder:OutlineInputBorder(
                                //         borderSide:  BorderSide(color: Color(0xff50D489), width: 2.0),
                                //         borderRadius: BorderRadius.circular(25.0),
                                //       ),
                                //       contentPadding: EdgeInsets.symmetric(
                                //           vertical: 10.0, horizontal: 10.0),
                                //       border: OutlineInputBorder(
                                //         borderRadius: BorderRadius.circular(25.0),
                                //       ),
                                //       hintText: 'Change Username'),
                                //   enabled: true,
                                //   onChanged: (value) {
                                //     editProfileModel.username = value;
                                //   },
                                // ),
                                Padding(
                                  padding: const EdgeInsets.only(
                                      left: 18.0, bottom: 8),
                                  child: Text(
                                    'Email',
                                    style: TextStyle(
                                      fontFamily: fontandcolor.fontfaimly,
                                      fontSize:
                                          MediaQuery.of(context).orientation ==
                                                  Orientation.portrait
                                              ? (18 / 8.148314082864863) *
                                                  SizeConfig.heightMultiplier
                                              : (20 / 8.148314082864863) *
                                                  SizeConfig.heightMultiplier,
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(
                                      left: 18.0, bottom: 8, right: 18.0),
                                  child: Stack(
                                    children: [
                                      material(),
                                      TextFormField(
                                        controller: _emailcontroller,
                                        validator: (value) {
                                          if (value.isEmpty &&
                                              value.length < 6) {
                                            return 'required field';
                                          }
                                          return null;
                                        },
                                        style: TextStyle(
                                          fontFamily:
                                              fontandcolor.fontfaimlyregular,
                                          fontSize: 14,
                                        ),
                                        decoration: InputDecoration(
                                            errorBorder: outlineInputBorder(),
                                            focusedErrorBorder:
                                                outlineInputBorder(),
                                            focusedBorder: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(25),
                                              borderSide: BorderSide(
                                                  color: Color(0xff69BEF7)),
                                            ),
                                            contentPadding:
                                                EdgeInsets.symmetric(
                                                    vertical: 10.0,
                                                    horizontal: 10.0),
                                            border: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(25.0),
                                            ),
                                            hintText: 'Change your Email'),
                                        enabled: true,
                                        onChanged: (value) {
                                          editProfileModel.email = value;
                                        },
                                      ),
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(
                                      left: 18.0, bottom: 8),
                                  child: Text(
                                    'Country',
                                    style: TextStyle(
                                      fontFamily: fontandcolor.fontfaimly,
                                      fontSize:
                                          MediaQuery.of(context).orientation ==
                                                  Orientation.portrait
                                              ? (18 / 8.148314082864863) *
                                                  SizeConfig.heightMultiplier
                                              : (20 / 8.148314082864863) *
                                                  SizeConfig.heightMultiplier,
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(
                                      left: 18.0, right: 18.0),
                                  child: DropDownWidget(
                                    image: selectcountry != null
                                        ? 'assets/images/flag2.png'
                                        : 'assets/images/countryicon.svg',
                                    context: context,
                                    hintext: "Update Your Country",
                                    selectedCategory: selectcountry,
                                    onChanged: (value) {
                                      setState(() {
                                        if (selectcity != null) {
                                          selectcity = null;
                                        }
                                        citiesList = [];
                                      });
                                      countryList.forEach((f) async {
                                        if (f.title == value) {
                                          getCities(f.sId).then((list) {
                                            setState(() {
                                              citiesList = list;
                                              selectcountry = value;
                                              editProfileModel.countryId =
                                                  f.sId;
                                            });
                                          });
                                        }
                                      });
                                    },
                                    value: selectcountry,
                                    categories: countryList,
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(
                                      left: 18.0,
                                      bottom: 8,
                                      right: 18.0,
                                      top: 8),
                                  child: Text(
                                    'City',
                                    style: TextStyle(
                                      fontFamily: fontandcolor.fontfaimly,
                                      fontSize:
                                          MediaQuery.of(context).orientation ==
                                                  Orientation.portrait
                                              ? (18 / 8.148314082864863) *
                                                  SizeConfig.heightMultiplier
                                              : (20 / 8.148314082864863) *
                                                  SizeConfig.heightMultiplier,
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(
                                      left: 18.0, right: 18.0),
                                  child: DropDownWidget(
                                      context: context,
                                      categories: citiesList,
                                      hintext: "Update Your City",
                                      image: "assets/images/city.svg",
                                      value: selectcity,
                                      selectedCategory: selectcity,
                                      onChanged: (value) {
                                        setState(() {
                                          if (selectcity != null) {
                                            selectcity = null;
                                          }
                                          
                                                selectcity = value;
                                        });
                                        citiesList.forEach((f) async {
                                          if (f.title == value) {
                                              setState(() {
                                                editProfileModel.city =
                                                    f.sId;
                                            });
                                          }
                                        });
                                      }),
                                ),
                                users.roleName == "Broker"
                                    ? Container()
                                    : Padding(
                                        padding: const EdgeInsets.only(
                                            left: 18.0,
                                            bottom: 8,
                                            right: 18.0,
                                            top: 8),
                                        child: Text(
                                          'User Role',
                                          style: TextStyle(
                                            fontFamily: fontandcolor.fontfaimly,
                                            fontSize: MediaQuery.of(context)
                                                        .orientation ==
                                                    Orientation.portrait
                                                ? (20 / 8.148314082864863) *
                                                    SizeConfig.heightMultiplier
                                                : (20 / 8.148314082864863) *
                                                    SizeConfig.heightMultiplier,
                                          ),
                                        ),
                                      ),
                                users.roleName == "Broker"
                                    ? Container()
                                    : Padding(
                                        padding: const EdgeInsets.only(
                                            left: 18.0, bottom: 8, right: 18.0),
                                        child: Material(
                                          shadowColor: Color(0xffcddaff),
                                          elevation: 5,
                                          borderRadius:
                                              BorderRadius.circular(25),
                                          child: Container(
                                            padding: EdgeInsets.all(
                                                showrole ? 10.0 : 0),
                                            width: double.infinity,
                                            decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(25.0),
                                              border: Border.all(
                                                  color: Colors.grey),
                                            ),
                                            child: showrole
                                                ? Card(
                                                    child: Padding(
                                                      padding:
                                                          const EdgeInsets.all(
                                                              8.0),
                                                      child: Column(
                                                        children: [
                                                          Row(
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .spaceBetween,
                                                            children: [
                                                              Text(
                                                                  rolesModelList[
                                                                          0]
                                                                      .title),
                                                              Checkbox(
                                                                  value: editProfileModel
                                                                      .addingrole
                                                                      .contains(
                                                                          rolesModelList[0]
                                                                              .id),
                                                                  onChanged: (bool
                                                                      selected) {
                                                                    if (selected ==
                                                                        true) {
                                                                      this.setState(
                                                                          () {
                                                                        editProfileModel
                                                                            .addingrole
                                                                            .add(rolesModelList[0].id);
                                                                        print(
                                                                            addingrole);
                                                                      });
                                                                    } else {
                                                                      this.setState(
                                                                          () {
                                                                        editProfileModel
                                                                            .addingrole
                                                                            .remove(rolesModelList[0].id);
                                                                        print(
                                                                            addingrole);
                                                                      });
                                                                    }
                                                                  }),
                                                            ],
                                                          ),
                                                          Row(
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .spaceBetween,
                                                            children: [
                                                              Text(
                                                                  rolesModelList[
                                                                          1]
                                                                      .title),
                                                              Checkbox(
                                                                  value: editProfileModel
                                                                      .addingrole
                                                                      .contains(
                                                                          rolesModelList[1]
                                                                              .id),
                                                                  onChanged: (bool
                                                                      selected) {
                                                                    if (selected ==
                                                                        true) {
                                                                      this.setState(
                                                                          () {
                                                                        editProfileModel
                                                                            .addingrole
                                                                            .add(rolesModelList[1].id);
                                                                        print(editProfileModel
                                                                            .addingrole);
                                                                      });
                                                                    } else {
                                                                      this.setState(
                                                                          () {
                                                                        editProfileModel
                                                                            .addingrole
                                                                            .remove(rolesModelList[1].id);
                                                                        print(editProfileModel
                                                                            .addingrole);
                                                                      });
                                                                    }
                                                                  }),
                                                            ],
                                                          ),
                                                          Row(
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .spaceBetween,
                                                            children: [
                                                              Text(
                                                                  rolesModelList[
                                                                          2]
                                                                      .title),
                                                              Checkbox(
                                                                  value: editProfileModel
                                                                      .addingrole
                                                                      .contains(
                                                                          rolesModelList[2]
                                                                              .id),
                                                                  onChanged: (bool
                                                                      selected) {
                                                                    if (selected ==
                                                                        true) {
                                                                      this.setState(
                                                                          () {
                                                                        editProfileModel
                                                                            .addingrole
                                                                            .add(rolesModelList[2].id);
                                                                        print(editProfileModel
                                                                            .addingrole);
                                                                      });
                                                                    } else {
                                                                      this.setState(
                                                                          () {
                                                                        editProfileModel
                                                                            .addingrole
                                                                            .remove(rolesModelList[2].id);
                                                                        print(editProfileModel
                                                                            .addingrole);
                                                                      });
                                                                    }
                                                                  }),
                                                            ],
                                                          ),
                                                          Row(
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .spaceBetween,
                                                            children: [
                                                              Text(
                                                                  rolesModelList[
                                                                          3]
                                                                      .title),
                                                              Checkbox(
                                                                  value: editProfileModel
                                                                      .addingrole
                                                                      .contains(
                                                                          rolesModelList[3]
                                                                              .id),
                                                                  onChanged: (bool
                                                                      selected) {
                                                                    if (selected ==
                                                                        true) {
                                                                      this.setState(
                                                                          () {
                                                                        editProfileModel
                                                                            .addingrole
                                                                            .add(rolesModelList[3].id);
                                                                        print(editProfileModel
                                                                            .addingrole);
                                                                      });
                                                                    } else {
                                                                      this.setState(
                                                                          () {
                                                                        editProfileModel
                                                                            .addingrole
                                                                            .remove(rolesModelList[3].id);
                                                                        print(editProfileModel
                                                                            .addingrole);
                                                                      });
                                                                    }
                                                                  }),
                                                            ],
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                  )
                                                : Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceBetween,
                                                    children: [
                                                      Padding(
                                                        padding:
                                                            const EdgeInsets
                                                                    .only(
                                                                left: 10.0),
                                                        child: Text(
                                                          'Enter one or more roles',
                                                          style: TextStyle(
                                                            fontFamily: fontandcolor
                                                                .fontfaimlyregular,
                                                          ),
                                                        ),
                                                      ),
                                                      IconButton(
                                                        icon: Image.asset(
                                                          'assets/images/dropdown.png',
                                                          height: 15,
                                                          width: 15,
                                                        ),
                                                        onPressed: () {
                                                          setState(() {
                                                            showrole = true;
                                                          });
                                                        },
                                                      ),
                                                    ],
                                                  ),
                                          ),
                                        ),
                                      ),
                                Padding(
                                  padding: const EdgeInsets.only(
                                      top: 8.0, bottom: 8.0),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      InkWell(
                                        onTap: () {
                                          Navigator.of(context).push(
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      ChangePassword()));
                                        },
                                        child: Container(
                                          child: Text(
                                            'Change your Password',
                                            style: TextStyle(
                                              fontFamily: fontandcolor
                                                  .fontfaimlyregular,
                                              fontSize: MediaQuery.of(context)
                                                          .orientation ==
                                                      Orientation.portrait
                                                  ? (14 / 8.148314082864863) *
                                                      SizeConfig
                                                          .heightMultiplier
                                                  : (14 / 8.148314082864863) *
                                                      SizeConfig
                                                          .heightMultiplier,
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: filledButton(
                                txt: "Save Changes",
                                onTap: () {
                                  if (formKey.currentState.validate()) {
                                    if (users.roleName == "Broker") {
                                      setState(() {
                                        isvisible = true;
                                      });
                                      profileupdateforbroker(editProfileModel,
                                              user, context)
                                          .then((value) {
                                        profiledata
                                            .onprofilechange(value['user']);
                                        setState(() {
                                          isvisible = false;
                                        });
                                        Navigator.of(context).pop();
                                      });
                                    } else {
                                      setState(() {
                                        isvisible = true;
                                      });
                                      profileupdate(editProfileModel, user,context, addingrole)
                                          .then((map) {
                                        setState(() {
                                          isvisible = false;
                                        });
                                        profiledata
                                            .onprofilechange(map['user']);
                                        Navigator.of(context).pop();
                                      });
                                    }
                                  }
                                },
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
        isvisible
            ? Stack(
                children: <Widget>[
                  Container(
                    color: Color(0xffffffff).withOpacity(0.6),
                    height: double.infinity,
                    width: double.infinity,
                    child: Opacity(
                      opacity: 1.0,
                      child: Center(
                          child: Image.asset(
                        "assets/images/logo2.gif",
                        height: 200,
                        width: 100,
                      )),
                    ),
                  ),
                ],
              )
            : Container(),
      ],
    );
  }
}
