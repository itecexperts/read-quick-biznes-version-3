import 'package:biznes/helper/counthelper.dart';
import 'package:biznes/helper/login_helper.dart';
import 'package:biznes/products_Detail/extra.dart';
import 'package:biznes/services/register_logic.dart';
import 'package:biznes/setting/notification.dart';
import 'package:biznes/size_config.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:provider/provider.dart';
import 'package:biznes/repeatedWigets/filledButton.dart';
import 'package:biznes/res/color.dart' as fontandcolor;

class NewPassword extends StatefulWidget {
  @override
  _NewPasswordState createState() => _NewPasswordState();
}

class _NewPasswordState extends State<NewPassword> {
  String _password;
  String _cpassword;
  var formKey = GlobalKey<FormState>();
  bool isvisible = false;

  @override
  Widget build(BuildContext context) {
    var user = Provider.of<LoginHelper>(context).user;

    var counters = Provider.of<CountHelper>(context, listen: false).counter;
    var counterset = Provider.of<CountHelper>(context);
    return Stack(
      children: [
        Scaffold(
          backgroundColor: Colors.white,
          body: Form(
            key: formKey,
            child: SingleChildScrollView(
              child: Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    CustomAppBar(
                        icon: Icon(Icons.arrow_back),
                        IconPressed: () {
                          Navigator.of(context).pop();
                        },
                        context: context,
                        text: "Change Password",
                        length: counters.toString(),
                        onTap: () {
                          counterset.zerocunter();
                          Navigator.of(context).push(PageTransition(
                              child: MyNotification(),
                              duration: Duration(milliseconds: 700),
                              type: PageTransitionType.leftToRightWithFade));
                        }),
                    SizedBox(
                      height: 4 * SizeConfig.heightMultiplier,
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        'Enter your new password',
                        style: TextStyle(
                          fontFamily: fontandcolor.fontfaimly,
                          fontSize: (18.9 / 8.148314082864863) *
                              SizeConfig.textMultiplier,
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(4.0),
                      child: Stack(
                        children: [
                          material(),
                          TextFormField(
                            validator: (value) {
                              if (value.length < 6) {
                                return 'please enter your new password';
                              }
                              return null;
                            },
                            decoration: InputDecoration(
                                hintStyle: TextStyle(
                                  fontSize: 12,
                                  fontFamily: fontandcolor.fontfaimlyregular,
                                ),
                                focusedErrorBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.red),
                                  borderRadius: BorderRadius.circular(30.0),
                                ),
                                focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(25),
                                  borderSide:
                                      BorderSide(color: Color(0xff69BEF7)),
                                ),
                                focusColor: Color(0xff69BEF7),
                                contentPadding: EdgeInsets.symmetric(
                                    vertical: 10.0, horizontal: 10.0),
                                border: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.red),
                                  borderRadius: BorderRadius.circular(30.0),
                                ),
                                hintText: 'Enter new password'),
                            enabled: true,
                            onChanged: (value) {
                              _password = value;
                            },
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        'Confirm your new password',
                        style: TextStyle(
                            fontFamily: fontandcolor.fontfaimly,
                            fontSize: (18.9 / 8.148314082864863) *
                                SizeConfig.textMultiplier),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(4.0),
                      child: Stack(
                        children: [
                          material(),
                          TextFormField(
                            validator: (value) {
                              if (value.length < 6) {
                                return 'please confirm your new password';
                              }
                              return null;
                            },
                            decoration: InputDecoration(
                                hintStyle: TextStyle(
                                  fontSize: 12,
                                  fontFamily: fontandcolor.fontfaimlyregular,
                                ),
                                focusedErrorBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.red),
                                  borderRadius: BorderRadius.circular(30.0),
                                ),
                                focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(25),
                                  borderSide:
                                      BorderSide(color: Color(0xff69BEF7)),
                                ),
                                focusColor: Color(0xff69BEF7),
                                contentPadding: EdgeInsets.symmetric(
                                    vertical: 10.0, horizontal: 10.0),
                                border: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.red),
                                  borderRadius: BorderRadius.circular(30.0),
                                ),
                                hintText: 'Confirm Your New Password'),
                            enabled: true,
                            onChanged: (value) {
                              _cpassword = value;
                            },
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 18.0),
                      child: Center(
                        child: filledButton(
                          txt: "Done",
                          onTap: () {
                            if (formKey.currentState.validate()) {
                              setState(() {
                                isvisible = true;
                              });
                              changenewpasswordinprofile(
                                      user.sId, _password, _cpassword, context)
                                  .then((value) {
                                if (value["Success"] == true) {
                                  setState(() {
                                    isvisible = false;
                                  });
                                  Navigator.of(context).pop();
                                  Navigator.of(context).pop();
                                  Navigator.of(context).pop();
                                }
                              });
                            }
                          },
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
        isvisible
            ? Stack(
                children: <Widget>[
                  Container(
                    color: Color(0xffffffff).withOpacity(0.4),
                    height: double.infinity,
                    width: double.infinity,
                    child: Opacity(
                      opacity: 1.0,
                      child: Center(
                          child: Image.asset(
                        "assets/images/logo2.gif",
                        height: 200,
                        width: 100,
                      )),
                    ),
                  ),
                ],
              )
            : Container(),
      ],
    );
  }
}
