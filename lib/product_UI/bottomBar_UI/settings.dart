import 'package:biznes/helper/counthelper.dart';
import 'package:biznes/helper/login_helper.dart';
import 'package:app_settings/app_settings.dart';
import 'package:flutter_svg/flutter_svg.dart';
import './soundhandling.dart';
import 'package:biznes/products_Detail/extra.dart';
import 'package:biznes/login_UI/home.dart';
import 'package:biznes/services/register_logic.dart';
import 'package:flutter_share/flutter_share.dart';
import 'package:biznes/setting/privacy.dart';
import 'package:biznes/setting/termsandcondition.dart';
import 'package:biznes/setting/Report.dart';
import 'package:biznes/setting/contactus.dart';
import 'package:biznes/setting/notification.dart';
import 'package:biznes/res/size.dart';
import 'package:biznes/res/style.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:biznes/res/color.dart' as fontscolors;
import 'package:biznes/size_config.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:toast/toast.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Setting extends StatefulWidget {
  const Setting({Key key}) : super(key: key);

  @override
  _SettingState createState() => _SettingState();
}

class _SettingState extends State<Setting> {
  bool isvisible = false;

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var loginHelper = Provider.of<LoginHelper>(context);
    var guestuser = Provider.of<LoginHelper>(context).user.roleName;
    var counters = Provider.of<CountHelper>(context).counter;
    var counterset = Provider.of<CountHelper>(context);
    return Stack(
      children: [
        Scaffold(
          backgroundColor: Colors.white,
          body: OrientationBuilder(
            builder: (context, orientation) {
              return Stack(
                alignment: Alignment.bottomCenter,
                children: [
                  Image.asset('assets/images/newback.jpg'),
                  Container(
                    child: SvgPicture.asset(
                      'assets/images/HomeVector.svg',
                      fit: BoxFit.fitHeight,
                    ),
                  ),
                  Column(
                    children: [
                      CustomAppBar(
                        context: context,
                        role: guestuser,
                        onTap: () {
                          counterset.zerocunter();
                          Navigator.of(context).push(PageTransition(
                              child: MyNotification(),
                              duration: Duration(milliseconds: 700),
                              type: PageTransitionType.leftToRightWithFade));
                        },
                        text: "Settings",
                        length: counters.toString(),
                      ),
                      Expanded(
                        child: Container(
                          child: SingleChildScrollView(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Divider(
                                  thickness: 1,
                                  height: 3,
                                ),
                                GestureDetector(
                                  onTap: () {
                                    AppSettings.openNotificationSettings();
                                  },
                                  child: ListTile(
                                    trailing: Icon(
                                      Icons.arrow_forward_ios,
                                      size: (18 / 8.148314082864863) *
                                          SizeConfig.heightMultiplier,
                                    ),
                                    dense: true,
                                    title: settingpagefonts("Sound"),
                                  ),
                                ),
                                Divider(
                                  thickness: 1,
                                  height: 3,
                                ),
                                InkWell(
                                  onTap: () => Navigator.of(context)
                                      .push(MaterialPageRoute(
                                    builder: (context) =>
                                        SoundHandling("other"),
                                  )),
                                  child: ListTile(
                                    dense: true,
                                    title: settingpagefonts("How it Works"),
                                    trailing: Icon(
                                      Icons.arrow_forward_ios,
                                      size: (18 / 8.148314082864863) *
                                          SizeConfig.heightMultiplier,
                                    ),
                                  ),
                                ),
                                Divider(
                                  thickness: 1,
                                  height: 3,
                                ),
                                ListTile(
                                  dense: true,
                                  onTap: () {
                                    if (guestuser == "Guest") {
                                      Navigator.of(context, rootNavigator: true)
                                          .pushAndRemoveUntil(
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      LoginScreen()),
                                              (route) => false);
                                    } else {
                                      Navigator.of(context).push(
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  ContactUs(true)));
                                    }
                                  },
                                  title: settingpagefonts("Customer Support"),
                                  trailing: Icon(
                                    Icons.arrow_forward_ios,
                                    size: (18 / 8.148314082864863) *
                                        SizeConfig.heightMultiplier,
                                  ),
                                ),
                                Divider(
                                  thickness: 1,
                                  height: 3,
                                ),
                                ListTile(
                                  onTap: () {
                                    Navigator.of(context).push(
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                Privacy(role: "other")));
                                  },
                                  dense: true,
                                  title: settingpagefonts("Privacy Policy"),
                                  trailing: Icon(
                                    Icons.arrow_forward_ios,
                                    size: (18 / 8.148314082864863) *
                                        SizeConfig.heightMultiplier,
                                  ),
                                ),
                                Divider(
                                  thickness: 1,
                                  height: 3,
                                ),
                                ListTile(
                                  onTap: () {
                                    Navigator.of(context).push(
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                TermsAndCondition("other")));
                                  },
                                  dense: true,
                                  title: settingpagefonts("Terms & Condition"),
                                  trailing: Icon(
                                    Icons.arrow_forward_ios,
                                    size: (18 / 8.148314082864863) *
                                        SizeConfig.heightMultiplier,
                                  ),
                                ),
                                Divider(
                                  thickness: 1,
                                  height: 3,
                                ),
                                ListTile(
                                  dense: true,
                                  title: settingpagefonts("Report"),
                                  onTap: () {
                                    Navigator.of(context).push(
                                        MaterialPageRoute(
                                            builder: (context) => Report()));
                                  },
                                  trailing: Icon(
                                    Icons.arrow_forward_ios,
                                    size: (18 / 8.148314082864863) *
                                        SizeConfig.heightMultiplier,
                                  ),
                                ),
                                Divider(
                                  thickness: 1,
                                  height: 3,
                                ),
                                InkWell(
                                  enableFeedback: false,
                                  onTap: () async {
                                    await FlutterShare.share(
                                        title: 'Quick Biznes',
                                        text:
                                            'Join Quick Buzines and discover the Ultimate way to connect with Suppliers and find products. Lets go for Quick Buzines',
                                        linkUrl: "http://quickbiznes.com",
                                        chooserTitle: 'Example Chooser Title');
                                  },
                                  child: ListTile(
                                    dense: true,
                                    title: settingpagefonts("Invite friends"),
                                    trailing: Icon(
                                      Icons.arrow_forward_ios,
                                      size: (18 / 8.148314082864863) *
                                          SizeConfig.heightMultiplier,
                                    ),
                                  ),
                                ),
                                Divider(
                                  thickness: 1,
                                  height: 3,
                                ),
                                InkWell(
                                  onTap: () async {
                                    if (guestuser != "Guest") {
                                      setState(() {
                                        isvisible = true;
                                      });
                                      SharedPreferences _prefs =
                                          await SharedPreferences.getInstance();
                                      appuserlogout(loginHelper.user.sId)
                                          .then((value) {
                                        if (value["Success"] == true) {
                                          Toast.show(
                                              "Logout successfully", context,
                                              gravity: Toast.TOP);
                                          loginHelper.onSignOut();
                                          loginHelper.removeListener(() {});
                                          _prefs.remove("userid");
                                          _prefs.remove("token");
                                          setState(() {
                                            isvisible = false;
                                          });
                                          Navigator.of(context).pop();
                                          Navigator.of(context,
                                                  rootNavigator: true)
                                              .pushAndRemoveUntil(
                                                  MaterialPageRoute(
                                                      builder: (context) =>
                                                          LoginScreen()),
                                                  (route) => false);
                                        } else {
                                          setState(() {
                                            isvisible = false;
                                          });
                                        }
                                      });
                                    } else {
                                      Navigator.of(context, rootNavigator: true)
                                          .pushAndRemoveUntil(
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      LoginScreen()),
                                              (route) => false);
                                    }
                                  },
                                  child: ListTile(
                                    trailing: Icon(
                                      Icons.arrow_forward_ios,
                                      size: (18 / 8.148314082864863) *
                                          SizeConfig.heightMultiplier,
                                    ),
                                    dense: true,
                                    title: settingpagefonts(guestuser == "Guest"
                                        ? "Get Register into App"
                                        : "Log out"),
                                  ),
                                ),
                                Divider(
                                  thickness: 1,
                                  height: 3,
                                ),
                                SizedBox(
                                  height: size.convert(context, 20),
                                ),
                                Center(
                                  child: Container(
                                      child: Column(
                                    children: [
                                      Container(
                                        child: Text(
                                          "App Developed By",
                                          style: style.QuicksandBold(
                                            color: Color(0xff394c81),
                                            fontSize: size.convert(context, 10),
                                          ),
                                        ),
                                      ),
                                      // GestureDetector(
                                      //   onTap: () async {
                                      //     await launch("https://itecexperts.com/");
                                      //   },
                                      //   child: Container(
                                      //     width: size.convertWidth(context, 90),
                                      //     child: Image.asset(
                                      //       "assets/images/newLogo.png",
                                      //       fit: BoxFit.contain,
                                      //     ),
                                      //   ),
                                      // ),
                                      InkWell(
                                        onTap: () async {
                                          await launch(
                                              "https://itecexperts.com/");
                                        },
                                        child: Container(
                                          child: Text(
                                            "www.itecExperts.com",
                                            style: TextStyle(
                                              decoration:
                                                  TextDecoration.underline,
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  )),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              );
            },
          ),
        ),
        isvisible
            ? Stack(
                children: <Widget>[
                  Container(
                    color: Color(0xffffffff).withOpacity(0.6),
                    height: double.infinity,
                    width: double.infinity,
                    child: Opacity(
                      opacity: 1.0,
                      child: Center(
                          child: Image.asset(
                        "assets/images/logo2.gif",
                        height: 200,
                        width: 100,
                      )),
                    ),
                  ),
                ],
              )
            : Container(),
      ],
    );
  }
}
