import 'package:biznes/helper/categories.dart';
import 'package:biznes/helper/login_helper.dart';
import 'package:biznes/model/search_product.dart';
import 'package:biznes/model/roleModel.dart';
import 'package:biznes/product_UI/bottomBar_UI/DropDownWidget.dart';
import 'package:biznes/products_Detail/extra.dart';
import 'package:biznes/repeatedWigets/filledButton.dart';
import 'package:biznes/res/color.dart';
import 'package:biznes/setting/contactus.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:toast/toast.dart';
import 'package:biznes/products_Detail/searchedProducts.dart';
import 'package:biznes/services/register_logic.dart';
import 'package:biznes/helper/searchhelper.dart';
import 'package:provider/provider.dart';
import 'package:biznes/helper/counthelper.dart';
import 'package:biznes/model/citiesModel.dart';
import 'package:biznes/model/countriesModel.dart';
import 'package:biznes/model/subCategoryModel.dart';
import 'package:biznes/setting/notification.dart';
import 'package:biznes/size_config.dart';
import 'package:biznes/res/size.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';

class ProductMain extends StatefulWidget {
  const ProductMain({Key key}) : super(key: key);

  @override
  _ProductMainState createState() => _ProductMainState();
}

class _ProductMainState extends State<ProductMain> {
  SearchProductModel searchModel;
  List<SubCategories> subCategory = [];
  List<CountriesModel> countryList = [];
  List<CitiesModel> citiesList = [];
  List<RoleModel> rolesModelList = [];
  int maxImageNo = 10;
  bool isUploading = false;
  String selectedCategory;
  String selectedRole;
  String title = "Pakistan";
  String selectedSubCategory;
  String selectedCountry;
  String selectedCity;
  bool isvisible = false;

  @override
  void initState() {
    getRoles().then((value) {
      setState(() {
        rolesModelList = value;
      });
    });
    getCategories().then((_list) {
      Provider.of<CategoriesData>(context, listen: false).setCategories(_list);
    });
    getCountries().then((countries) {
      setState(() {
        countryList = countries;
      });
    });
    searchModel = SearchProductModel(
        category: '', subcategory: '', country: '', city: '');
    super.initState();
  }

  Widget buildForm(String token, context) {
    var categories = Provider.of<CategoriesData>(context).categories;
    bool isselected = false;
    Color color = Color(0xffc7c7c7);
    Color color2 = Colors.blue;
    return Padding(
      padding: EdgeInsets.symmetric(
        horizontal: (25.0 / 4.853932272197492) * SizeConfig.widthMultiplier,
      ),
      child: FutureBuilder(
          future: buildFutures(),
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            switch (snapshot.connectionState) {
              default:
                if (snapshot.hasError) {
                  return Text(snapshot.error.toString());
                } else {
                  if (snapshot.data != null)
                    return SingleChildScrollView(
                        child: Column(
                      children: <Widget>[
                        Box(60),
                        DropDownWidget(
                          value: selectedCategory,
                          image: "assets/icons/Category.svg",
                          color: color,
                          color2: color2,
                          selectedCategory: selectedCategory,
                          onChanged: (value) {
                            setState(() {
                              if (selectedSubCategory != null) {
                                selectedSubCategory = null;
                              }
                              searchModel.subcategory = null;
                              subCategory = [];
                            });
                            categories.forEach((f) async {
                              if (f.title == value) {
                                getSubCategories(catId: f.sId).then((list) {
                                  setState(() {
                                    searchModel.category = f.sId;
                                    subCategory = list;
                                    selectedCategory = value;
                                  });
                                });
                              }
                            });
                          },
                          context: context,
                          hintext: "Category",
                          categories: categories,
                        ),
                        Box(20),
                        DropDownWidget(
                          context: context,
                          image: 'assets/icons/subCategory.svg',
                          color2: color2,
                          categories: subCategory,
                          selectedCategory: selectedSubCategory,
                          value: selectedSubCategory,
                          onChanged: (value) {
                            setState(() {
                              isselected = !isselected;
                              selectedSubCategory = value;
                            });
                            subCategory.forEach((f) {
                              if (f.title == value) {
                                searchModel.subcategory = f.sId;
                                // widget.regModel.
                              }
                            });
                          },
                          color: color,
                          hintext: 'Sub-Category',
                        ),
                        Box(20),
                        DropDownWidget(
                            context: context,
                            hintext: "Country",
                            color: color,
                            value: selectedCountry,
                            selectedCategory: selectedCountry,
                            categories: countryList,
                            color2: color2,
                            image: selectedCountry != null
                                ? 'assets/images/flag2.png'
                                : 'assets/images/countryicon.svg',
                            onChanged: (value) {
                              setState(() {
                                if (selectedCity != null) {
                                  selectedCity = null;
                                }
                                searchModel.city = null;
                                citiesList = [];
                              });
                              countryList.forEach((f) async {
                                if (f.title == value) {
                                  searchModel.country = f.sId;
                                  getCities(f.sId).then((list) {
                                    setState(() {
                                      citiesList = list;
                                      selectedCountry = value;
                                    });
                                  });
                                }
                              });
                            }),
                        Box(20),
                        DropDownWidget(
                          color2: color2,
                          color: color,
                          context: context,
                          image: 'assets/images/city.svg',
                          selectedCategory: selectedCity,
                          categories: citiesList,
                          hintext: "City",
                          onChanged: (value) {
                            setState(() {
                              selectedCity = value;
                            });
                            citiesList.forEach((f) async {
                              if (f.title == value) {
                                searchModel.city = f.sId;
                              }
                            });
                          },
                          value: selectedCity,
                        ),
                        Box(20),
                        DropDownWidget(
                          value: selectedRole,
                          image: "assets/images/roles.svg",
                          color: color,
                          color2: color2,
                          selectedCategory: selectedRole,
                          onChanged: (value) {
                            setState(() {
                              rolesModelList.forEach((f) async {
                                if (f.title == value) {
                                  setState(() {
                                    searchModel.userRole = f.id;
                                    selectedRole = value;
                                  });
                                }
                              });
                            });
                          },
                          context: context,
                          hintext: "Role",
                          categories: rolesModelList,
                        ),
                        SizedBox(
                          height: 7.0,
                        ),
                      ],
                    ));
                  else
                    return LinearProgressIndicator();
                }
            }
          }),
    );
  }

  @override
  Widget build(BuildContext context) {
    var token = Provider.of<LoginHelper>(context).token;
    var userid = Provider.of<LoginHelper>(context).user.sId;
    var roleName = Provider.of<LoginHelper>(context).user.roleName;
    var counters = Provider.of<CountHelper>(context, listen: false).counter;
    var counterset = Provider.of<CountHelper>(context);
    var search = Provider.of<SearchHelper>(context);
    return Stack(
      children: [
        Stack(
          children: [
            Container(
              alignment: Alignment.bottomCenter,
              decoration: BoxDecoration(
                // color: Colors.red,
                image: DecorationImage(
                  fit: BoxFit.cover,
                  image: AssetImage("assets/images/newback.jpg"),
                ),
              ),
              child: SingleChildScrollView(
                child: Container(
                  // padding: EdgeInsets.only(
                  //   left: size.convert(context, size.convertWidth(context, 16)),
                  //   right: size.convert(
                  //     context,
                  //     size.convertWidth(context, 16),
                  //   ),
                  //   top: size.convert(context, 10),
                  // ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      buildForm(token, context),
                      SizedBox(
                        height: 4 * SizeConfig.heightMultiplier,
                      ),
                      Container(
                        padding: EdgeInsets.only(bottom: 5),
                        alignment: Alignment.center,
                        child: filledButton(
                          txt: "Search",
                          onTap: () {
                            if (isvalid(context)) {
                              Navigator.of(context).push(MaterialPageRoute(
                                  builder: (_) => SearchedProducts(
                                        userId: userid,
                                        product: searchModel,
                                      )));
                            }
                            //   setState(() {
                            //     isvisible = true;
                            //   });
                            //   searchedProduct(
                            //     search: searchModel,
                            //     context: context,
                            //     userid: userid,
                            //   ).then((products) {
                            //     search.productsearch = products;
                            //     setState(() {
                            //       isvisible = false;
                            //     });
                            //     Provider.of<SearchHelper>(context,
                            //             listen: false)
                            //         .changeSearchString('');
                            //     if (products != null) {
                            //       Navigator.of(context).push(MaterialPageRoute(
                            //           builder: (_) => SearchedProducts(
                            //                 userId: userid,
                            //                 product: searchModel,
                            //               )));
                            //     }
                            //   });
                            // }
                          },
                        ),
                      ),
                      Container(
                        child: SvgPicture.asset(
                          'assets/images/HomeVector.svg',
                          fit: BoxFit.fitHeight,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            Positioned(
              top: 70,
              left: 0,
              right: 0,
              child: Center(
                child: Container(
                  child: Image.asset(
                    "assets/images/logo.png",
                    height: size.convert(context, 40),
                  ),
                ),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                roleName == "Guest"
                    ? Container()
                    : CustomAppBar(
                        context: context,
                        length: counters.toString(),
                        onTap: () {
                          counterset.zerocunter();
                          Navigator.of(context).push(PageTransition(
                              child: MyNotification(),
                              duration: Duration(milliseconds: 700),
                              type: PageTransitionType.leftToRightWithFade));
                        })
              ],
            ),
            roleName == "Guest"
                ? Container()
                : Positioned(
                    top: 60,
                    left: 15,
                    child: Center(
                      child: GestureDetector(
                        onTap: () {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => ContactUs(true)));
                        },
                        child: SvgPicture.asset(
                          "assets/images/customerSupport.svg",
                          height: 40,
                          alignment: Alignment.center,
                        ),
                      ),
                    ),
                  ),
          ],
        ),
        isvisible
            ? Stack(
                children: <Widget>[
                  Container(
                    color: Color(0xffffffff).withOpacity(0.6),
                    height: double.infinity,
                    width: double.infinity,
                    child: Opacity(
                      opacity: 1.0,
                      child: Center(
                          child: Image.asset(
                        "assets/images/logo2.gif",
                        height: 200,
                        width: 100,
                      )),
                    ),
                  ),
                ],
              )
            : Container(),
      ],
    );
  }

  Future<bool> buildFutures() async {
    return true;
  }

  bool isvalid(context) {
    if (searchModel.category == null || searchModel.category == "") {
      Toast.show('Select Category', context,
          duration: Toast.LENGTH_SHORT, gravity: Toast.TOP);
      return false;
    } else if (searchModel.subcategory == null ||
        searchModel.subcategory == "") {
      Toast.show('Select SubCategory', context,
          duration: Toast.LENGTH_SHORT, gravity: Toast.TOP);
      return false;
    } else if (searchModel.country == null || searchModel.country == "") {
      Toast.show('Select Country', context,
          duration: Toast.LENGTH_SHORT, gravity: Toast.TOP);
      return false;
    } else if (searchModel.city == null || searchModel.city == "") {
      Toast.show('Select City', context,
          duration: Toast.LENGTH_SHORT, gravity: Toast.TOP);
      return false;
    } else if (searchModel.userRole == null || searchModel.userRole == "") {
      Toast.show('Select Role', context,
          duration: Toast.LENGTH_SHORT, gravity: Toast.TOP);
      return false;
    } else {
      return true;
    }
  }
}
