import 'package:biznes/guest/guestdata.dart';
import 'package:biznes/helper/login_helper.dart';
import 'package:biznes/products_Detail/extra.dart';
import 'package:biznes/res/size.dart';
import 'package:flutter/material.dart';
import 'package:biznes/setting/notification.dart';
import 'package:page_transition/page_transition.dart';
import 'package:provider/provider.dart';
import 'package:biznes/res/color.dart' as fontandcolor;
import 'package:biznes/helper/counthelper.dart';

class SoundHandling extends StatefulWidget {
  final String role;

  SoundHandling(this.role);

  @override
  _SoundHandlingState createState() => _SoundHandlingState();
}

class _SoundHandlingState extends State<SoundHandling> {
  @override
  Widget build(BuildContext context) {
    var counters = Provider.of<CountHelper>(context).counter;
    var counterSet = Provider.of<CountHelper>(context);
    var roleName = Provider.of<LoginHelper>(context).user.roleName;
    return Scaffold(
      backgroundColor: Colors.white,
      body: Column(
        children: [
          CustomAppBar(
              role: roleName,
              icon: Icon(Icons.arrow_back),
              IconPressed: (){
                Navigator.of(context).pop();
              },
              context: context,
              text: "How it works",
              length: counters.toString(),
              onTap: () {
                counterSet.zerocunter();
                Navigator.of(context).push(PageTransition(
                    child: MyNotification(),
                    duration: Duration(milliseconds: 700),
                    type: PageTransitionType
                        .leftToRightWithFade));
              }),
          Expanded(
            child: Container(
              child: ListView(
                padding: EdgeInsets.zero,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 4.0, right: 4.0),
                    child: Container(
                      margin: EdgeInsets.all(10),
                      decoration: BoxDecoration(
                          color: Color(0xffEbf3fa),
                          borderRadius: BorderRadius.circular(20)),
                      width: double.infinity,
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                          howItWorkText,
                          style: TextStyle(
                            fontSize: size.convert(context, 13),
                            fontFamily: fontandcolor.fontfaimly,
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
